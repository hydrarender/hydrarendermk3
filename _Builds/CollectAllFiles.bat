@echo on


copy "c:\Program Files\Autodesk\3ds Max 2017\Plugins\HydraRender_2017.dlr" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2017\"

copy "c:\Program Files\Autodesk\3ds Max 2017\Plugins\HydraMtl_2017.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2017\"

copy "c:\Program Files\Autodesk\3ds Max 2017\Plugins\HydraMtlCatcher_2017.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2017\"




copy "c:\Program Files\Autodesk\3ds Max 2018\Plugins\HydraRender_2018.dlr" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2018\"

copy "c:\Program Files\Autodesk\3ds Max 2018\Plugins\HydraMtl_2018.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2018\"

copy "c:\Program Files\Autodesk\3ds Max 2018\Plugins\HydraMtlCatcher_2018.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2018\"




copy "c:\Program Files\Autodesk\3ds Max 2019\Plugins\HydraRender_2019.dlr" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2019\"

copy "c:\Program Files\Autodesk\3ds Max 2019\Plugins\HydraMtl_2019.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2019\"

copy "c:\Program Files\Autodesk\3ds Max 2019\Plugins\HydraMtlCatcher_2019.dlt" "d:\Works\Ray-Tracing_Systems\Hydra\PluginFor3dsMax\_Builds\max2019\"



pause