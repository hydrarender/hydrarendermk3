#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include <string>
#include <IMaterialBrowserEntryInfo.h>

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define hydraMtlLayers_CLASS_ID	Class_ID(0x7a8d0218, 0x6e502df)

#define NSUBMTL		4 // TODO: number of sub-materials supported by this plugin 
#define NSUBTEX   3

#define VERSION_CURRENT 3

#define MTL1_SLOT 1
#define MTL2_SLOT 2
#define MTL3_SLOT 3
#define MTL4_SLOT 4

#define MASK2_TEX_SLOT 5
#define MASK3_TEX_SLOT 6
#define MASK4_TEX_SLOT 7

#define PBLOCK_REF	NSUBMTL + NSUBTEX


#ifdef MAX2012
typedef std::string hydraStr;
typedef char hydraChar;
#define paramEnd end 
#else
typedef std::wstring hydraStr;
typedef wchar_t hydraChar;
#define paramEnd p_end 
#endif

class hydraMtlLayers : public Mtl 
{
  public:

    // Parameter block
    IParamBlock2	*pblock;	//ref 0
    Texmap* subTex[NSUBTEX]; //ref 1-10
    Mtl*    subMtl[NSUBMTL];

   // HBITMAP hBitmap;
   /*Mtl		*submtl[NSUBMTL];  
    BOOL			mapOn[NSUBMTL];
    float			spin;*/

    BOOL  layer2_reliefFromBot_on, layer3_reliefFromBot_on, layer4_reliefFromBot_on;
    float layer2_blurRelief, layer3_blurRelief, layer4_blurRelief;

    Interval		ivalid;


    ParamDlg *CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
    void Update(TimeValue t, Interval& valid);
    Interval Validity(TimeValue t);
    void Reset();

    void NotifyChanged();

    // From MtlBase and Mtl
    void SetAmbient(Color c, TimeValue t);		
    void SetDiffuse(Color c, TimeValue t);		
    void SetSpecular(Color c, TimeValue t);
    void SetShininess(float v, TimeValue t);
    Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE);
    Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE);
    Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE);
    float GetXParency(int mtlNum=0, BOOL backFace=FALSE);
    float GetShininess(int mtlNum=0, BOOL backFace=FALSE);		
    float GetShinStr(int mtlNum=0, BOOL backFace=FALSE);
    float WireSize(int mtlNum=0, BOOL backFace=FALSE);
        

    // Shade and displacement calculation
    //
    void Shade(ShadeContext& sc);
    float EvalDisplacement(ShadeContext& sc); 
    Interval DisplacementValidity(TimeValue t); 	

    // SubMaterial access methods
    int NumSubMtls() {return NSUBMTL;}
    Mtl* GetSubMtl(int i);
    void SetSubMtl(int i, Mtl *m);
    TSTR GetSubMtlSlotName(int i);
    TSTR GetSubMtlTVName(int i);

    // SubTexmap access methods
    int NumSubTexmaps() {return NSUBTEX;}
    Texmap* GetSubTexmap(int i);
    void SetSubTexmap(int i, Texmap *m);
    TSTR GetSubTexmapSlotName(int i);
    TSTR GetSubTexmapTVName(int i);
    
    BOOL SetDlgThing(ParamDlg* dlg);
    hydraMtlLayers(BOOL loading);
   // ~hydraMaterial(){ DeleteObject(hBitmap);};
    
    
    // Loading/Saving
    IOResult Load(ILoad *iload);
    IOResult Save(ISave *isave);

    //From Animatable
    Class_ID ClassID() {return hydraMtlLayers_CLASS_ID;}		
    SClass_ID SuperClassID() { return MATERIAL_CLASS_ID; }
    void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

    RefTargetHandle Clone( RemapDir &remap );

#ifdef MAX2014
		RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message);
#else
		virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);
#endif


    int NumSubs() { return 1+NSUBMTL; }
    Animatable* SubAnim(int i); 
    TSTR SubAnimName(int i);

    // TODO: Maintain the number or references here 
    int NumRefs() { return 1+NSUBMTL+NSUBTEX; }
    RefTargetHandle GetReference(int i);
    void SetReference(int i, RefTargetHandle rtarg);
		/*void PostLoadCallback( ILoad* iload )
		{
			IParamBlock2PostLoadInfo* postLoadInfo = (IParamBlock2PostLoadInfo*)
				pblock->GetInterface( IPARAMBLOCK2POSTLOADINFO_ID );

			// Check for specific obsolete version
			if(postLoadInfo!=NULL && postLoadInfo->GetVersion() < VERSION_CURRENT)
			{
				qqq = 3;
			}
		}*/


    int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
    IParamBlock2* GetParamBlock(int i) { return pblock; } // return i'th ParamBlock
    IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

    void DeleteThis() { delete this; }

    protected:

       Color getShadeDiffuseColor(ShadeContext &sc);
       Color getShadeReflectionColor(ShadeContext &sc);
       Color getShadeSpecularColor(ShadeContext &sc);
       Color getShadeEmissiveColor(ShadeContext &sc);
       Color getShadeTransparencyColor(ShadeContext &sc);

       float getShadeSpecularCosPow(ShadeContext &sc);
       float getShadeReflectionCosPow(ShadeContext &sc);
};



class hydraMtlLayersClassDesc : public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  virtual int IsPublic() 							{ return TRUE; }
  virtual void* Create(BOOL loading = FALSE) 		{ return new hydraMtlLayers(loading); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  virtual SClass_ID SuperClassID() 				{ return MATERIAL_CLASS_ID; }
  virtual Class_ID ClassID() 						{ return hydraMtlLayers_CLASS_ID; }
  virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }
  virtual const TCHAR* InternalName() 			{ return _T("hydraMtlLayers"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
  
  // For entry category
  virtual FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  virtual const MCHAR* GetEntryName() const override;
  virtual const MCHAR* GetEntryCategory() const override;
  virtual Bitmap* GetEntryThumbnail() const override;
};


ClassDesc2* GethydraMtlLayersDesc(); 

enum { hydraMtlLayers_params };


//TODO: Add enums for various parameters
enum { 
  pb_baseMtl,

  pb_layer2_mtl,
  pb_layer2_mask,
  pb_layer2_reliefFromBot_on,
  pb_layer2_blurRelief,

  pb_layer3_mtl,
  pb_layer3_mask,
  pb_layer3_reliefFromBot_on,
  pb_layer3_blurRelief,

  pb_layer4_mtl,
  pb_layer4_mask,
  pb_layer4_reliefFromBot_on,
  pb_layer4_blurRelief
};


