//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "HydraMtlLayers.h"
#include <math.h> 
#include <fstream> 
#include "3dsmaxport.h"
#include "stdmat.h"
#include "AssetManagement\iassetmanager.h"


static ParamBlockDesc2 hydraMtlLayers_param_blk ( hydraMtlLayers_params, _T("params"),  0, GethydraMtlLayersDesc(), 
  P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, VERSION_CURRENT, PBLOCK_REF, 
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  pb_baseMtl,	_T("mtl_baseMtl"), TYPE_MTL, P_OWNERS_REF,	IDS_MTL_SLOT1,
    p_refno, 0,
    p_submtlno, 0,
    p_ui, TYPE_MTLBUTTON, IDC_MAT_SLOT1,
    paramEnd,
  // Layer 02
  pb_layer2_mtl, _T("mtl_layer2_mtl"), TYPE_MTL, P_OWNERS_REF, IDS_MTL_SLOT2,
    p_refno, 1,
    p_submtlno, 1,
    p_ui, TYPE_MTLBUTTON, IDC_MAT_SLOT2,
    paramEnd,
  pb_layer2_mask, _T("mtl_layer2_mask"), TYPE_TEXMAP, P_OWNERS_REF, IDS_MASK_SLOT2,
    p_refno, 4,
    p_subtexno, 0,
    p_ui, TYPE_TEXMAPBUTTON, IDC_MASK_SLOT2,
    paramEnd,
  pb_layer2_reliefFromBot_on, _T("mtl_layer2_reliefFromBot_on"), TYPE_BOOL, 0, IDS_RELIEF_FROM_BOTTOM,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_RELIEF_FROM_BOTTOM_ON2,
    paramEnd,
  pb_layer2_blurRelief, _T("mtl_layer2_blurRelief"), TYPE_FLOAT, P_ANIMATABLE, IDS_BLUR_RELIEF,
    p_default, 0.1f,
    p_range, 0.0f, 1.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BLUR_RELIEF_MULT2, IDC_BLUR_RELIEF_MULT_SPINNER2, 0.1f,
    paramEnd,
  // Layer 03
  pb_layer3_mtl, _T("mtl_layer3_mtl"), TYPE_MTL, P_OWNERS_REF, IDS_MTL_SLOT3,
    p_refno, 2,
    p_submtlno, 2,
    p_ui, TYPE_MTLBUTTON, IDC_MAT_SLOT3,
    paramEnd,
  pb_layer3_mask, _T("mtl_layer3_mask"), TYPE_TEXMAP, P_OWNERS_REF, IDS_MASK_SLOT3,
    p_refno, 5,
    p_subtexno, 1,
    p_ui, TYPE_TEXMAPBUTTON, IDC_MASK_SLOT3,
    paramEnd,
  pb_layer3_reliefFromBot_on, _T("mtl_layer3_reliefFromBot_on"), TYPE_BOOL, 0, IDS_RELIEF_FROM_BOTTOM,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_RELIEF_FROM_BOTTOM_ON3,
    paramEnd,
  pb_layer3_blurRelief, _T("mtl_layer3_blurRelief"), TYPE_FLOAT, P_ANIMATABLE, IDS_BLUR_RELIEF,
    p_default, 0.2f,
    p_range, 0.0f, 1.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BLUR_RELIEF_MULT3, IDC_BLUR_RELIEF_MULT_SPINNER3, 0.1f,
    paramEnd,
  // Layer 04
  pb_layer4_mtl, _T("mtl_layer4_mtl"), TYPE_MTL, P_OWNERS_REF, IDS_MTL_SLOT4,
    p_refno, 3,
    p_submtlno, 3,
    p_ui, TYPE_MTLBUTTON, IDC_MAT_SLOT4,
    paramEnd,
  pb_layer4_mask, _T("mtl_layer4_mask"), TYPE_TEXMAP, P_OWNERS_REF, IDS_MASK_SLOT4,
    p_refno, 6,
    p_subtexno, 2,
    p_ui, TYPE_TEXMAPBUTTON, IDC_MASK_SLOT4,
    paramEnd,
  pb_layer4_reliefFromBot_on, _T("mtl_layer4_reliefFromBot_on"), TYPE_BOOL, 0, IDS_RELIEF_FROM_BOTTOM,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_RELIEF_FROM_BOTTOM_ON4,
    paramEnd,
  pb_layer4_blurRelief, _T("mtl_layer4_blurRelief"), TYPE_FLOAT, P_ANIMATABLE, IDS_BLUR_RELIEF,
    p_default, 0.3f,
    p_range, 0.0f, 1.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BLUR_RELIEF_MULT4, IDC_BLUR_RELIEF_MULT_SPINNER4, 0.1f,
    paramEnd,
  paramEnd
  );

class hydraMaterialDlgProc : public ParamMap2UserDlgProc {
	public:
		hydraMtlLayers *hM;
		HWND thishWnd;

    ICustButton *matBase;
    ICustButton *matLayer02;
    ICustButton *matLayer03;
    ICustButton *matLayer04;

    ICustButton *mMaskLayer02;
    ICustButton *mMaskLayer03;
    ICustButton *mMaskLayer04;

    ICustEdit *blurReliefLayer02_edit;
    ICustEdit *blurReliefLayer03_edit;
    ICustEdit *blurReliefLayer04_edit;

    ISpinnerControl *blurReliefLayer02_spin;
    ISpinnerControl *blurReliefLayer03_spin;
    ISpinnerControl *blurReliefLayer04_spin;


		bool additional_init;

		hydraMaterialDlgProc(hydraMtlLayers *cb);
		INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void DropFileInMapSlot(int mapControlID, hydraChar* filename);
		void DeleteThis() {delete this;}
};

hydraMaterialDlgProc::hydraMaterialDlgProc(hydraMtlLayers *cb)
{
	hM = cb; 
	additional_init = true;

  matBase = nullptr;
  matLayer02 = nullptr;
  matLayer03 = nullptr;
  matLayer04 = nullptr;

  mMaskLayer02 = nullptr;
  mMaskLayer03 = nullptr;
  mMaskLayer04 = nullptr;

  blurReliefLayer02_edit = nullptr;
  blurReliefLayer03_edit = nullptr;
  blurReliefLayer04_edit = nullptr;

  blurReliefLayer02_spin = nullptr;
  blurReliefLayer03_spin = nullptr;
  blurReliefLayer04_spin = nullptr;
}



void hydraMaterialDlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
	BitmapTex *btex = NewDefaultBitmapTex();
	
	//MaxSDK::AssetManagement::AssetUser u;

	//u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

	hydraChar dir[1024];
	hydraChar name[256];
	hydraChar extension[16];

	SplitFilename(filename, dir, name, extension);

	//std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};
	
	//tex->SetMap(u);
	btex->SetMapName(filename);
	btex->SetName(name);

	Texmap *tex = (Texmap *)btex;
  //Mtl *mat = (Mtl *) ;

	switch (mapControlID)
	{
	case IDC_MASK_SLOT2:
		hM->SetSubTexmap(0, tex);
		hM->subTex[0] = tex;
		break;
  case IDC_MASK_SLOT3:
    hM->SetSubTexmap(1, tex);
    hM->subTex[1] = tex;
    break;
  case IDC_MASK_SLOT4:
    hM->SetSubTexmap(2, tex);
    hM->subTex[2] = tex;
    break;
	default:
		break;
	}
}


INT_PTR hydraMaterialDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{
  thishWnd=hWnd;
	hydraMaterialDlgProc *dlg = DLGetWindowLongPtr<hydraMaterialDlgProc*>(hWnd);

	if (!dlg && msg != WM_INITDIALOG) return FALSE;

	switch (msg)
	{
		case WM_INITDIALOG:
		{
			//dlg = (hydraMaterialDlgProc *)lParam;
			//DLSetWindowLongPtr(hWnd, dlg);

      if (!blurReliefLayer02_edit) blurReliefLayer02_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT2));
      if (!blurReliefLayer02_spin) blurReliefLayer02_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT_SPINNER2));

      if (!matBase) matBase = GetICustButton(GetDlgItem(hWnd, IDC_MAT_SLOT1));
      if (!matLayer02) matLayer02 = GetICustButton(GetDlgItem(hWnd, IDC_MAT_SLOT2));
      if (!matLayer03) matLayer03 = GetICustButton(GetDlgItem(hWnd, IDC_MAT_SLOT3));
      if (!matLayer04) matLayer04 = GetICustButton(GetDlgItem(hWnd, IDC_MAT_SLOT4));

      if (!mMaskLayer02) mMaskLayer02 = GetICustButton(GetDlgItem(hWnd, IDC_MASK_SLOT2));
      if (!mMaskLayer03) mMaskLayer03 = GetICustButton(GetDlgItem(hWnd, IDC_MASK_SLOT3));
      if (!mMaskLayer04) mMaskLayer04 = GetICustButton(GetDlgItem(hWnd, IDC_MASK_SLOT4));

      if (!blurReliefLayer02_edit) blurReliefLayer02_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT2));
      if (!blurReliefLayer03_edit) blurReliefLayer03_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT3));
      if (!blurReliefLayer04_edit) blurReliefLayer04_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT4));

      if (!blurReliefLayer02_spin) blurReliefLayer02_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT_SPINNER2));
      if (!blurReliefLayer03_spin) blurReliefLayer03_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT_SPINNER3));
      if (!blurReliefLayer04_spin) blurReliefLayer04_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLUR_RELIEF_MULT_SPINNER4));

      DragAcceptFiles(GetDlgItem(hWnd, IDC_MASK_SLOT2), true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_MASK_SLOT3), true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_MASK_SLOT4), true);

			return TRUE;
		}
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_EMISSION_MAP:
				case IDC_DIFFUSE_MAP:
				case IDC_SPECULAR_MAP:
				case IDC_REFLECT_MAP:
				case IDC_TRANSP_MAP:
				case IDC_SPEC_GL_MAP:
				case IDC_REFL_GL_MAP:
				case IDC_TRANSP_GL_MAP:
				case IDC_OPACITY_MAP:
				case IDC_TRANSLUCENCY_MAP:
					if (HIWORD(wParam)==BN_BUTTONUP || HIWORD(wParam)==BN_RIGHTCLICK || HIWORD(wParam)==BN_BUTTONDOWN) 
					{
					}
					break;
				case IDC_NORMAL_MAP:
					if (HIWORD(wParam)==BN_BUTTONUP || HIWORD(wParam)==BN_RIGHTCLICK || HIWORD(wParam)==BN_BUTTONDOWN) 
					{
					}
					break;
				case IDC_SPEC_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_SPEC_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_REFL_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_REFL_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_TRANSP_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_TRANSP_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
				case IDC_LOCK_SPECULAR:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					}
					break;
			}
		break;	
		case WM_DESTROY:

      ReleaseICustButton(matBase);
      ReleaseICustButton(matLayer02);
      ReleaseICustButton(matLayer03);
      ReleaseICustButton(matLayer04);

      ReleaseICustButton(mMaskLayer02);
      ReleaseICustButton(mMaskLayer03);
      ReleaseICustButton(mMaskLayer04);

      ReleaseISpinner(blurReliefLayer02_spin);
      ReleaseISpinner(blurReliefLayer03_spin);
      ReleaseISpinner(blurReliefLayer04_spin);

      ReleaseICustEdit(blurReliefLayer02_edit);
      ReleaseICustEdit(blurReliefLayer03_edit);
      ReleaseICustEdit(blurReliefLayer04_edit);


      break;  
		case WM_DROPFILES:
			POINT pt;
			WORD numFiles;
			HWND dropTarget;
			int dropTargetID;

			hydraChar lpszFile[80];

			DragQueryPoint((HDROP)wParam, &pt);

#ifdef MAX2012
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPSTR)NULL, 0);
#else
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);
#endif


				if (numFiles == 0)
				{
					DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
					dropTarget = RealChildWindowFromPoint(hWnd, pt);
					dropTargetID = GetDlgCtrlID(dropTarget);
					DropFileInMapSlot(dropTargetID, lpszFile);
				}

				DragFinish((HDROP)wParam);

				break;
		default:
			return FALSE;
	}
	return TRUE; 
}


hydraMtlLayers::hydraMtlLayers(BOOL loading) 
{
  for (int i = 0; i<NSUBMTL; i++) subMtl[i] = NULL;
  for (int i = 0; i<NSUBTEX; i++) subTex[i] = NULL;
  pblock = NULL;
	//hydramaterial_param_blk.SetUserDlgProc

  layer2_reliefFromBot_on = true;
  layer3_reliefFromBot_on = true;
  layer4_reliefFromBot_on = true;

  layer2_blurRelief = 0.1f;
  layer3_blurRelief = 0.2f;
  layer4_blurRelief = 0.3f;


  if (!loading) 
    Reset();
}


void hydraMtlLayers::Reset() 
{
  ivalid.SetEmpty();
  for (int i = 0; i < NSUBMTL; i++) 
  {
    if( subMtl[i] )
    { 
      DeleteReference(i);
      subMtl[i] = NULL;
    }
  }

  for (int i = NSUBMTL; i < NSUBTEX + NSUBMTL; i++) 
  {
    if( subTex[i - NSUBMTL] )
		{ 
      DeleteReference(i);
      subTex[i - NSUBMTL] = NULL;
    }
  }

  GethydraMtlLayersDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* hydraMtlLayers::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
  IAutoMParamDlg* masterDlg = GethydraMtlLayersDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  
/*  hBitmap = reinterpret_cast<HBITMAP>(LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(IDB_BITMAP1),IMAGE_BITMAP,0,0,0));

  SendDlgItemMessage(hwMtlEdit,IDC_LOGO,STM_SETIMAGE,IMAGE_BITMAP,reinterpret_cast<LPARAM>(hBitmap));*/

  // TODO: Set param block user dialog if necessary
	hydraMtlLayers_param_blk.SetUserDlgProc(new hydraMaterialDlgProc(this));
  return masterDlg;
}

BOOL hydraMtlLayers::SetDlgThing(ParamDlg* dlg)
{
  return FALSE;
}

Interval hydraMtlLayers::Validity(TimeValue t)
{
  Interval valid = FOREVER;		

  for (int i = 0; i < NSUBMTL; i++)
  {
    if (subMtl[i]) 
      valid &= subMtl[i]->Validity(t);
  }

  for (int i = 0 ; i < NSUBTEX; i++) 
  {
		if (subTex[i]) 
      valid &= subTex[i]->Validity(t);
  }
  

  /*float u;
  pblock->GetValue(pb_spin,t,u,valid);*/
  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle hydraMtlLayers::GetReference(int i) 
{	
  if (i < NSUBMTL)
    return subMtl[i];
  else if (i - NSUBMTL < NSUBTEX)
  {
    int j = i - NSUBMTL;
    return subTex[j];
  }
  else
    return pblock;
}

void hydraMtlLayers::SetReference(int i, RefTargetHandle rtarg) 
{
	MSTR s = L"";
	if(rtarg)
		rtarg->GetClassName(s);

  if (i < NSUBMTL && s != L"ParamBlock2")
    subMtl[i] = (Mtl *)rtarg;

  else if (i < NSUBTEX + NSUBMTL && s != L"ParamBlock2")
  {
    subTex[i - NSUBMTL] = (Texmap *)rtarg;
  }
  else
  {
    pblock = (IParamBlock2 *)rtarg;
  }	
}

TSTR hydraMtlLayers::SubAnimName(int i) 
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else return TSTR(_T(""));
  //return TSTR(_T(""));
}

Animatable* hydraMtlLayers::SubAnim(int i)
{
  if (i < NSUBMTL)
    return subMtl[i];
  else if (i < NSUBTEX + NSUBMTL)
    return subTex[i - NSUBMTL];
  else
    return pblock;
}


#ifdef MAX2014
RefResult hydraMaterial::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();
		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydramaterial_param_blk.InvalidateUI(changing_param);
		}
		break;

	}
	return REF_SUCCEED;
}
#else
RefResult hydraMtlLayers::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();

  /*  IParamBlock2* cpb = NULL;
    ParamID changing_param = GethydraMaterialDesc()->LastNotifyParamID(this, cpb);
    if (changing_param != -1)
    {
      cpb->GetDesc()->InvalidateUI(changing_param);
    }
    else
    {
      GethydraMaterialDesc()->InvalidateUI();
    }*/
 

		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydraMtlLayers_param_blk.InvalidateUI(changing_param);
		}
    break;
	}
	return REF_SUCCEED;
}
#endif


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* hydraMtlLayers::GetSubMtl(int i)
{
  if (i < NSUBMTL )
    return subMtl[i];
  else
    return NULL;
}

void hydraMtlLayers::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i,m);

  // TODO: Set the material and update the UI	

  switch (i)
  {
  case 0:
    hydraMtlLayers_param_blk.InvalidateUI(pb_baseMtl);
    pblock->SetValue(pb_baseMtl, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 1:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer2_mtl);
    pblock->SetValue(pb_layer2_mtl, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 2:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer3_mtl);
    pblock->SetValue(pb_layer3_mtl, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 4:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer4_mtl);
    pblock->SetValue(pb_layer4_mtl, TimeValue(0), m);
    ivalid.SetEmpty();
    break;

  default:
    break;
  }

  NotifyChanged();
}

TSTR hydraMtlLayers::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  switch (i)
  {
  case 0:  return GetString(IDS_MTL_SLOT1);
  case 1:  return GetString(IDS_MTL_SLOT2);
  case 2:  return GetString(IDS_MTL_SLOT3);
  case 3:  return GetString(IDS_MTL_SLOT4);
  default: return _T("");
  }
  //return _T("");
}

TSTR hydraMtlLayers::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}

/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* hydraMtlLayers::GetSubTexmap(int i)
{
  if (i < NSUBTEX)
    return subTex[i];
  else
    return NULL;
}

void hydraMtlLayers::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i + NSUBMTL,m);
  
  switch(i)
  {
  case 0:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer2_mask);
    pblock->SetValue(pb_layer2_mask, TimeValue(0), m);
    ivalid.SetEmpty();

    break;
  case 1:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer3_mask);
    pblock->SetValue(pb_layer3_mask, TimeValue(0), m);
    ivalid.SetEmpty();

    break;
  case 2:
    hydraMtlLayers_param_blk.InvalidateUI(pb_layer4_mask);
    pblock->SetValue(pb_layer4_mask, TimeValue(0), m);
    ivalid.SetEmpty();

    break;
  default:
    break;
  }

  NotifyChanged();
}

TSTR hydraMtlLayers::GetSubTexmapSlotName(int i)
{
  switch(i) 
  {
  case 0:  return GetString(IDS_MASK_SLOT2);
  case 1:  return GetString(IDS_MASK_SLOT3);
  case 2:  return GetString(IDS_MASK_SLOT4);

		default: return _T("");
	}
}

TSTR hydraMtlLayers::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult hydraMtlLayers::Save(ISave *isave) { 
  IOResult res;
	ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res!=IO_OK) return res;
  isave->EndChunk();

	isave->BeginChunk(HYDRA_CHUNK);

  isave->Write(&layer2_reliefFromBot_on, sizeof(bool), &nb);
  isave->Write(&layer3_reliefFromBot_on, sizeof(bool), &nb);
  isave->Write(&layer4_reliefFromBot_on, sizeof(bool), &nb);

  isave->Write(&layer2_blurRelief, sizeof(float), &nb);
  isave->Write(&layer3_blurRelief, sizeof(float), &nb);
  isave->Write(&layer4_blurRelief, sizeof(float), &nb);

	isave->EndChunk();

  return IO_OK;
  }	

IOResult hydraMtlLayers::Load(ILoad *iload) { 
  IOResult res;
  int id;
	ULONG nb;
  while (IO_OK==(res=iload->OpenChunk())) 
	{
    switch(id = iload->CurChunkID())  
		{
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
			case HYDRA_CHUNK:
        
        res = iload->Read(&layer2_reliefFromBot_on, sizeof(bool), &nb);
        res = iload->Read(&layer3_reliefFromBot_on, sizeof(bool), &nb);
        res = iload->Read(&layer4_reliefFromBot_on, sizeof(bool), &nb);

        res = iload->Read(&layer2_blurRelief, sizeof(float), &nb);
        res = iload->Read(&layer3_blurRelief, sizeof(float), &nb);
        res = iload->Read(&layer4_blurRelief, sizeof(float), &nb);

				break;
    }
    iload->CloseChunk();
    if (res!=IO_OK) 
      return res;
  }

  return IO_OK;
  }


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle hydraMtlLayers::Clone(RemapDir &remap) {
  hydraMtlLayers *mnew = new hydraMtlLayers(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this); 

  mnew->ReplaceReference(NSUBMTL,remap.CloneRef(pblock));

  mnew->layer2_reliefFromBot_on = layer2_reliefFromBot_on;
  mnew->layer3_reliefFromBot_on = layer3_reliefFromBot_on;
  mnew->layer4_reliefFromBot_on = layer4_reliefFromBot_on;

  mnew->layer2_blurRelief = layer2_blurRelief;
  mnew->layer3_blurRelief = layer3_blurRelief;
  mnew->layer4_blurRelief = layer4_blurRelief;

  mnew->ivalid.SetEmpty();	

  for (int i = 0; i<NSUBMTL; i++) 
  {
    mnew->subMtl[i] = NULL;
    if (subMtl[i])
      mnew->ReplaceReference(i,remap.CloneRef(subMtl[i]));
      //mnew->mapOn[i] = mapOn[i];
    }

  for (int i = 0; i<NSUBTEX; i++) 
  {
		mnew->subTex[i] = NULL;
		if (subTex[i])
			mnew->ReplaceReference(i + NSUBMTL,remap.CloneRef(subTex[i]));
	}

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
  }

void hydraMtlLayers::NotifyChanged() 
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void hydraMtlLayers::Update(TimeValue t, Interval& valid) 
{	
  if (!ivalid.InInterval(t)) 
  {
    ivalid.SetInfinite();

    pblock->GetValue(pb_layer2_reliefFromBot_on, t, layer2_reliefFromBot_on, ivalid);
    pblock->GetValue(pb_layer2_blurRelief, t,       layer2_blurRelief, ivalid);

    pblock->GetValue(pb_layer3_reliefFromBot_on, t, layer3_reliefFromBot_on, ivalid);
    pblock->GetValue(pb_layer3_blurRelief, t,       layer3_blurRelief, ivalid);

    pblock->GetValue(pb_layer4_reliefFromBot_on, t, layer4_reliefFromBot_on, ivalid);
    pblock->GetValue(pb_layer4_blurRelief, t,       layer4_blurRelief, ivalid);


    NotifyDependents(FOREVER, PART_TEXMAP, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (int i=0; i<NSUBMTL; i++) 
    {
      if (subMtl[i]) 
        subMtl[i]->Update(t,ivalid);
    }

    for (int i=0; i<NSUBTEX; i++) 
    {
      if (subTex[i])
      {
        subTex[i]->Update(t, ivalid);
        subTex[i]->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
      }
		}

  }
  valid &= ivalid;
  NotifyChanged();
  //ExportMaterialXML();
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void hydraMtlLayers::SetAmbient(Color c, TimeValue t) {}		
void hydraMtlLayers::SetDiffuse(Color c, TimeValue t) 
{
  //diffuse_color = c;
  //pblock->SetValue( mtl_diffuse_color, t, diffuse_color);
  //NotifyChanged();
}		
void hydraMtlLayers::SetSpecular(Color c, TimeValue t) 
{
  //specular_color = c;
  //pblock->SetValue( mtl_specular_color, t, specular_color);
  //NotifyChanged();
}
void hydraMtlLayers::SetShininess(float v, TimeValue t) 
{
  ////log(reflect_cospower)/log(100000.0)
  ////derp
  //specular_roughness = v;
  //pblock->SetValue( mtl_specular_roughness, t, specular_roughness);
  //NotifyChanged();
}
        
Color hydraMtlLayers::GetAmbient(int mtlNum, BOOL backFace)
{
	////return submtl[0]?submtl[0]->GetAmbient(mtlNum,backFace):Color(0,0,0);
	//return diffuse_color*diffuse_mult;
  return { 0.0f, 0.0f, 0.0f };
}

Color hydraMtlLayers::GetDiffuse(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
  //return diffuse_color*diffuse_mult;
  return { 0.8f, 0.8f, 0.8f };
}

Color hydraMtlLayers::GetSpecular(int mtlNum, BOOL backFace)
{
 // //return submtl[0]?submtl[0]->GetSpecular(mtlNum,backFace):Color(0,0,0);
	//return reflect_color*reflect_mult;
  return { 0.0f, 0.0f, 0.0f };
}

float hydraMtlLayers::GetXParency(int mtlNum, BOOL backFace)
{
  //return 0.33333f*(transparency_color.r*transparency_mult + transparency_color.g*transparency_mult + transparency_color.b*transparency_mult);
  return 0.0f;
}

float hydraMtlLayers::GetShininess(int mtlNum, BOOL backFace)
{
	//return log(reflect_cospower) / log(100000.0);
  return 0.0f;
}

float hydraMtlLayers::GetShinStr(int mtlNum, BOOL backFace)
{
  return 0.0f;
}

float hydraMtlLayers::WireSize(int mtlNum, BOOL backFace)
{
  return 0.0f;
}

 


ClassDesc2* GethydraMtlLayersDesc() 
{ 
  static hydraMtlLayersClassDesc hydraMtlLayersDesc;
  return &hydraMtlLayersDesc; 
}



FPInterface * hydraMtlLayersClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * hydraMtlLayersClassDesc::GetEntryName() const
{
  return const_cast<hydraMtlLayersClassDesc*>(this)->ClassName();
  
}

const MCHAR * hydraMtlLayersClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * hydraMtlLayersClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}
