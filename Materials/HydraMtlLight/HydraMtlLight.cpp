//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "HydraMtlLight.h"
#include <math.h> 
#include <fstream> 
#include "3dsmaxport.h"
#include "stdmat.h"
#include "AssetManagement\iassetmanager.h"


static ParamBlockDesc2 hydraMtlLight_param_blk ( HydraMtlLight_params, _T("params"),  0, GetHydraMtlLightDesc(), 
  P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, VERSION_CURRENT,
	PBLOCK_REF, 
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  mtl_light_color_mult, _T("mtl_light_color_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_COLOR_MULT, 
    p_default, 1.0f, 
    p_range, 0.0f, 10000.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_COLOR_MULT,	IDC_COLOR_MULT_SPINNER, 1.0f, 
    paramEnd,
  mtl_light_color_map,	_T("mtl_light_color_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_COLOR_MAP,		
    p_refno,		0,
		p_subtexno,		0,	
    p_ui, TYPE_TEXMAPBUTTON, IDC_COLOR_MAP,
    paramEnd,
  mtl_light_color,	_T("mtl_light_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_COLOR,
    paramEnd,
  mtl_light_color_tint_on, _T("mtl_light_color_tint_on"), TYPE_BOOL, 0, IDS_COLOR_TINT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_COLOR_TINT_ON,
    paramEnd,
  paramEnd
  );

class HydraMtlLayerDlgProc : public ParamMap2UserDlgProc {
	public:
		HydraMtlLight *hM;
		HWND thishWnd;

		ICustEdit       *gui_color_multEdit;
		ISpinnerControl *gui_color_multSpin;
		ICustButton     *gui_color_map;
		IColorSwatch    *gui_color_colorSwatch;


		bool additional_init;

		HydraMtlLayerDlgProc(HydraMtlLight *cb);
		INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void texBtnText();
		void DropFileInMapSlot(int mapControlID, hydraChar* filename);
		void DeleteThis() {delete this;}
};

HydraMtlLayerDlgProc::HydraMtlLayerDlgProc(HydraMtlLight *cb)
{
	hM = cb; 
	additional_init = true;

	gui_color_multEdit = nullptr;
  gui_color_multSpin = nullptr;
	gui_color_map = nullptr;
  gui_color_colorSwatch = nullptr;
}

void HydraMtlLayerDlgProc::texBtnText()
{
	if(hM->subTex[0]) gui_color_map->SetText(_M("M")); else gui_color_map->SetText(_M(" "));
}


void HydraMtlLayerDlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
	BitmapTex *btex = NewDefaultBitmapTex();
	
	//MaxSDK::AssetManagement::AssetUser u;

	//u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

	hydraChar dir[1024];
	hydraChar name[256];
	hydraChar extension[16];

	SplitFilename(filename, dir, name, extension);

	//std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};
	
	//tex->SetMap(u);
	btex->SetMapName(filename);
	btex->SetName(name);

	Texmap *tex = (Texmap *)btex;

	switch (mapControlID)
	{
	case IDC_COLOR_MAP:
		hM->SetSubTexmap(COLOR_TEX_SLOT, tex);
		hM->subTex[COLOR_TEX_SLOT] = tex;
		break;
	default:
		break;
	}
}


INT_PTR HydraMtlLayerDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{
  thishWnd=hWnd;
	HydraMtlLayerDlgProc *dlg = DLGetWindowLongPtr<HydraMtlLayerDlgProc*>(hWnd);

	if (!dlg && msg != WM_INITDIALOG) return FALSE;

	switch (msg)
	{
		case WM_INITDIALOG:
		{
			//dlg = (hydraMaterialDlgProc *)lParam;
			//DLSetWindowLongPtr(hWnd, dlg);

			if (!gui_color_multEdit) gui_color_multEdit = GetICustEdit(GetDlgItem(hWnd, IDC_COLOR_MULT));

			if (!gui_color_multSpin) gui_color_multSpin = GetISpinner(GetDlgItem(hWnd, IDC_COLOR_MULT_SPINNER));

			if (!gui_color_map) gui_color_map = GetICustButton(GetDlgItem(hWnd, IDC_COLOR_MAP));

			gui_color_colorSwatch = GetIColorSwatch(GetDlgItem(hWnd, IDC_COLOR));

			DragAcceptFiles(GetDlgItem(hWnd, IDC_COLOR_MAP), true);


			return TRUE;
			//break;
		}
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_DIFFUSE_MAP:
        break;
			}
		break;	
		case WM_DESTROY:

				ReleaseICustEdit(gui_color_multEdit);
				ReleaseISpinner(gui_color_multSpin);
				ReleaseICustButton(gui_color_map);
				ReleaseIColorSwatch(gui_color_colorSwatch);

        break;  
		case WM_DROPFILES:
				POINT pt;
				WORD numFiles;
				HWND dropTarget;
				int dropTargetID;

				hydraChar lpszFile[80];

				DragQueryPoint((HDROP)wParam, &pt);

#ifdef MAX2012
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPSTR)NULL, 0);
#else
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);
#endif


				if (numFiles == 0)
				{
					DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
					dropTarget = RealChildWindowFromPoint(hWnd, pt);
					dropTargetID = GetDlgCtrlID(dropTarget);
					DropFileInMapSlot(dropTargetID, lpszFile);
				}

				DragFinish((HDROP)wParam);

				break;
		default:
			return FALSE;
	}
	return TRUE; 
}


HydraMtlLight::HydraMtlLight(BOOL loading) 
{
 // for (int i=0; i<NSUBMTL; i++) submtl[i] = NULL;
  for (int i=NSUBMTL; i<NSUBTEX+NSUBMTL; i++) subTex[i] = NULL;
  pblock = NULL;
	//hydramaterial_param_blk.SetUserDlgProc

  light_color_mult = 1.0f;
	light_color = Color(1.0, 1.0, 1.0);
	light_color_tint_on = false;

  if (!loading) 
    Reset();
}


void HydraMtlLight::Reset() 
{
  ivalid.SetEmpty();
  for (int i=0; i<NSUBMTL; i++) 
  {
   /* if( submtl[i] ){ 
      DeleteReference(i);
      submtl[i] = NULL;
    }*/
  }

  for (int i=NSUBMTL; i<NSUBTEX+NSUBMTL; i++) 
  {
    if( subTex[i] )
		{ 
      DeleteReference(i);
      subTex[i] = NULL;
    }
  }

  GetHydraMtlLightDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* HydraMtlLight::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
  IAutoMParamDlg* masterDlg = GetHydraMtlLightDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  
/*  hBitmap = reinterpret_cast<HBITMAP>(LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(IDB_BITMAP1),IMAGE_BITMAP,0,0,0));

  SendDlgItemMessage(hwMtlEdit,IDC_LOGO,STM_SETIMAGE,IMAGE_BITMAP,reinterpret_cast<LPARAM>(hBitmap));*/

  // TODO: Set param block user dialog if necessary
	hydraMtlLight_param_blk.SetUserDlgProc(new HydraMtlLayerDlgProc(this));
  return masterDlg;
}

BOOL HydraMtlLight::SetDlgThing(ParamDlg* dlg)
{
  return FALSE;
}

Interval HydraMtlLight::Validity(TimeValue t)
{
  Interval valid = FOREVER;		

  for (int i=0; i<NSUBMTL; i++) 
  {
   /* if (submtl[i]) 
      valid &= submtl[i]->Validity(t);*/
  }

  for (int i=NSUBMTL; i<NSUBMTL+NSUBTEX; i++) 
  {
		if (subTex[i]) 
      valid &= subTex[i]->Validity(t);
  }
  
  /*float u;
  pblock->GetValue(pb_spin,t,u,valid);*/
  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle HydraMtlLight::GetReference(int i) 
{
	
	/*if (i < NSUBMTL )
		return submtl[i];
	else*/ 
	if (i < NSUBTEX+NSUBMTL )
		return subTex[i];
	else 
    return pblock;
	
}

void HydraMtlLight::SetReference(int i, RefTargetHandle rtarg) 
{

	/* if (i < NSUBMTL)
		submtl[i] = (Mtl *)rtarg; 
	else*/ 
	int n = NSUBTEX + NSUBMTL;

	MSTR s = L"";
	if(rtarg)
		rtarg->GetClassName(s);

  if (i < n && s != L"ParamBlock2")
  {
    subTex[i] = (Texmap *)rtarg;
  }
  else
  {
    pblock = (IParamBlock2 *)rtarg;
  }
	
}

TSTR HydraMtlLight::SubAnimName(int i) 
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else return TSTR(_T(""));
}

Animatable* HydraMtlLight::SubAnim(int i) {
 /* if (i < NSUBMTL)
    return submtl[i]; 
  else*/ if (i < NSUBTEX+NSUBMTL )
    return subTex[i];
  else return pblock;
  }


#ifdef MAX2014
RefResult hydraMaterial::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();
		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydramaterial_param_blk.InvalidateUI(changing_param);
		}
		break;

	}
	return REF_SUCCEED;
}
#else
RefResult HydraMtlLight::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();

  /*  IParamBlock2* cpb = NULL;
    ParamID changing_param = GethydraMaterialDesc()->LastNotifyParamID(this, cpb);
    if (changing_param != -1)
    {
      cpb->GetDesc()->InvalidateUI(changing_param);
    }
    else
    {
      GethydraMaterialDesc()->InvalidateUI();
    }*/
 

		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydraMtlLight_param_blk.InvalidateUI(changing_param);
		}
    break;
	}
	return REF_SUCCEED;
}
#endif


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* HydraMtlLight::GetSubMtl(int i)
{
  /*if (i < NSUBMTL )
    return submtl[i];*/
  return NULL;
}

void HydraMtlLight::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i,m);
  // TODO: Set the material and update the UI	
}

TSTR HydraMtlLight::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  return _T(""); 
}

TSTR HydraMtlLight::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}

/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* HydraMtlLight::GetSubTexmap(int i)
{
   return subTex[i];
}

void HydraMtlLight::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i,m);
  
  switch(i)
  {
  case 0:
    hydraMtlLight_param_blk.InvalidateUI(mtl_light_color_map);
    pblock->SetValue(mtl_light_color_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  default:
    break;
  }

  NotifyChanged();
}

TSTR HydraMtlLight::GetSubTexmapSlotName(int i)
{
  switch(i) 
  {
		case 0:  return GetString(IDS_COLOR_MAP); 
		default: return _T("");
	}
}

TSTR HydraMtlLight::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraMtlLight::Save(ISave *isave) { 
  IOResult res;
	ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res!=IO_OK) return res;
  isave->EndChunk();

	isave->BeginChunk(HYDRA_CHUNK);
  
	isave->Write(&light_color_mult, sizeof(float), &nb);
	isave->Write(&light_color, sizeof(Color), &nb);
	isave->Write(&light_color_tint_on, sizeof(bool), &nb);

	isave->EndChunk();


  return IO_OK;
  }	

IOResult HydraMtlLight::Load(ILoad *iload) { 
  IOResult res;
  int id;
	ULONG nb;
  while (IO_OK==(res=iload->OpenChunk())) 
	{
    switch(id = iload->CurChunkID())  
		{
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
			case HYDRA_CHUNK:

				res = iload->Read(&light_color_mult, sizeof(float), &nb);
				res = iload->Read(&light_color, sizeof(Color), &nb);
				res = iload->Read(&light_color_tint_on, sizeof(bool), &nb);

				break;
    }
    iload->CloseChunk();
    if (res!=IO_OK) 
      return res;
  }

  return IO_OK;
  }


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle HydraMtlLight::Clone(RemapDir &remap) {
  HydraMtlLight *mnew = new HydraMtlLight(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this); 
  mnew->ReplaceReference(NSUBMTL,remap.CloneRef(pblock));

  mnew->light_color_mult = light_color_mult;
  mnew->light_color_tint_on = light_color_tint_on;
  mnew->light_color = light_color;

  mnew->ivalid.SetEmpty();	
  for (int i = 0; i<NSUBMTL; i++) {
    /*mnew->submtl[i] = NULL;
    if (submtl[i])
      mnew->ReplaceReference(i,remap.CloneRef(submtl[i]));
      mnew->mapOn[i] = mapOn[i];*/
    }

  for (int i = 0; i<NSUBTEX; i++) 
  {
		mnew->subTex[i] = NULL;
		if (subTex[i])
			mnew->ReplaceReference(i,remap.CloneRef(subTex[i]));
	}

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
  }

void HydraMtlLight::NotifyChanged() 
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraMtlLight::Update(TimeValue t, Interval& valid) 
{	
  if (!ivalid.InInterval(t)) 
  {
    ivalid.SetInfinite();

    pblock->GetValue( mtl_light_color_mult, t, light_color_mult, ivalid);
    pblock->GetValue( mtl_light_color, t, light_color, ivalid);
    pblock->GetValue( mtl_light_color_tint_on, t, light_color_tint_on, ivalid);

	
    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (int i=0; i<NSUBMTL; i++) 
    {
      /*if (submtl[i]) 
        submtl[i]->Update(t,ivalid);*/
    }

    for (int i=0; i<NSUBTEX; i++) 
    {
      if (subTex[i])
      {
        subTex[i]->Update(t, ivalid);
       // subTex[i]->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
      }
		}

  }
  valid &= ivalid;
  //NotifyChanged();
  //ExportMaterialXML();
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void HydraMtlLight::SetAmbient(Color c, TimeValue t) {}		
void HydraMtlLight::SetDiffuse(Color c, TimeValue t) {}		
void HydraMtlLight::SetSpecular(Color c, TimeValue t){}
void HydraMtlLight::SetShininess(float v, TimeValue t) {}
        
Color HydraMtlLight::GetAmbient(int mtlNum, BOOL backFace)
{
	return { 0, 0, 0 };
}

Color HydraMtlLight::GetDiffuse(int mtlNum, BOOL backFace)
{
  return {0, 0, 0};
}

Color HydraMtlLight::GetSpecular(int mtlNum, BOOL backFace)
{
	return { 0, 0, 0 };
}

Color HydraMtlLight::GetSelfIllumColor(int mtlNum, BOOL backFace)
{  
  return light_color * light_color_mult;
}

float HydraMtlLight::GetXParency(int mtlNum, BOOL backFace)
{
  return 0;
}

float HydraMtlLight::GetShininess(int mtlNum, BOOL backFace)
{
  return 0;  
}

float HydraMtlLight::GetShinStr(int mtlNum, BOOL backFace)
{
  return 1.0f;
}

float HydraMtlLight::WireSize(int mtlNum, BOOL backFace)
{
  return 0.0f;
}

 


ClassDesc2* GetHydraMtlLightDesc() 
{ 
  static HydraMtlLightClassDesc hydraMtlLightDesc;
  return &hydraMtlLightDesc; 
}



FPInterface * HydraMtlLightClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraMtlLightClassDesc::GetEntryName() const
{
  return const_cast<HydraMtlLightClassDesc*>(this)->ClassName();
  
}

const MCHAR * HydraMtlLightClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * HydraMtlLightClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}
