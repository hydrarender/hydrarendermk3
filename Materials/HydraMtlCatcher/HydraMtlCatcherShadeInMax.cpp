#include "HydraMtlCatcher.h"
#include "..\..\hydraRender mk3\ImportStructs.h"
#include <math.h> 
#include <fstream> 

inline float clamp(float u, float a, float b) { float r = max(a, u); return min(r, b); }


/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/

void HydraMtlCatcher::Shade(ShadeContext& sc) 
{
  if(gbufID) 
    sc.SetGBufferID(gbufID);

  //sc.mode = SCMODE_SHADOW;//SCMODE_NORMAL;

  //if(sc.mode == SCMODE_SHADOW) // In shadow mode, you are just trying to find out what color the shadow is that is falling on an object. In this case, all you care about is transmitted color.
  //{
  //  sc.out.c = getShadeTransparencyColor(sc);
  //  return;
  //}

  //Color resultingColor(0.0f, 0.0f, 0.0f);

  //for (int i=0; i<sc.nLights; i++) 
  //{
  //  LightDesc* l = sc.Light(i);

  //  if(l != NULL)
  //  {
  //    float nDotL = 0.0f;
  //    float diffCoeff = 0.0f;

  //    Color  color(1.0f, 1.0f, 1.0f);
  //    Point3 N, L, R;
  //   
  //    N = sc.Normal();
  //    N.x *= (-1.0);
  //    N.y *= (-1.0);

      //if(subTex[NORMALMAP_TEX_SLOT] != NULL && sc.doMaps)   // perturb normal if we have any such and move all vectors to tangent space
      //{
      //  Point3 tbnBasis[3];
      //  sc.DPdUVW(tbnBasis);

      //  Matrix3 tangentTransform(1);

      //  tangentTransform.SetRow(0, tbnBasis[0]);
      //  tangentTransform.SetRow(1, tbnBasis[1]);
      //  tangentTransform.SetRow(2, tbnBasis[2]);

	     // N = Normalize( subTex[NORMALMAP_TEX_SLOT]->EvalNormalPerturb(sc)*(-1.0f) );
      //  R = tangentTransform*R;
      //  L = tangentTransform*L;
      //}


      //l->Illuminate(sc, N, color, L, nDotL, diffCoeff);
      //R = sc.ReflectVector();

      //float power = getShadeReflectionCosPow(sc);

      //float specularFalloff = powf(max(DotProd(L,R), 0.0f), power);


  //    Color diffColor/*, specColor*/;

  //    diffColor = getShadeDiffuseColor(sc);

  //    //specColor = getShadeReflectionColor(sc);

  //    resultingColor += color*diffColor*nDotL*PI /* + specColor*(specularFalloff)*PI*/;
  //  }
  //}

  sc.out.c = getShadeDiffuseColor(sc);
  sc.out.t = getShadeTransparencyColor(sc);
  //sc.out.ior = ior;
  //sc.SetIOR(ior);
}

float HydraMtlCatcher::EvalDisplacement(ShadeContext& sc)
{
  //if(subTex[NORMALMAP_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  TSTR className;
  //  subTex[NORMALMAP_TEX_SLOT]->GetClassName(className);

  //  if(className == L"Normal Bump")
  //  {
  //    return 0.0f;
  //  }
  //  else
  //  {
  //    return bump_amount*subTex[NORMALMAP_TEX_SLOT]->EvalMono(sc);
  //  }
  //}
  return 0.0f;
}

Interval HydraMtlCatcher::DisplacementValidity(TimeValue t)
{
  //Interval iv; iv.SetInfinite();
  //return iv;	
  return FOREVER;
}


Color HydraMtlCatcher::getShadeDiffuseColor(ShadeContext &sc)
{
  //if(subTex[CAMERAMAPPED_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  Color texColor(1,1,1);
  //  texColor = subTex[CAMERAMAPPED_TEX_SLOT]->EvalColor(sc);

  //  if(diffuse_mult_on)
  //    return texColor*diffuse_color*diffuse_mult;
  //  else
  //    return texColor*diffuse_mult;
  //}
  //else
  //  return diffuse_color*diffuse_mult;
  Color color(0.5f, 0.5f, 0.5f);
  return color;
}

Color HydraMtlCatcher::getShadeSpecularColor(ShadeContext &sc)
{
  //if(subTex[SPECULAR_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  Color texColor(1,1,1);
  //  texColor = subTex[SPECULAR_TEX_SLOT]->EvalColor(sc);

  //  if(specular_mult_on)
  //    return texColor*specular_color*specular_mult;
  //  else
  //    return texColor*specular_mult;
  //}
  //else
  //  return specular_color*specular_mult;
  Color color(0, 0, 0);
  return color;
}

Color HydraMtlCatcher::getShadeReflectionColor(ShadeContext &sc)
{
  //if(subTex[REFLECT_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  Color texColor(1,1,1);
  //  texColor = subTex[REFLECT_TEX_SLOT]->EvalColor(sc);

  //  if(reflect_mult_on)
  //    return texColor*reflectColor*reflectMult;
  //  else
  //    return texColor*reflectMult;
  //}
  //else
  //  return reflectColor*reflectMult;
  Color color(0, 0, 0);
  return color;

}

Color HydraMtlCatcher::getShadeEmissiveColor(ShadeContext &sc)
{
  //if(subTex[EMISSION_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  Color texColor(1,1,1);
  //  texColor = subTex[EMISSION_TEX_SLOT]->EvalColor(sc);

  //  if(emission_mult_on)
  //    return texColor*emission_color*emission_mult;
  //  else
  //    return texColor*emission_mult;
  //}
  //else
  //  return emission_color*emission_mult;
  Color color(0.1, 0.1, 0.1);
  return color;
}

Color HydraMtlCatcher::getShadeTransparencyColor(ShadeContext &sc)
{
  //Color fogTransparency(1,1,1);

  //if(!transparency_thin_on)
  //{
  //  fogTransparency = fog_color*clamp(10.0f - fog_multiplier, 0.0f, 1.0f); 
  //}

  //if(subTex[TRANSPAR_TEX_SLOT] != NULL && sc.doMaps)
  //{
  //  Color texColor(1,1,1);
  //  texColor = subTex[TRANSPAR_TEX_SLOT]->EvalColor(sc);

  //  if(transparency_mult_on)
  //    return texColor*transparency_color*transparency_mult*fogTransparency;
  //  else
  //    return texColor*transparency_mult*fogTransparency;
  //}
  //else
  //  return transparency_color*transparency_mult*fogTransparency;

  Color color(0.5f, 0.5f, 0.5f);
  return color;
}



inline float SpecularCosPowerFromGlosiness(float a_shiness)
{
  //float cosPower = GetCosPowerFromMaxShiness(a_shiness);
  //if(cosPower < 1.0f) cosPower = 1.0f;
  //if(cosPower > 5000.0f) cosPower = 5000.0f;
  //return cosPower;
  return 0;
}

float HydraMtlCatcher::getShadeSpecularCosPow(ShadeContext &sc)
{
  //if(specular_gloss_or_cos == 0)
  //{
  //  float glosiness;

  //  if(subTex[SPEC_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
  //    glosiness = specular_gloss*(subTex[SPEC_GLOSS_TEX_SLOT]->EvalMono(sc));
  //  else
  //    glosiness = specular_gloss;

  //  return SpecularCosPowerFromGlosiness(glosiness);
  //}
  //else
  //  return specular_cospower;  
  return 0;
}

float HydraMtlCatcher::getShadeReflectionCosPow(ShadeContext &sc)
{
  //if(reflect_gloss_or_cos == 0)
  //{
  //  float glosiness;

  //  if(subTex[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
  //    glosiness = reflect_gloss*(subTex[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
  //  else
  //    glosiness = reflect_gloss;

  //  return SpecularCosPowerFromGlosiness(glosiness);
  //}
  //else
  //  return reflect_cospower;
  return 0;
}

