//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "HydraMtlCatcher.h"
#include <math.h> 
#include <fstream> 
#include "3dsmaxport.h"
#include "stdmat.h"
#include "AssetManagement\iassetmanager.h"


static ParamBlockDesc2 hydraMtlCatcher_param_blk(hydramaterial_params, _T("params"), 0, GethydraMaterialDesc(),
  P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, VERSION_CURRENT,
  PBLOCK_REF,
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  mtl_mat1_on, _T("mtl_mat1_on"), TYPE_BOOL, 0, IDS_MTL1ON,
  p_default, TRUE,
  p_ui, TYPE_SINGLECHEKBOX, IDC_MTLON1,
  paramEnd,
  mtl_cameraMappedBackMap, _T("cameraMappedBackMap"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_CAMERAMAPPEDBACK_MAP,
  p_refno, 0,
  p_subtexno, 0,
  p_ui, TYPE_TEXMAPBUTTON, IDC_CAMERAMAPPEDBACK_MAP,
  paramEnd,
  mtl_opacityMult, _T("opacityMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_OPACITY_MULT,
  p_default, 1.0f,
  p_range, 0.0f, 1.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_OPACITY_MULT, IDC_OPACITY_MULT_SPINNER, 0.1f,
  paramEnd,
  mtl_opacityMap, _T("opacityMap"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OPACITY_MAP,
  p_refno, 1,
  p_subtexno, 1,
  p_ui, TYPE_TEXMAPBUTTON, IDC_OPACITY_MAP,
  paramEnd,
  mtl_receiveShadowOn, _T("recieveShadowOn"), TYPE_BOOL, 0, IDS_RECEIVE_SHADOW_ON,
  p_default, FALSE,
  p_ui, TYPE_SINGLECHEKBOX, IDC_RECEIVE_SHADOW_ON,
  paramEnd,
  mtl_aoMult, _T("aoMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_AO_MULT,
  p_default, 0.0f,
  p_range, 0.0f, 1.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_AO_MULT, IDC_AO_MULT_SPINNER, 0.1f,
  paramEnd,
  mtl_aoDistance, _T("aoDistance"), TYPE_FLOAT, P_ANIMATABLE, IDS_AO_DISTANCE_MULT,
  p_default, 0.0f,
  p_range, 0.0f, 100.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_AO_DISTANCE_MULT, IDC_AO_DISTANCE_MULT_SPINNER, 0.1f,
  paramEnd,
  mtl_reflectColor, _T("reflectColor"), TYPE_RGBA, P_ANIMATABLE, IDS_REFLECT_COLOR,
  p_default, Color(1.0, 1.0, 1.0),
  p_ui, TYPE_COLORSWATCH, IDC_REFLECT_COLOR,
  paramEnd,
  mtl_reflectMult, _T("reflectMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_MULT,
  p_default, 0.0f,
  p_range, 0.0f, 1.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_MULT, IDC_REFLECT_MULT_SPINNER, 0.1f,
  paramEnd,
  mtl_reflectGloss, _T("reflectGloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_GLOSSINESS,
  p_default, 1.0f,
  p_range, 0.0f, 1.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_ROUGHNESS, IDC_REFLECT_GLOSSINESS_SPINNER, 0.1f,
  paramEnd,
  mtl_reflect_ior, _T("reflect_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_IOR,
  p_default, 1.5f,
  p_range, 0.0f, 50.0f,
  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_IOR, IDC_REFLECT_IOR_SPINNER, 0.5f,
  paramEnd,
  mtl_fresnelOn, _T("fresnelOn"), TYPE_BOOL, 0, IDS_REFL_FRESNEL_ON,
  p_default, TRUE,
  p_ui, TYPE_SINGLECHEKBOX, IDC_REFL_FRESNEL_ON,
  paramEnd,
  paramEnd
);

class HydraMtlCatcherDlgProc : public ParamMap2UserDlgProc
{
public:
  HydraMtlCatcher *hMltC;
  HWND thishWnd;

  ICustButton *mCameraMap;
  ICustButton *mOpacity;

  ICustEdit *opacity_edit;
  ICustEdit *ao_mult_edit;
  ICustEdit *ao_distance_edit;
  ICustEdit *refl_mult_edit;
  ICustEdit *refl_gloss_edit;
  ICustEdit *refl_ior_edit;

  ISpinnerControl *opacity_spin;
  ISpinnerControl *aoMult_spin;
  ISpinnerControl *aoDistance_spin;
  ISpinnerControl *reflMult_spin;
  ISpinnerControl *reflGloss_spin;
  ISpinnerControl *refl_ior_spin;

  IColorSwatch *reflColor_selector;

  bool additional_init;

  HydraMtlCatcherDlgProc(HydraMtlCatcher *cb);
  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  void texBtnText();
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() { delete this; }
};

HydraMtlCatcherDlgProc::HydraMtlCatcherDlgProc(HydraMtlCatcher *cb)
{
  hMltC = cb;
  additional_init = true;

  mCameraMap = nullptr;
  mOpacity = nullptr;

  opacity_edit = nullptr;
  ao_mult_edit = nullptr;
  ao_distance_edit = nullptr;
  refl_mult_edit = nullptr;
  refl_gloss_edit = nullptr;
  refl_ior_edit = nullptr;


  opacity_spin = nullptr;
  aoMult_spin = nullptr;
  aoDistance_spin = nullptr;
  reflMult_spin = nullptr;
  reflGloss_spin = nullptr;
  refl_ior_spin = nullptr;

  reflColor_selector = nullptr;

}

void HydraMtlCatcherDlgProc::texBtnText()
{
  if (hMltC->subTex[0]) mCameraMap->SetText(_M("M")); else mCameraMap->SetText(_M(" "));
  if (hMltC->subTex[1]) mOpacity->SetText(_M("M"));   else mOpacity->SetText(_M(" "));
}


void HydraMtlCatcherDlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  //hydraChar dir[1024];
  //hydraChar name[256];
  //hydraChar extension[16];

  //SplitFilename(filename, dir, name, extension);

  WStr dir;
  WStr name;
  WStr extension;
  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);


  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap *tex = (Texmap *)btex;

  switch (mapControlID)
  {
  case IDC_CAMERAMAPPEDBACK_MAP:
    hMltC->SetSubTexmap(CAMERAMAPPED_TEX_SLOT, tex);
    hMltC->subTex[CAMERAMAPPED_TEX_SLOT] = tex;
    break;
  case IDC_OPACITY_MAP:
    hMltC->SetSubTexmap(OPACITY_TEX_SLOT, tex);
    hMltC->subTex[OPACITY_TEX_SLOT] = tex;
    break;
  default:
    break;
  }
}

INT_PTR HydraMtlCatcherDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd = hWnd;
  HydraMtlCatcherDlgProc *dlg = DLGetWindowLongPtr<HydraMtlCatcherDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
  case WM_INITDIALOG:
  {

    if (!opacity_edit)     opacity_edit = GetICustEdit(GetDlgItem(hWnd, IDC_OPACITY_MULT));
    if (!ao_mult_edit)     ao_mult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_AO_MULT));
    if (!ao_distance_edit) ao_distance_edit = GetICustEdit(GetDlgItem(hWnd, IDC_AO_DISTANCE_MULT));
    if (!refl_mult_edit)   refl_mult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_REFLECT_MULT));
    if (!refl_gloss_edit)  refl_gloss_edit = GetICustEdit(GetDlgItem(hWnd, IDC_REFLECT_GLOSSINESS));
    if (!refl_ior_edit)    refl_ior_edit = GetICustEdit(GetDlgItem(hWnd, IDC_REFLECT_IOR));


    if (!opacity_spin) opacity_spin = GetISpinner(GetDlgItem(hWnd, IDC_OPACITY_MULT_SPINNER));
    if (!aoMult_spin) aoMult_spin = GetISpinner(GetDlgItem(hWnd, IDC_AO_MULT_SPINNER));
    if (!aoDistance_spin) aoDistance_spin = GetISpinner(GetDlgItem(hWnd, IDC_AO_DISTANCE_MULT_SPINNER));
    if (!reflMult_spin) reflMult_spin = GetISpinner(GetDlgItem(hWnd, IDC_REFLECT_MULT_SPINNER));
    if (!reflGloss_spin) reflGloss_spin = GetISpinner(GetDlgItem(hWnd, IDC_REFLECT_GLOSSINESS_SPINNER));
    if (!refl_ior_spin) refl_ior_spin = GetISpinner(GetDlgItem(hWnd, IDC_REFLECT_IOR_SPINNER));


    if (!mCameraMap) mCameraMap = GetICustButton(GetDlgItem(hWnd, IDC_CAMERAMAPPEDBACK_MAP));
    if (!mOpacity) mOpacity = GetICustButton(GetDlgItem(hWnd, IDC_OPACITY_MAP));

    reflColor_selector = GetIColorSwatch(GetDlgItem(hWnd, IDC_REFLECT_COLOR));

    //spec_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_SPECULAR_COLOR));

    DragAcceptFiles(GetDlgItem(hWnd, IDC_CAMERAMAPPEDBACK_MAP), true);
    DragAcceptFiles(GetDlgItem(hWnd, IDC_OPACITY_MAP), true);

    return TRUE;
  }
  case WM_COMMAND:
    switch (LOWORD(wParam))
    {
    case IDC_CAMERAMAPPEDBACK_MAP:
    case IDC_OPACITY_MAP:
    default:
      break;
    }
    break;
  case WM_DESTROY:
    ReleaseICustButton(mCameraMap);
    ReleaseICustButton(mOpacity);

    ReleaseICustEdit(opacity_edit);
    ReleaseICustEdit(ao_mult_edit);
    ReleaseICustEdit(ao_distance_edit);
    ReleaseICustEdit(refl_mult_edit);
    ReleaseICustEdit(refl_gloss_edit);
    ReleaseICustEdit(refl_ior_edit);

    ReleaseISpinner(opacity_spin);
    ReleaseISpinner(aoMult_spin);
    ReleaseISpinner(aoDistance_spin);
    ReleaseISpinner(reflMult_spin);
    ReleaseISpinner(reflGloss_spin);
    ReleaseISpinner(refl_ior_spin);


    ReleaseIColorSwatch(reflColor_selector);

    break;
  case WM_DROPFILES:
    POINT pt;
    WORD numFiles;
    HWND dropTarget;
    int dropTargetID;

    hydraChar lpszFile[80];

    DragQueryPoint((HDROP)wParam, &pt);

    #ifdef MAX2012
    numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPSTR)NULL, 0);
    #else
    numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);
    #endif


    if (numFiles == 0)
    {
      DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
      dropTarget = RealChildWindowFromPoint(hWnd, pt);
      dropTargetID = GetDlgCtrlID(dropTarget);
      DropFileInMapSlot(dropTargetID, lpszFile);
    }

    DragFinish((HDROP)wParam);

    break;
  default:
    return FALSE;

  }
  return TRUE;
}


HydraMtlCatcher::HydraMtlCatcher(BOOL loading)
{
  for (int i = NSUBMTL; i < NSUBTEX + NSUBMTL; i++) subTex[i] = NULL;
  pblock = NULL;

  reflectColor = Color(1.0, 1.0, 1.0);

  receiveShadowOn = true;
  fresnelOn = true;

  reflectMult = 0.0f;
  reflectGloss = 1.0f;  
  reflect_ior = 1.5f;

  if (!loading)
    Reset();
}


void HydraMtlCatcher::Reset()
{
  ivalid.SetEmpty();
  for (int i = 0; i < NSUBMTL; i++)
  {

  }

  for (int i = NSUBMTL; i < NSUBTEX + NSUBMTL; i++)
  {
    if (subTex[i])
    {
      DeleteReference(i);
      subTex[i] = NULL;
    }
  }

  GethydraMaterialDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* HydraMtlCatcher::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)
{
  IAutoMParamDlg* masterDlg = GethydraMaterialDesc()->CreateParamDlgs(hwMtlEdit, imp, this);

    // TODO: Set param block user dialog if necessary
  hydraMtlCatcher_param_blk.SetUserDlgProc(new HydraMtlCatcherDlgProc(this));
  return masterDlg;
}

BOOL HydraMtlCatcher::SetDlgThing(ParamDlg* dlg)
{
  return FALSE;
}

Interval HydraMtlCatcher::Validity(TimeValue t)
{
  Interval valid = FOREVER;

  for (int i = 0; i < NSUBMTL; i++)
  {
    /* if (submtl[i])
       valid &= submtl[i]->Validity(t);*/
  }

  for (int i = NSUBMTL; i < NSUBMTL + NSUBTEX; i++)
  {
    if (subTex[i])
      valid &= subTex[i]->Validity(t);
  }

  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle HydraMtlCatcher::GetReference(int i)
{

  if (i < NSUBTEX + NSUBMTL)
    return subTex[i];
  else
    return pblock;

}

void HydraMtlCatcher::SetReference(int i, RefTargetHandle rtarg)
{

  int n = NSUBTEX + NSUBMTL;

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < n && s != L"ParamBlock2")
  {
    subTex[i] = (Texmap *)rtarg;
  }
  else
  {
    pblock = (IParamBlock2 *)rtarg;
  }

}

TSTR HydraMtlCatcher::SubAnimName(int i)
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else return TSTR(_T(""));
}

Animatable* HydraMtlCatcher::SubAnim(int i)
{
  /* if (i < NSUBMTL)
     return submtl[i];
   else*/ if (i < NSUBTEX + NSUBMTL)
     return subTex[i];
   else return pblock;
}


#ifdef MAX2014
RefResult hydraMaterial::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
  switch (message)
  {
  case REFMSG_CHANGE:
    ivalid.SetEmpty();
    if (hTarget == pblock)
    {
      ParamID changing_param = pblock->LastNotifyParamID();
      hydramaterial_param_blk.InvalidateUI(changing_param);
    }
    break;

  }
  return REF_SUCCEED;
}
#else
RefResult HydraMtlCatcher::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
  case REFMSG_CHANGE:
    ivalid.SetEmpty();

    /*  IParamBlock2* cpb = NULL;
      ParamID changing_param = GethydraMaterialDesc()->LastNotifyParamID(this, cpb);
      if (changing_param != -1)
      {
        cpb->GetDesc()->InvalidateUI(changing_param);
      }
      else
      {
        GethydraMaterialDesc()->InvalidateUI();
      }*/


    if (hTarget == pblock)
    {
      ParamID changing_param = pblock->LastNotifyParamID();
      hydraMtlCatcher_param_blk.InvalidateUI(changing_param);
    }
    break;
  }
  return REF_SUCCEED;
}
#endif


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* HydraMtlCatcher::GetSubMtl(int i)
{

  return NULL;
}

void HydraMtlCatcher::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i, m);
}

TSTR HydraMtlCatcher::GetSubMtlSlotName(int i)
{
  return _T("");
}

TSTR HydraMtlCatcher::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}

/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* HydraMtlCatcher::GetSubTexmap(int i)
{
  return subTex[i];
}

void HydraMtlCatcher::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i, m);

  switch (i)
  {
  case 0:
    hydraMtlCatcher_param_blk.InvalidateUI(mtl_cameraMappedBackMap);
    pblock->SetValue(mtl_cameraMappedBackMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 1:
    hydraMtlCatcher_param_blk.InvalidateUI(mtl_opacityMap);
    pblock->SetValue(mtl_opacityMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;

  default:
    break;
  }

  NotifyChanged();
}

TSTR HydraMtlCatcher::GetSubTexmapSlotName(int i)
{
  switch (i)
  {
  case 0:  return GetString(IDS_CAMERAMAPPEDBACK_MAP);
  case 1:  return GetString(IDS_OPACITY_MAP);

  default: return _T("");
  }
}

TSTR HydraMtlCatcher::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraMtlCatcher::Save(ISave *isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res != IO_OK) return res;
  isave->EndChunk();

  isave->BeginChunk(HYDRA_CHUNK);
  
  isave->Write(&reflectColor, sizeof(Color), &nb);

  isave->Write(&receiveShadowOn, sizeof(bool), &nb);
  isave->Write(&fresnelOn, sizeof(bool), &nb);


  isave->Write(&aoMult, sizeof(float), &nb);
  isave->Write(&aoDistance, sizeof(float), &nb);
  isave->Write(&reflectMult, sizeof(float), &nb);
  isave->Write(&reflectGloss, sizeof(float), &nb);
  isave->Write(&reflect_ior, sizeof(float), &nb);

  isave->EndChunk();


  return IO_OK;
}

IOResult HydraMtlCatcher::Load(ILoad *iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
    case MTL_HDR_CHUNK:
      res = MtlBase::Load(iload);
      break;
    case HYDRA_CHUNK:
      res = iload->Read(&reflectColor, sizeof(Color), &nb);

      res = iload->Read(&receiveShadowOn, sizeof(bool), &nb);
      res = iload->Read(&fresnelOn, sizeof(bool), &nb);

      res = iload->Read(&aoMult, sizeof(float), &nb);
      res = iload->Read(&aoDistance, sizeof(float), &nb);
      res = iload->Read(&reflectMult, sizeof(float), &nb);
      res = iload->Read(&reflectGloss, sizeof(float), &nb);
      res = iload->Read(&reflect_ior, sizeof(float), &nb);

      break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle HydraMtlCatcher::Clone(RemapDir &remap)
{
  HydraMtlCatcher *mnew = new HydraMtlCatcher(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this);
  mnew->ReplaceReference(NSUBMTL, remap.CloneRef(pblock));

  mnew->reflectColor = reflectColor;

  mnew->receiveShadowOn = receiveShadowOn;
  mnew->fresnelOn = fresnelOn;

  mnew->aoMult = aoMult;
  mnew->aoDistance = aoDistance;
  mnew->reflectMult = reflectMult;
  mnew->reflectGloss = reflectGloss;
  mnew->reflect_ior = reflect_ior;

  mnew->ivalid.SetEmpty();
  for (int i = 0; i < NSUBMTL; i++)
  {
  }

  for (int i = 0; i < NSUBTEX; i++)
  {
    mnew->subTex[i] = NULL;
    if (subTex[i])
      mnew->ReplaceReference(i, remap.CloneRef(subTex[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}

void HydraMtlCatcher::NotifyChanged()
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraMtlCatcher::Update(TimeValue t, Interval& valid)
{
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();
    pblock->GetValue(mtl_mat1_on, t, affect_shadows_on, ivalid);

    pblock->GetValue(mtl_receiveShadowOn, t, receiveShadowOn, ivalid);
    pblock->GetValue(mtl_aoMult, t, aoMult, ivalid);
    pblock->GetValue(mtl_aoDistance, t, aoDistance, ivalid);
    pblock->GetValue(mtl_reflectColor, t, reflectColor, ivalid);
    pblock->GetValue(mtl_reflectMult, t, reflectMult, ivalid);
    pblock->GetValue(mtl_reflectGloss, t, reflectGloss, ivalid);
    pblock->GetValue(mtl_reflect_ior, t, reflect_ior, ivalid);
    pblock->GetValue(mtl_fresnelOn, t, fresnelOn, ivalid);


    for (int i = 0; i < NSUBMTL; i++)
    {
      /*if (submtl[i])
        submtl[i]->Update(t,ivalid);*/
    }

    for (int i = 0; i < NSUBTEX; i++)
    {
      if (subTex[i])
      {
        subTex[i]->Update(t, ivalid);
      }
    }

  }
  valid &= ivalid;
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/


void HydraMtlCatcher::SetAmbient(Color c, TimeValue t) {}
void HydraMtlCatcher::SetDiffuse(Color c, TimeValue t)
{
  //diffuse_color = c;
  //pblock->SetValue(mtl_diffuse_color, t, diffuse_color);
  //NotifyChanged();
}
void HydraMtlCatcher::SetSpecular(Color c, TimeValue t)
{
  //specular_color = c;
  //pblock->SetValue(mtl_specular_color, t, specular_color);
  //NotifyChanged();
}
void HydraMtlCatcher::SetShininess(float v, TimeValue t)
{
  //log(reflect_cospower)/log(100000.0)
  //derp

  //specular_roughness = v;
  //pblock->SetValue(mtl_specular_roughness, t, specular_roughness);
  //NotifyChanged();
}

Color HydraMtlCatcher::GetAmbient(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetAmbient(mtlNum,backFace):Color(0,0,0);
  //return diffuse_color*diffuse_mult;
  Color color(0.2f, 0.2f, 0.2f);
  return color;
}

Color HydraMtlCatcher::GetDiffuse(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
  //return diffuse_color*diffuse_mult;
  Color color(1, 1, 1);
  return color;
}

Color HydraMtlCatcher::GetSpecular(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetSpecular(mtlNum,backFace):Color(0,0,0);
  //return reflectColor*reflectMult;
  Color color(0, 0, 0);
  return color;
}

float HydraMtlCatcher::GetXParency(int mtlNum, BOOL backFace)
{
  //return 0.33333f*(opacityMult.r*transparency_mult + transparency_color.g*transparency_mult + transparency_color.b*transparency_mult);
  return 0.9f;
}

float HydraMtlCatcher::GetShininess(int mtlNum, BOOL backFace)
{
  return 0.0f;
}

float HydraMtlCatcher::GetShinStr(int mtlNum, BOOL backFace)
{
  return 0.0f;
}

float HydraMtlCatcher::WireSize(int mtlNum, BOOL backFace)
{
  return 0.0f;
}




ClassDesc2* GethydraMaterialDesc()
{
  static HydraMtlCatcherClassDesc hydraMaterialDesc;
  return &hydraMaterialDesc;
}

FPInterface * HydraMtlCatcherClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraMtlCatcherClassDesc::GetEntryName() const
{
  return const_cast<HydraMtlCatcherClassDesc*>(this)->ClassName();

}

const MCHAR * HydraMtlCatcherClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * HydraMtlCatcherClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}
