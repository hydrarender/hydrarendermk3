//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "HydraMtl.h"
#include <math.h> 
#include <fstream> 
#include "3dsmaxport.h"
#include "AssetManagement\iassetmanager.h"



// Make a bitmap texture map.
Interface* hydraMaterial::ip         = GetCOREInterface();
BitmapTex* hydraMaterial::bmapTexEnv = NewDefaultBitmapTex();
StdUVGen* hydraMaterial::uvGenEnv    = bmapTexEnv->GetUVGen();
Texmap* hydraMaterial::texmapEnv     = (Texmap*)NewDefaultBitmapTex();




static ParamBlockDesc2 hydramaterial_param_blk ( hydramaterial_params, _T("params"),  0, GethydraMaterialDesc(), 
  P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, VERSION_CURRENT,
	PBLOCK_REF, 
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  mtl_mat1_on, _T("mtl_mat1_on"), TYPE_BOOL, 0, IDS_MTL1ON,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_MTLON1,
		paramEnd,
  mtl_diffuse_color,	_T("mtl_diffuse_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_DIFFUSE_COLOR,
    p_default, Color(0.8, 0.8, 0.8),
    p_ui, TYPE_COLORSWATCH, IDC_DIFFUSE_COLOR,
    paramEnd,
  mtl_diffuse_map,	_T("mtl_diffuse_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_DIFFUSE_MAP,		
    p_refno,		0,
		p_subtexno,		0,	
    p_ui, TYPE_TEXMAPBUTTON, IDC_DIFFUSE_MAP,
    paramEnd,
  mtl_diffuse_tint_on, _T("mtl_diffuse_mult_on"), TYPE_BOOL, 0, IDS_DIFFUSE_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DIFFUSE_MULT_ON,
    paramEnd,
  mtl_diffuse_mult, _T("mtl_diffuse_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_DIFFUSE_MULT, 
    p_default, 1.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DIFFUSE_MULT,	IDC_DIFFUSE_MULT_SPINNER, 0.1f, 
    paramEnd,
  mtl_roughness_mult, _T("mtl_roughness_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_DIFFUSE_ROUGHNESS,
	  p_default, 0.0f,
	  p_range, 0.0f, 1.0f,
	  p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DIFFUSE_ROUGHNESS, IDC_DIFFUSE_ROUGHNESS_SPINNER, 0.1f,
	  paramEnd,
  mtl_specular_color, _T("mtl_specular_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_SPECULAR_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_SPECULAR_COLOR,
    paramEnd,
	mtl_specular_map, _T("mtl_specular_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_SPECULAR_MAP,
    p_refno,		1,
		p_subtexno,		1,	 
    p_ui, TYPE_TEXMAPBUTTON, IDC_SPECULAR_MAP,
    paramEnd,
  mtl_specular_tint_on, _T("mtl_specular_mult_on"), TYPE_BOOL, 0, IDS_SPECULAR_MULT,
		p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_SPECULAR_MULT_ON,
    paramEnd,
  mtl_specular_mult, _T("mtl_specular_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_MULT, 
    p_default, 0.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_MULT,	IDC_SPECULAR_MULT_SPINNER, 0.1f, 
    paramEnd,
  mtl_specular_brdf,	_T("mtl_specular_brdf"), TYPE_INT, P_RESET_DEFAULT+P_ANIMATABLE, IDS_SPECULAR_BRDF,  
    p_ui, TYPE_INT_COMBOBOX, IDC_SPECULAR_BRDF, 1, IDS_BRDF1, /*IDS_BRDF2, IDS_BRDF3, IDS_BRDF4, IDS_BRDF5,*/
    p_tooltip, IDS_SPECULAR_BRDF,
    paramEnd,
  mtl_specular_roughness, _T("mtl_specular_roughness"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_ROUGHNESS, 
    p_default, 1.0f, 
    p_range, 0.0f, 1.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_ROUGHNESS,	IDC_SPECULAR_ROUGHNESS_SPINNER, 0.1f, 
    paramEnd,
	mtl_specular_cospower, _T("mtl_specular_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_COS, 
    p_default, 256.0f, 
    p_range, 1.0f, 1000000.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_COS,	IDC_SPECULAR_COS_SPINNER, 0.1f, 
    paramEnd,
	mtl_specular_ior, _T("mtl_specular_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_IOR, 
    p_default, 1.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_IOR,	IDC_SPECULAR_IOR_SPINNER, 0.1f, 
    paramEnd,
  mtl_reflect_color, _T("mtl_reflect_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_REFLECT_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_REFLECT_COLOR,
    paramEnd,
  mtl_reflect_map,	_T("mtl_reflect_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_REFLECT_MAP,		
    p_refno,		2,
		p_subtexno,		2,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFLECT_MAP,
    paramEnd,
  mtl_reflect_tint_on, _T("mtl_reflect_mult_on"), TYPE_BOOL, 0, IDS_REFLECT_MULT,
		p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_REFLECT_MULT_ON,
    paramEnd,
  mtl_reflect_mult, _T("mtl_reflect_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_MULT, 
    p_default, 0.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_MULT,	IDC_REFLECT_MULT_SPINNER, 0.1f, 
    paramEnd,
  mtl_reflect_brdf,	_T("mtl_reflect_brdf"), TYPE_INT, P_RESET_DEFAULT+P_ANIMATABLE, IDS_REFLECT_BRDF,  
    p_ui, TYPE_INT_COMBOBOX, IDC_REFLECT_BRDF, 2, IDS_BRDF1, IDS_BRDF2, /*IDS_BRDF3, IDS_BRDF4, IDS_BRDF5,*/
    p_tooltip, IDS_REFLECT_BRDF,
    paramEnd,
  mtl_reflect_cospower, _T("mtl_reflect_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_COS, 
    p_default, 256.0f, 
    p_range, 1.0f, 1000000.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_COS,	IDC_REFLECT_COS_SPINNER, 0.1f, 
    paramEnd,
  mtl_reflect_roughness, _T("mtl_reflect_roughness"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_ROUGHNESS, 
    p_default, 1.0f, 
    p_range, 0.0f, 1.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_ROUGHNESS,	IDC_REFLECT_ROUGHNESS_SPINNER, 0.1f, 
    paramEnd,
	mtl_reflect_ior, _T("mtl_reflect_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_IOR, 
    p_default, 1.5f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_IOR,	IDC_REFLECT_IOR_SPINNER, 0.1f, 
    paramEnd,
  mtl_emission_color, _T("mtl_emission_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_EMISSION_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_EMISSION_COLOR,
    paramEnd,
  mtl_emission_map,	_T("mtl_emission_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_EMISSION_MAP,		
    p_refno,		3,
		p_subtexno,		3,	 
    p_ui, TYPE_TEXMAPBUTTON, IDC_EMISSION_MAP,
    paramEnd,
  mtl_emission_tint_on, _T("mtl_emission_mult_on"), TYPE_BOOL, 0, IDS_EMISSION_MULT,
		p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_EMISSION_MULT_ON,
    paramEnd,
  mtl_emission_mult, _T("mtl_emission_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_EMISSION_MULT, 
    p_default, 0.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_EMISSION_MULT,	IDC_EMISSION_MULT_SPINNER, 0.1f, 
    paramEnd,
  mtl_transparency_color, _T("mtl_transparency_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_TRANSP_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_TRANSP_COLOR,
    paramEnd,
  mtl_transparency_map,	_T("mtl_transparency_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_TRANSP_MAP,		
    p_refno,		4,
		p_subtexno,		4,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSP_MAP,
    paramEnd,
  mtl_transparency_tint_on, _T("mtl_transparency_mult_on"), TYPE_BOOL, 0, IDS_TRANSP_MULT,
		p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSP_MULT_ON,
    paramEnd,
  mtl_transparency_mult, _T("mtl_transparency_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_MULT, 
    p_default, 0.0f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_MULT,	IDC_TRANSP_MULT_SPINNER, 0.1f, 
    paramEnd,
  mtl_ior, _T("mtl_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_IOR, 
    p_default, 1.5f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_IOR,	IDC_IOR_SPINNER, 0.1f, 
    paramEnd,
  mtl_transparency_cospower, _T("mtl_transparency_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_COS, 
    p_default, 256.0f, 
    p_range, 1.0f, 1000000.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_COS,	IDC_TRANSP_COS_SPINNER, 0.1f, 
    paramEnd,
  mtl_transparency_thin_on, _T("mtl_transparency_thin_on"), TYPE_BOOL, 0, IDS_TRANSP_THIN,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSP_THIN_ON,
    paramEnd,
  mtl_fog_color, _T("mtl_fog_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_FOG_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_FOG_COLOR,
    paramEnd,
  mtl_fog_multiplier, _T("mtl_fog_multiplier"), TYPE_FLOAT, P_ANIMATABLE, IDS_FOG_MULT, 
    p_default, 0.0f, 
    p_range, 0.0f, 1000.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_FOG_MULT,	IDC_FOG_MULT_SPINNER, 1.0f, 
    paramEnd,
  mtl_exit_color, _T("mtl_exit_color"), TYPE_RGBA, P_ANIMATABLE,	IDS_EXIT_COLOR,
	p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_EXIT_COLOR,
    paramEnd,
  mtl_displacement_on, _T("mtl_displacement_on"), TYPE_BOOL, 0, IDS_DISPLACEMENT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DISPLACEMENT_ON,
    paramEnd,
  mtl_displacement_height, _T("mtl_displacement_height"), TYPE_FLOAT, P_ANIMATABLE, IDS_DISPLACE_HEIGHT, 
    p_default, 0.5f, 
    p_range, 0.0f, 100.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DISPLACE_HEIGHT,	IDC_DISPLACE_HEIGHT_SPINNER, 0.1f, 
    paramEnd,
  mtl_normal_map,	_T("mtl_normal_map"), TYPE_TEXMAP,/*0*/ P_OWNERS_REF, IDS_NORMAL_MAP,		
    p_refno,		5,
		p_subtexno,		5,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_NORMAL_MAP,
    paramEnd,
  mtl_displacement_invert_height_on, _T("mtl_displacement_invert_height_on"), TYPE_BOOL, 0, IDS_DISPLACE_INVERTHEIGHT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DISPLACE_INVERTHEIGHT_ON,
    paramEnd,
	mtl_no_ic_records, _T("mtl_no_ic_records"), TYPE_BOOL, 0, IDS_NO_IC_RECORDS,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_NO_IC_RECORDS,
    paramEnd,
	mtl_spec_gloss,  _T("mtl_spec_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPEC_GLOSSINESS, 
    p_default, 1.0f, 
    p_range, 0.0f, 1.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_GL,	IDC_SPECULAR_GL_SPINNER, 0.01f, 
    paramEnd,
		mtl_spec_gl_map, _T("mtl_spec_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_SPEC_GLOSSINESS_MAP,
    p_refno,		6,
		p_subtexno,		6,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_SPEC_GL_MAP,
    paramEnd,
	mtl_spec_gloss_or_cos, _T("mtl_spec_gloss_or_cos"), TYPE_INT, 0, IDS_SPEC_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2,  IDC_SPEC_GL_RAD, IDC_SPEC_COS_RAD,
    paramEnd,
	mtl_spec_fresnel_on, _T("mtl_spec_fresnel_on"), TYPE_BOOL, 0, IDS_SPEC_FRESNEL_ON,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_SPEC_FRESNEL_ON,
    paramEnd,
	mtl_refl_gloss, _T("mtl_refl_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_GLOSSINESS, 
    p_default, 1.0f, 
    p_range, 0.0f, 10.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFL_GL,	IDC_REFL_GL_SPINNER, 0.01f, 
    paramEnd,
	mtl_refl_gl_map, _T("mtl_refl_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_REFL_GLOSSINESS_MAP,		
    p_refno,		7,
		p_subtexno,		7,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFL_GL_MAP,
    paramEnd,
	mtl_refl_gloss_or_cos, _T("mtl_refl_gloss_or_cos"), TYPE_INT, 0, IDS_REFL_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2,  IDC_REFL_GL_RAD, IDC_REFL_COS_RAD,
    paramEnd,
	mtl_refl_fresnel_on, _T("mtl_refl_fresnel_on,"), TYPE_BOOL, 0, IDS_REFL_FRESNEL_ON,
		p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_REFL_FRESNEL_ON,
    paramEnd,
	mtl_transp_gloss, _T("mtl_transp_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_GLOSSINESS, 
    p_default, 1.0f, 
    p_range, 0.0f, 10.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_GL,	IDC_TRANSP_GL_SPINNER, 0.01f, 
    paramEnd,
	mtl_transp_gl_map, _T("mtl_transp_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_TRANSP_GLOSSINESS_MAP,		
    p_refno,		8,
		p_subtexno,		8,	  
    p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSP_GL_MAP,
    paramEnd,
	mtl_transp_gloss_or_cos, _T("mtl_transp_gloss_or_cos"), TYPE_INT, 0, IDS_TRANSP_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2,  IDC_TRANSP_GL_RAD, IDC_TRANSP_COS_RAD,
    paramEnd,
	mtl_lock_specular, _T("mtl_lock_specular,"), TYPE_BOOL, 0, IDS_LOCK_SPECULAR,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_LOCK_SPECULAR,
    paramEnd,
	mtl_bump_amount, _T("mtl_bump_amount"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_AMT, 
    p_default, 0.2f, 
    p_range, 0.0f, 5.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_AMT,	IDC_BUMP_AMT_SPINNER, 0.1f, 
    paramEnd,
	mtl_bump_radius, _T("mtl_bump_radius"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_RADIUS, 
    p_default, 0.25f, 
    p_range, 0.0f, 1.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_RADIUS,	IDC_BUMP_RADIUS_SPINNER, 0.1f, 
    paramEnd,
	mtl_bump_sigma, _T("mtl_bump_sigma"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_SIGMA, 
    p_default, 1.5f, 
    p_range, 1.0f, 16.0f, 
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_SIGMA,	IDC_BUMP_SIGMA_SPINNER, 0.5f, 
    paramEnd,
	mtl_emission_gi, _T("mtl_emission_gi"), TYPE_BOOL, 0, IDS_EMISSION_GI,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_EMISSION_GI,
    paramEnd,
	mtl_opacity_map, _T("mtl_opacity_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OPACITY_MAP,
		p_refno, 9,
		p_subtexno, 9,
		p_ui, TYPE_TEXMAPBUTTON, IDC_OPACITY_MAP,
		paramEnd,
  mtl_opacity_smooth, _T("mtl_opacity_smooth"), TYPE_BOOL, 0, IDS_SMOOTH,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_SMOOTH,
		paramEnd,
	mtl_translucency_color, _T("mtl_translucency_color"), TYPE_RGBA, P_ANIMATABLE, IDS_TRANSLUCENCY_COLOR,
		p_default, Color(1.0, 1.0, 1.0),
		p_ui, TYPE_COLORSWATCH, IDC_TRANSLUCENCY_COLOR,
		paramEnd,
	mtl_translucency_mult, _T("mtl_translucency_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSLUCENCY_MULT,
		p_default, 0.0f,
		p_range, 0.0f, 100.0f,
		p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSLUCENCY_MULT, IDC_TRANSLUCENCY_MULT_SPINNER, 0.1f,
		paramEnd,
	mtl_translucency_map, _T("mtl_translucency_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_TRANSLUCENCY_MAP,
		p_refno, 10,
		p_subtexno, 10,
		p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSLUCENCY_MAP,
		paramEnd,
	mtl_translucency_tint_on, _T("mtl_translucency_mult_on"), TYPE_BOOL, 0, IDS_TRANSLUCENCY_MULT_ON,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSLUCENCY_MULT_ON,
		paramEnd,
	mtl_reflect_extrusion, _T("mtl_reflect_extrusion"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_REFLECT_EXTRUSION,
		p_default, 0,
    p_ui, TYPE_INT_COMBOBOX, IDC_REFLECT_EXTRUSION, 2, /*IDS_EXTRUSION_COMP,*/ IDS_EXTRUSION_STRONG, IDS_EXTRUSION_LUM,
		p_tooltip, IDS_REFLECT_EXTRUSION,
		paramEnd,
  paramEnd
  );





hydraMaterialDlgProc::hydraMaterialDlgProc(hydraMaterial *cb)
{
	hM = cb; 

  hM->uvGenEnv->SetCoordMapping(UVMAP_SPHERE_ENV);  
  hM->bmapTexEnv->SetMapName(_T("c:\\[Hydra]\\pluginFiles\\EnvMatEdit.hdr"));
  hM->bmapTexEnv->SetFilterType(FILTER_NADA); 
  hM->texmapEnv = hM->bmapTexEnv;
  

	additional_init = true;

	mEmission = nullptr;
	mDiffuse = nullptr;
	mSpecular = nullptr;
	mReflect = nullptr;
	mTransparency = nullptr;
	mNormal = nullptr;
	mSpecGloss = nullptr;
	mReflectGloss = nullptr;
	mTranspGloss = nullptr;
	mOpacity = nullptr;
	mTranslucency = nullptr;

	spec_gloss_edit = nullptr;
	spec_cos_edit = nullptr;
	spec_mult_edit = nullptr;
	spec_ior_edit = nullptr;
	refl_gloss_edit = nullptr;
	refl_cos_edit = nullptr;
	transp_gloss_edit = nullptr;
	transp_cos_edit = nullptr;
	bump_amt_edit = nullptr;
	diffuse_roughness_edit = nullptr;

	spec_gloss_spin = nullptr;
	spec_cos_spin = nullptr;
	spec_mult_spin = nullptr;
	spec_ior_spin = nullptr;
	refl_gloss_spin = nullptr;
	refl_cos_spin = nullptr;
	transp_gloss_spin = nullptr;
	transp_cos_spin = nullptr;
	bump_amt_spin = nullptr;
	diffuse_roughness_spin = nullptr;

	spec_color = nullptr;

}

void hydraMaterialDlgProc::texBtnText()
{
	if(hM->subTex[0]) mDiffuse->SetText(_M("M")); else mDiffuse->SetText(_M(" "));
	if(hM->subTex[1]) mSpecular->SetText(_M("M")); else mSpecular->SetText(_M(" "));
	if(hM->subTex[2]) mReflect->SetText(_M("M")); else mReflect->SetText(_M(" "));
	if(hM->subTex[3]) mEmission->SetText(_M("M")); else mEmission->SetText(_M(" "));
	if(hM->subTex[4]) mTransparency->SetText(_M("M")); else mTransparency->SetText(_M(" "));
	if(hM->subTex[5]) mNormal->SetText(_M("M")); else mNormal->SetText(_M(" "));
	if(hM->subTex[6]) mSpecGloss->SetText(_M("M")); else mSpecGloss->SetText(_M(" "));
	if(hM->subTex[7]) mReflectGloss->SetText(_M("M")); else mReflectGloss->SetText(_M(" "));
	if(hM->subTex[8]) mTranspGloss->SetText(_M("M")); else mTranspGloss->SetText(_M(" "));
	if(hM->subTex[9]) mOpacity->SetText(_M("M")); else mOpacity->SetText(_M(" "));
	if (hM->subTex[10]) mTranslucency->SetText(_M("M")); else mTranslucency->SetText(_M(" "));
}

void hydraMaterialDlgProc::bumpAmountLock() 
{
	TSTR className;
	if(hM->subTex[5])
	{
		hM->subTex[5]->GetClassName(className);
		if(className == L"Normal Bump")
		{
			bump_amt_edit->Disable();
			bump_amt_spin->Disable();
		}
		else
		{
			bump_amt_edit->Enable();
			bump_amt_spin->Enable();
		}
	}
	else
	{
		bump_amt_edit->Disable();
		bump_amt_spin->Disable();
	}
}

void hydraMaterialDlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
	BitmapTex *btex = NewDefaultBitmapTex();
	
	//MaxSDK::AssetManagement::AssetUser u;

	//u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

	SplitFilename(filename_wstr, &dir, &name, &extension);

	//std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};
	
	//tex->SetMap(u);
	btex->SetMapName(filename);
	btex->SetName(name.data());

	Texmap *tex = (Texmap *)btex;

	switch (mapControlID)
	{
	case IDC_EMISSION_MAP:
		hM->SetSubTexmap(EMISSION_TEX_SLOT, tex);
		hM->subTex[EMISSION_TEX_SLOT] = tex;
		break;
	case IDC_DIFFUSE_MAP:
		hM->SetSubTexmap(DIFFUSE_TEX_SLOT, tex);
		hM->subTex[DIFFUSE_TEX_SLOT] = tex;
		break;
	case IDC_REFLECT_MAP:
		hM->SetSubTexmap(REFLECT_TEX_SLOT, tex);
		hM->subTex[REFLECT_TEX_SLOT] = tex;
		break;
	case IDC_TRANSP_MAP:
		hM->SetSubTexmap(TRANSPAR_TEX_SLOT, tex);
		hM->subTex[TRANSPAR_TEX_SLOT] = tex;
		break;
	case IDC_NORMAL_MAP:
		hM->SetSubTexmap(NORMALMAP_TEX_SLOT, tex);
		hM->subTex[NORMALMAP_TEX_SLOT] = tex;
		break;
	case IDC_REFL_GL_MAP:
		hM->SetSubTexmap(REFL_GLOSS_TEX_SLOT, tex);
		hM->subTex[REFL_GLOSS_TEX_SLOT] = tex;
		break;
	case IDC_TRANSP_GL_MAP:
		hM->SetSubTexmap(TRANSP_GLOSS_TEX_SLOT, tex);
		hM->subTex[TRANSP_GLOSS_TEX_SLOT] = tex;
		break;
	case IDC_OPACITY_MAP:
		hM->SetSubTexmap(OPACITY_TEX_SLOT, tex);
		hM->subTex[OPACITY_TEX_SLOT] = tex;
		break;
	case IDC_TRANSLUCENCY_MAP:
		hM->SetSubTexmap(TRANSLUCENCY_TEX_SLOT, tex);
		hM->subTex[TRANSLUCENCY_TEX_SLOT] = tex;
		break;
	default:
		break;
	}
}

void hydraMaterialDlgProc::lockSpec(bool lock) 
{
	EnableWindow(GetDlgItem( thishWnd, IDC_SPEC_GL_RAD), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPECULAR_MULT_ON), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPEC_COS_RAD), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPECULAR_ROUGHNESS), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPECULAR_ROUGHNESS_SPINNER), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPECULAR_BRDF), !lock);
	EnableWindow(GetDlgItem( thishWnd, IDC_SPEC_FRESNEL_ON), !lock);
	if(lock)
	{
		spec_color->Disable();
		spec_gloss_edit->Disable();
		spec_gloss_spin->Disable();
		spec_cos_edit->Disable();
		spec_cos_spin->Disable();
		mSpecular->Disable();
		mSpecGloss->Disable();
		spec_mult_edit->Disable();
		spec_ior_edit->Disable();
		spec_mult_spin->Disable();
		spec_ior_spin->Disable();
	}
	else
	{
		spec_color->Enable();
		if(IsDlgButtonChecked(thishWnd, IDC_SPEC_GL_RAD))
		{
			spec_gloss_edit->Enable();
			spec_gloss_spin->Enable();
			spec_cos_edit->Disable();
			spec_cos_spin->Disable();
		}
		else
		{
			spec_gloss_edit->Disable();
			spec_gloss_spin->Disable();
			spec_cos_edit->Enable();
			spec_cos_spin->Enable();
		}
		mSpecular->Enable();
		mSpecGloss->Enable();
		spec_mult_edit->Enable();
		spec_ior_edit->Enable();
		spec_mult_spin->Enable();
		spec_ior_spin->Enable();
		//CheckRadioButton(thishWnd, IDC_SPEC_GL_RAD, IDC_SPEC_COS_RAD, IDC_SPEC_GL_RAD);
	}
}

INT_PTR hydraMaterialDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{
  thishWnd=hWnd;
	hydraMaterialDlgProc *dlg = DLGetWindowLongPtr<hydraMaterialDlgProc*>(hWnd);

	if (!dlg && msg != WM_INITDIALOG) return FALSE;

	switch (msg)
	{
		case WM_INITDIALOG:
		{
			//dlg = (hydraMaterialDlgProc *)lParam;
			//DLSetWindowLongPtr(hWnd, dlg);

			if (!spec_gloss_edit) spec_gloss_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SPECULAR_GL));
			if (!spec_cos_edit) spec_cos_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SPECULAR_COS));
			if (!spec_mult_edit) spec_mult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SPECULAR_MULT));
			if (!spec_ior_edit) spec_ior_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SPECULAR_IOR));
			if (!refl_gloss_edit) refl_gloss_edit = GetICustEdit(GetDlgItem(hWnd, IDC_REFL_GL));
			if (!refl_cos_edit) refl_cos_edit = GetICustEdit(GetDlgItem(hWnd, IDC_REFLECT_COS));
			if (!transp_gloss_edit) transp_gloss_edit = GetICustEdit(GetDlgItem(hWnd, IDC_TRANSP_GL));
			if (!transp_cos_edit) transp_cos_edit = GetICustEdit(GetDlgItem(hWnd, IDC_TRANSP_COS));
			if (!bump_amt_edit) bump_amt_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BUMP_AMT));
			if (!diffuse_roughness_edit) diffuse_roughness_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFUSE_ROUGHNESS));

			if (!spec_gloss_spin) spec_gloss_spin = GetISpinner(GetDlgItem(hWnd, IDC_SPECULAR_GL_SPINNER));
			if (!spec_cos_spin) spec_cos_spin = GetISpinner(GetDlgItem(hWnd, IDC_SPECULAR_COS_SPINNER));
			if (!spec_mult_spin) spec_mult_spin = GetISpinner(GetDlgItem(hWnd, IDC_SPECULAR_MULT_SPINNER));
			if (!spec_ior_spin) spec_ior_spin = GetISpinner(GetDlgItem(hWnd, IDC_SPECULAR_IOR_SPINNER));
			if (!refl_gloss_spin) refl_gloss_spin = GetISpinner(GetDlgItem(hWnd, IDC_REFL_GL_SPINNER));
			if (!refl_cos_spin) refl_cos_spin = GetISpinner(GetDlgItem(hWnd, IDC_REFLECT_COS_SPINNER));
			if (!transp_gloss_spin) transp_gloss_spin = GetISpinner(GetDlgItem(hWnd, IDC_TRANSP_GL_SPINNER));
			if (!transp_cos_spin) transp_cos_spin = GetISpinner(GetDlgItem(hWnd, IDC_TRANSP_COS_SPINNER));
			if (!bump_amt_spin) bump_amt_spin = GetISpinner(GetDlgItem(hWnd, IDC_BUMP_AMT_SPINNER));
			if (!diffuse_roughness_spin) diffuse_roughness_spin = GetISpinner(GetDlgItem(hWnd, IDC_DIFFUSE_ROUGHNESS_SPINNER));

			if (!mEmission) mEmission = GetICustButton(GetDlgItem(hWnd, IDC_EMISSION_MAP));
			if (!mDiffuse) mDiffuse = GetICustButton(GetDlgItem(hWnd, IDC_DIFFUSE_MAP));
			if (!mSpecular) mSpecular = GetICustButton(GetDlgItem(hWnd, IDC_SPECULAR_MAP));
			if (!mReflect) mReflect = GetICustButton(GetDlgItem(hWnd, IDC_REFLECT_MAP));
			if (!mTransparency) mTransparency = GetICustButton(GetDlgItem(hWnd, IDC_TRANSP_MAP));
			if (!mNormal) mNormal = GetICustButton(GetDlgItem(hWnd, IDC_NORMAL_MAP));
			if (!mSpecGloss) mSpecGloss = GetICustButton(GetDlgItem(hWnd, IDC_SPEC_GL_MAP));
			if (!mReflectGloss) mReflectGloss = GetICustButton(GetDlgItem(hWnd, IDC_REFL_GL_MAP));
			if (!mTranspGloss) mTranspGloss = GetICustButton(GetDlgItem(hWnd, IDC_TRANSP_GL_MAP));
			if (!mOpacity) mOpacity = GetICustButton(GetDlgItem(hWnd, IDC_OPACITY_MAP));
			if (!mTranslucency) mTranslucency = GetICustButton(GetDlgItem(hWnd, IDC_TRANSLUCENCY_MAP));

			spec_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_SPECULAR_COLOR));

			DragAcceptFiles(GetDlgItem(hWnd, IDC_EMISSION_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_DIFFUSE_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_REFLECT_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSP_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_NORMAL_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_SPEC_GL_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_REFL_GL_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSP_GL_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_OPACITY_MAP), true);
			DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSLUCENCY_MAP), true);


			//bumpAmountLock();
			//texBtnText();
			return TRUE;
			//break;
		}
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_EMISSION_MAP:
				case IDC_DIFFUSE_MAP:
				case IDC_SPECULAR_MAP:
				case IDC_REFLECT_MAP:
				case IDC_TRANSP_MAP:
				case IDC_SPEC_GL_MAP:
				case IDC_REFL_GL_MAP:
				case IDC_TRANSP_GL_MAP:
				case IDC_OPACITY_MAP:
				case IDC_TRANSLUCENCY_MAP:
					if (HIWORD(wParam)==BN_BUTTONUP || HIWORD(wParam)==BN_RIGHTCLICK || HIWORD(wParam)==BN_BUTTONDOWN) 
					{
						//texBtnText();
					}
					break;
				case IDC_NORMAL_MAP:
					if (HIWORD(wParam)==BN_BUTTONUP || HIWORD(wParam)==BN_RIGHTCLICK || HIWORD(wParam)==BN_BUTTONDOWN) 
					{
						//bumpAmountLock();
						//texBtnText();
					}
					break;
				case IDC_SPEC_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
				//		CheckRadioButton(hWnd, IDC_SPEC_GL_RAD, IDC_SPEC_COS_RAD, IDC_SPEC_GL_RAD);
					/*	spec_gloss_edit->Enable();
						spec_gloss_spin->Enable();
						spec_cos_edit->Disable();
						spec_cos_spin->Disable();*/
					}
					break;
				case IDC_SPEC_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					//	CheckRadioButton(hWnd, IDC_SPEC_GL_RAD, IDC_SPEC_COS_RAD, IDC_SPEC_COS_RAD);
					/*	spec_gloss_edit->Disable();
						spec_gloss_spin->Disable();
						spec_cos_edit->Enable();
						spec_cos_spin->Enable();*/
					}
					break;
				case IDC_REFL_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					//	CheckRadioButton(hWnd, IDC_REFL_GL_RAD, IDC_REFL_COS_RAD, IDC_REFL_GL_RAD);
					/*	refl_gloss_edit->Enable();
						refl_gloss_spin->Enable();
						refl_cos_edit->Disable();
						refl_cos_spin->Disable();*/
					}
					break;
				case IDC_REFL_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
					//	CheckRadioButton(hWnd, IDC_REFL_GL_RAD, IDC_REFL_COS_RAD, IDC_REFL_COS_RAD);
				/*		refl_gloss_edit->Disable();
						refl_gloss_spin->Disable();
						refl_cos_edit->Enable();
						refl_cos_spin->Enable();*/
					}
					break;
				case IDC_TRANSP_GL_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
				//		CheckRadioButton(hWnd, IDC_TRANSP_GL_RAD, IDC_TRANSP_COS_RAD, IDC_TRANSP_GL_RAD);
					/*	transp_gloss_edit->Enable();
						transp_gloss_spin->Enable();
						transp_cos_edit->Disable();
						transp_cos_spin->Disable();*/
					}
					break;
				case IDC_TRANSP_COS_RAD:
					if(HIWORD(wParam)== BN_CLICKED)
					{
			//			CheckRadioButton(hWnd, IDC_TRANSP_GL_RAD, IDC_TRANSP_COS_RAD, IDC_TRANSP_COS_RAD);
					/*	transp_gloss_edit->Disable();
						transp_gloss_spin->Disable();
						transp_cos_edit->Enable();
						transp_cos_spin->Enable();*/
					}
					break;
				case IDC_LOCK_SPECULAR:
					if(HIWORD(wParam)== BN_CLICKED)
					{
				//		lockSpec(IsDlgButtonChecked(hWnd, IDC_LOCK_SPECULAR));
					}
					break;
			}
		break;	
		case WM_DESTROY:
				ReleaseICustButton(mEmission);
				ReleaseICustButton(mDiffuse);
				ReleaseICustButton(mSpecular);
				ReleaseICustButton(mReflect);
				ReleaseICustButton(mTransparency);
				ReleaseICustButton(mNormal);
				ReleaseICustButton(mSpecGloss);
				ReleaseICustButton(mReflectGloss);
				ReleaseICustButton(mTranspGloss);
				ReleaseICustButton(mOpacity);
				ReleaseICustButton(mTranslucency);

				ReleaseISpinner(spec_gloss_spin);
				ReleaseISpinner(spec_cos_spin);
				ReleaseISpinner(spec_mult_spin);
				ReleaseISpinner(spec_ior_spin);
				ReleaseISpinner(refl_gloss_spin);
				ReleaseISpinner(refl_cos_spin);
				ReleaseISpinner(transp_gloss_spin);
				ReleaseISpinner(transp_cos_spin);
				ReleaseISpinner(bump_amt_spin);
        ReleaseISpinner(diffuse_roughness_spin);

				ReleaseICustEdit(spec_gloss_edit);
				ReleaseICustEdit(spec_cos_edit);
				ReleaseICustEdit(spec_mult_edit);
				ReleaseICustEdit(spec_ior_edit);
				ReleaseICustEdit(refl_gloss_edit);
				ReleaseICustEdit(refl_cos_edit);
				ReleaseICustEdit(transp_gloss_edit);
				ReleaseICustEdit(transp_cos_edit);
				ReleaseICustEdit(bump_amt_edit);
        ReleaseICustEdit(diffuse_roughness_edit);

				ReleaseIColorSwatch(spec_color);

        break;  
		case WM_DROPFILES:
				POINT pt;
				WORD numFiles;
				HWND dropTarget;
				int dropTargetID;

				hydraChar lpszFile[80];

				DragQueryPoint((HDROP)wParam, &pt);

#ifdef MAX2012
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPSTR)NULL, 0);
#else
				numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);
#endif


				if (numFiles == 0)
				{
					DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
					dropTarget = RealChildWindowFromPoint(hWnd, pt);
					dropTargetID = GetDlgCtrlID(dropTarget);
					DropFileInMapSlot(dropTargetID, lpszFile);
				}

				DragFinish((HDROP)wParam);

				break;
		default:
			return FALSE;
			//texBtnText();
			//lockSpec(IsDlgButtonChecked(hWnd, IDC_LOCK_SPECULAR));
	}
	return TRUE; 
}


hydraMaterial::hydraMaterial(BOOL loading) 
{
 // for (int i=0; i<NSUBMTL; i++) submtl[i] = NULL;
  for (int i=NSUBMTL; i<NSUBTEX+NSUBMTL; i++) subTex[i] = NULL;
  pblock = NULL;
	//hydramaterial_param_blk.SetUserDlgProc


	diffuse_color = Color(0.8, 0.8, 0.8);
	specular_color = Color(1.0, 1.0, 1.0);
	reflect_color = Color(1.0, 1.0, 1.0);
	emission_color = Color(1.0, 1.0, 1.0);
	transparency_color = Color(1.0, 1.0, 1.0);
	fog_color = Color(1.0, 1.0, 1.0);
	exit_color = Color(1.0, 1.0, 1.0);
	translucency_color = Color(1.0, 1.0, 1.0);
  
	diffuse_tint_on = false;
	reflect_tint_on = false;
	specular_fresnel_on = true;
	reflect_fresnel_on = true;
	lock_specular = true;
	specular_tint_on = false;
	transparency_tint_on = false;
  transparency_thin_on = false;
	no_ic_records = false;
	affect_shadows_on = true;
	opacity_smooth = false;
	translucency_tint_on = false;
	emission_tint_on = false;
	emission_gi = true;
  displacement_on = false;
  invertHeightOn = false;

  specular_roughness = 0.0f;
  reflect_roughness = 0.0f;
  reflect_cospower = 0.0f;
  ior = 1.5f;
	specular_ior = 0.0f;
	reflect_ior = 0.0f;
  transparency_cospower = 0.0f;
	specular_cospower = 0.0f;
  fog_multiplier = 0.0f;
  displacement_height = 0.0f;
  diffuse_mult = 1.0f;
  specular_mult = 0.0f;
  emission_mult = 0.0f;
  transparency_mult = 0.0f;
  reflect_mult = 0.0f;
	specular_gloss = 0.0f;
	reflect_gloss = 0.0f;
	transp_gloss = 0.0f;
	bump_amount = 0.0f;
	bump_radius = 1.0f;
	bump_sigma = 1.5f;
	translucency_mult = 0.0f;
  diffuse_roughness = 0.0f;

  specular_brdf = 0;
  reflect_brdf = 0;
	specular_gloss_or_cos = 0;
	reflect_gloss_or_cos = 0;
	transp_gloss_or_cos = 0;
	reflect_extrusion = EXTRUSION_STRONG;

  if (!loading) 
    Reset();
}


void hydraMaterial::Reset() 
{
  ivalid.SetEmpty();
  for (int i=0; i<NSUBMTL; i++) 
  {
   /* if( submtl[i] ){ 
      DeleteReference(i);
      submtl[i] = NULL;
    }*/
  }

  for (int i=NSUBMTL; i<NSUBTEX+NSUBMTL; i++) 
  {
    if( subTex[i] )
		{ 
      DeleteReference(i);
      subTex[i] = NULL;
    }
  }

  GethydraMaterialDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* hydraMaterial::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
  IAutoMParamDlg* masterDlg = GethydraMaterialDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  
/*  hBitmap = reinterpret_cast<HBITMAP>(LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(IDB_BITMAP1),IMAGE_BITMAP,0,0,0));

  SendDlgItemMessage(hwMtlEdit,IDC_LOGO,STM_SETIMAGE,IMAGE_BITMAP,reinterpret_cast<LPARAM>(hBitmap));*/

  // TODO: Set param block user dialog if necessary
	hydramaterial_param_blk.SetUserDlgProc(new hydraMaterialDlgProc(this));
  return masterDlg;
}

BOOL hydraMaterial::SetDlgThing(ParamDlg* dlg)
{
  return FALSE;
}

Interval hydraMaterial::Validity(TimeValue t)
{
  Interval valid = FOREVER;		

  for (int i=0; i<NSUBMTL; i++) 
  {
   /* if (submtl[i]) 
      valid &= submtl[i]->Validity(t);*/
  }

  for (int i=NSUBMTL; i<NSUBMTL+NSUBTEX; i++) 
  {
		if (subTex[i]) 
      valid &= subTex[i]->Validity(t);
  }
  
  /*float u;
  pblock->GetValue(pb_spin,t,u,valid);*/
  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle hydraMaterial::GetReference(int i) 
{
	
	/*if (i < NSUBMTL )
		return submtl[i];
	else*/ 
	if (i < NSUBTEX+NSUBMTL )
		return subTex[i];
	else 
    return pblock;
	
}

void hydraMaterial::SetReference(int i, RefTargetHandle rtarg) 
{

	/* if (i < NSUBMTL)
		submtl[i] = (Mtl *)rtarg; 
	else*/ 
	int n = NSUBTEX + NSUBMTL;

	MSTR s = L"";
	if(rtarg)
		rtarg->GetClassName(s);

  if (i < n && s != L"ParamBlock2")
  {
    subTex[i] = (Texmap *)rtarg;
  }
  else
  {
    pblock = (IParamBlock2 *)rtarg;
  }
	
}

TSTR hydraMaterial::SubAnimName(int i) 
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else return TSTR(_T(""));
}

Animatable* hydraMaterial::SubAnim(int i) {
 /* if (i < NSUBMTL)
    return submtl[i]; 
  else*/ if (i < NSUBTEX+NSUBMTL )
    return subTex[i];
  else return pblock;
  }


#ifdef MAX2014
RefResult hydraMaterial::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();
		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydramaterial_param_blk.InvalidateUI(changing_param);
		}
		break;

	}
	return REF_SUCCEED;
}
#else
RefResult hydraMaterial::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
	switch (message) {
	case REFMSG_CHANGE:
		ivalid.SetEmpty();

  /*  IParamBlock2* cpb = NULL;
    ParamID changing_param = GethydraMaterialDesc()->LastNotifyParamID(this, cpb);
    if (changing_param != -1)
    {
      cpb->GetDesc()->InvalidateUI(changing_param);
    }
    else
    {
      GethydraMaterialDesc()->InvalidateUI();
    }*/
 

		if (hTarget == pblock)
		{
			ParamID changing_param = pblock->LastNotifyParamID();
			hydramaterial_param_blk.InvalidateUI(changing_param);
		}
    break;
	}
	return REF_SUCCEED;
}
#endif


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* hydraMaterial::GetSubMtl(int i)
{
  /*if (i < NSUBMTL )
    return submtl[i];*/
  return NULL;
}

void hydraMaterial::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i,m);
  // TODO: Set the material and update the UI	
}

TSTR hydraMaterial::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  return _T(""); 
}

TSTR hydraMaterial::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}

/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* hydraMaterial::GetSubTexmap(int i)
{
   return subTex[i];
}

void hydraMaterial::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i,m);
  
  switch(i)
  {
  case 0:
    hydramaterial_param_blk.InvalidateUI(mtl_diffuse_map);
    pblock->SetValue(mtl_diffuse_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  case 1:
    hydramaterial_param_blk.InvalidateUI(mtl_specular_map);
    pblock->SetValue(mtl_specular_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  case 2:
    hydramaterial_param_blk.InvalidateUI(mtl_reflect_map);
    pblock->SetValue(mtl_reflect_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  case 3:
    hydramaterial_param_blk.InvalidateUI(mtl_emission_map);
    pblock->SetValue(mtl_emission_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  case 4:
    hydramaterial_param_blk.InvalidateUI(mtl_transparency_map);
    pblock->SetValue(mtl_transparency_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
  case 5:
    hydramaterial_param_blk.InvalidateUI(mtl_normal_map);
    pblock->SetValue(mtl_normal_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
	case 6:
    hydramaterial_param_blk.InvalidateUI(mtl_spec_gl_map);
    pblock->SetValue(mtl_spec_gl_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
	case 7:
    hydramaterial_param_blk.InvalidateUI(mtl_refl_gl_map);
    pblock->SetValue(mtl_refl_gl_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
	case 8:
    hydramaterial_param_blk.InvalidateUI(mtl_transp_gl_map);
    pblock->SetValue(mtl_transp_gl_map, TimeValue(0), m);
		ivalid.SetEmpty();
    break;
	case 9:
		hydramaterial_param_blk.InvalidateUI(mtl_opacity_map);
    pblock->SetValue(mtl_opacity_map, TimeValue(0), m);
		ivalid.SetEmpty();
		break;
	case 10:
		hydramaterial_param_blk.InvalidateUI(mtl_translucency_map);
    pblock->SetValue(mtl_translucency_map, TimeValue(0), m);
		ivalid.SetEmpty();
		break;
  default:
    break;
  }

  NotifyChanged();
}

TSTR hydraMaterial::GetSubTexmapSlotName(int i)
{
  switch(i) 
  {
		case 0:  return GetString(IDS_DIFFUSE_MAP); 
		case 1:  return GetString(IDS_SPECULAR_MAP); 
    case 2:  return GetString(IDS_REFLECT_MAP);
    case 3:  return GetString(IDS_EMISSION_MAP); 
    case 4:  return GetString(IDS_TRANSP_MAP);
    case 5:  return GetString(IDS_NORMAL_MAP); 
		case 6:  return GetString(IDS_SPEC_GLOSSINESS_MAP); 
		case 7:  return GetString(IDS_REFL_GLOSSINESS_MAP); 
		case 8:  return GetString(IDS_TRANSP_GLOSSINESS_MAP);
		case 9:  return GetString(IDS_OPACITY_MAP);
		case 10:  return GetString(IDS_TRANSLUCENCY_MAP);
		default: return _T("");
	}
}

TSTR hydraMaterial::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult hydraMaterial::Save(ISave *isave) { 
  IOResult res;
	ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res!=IO_OK) return res;
  isave->EndChunk();

	isave->BeginChunk(HYDRA_CHUNK);
	isave->Write(&diffuse_color, sizeof(Color), &nb);
	isave->Write(&specular_color, sizeof(Color), &nb);
	isave->Write(&reflect_color, sizeof(Color), &nb);
	isave->Write(&emission_color, sizeof(Color), &nb);
	isave->Write(&transparency_color, sizeof(Color), &nb);
	isave->Write(&fog_color, sizeof(Color), &nb);
	isave->Write(&exit_color, sizeof(Color), &nb);
	isave->Write(&translucency_color, sizeof(Color), &nb);

	isave->Write(&displacement_on, sizeof(bool), &nb);
	isave->Write(&invertHeightOn, sizeof(bool), &nb);
	isave->Write(&diffuse_tint_on, sizeof(bool), &nb);
	isave->Write(&specular_tint_on, sizeof(bool), &nb);
	isave->Write(&emission_tint_on, sizeof(bool), &nb);
	isave->Write(&transparency_tint_on, sizeof(bool), &nb);
	isave->Write(&reflect_tint_on, sizeof(bool), &nb);
	isave->Write(&transparency_thin_on, sizeof(bool), &nb);
	isave->Write(&affect_shadows_on, sizeof(bool), &nb);
	isave->Write(&no_ic_records, sizeof(bool), &nb);
	isave->Write(&specular_fresnel_on, sizeof(bool), &nb);
	isave->Write(&reflect_fresnel_on, sizeof(bool), &nb);
	isave->Write(&lock_specular, sizeof(bool), &nb);
	isave->Write(&emission_gi, sizeof(bool), &nb);
	isave->Write(&opacity_smooth, sizeof(bool), &nb);
	isave->Write(&translucency_tint_on, sizeof(bool), &nb);

	isave->Write(&specular_roughness, sizeof(float), &nb);
	isave->Write(&reflect_roughness, sizeof(float), &nb);
	isave->Write(&reflect_cospower, sizeof(float), &nb);
	isave->Write(&specular_cospower, sizeof(float), &nb);
	isave->Write(&ior, sizeof(float), &nb);
	isave->Write(&specular_ior, sizeof(float), &nb);
	isave->Write(&reflect_ior, sizeof(float), &nb);
	isave->Write(&transparency_cospower, sizeof(float), &nb);
	isave->Write(&fog_multiplier, sizeof(float), &nb);
	isave->Write(&displacement_height, sizeof(float), &nb);
	isave->Write(&diffuse_mult, sizeof(float), &nb);
	isave->Write(&specular_mult, sizeof(float), &nb);
	isave->Write(&emission_mult, sizeof(float), &nb);
	isave->Write(&transparency_mult, sizeof(float), &nb);
	isave->Write(&reflect_mult, sizeof(float), &nb);
	isave->Write(&specular_gloss, sizeof(float), &nb);
	isave->Write(&reflect_gloss, sizeof(float), &nb);
	isave->Write(&transp_gloss, sizeof(float), &nb);
	isave->Write(&bump_amount, sizeof(float), &nb);
	isave->Write(&bump_radius, sizeof(float), &nb);
	isave->Write(&bump_sigma, sizeof(float), &nb);
	isave->Write(&translucency_mult, sizeof(float), &nb);
  isave->Write(&diffuse_roughness, sizeof(float), &nb);

	isave->Write(&specular_brdf, sizeof(int), &nb);
	isave->Write(&reflect_brdf, sizeof(int), &nb);
	isave->Write(&specular_gloss_or_cos, sizeof(int), &nb);
	isave->Write(&reflect_gloss_or_cos, sizeof(int), &nb);
	isave->Write(&transp_gloss_or_cos, sizeof(int), &nb);
	isave->Write(&reflect_extrusion, sizeof(int), &nb);
	isave->EndChunk();


  return IO_OK;
  }	

IOResult hydraMaterial::Load(ILoad *iload) { 
  IOResult res;
  int id;
	ULONG nb;
  while (IO_OK==(res=iload->OpenChunk())) 
	{
    switch(id = iload->CurChunkID())  
		{
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
			case HYDRA_CHUNK:
				res = iload->Read(&diffuse_color, sizeof(Color), &nb);
				res = iload->Read(&specular_color, sizeof(Color), &nb);
				res = iload->Read(&reflect_color, sizeof(Color), &nb);
				res = iload->Read(&emission_color, sizeof(Color), &nb);
				res = iload->Read(&transparency_color, sizeof(Color), &nb);
				res = iload->Read(&fog_color, sizeof(Color), &nb);
				res = iload->Read(&exit_color, sizeof(Color), &nb);
				res = iload->Read(&translucency_color, sizeof(Color), &nb);

				res = iload->Read(&displacement_on, sizeof(bool), &nb);
				res = iload->Read(&invertHeightOn, sizeof(bool), &nb);
				res = iload->Read(&diffuse_tint_on, sizeof(bool), &nb);
				res = iload->Read(&specular_tint_on, sizeof(bool), &nb);
				res = iload->Read(&emission_tint_on, sizeof(bool), &nb);
				res = iload->Read(&transparency_tint_on, sizeof(bool), &nb);
				res = iload->Read(&reflect_tint_on, sizeof(bool), &nb);
				res = iload->Read(&transparency_thin_on, sizeof(bool), &nb);
				res = iload->Read(&affect_shadows_on, sizeof(bool), &nb);
				res = iload->Read(&no_ic_records, sizeof(bool), &nb);
				res = iload->Read(&specular_fresnel_on, sizeof(bool), &nb);
				res = iload->Read(&reflect_fresnel_on, sizeof(bool), &nb);
				res = iload->Read(&lock_specular, sizeof(bool), &nb);
				res = iload->Read(&emission_gi, sizeof(bool), &nb);
				res = iload->Read(&opacity_smooth, sizeof(bool), &nb);
				res = iload->Read(&translucency_tint_on, sizeof(bool), &nb);

				res = iload->Read(&specular_roughness, sizeof(float), &nb);
				res = iload->Read(&reflect_roughness, sizeof(float), &nb);
				res = iload->Read(&reflect_cospower, sizeof(float), &nb);
				res = iload->Read(&specular_cospower, sizeof(float), &nb);
				res = iload->Read(&ior, sizeof(float), &nb);
				res = iload->Read(&specular_ior, sizeof(float), &nb);
				res = iload->Read(&reflect_ior, sizeof(float), &nb);
				res = iload->Read(&transparency_cospower, sizeof(float), &nb);
				res = iload->Read(&fog_multiplier, sizeof(float), &nb);
				res = iload->Read(&displacement_height, sizeof(float), &nb);
				res = iload->Read(&diffuse_mult, sizeof(float), &nb);
				res = iload->Read(&specular_mult, sizeof(float), &nb);
				res = iload->Read(&emission_mult, sizeof(float), &nb);
				res = iload->Read(&transparency_mult, sizeof(float), &nb);
				res = iload->Read(&reflect_mult, sizeof(float), &nb);
				res = iload->Read(&specular_gloss, sizeof(float), &nb);
				res = iload->Read(&reflect_gloss, sizeof(float), &nb);
				res = iload->Read(&transp_gloss, sizeof(float), &nb);
				res = iload->Read(&bump_amount, sizeof(float), &nb);
				res = iload->Read(&bump_radius, sizeof(float), &nb);
				res = iload->Read(&bump_sigma, sizeof(float), &nb);
				res = iload->Read(&translucency_mult, sizeof(float), &nb);
        res = iload->Read(&diffuse_roughness, sizeof(float), &nb);

				res = iload->Read(&specular_brdf, sizeof(int), &nb);
				res = iload->Read(&reflect_brdf, sizeof(int), &nb);
				res = iload->Read(&specular_gloss_or_cos, sizeof(int), &nb);
				res = iload->Read(&reflect_gloss_or_cos, sizeof(int), &nb);
				res = iload->Read(&transp_gloss_or_cos, sizeof(int), &nb);
				res = iload->Read(&reflect_extrusion, sizeof(int), &nb);
				break;
    }
    iload->CloseChunk();
    if (res!=IO_OK) 
      return res;
  }

  return IO_OK;
  }


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle hydraMaterial::Clone(RemapDir &remap) {
  hydraMaterial *mnew = new hydraMaterial(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this); 
  mnew->ReplaceReference(NSUBMTL,remap.CloneRef(pblock));

  mnew->diffuse_color = diffuse_color;
  mnew->specular_color = specular_color;
  mnew->reflect_color = reflect_color;
  mnew->emission_color  = emission_color;
  mnew->transparency_color = transparency_color;
  mnew->fog_color = fog_color;
  mnew->exit_color = exit_color;
	mnew->translucency_color = translucency_color;

  mnew->displacement_on = displacement_on;
  mnew->invertHeightOn = invertHeightOn;
  mnew->diffuse_tint_on = diffuse_tint_on;
  mnew->specular_tint_on = specular_tint_on;
  mnew->emission_tint_on = emission_tint_on;
  mnew->transparency_tint_on = transparency_tint_on;
  mnew->reflect_tint_on = reflect_tint_on;
  mnew->transparency_thin_on = transparency_thin_on;
	mnew->affect_shadows_on = affect_shadows_on;
	mnew->no_ic_records = no_ic_records;
	mnew->specular_fresnel_on = specular_fresnel_on;
	mnew->reflect_fresnel_on = reflect_fresnel_on;
	mnew->lock_specular = lock_specular;
	mnew->emission_gi = emission_gi;
	mnew->opacity_smooth = opacity_smooth;

  mnew->specular_roughness = specular_roughness;
  mnew->reflect_roughness = reflect_roughness;
  mnew->reflect_cospower = reflect_cospower;
  mnew->ior = ior;
	mnew->specular_ior = specular_ior;
	mnew->reflect_ior = reflect_ior;
  mnew->transparency_cospower = transparency_cospower;
	mnew->specular_cospower = specular_cospower;
  mnew->fog_multiplier = fog_multiplier;
  mnew->displacement_height = displacement_height;
  mnew->diffuse_mult = diffuse_mult;
  mnew->specular_mult = specular_mult;
  mnew->emission_mult = emission_mult;
  mnew->transparency_mult = transparency_mult;
  mnew->reflect_mult = reflect_mult;
	mnew->specular_gloss = specular_gloss; 
	mnew->reflect_gloss = reflect_gloss;
	mnew->transp_gloss = transp_gloss;
	mnew->bump_amount = bump_amount;
	mnew->bump_radius = bump_radius;
	mnew->bump_sigma = bump_sigma;
	mnew->translucency_mult = translucency_mult;
	mnew->translucency_tint_on = translucency_tint_on;
  mnew->diffuse_roughness = diffuse_roughness;

  mnew->specular_brdf = specular_brdf;
  mnew->reflect_brdf = reflect_brdf;
	mnew->specular_gloss_or_cos = specular_gloss_or_cos;
	mnew->reflect_gloss_or_cos = reflect_gloss_or_cos;
	mnew->transp_gloss_or_cos = transp_gloss_or_cos;
	mnew->reflect_extrusion = reflect_extrusion;

  mnew->ivalid.SetEmpty();	
  for (int i = 0; i<NSUBMTL; i++) {
    /*mnew->submtl[i] = NULL;
    if (submtl[i])
      mnew->ReplaceReference(i,remap.CloneRef(submtl[i]));
      mnew->mapOn[i] = mapOn[i];*/
    }

  for (int i = 0; i<NSUBTEX; i++) 
  {
		mnew->subTex[i] = NULL;
		if (subTex[i])
			mnew->ReplaceReference(i,remap.CloneRef(subTex[i]));
	}

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
  }

void hydraMaterial::NotifyChanged() 
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void hydraMaterial::Update(TimeValue t, Interval& valid) 
{	
  if (!ivalid.InInterval(t)) 
  {
    ivalid.SetInfinite();
    pblock->GetValue( mtl_mat1_on, t, affect_shadows_on, ivalid);
    //pblock->GetValue( pb_spin, t, spin, ivalid);

    pblock->GetValue( mtl_diffuse_color, t, diffuse_color, ivalid);
    pblock->GetValue( mtl_diffuse_tint_on, t, diffuse_tint_on, ivalid);
    pblock->GetValue( mtl_diffuse_mult, t, diffuse_mult, ivalid);
		pblock->GetValue( mtl_no_ic_records, t, no_ic_records, ivalid);
    pblock->GetValue( mtl_roughness_mult, t, diffuse_roughness, ivalid);

		pblock->GetValue( mtl_reflect_color, t, reflect_color, ivalid);
    pblock->GetValue( mtl_reflect_tint_on, t, reflect_tint_on, ivalid);
    pblock->GetValue( mtl_reflect_mult, t, reflect_mult, ivalid);
    pblock->GetValue( mtl_reflect_brdf, t, reflect_brdf, ivalid);
    pblock->GetValue( mtl_reflect_cospower, t, reflect_cospower, ivalid);
    pblock->GetValue( mtl_reflect_roughness, t, reflect_roughness, ivalid);
		pblock->GetValue( mtl_reflect_ior, t, reflect_ior, ivalid);
		pblock->GetValue( mtl_refl_gloss, t, reflect_gloss, ivalid);
		pblock->GetValue( mtl_refl_gloss_or_cos, t, reflect_gloss_or_cos, ivalid);
		pblock->GetValue( mtl_refl_fresnel_on, t, reflect_fresnel_on, ivalid);
		pblock->GetValue( mtl_reflect_extrusion, t, reflect_extrusion, ivalid);

		pblock->GetValue( mtl_lock_specular, t, lock_specular, ivalid);
		
		if(lock_specular)
		{
			specular_color = reflect_color;
			specular_tint_on = reflect_tint_on;
			specular_mult = reflect_mult;
			specular_brdf = reflect_brdf;
			specular_roughness = reflect_roughness;
			specular_cospower = reflect_cospower;
			specular_ior = reflect_ior;
			specular_gloss = reflect_gloss;
			specular_gloss_or_cos = reflect_gloss_or_cos;
			specular_fresnel_on = reflect_fresnel_on;
			
		/*	pblock->SetValue( mtl_specular_color, t, reflect_color);
			pblock->SetValue( mtl_specular_mult_on, t, reflect_mult_on);
			pblock->SetValue( mtl_specular_mult, t, reflect_mult);
			pblock->SetValue( mtl_specular_brdf, t, reflect_brdf);
			pblock->SetValue( mtl_specular_roughness, t, reflect_roughness);
			pblock->SetValue( mtl_specular_cospower, t, reflect_cospower);
			pblock->SetValue( mtl_specular_ior, t, reflect_ior);
			pblock->SetValue( mtl_spec_gloss, t, reflect_gloss);
			pblock->SetValue( mtl_spec_gloss_or_cos, t, reflect_gloss_or_cos);
			pblock->SetValue( mtl_spec_fresnel_on, t, reflect_fresnel_on);*/

			//SetSubTexmap(1, subTex[2]);
			//SetSubTexmap(6, subTex[7]);
		}
		else
		{
			pblock->GetValue( mtl_specular_color, t, specular_color, ivalid);
			pblock->GetValue( mtl_specular_tint_on, t, specular_tint_on, ivalid);
			pblock->GetValue( mtl_specular_mult, t, specular_mult, ivalid);
			pblock->GetValue( mtl_specular_brdf, t, specular_brdf, ivalid);
			pblock->GetValue( mtl_specular_roughness, t, specular_roughness, ivalid);
			pblock->GetValue( mtl_specular_cospower, t, specular_cospower, ivalid);
			pblock->GetValue( mtl_specular_ior, t, specular_ior, ivalid);
			pblock->GetValue( mtl_spec_gloss, t, specular_gloss, ivalid);
			pblock->GetValue( mtl_spec_gloss_or_cos, t, specular_gloss_or_cos, ivalid);
			pblock->GetValue( mtl_spec_fresnel_on, t, specular_fresnel_on, ivalid);
		}

    pblock->GetValue( mtl_emission_color, t, emission_color, ivalid);
    pblock->GetValue( mtl_emission_tint_on, t, emission_tint_on, ivalid);
    pblock->GetValue( mtl_emission_mult, t, emission_mult, ivalid);
		pblock->GetValue( mtl_emission_gi, t, emission_gi, ivalid);

    pblock->GetValue( mtl_transparency_color, t, transparency_color, ivalid);
    pblock->GetValue( mtl_transparency_tint_on, t, transparency_tint_on, ivalid);
    pblock->GetValue( mtl_transparency_mult, t, transparency_mult, ivalid);
    pblock->GetValue( mtl_transparency_thin_on, t, transparency_thin_on, ivalid);
    pblock->GetValue( mtl_transparency_cospower, t, transparency_cospower, ivalid);
    pblock->GetValue( mtl_ior, t, ior, ivalid);
    pblock->GetValue( mtl_fog_color, t, fog_color, ivalid);
    pblock->GetValue( mtl_fog_multiplier, t, fog_multiplier, ivalid);
    pblock->GetValue( mtl_exit_color, t, exit_color, ivalid);
		pblock->GetValue( mtl_transp_gloss, t, transp_gloss, ivalid);
		pblock->GetValue( mtl_transp_gloss_or_cos, t, transp_gloss_or_cos, ivalid);
		pblock->GetValue(mtl_opacity_smooth, t, opacity_smooth, ivalid);

		pblock->GetValue(mtl_translucency_color, t, translucency_color, ivalid);
		pblock->GetValue(mtl_translucency_mult, t, translucency_mult, ivalid);
		pblock->GetValue(mtl_translucency_tint_on, t, translucency_tint_on, ivalid);
		
		pblock->GetValue( mtl_bump_amount, t, bump_amount, ivalid);
    pblock->GetValue( mtl_displacement_on, t, displacement_on, ivalid);
    pblock->GetValue( mtl_displacement_height, t, displacement_height, ivalid);
    pblock->GetValue( mtl_displacement_invert_height_on, t, invertHeightOn, ivalid);
		pblock->GetValue( mtl_bump_radius, t, bump_radius, ivalid);
		pblock->GetValue( mtl_bump_sigma, t, bump_sigma, ivalid);

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (int i=0; i<NSUBMTL; i++) 
    {
      /*if (submtl[i]) 
        submtl[i]->Update(t,ivalid);*/
    }

    for (int i=0; i<NSUBTEX; i++) 
    {
      if (subTex[i])
      {
        subTex[i]->Update(t, ivalid);
       // subTex[i]->NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
      }
		}

  }
  valid &= ivalid;
  //NotifyChanged();
  //ExportMaterialXML();
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void hydraMaterial::SetAmbient(Color c, TimeValue t) {}		
void hydraMaterial::SetDiffuse(Color c, TimeValue t) 
{
  diffuse_color = c;
  pblock->SetValue( mtl_diffuse_color, t, diffuse_color);
  NotifyChanged();
}		
void hydraMaterial::SetSpecular(Color c, TimeValue t) 
{
  specular_color = c;
  pblock->SetValue( mtl_specular_color, t, specular_color);
  NotifyChanged();
}
void hydraMaterial::SetShininess(float v, TimeValue t) 
{
  //log(reflect_cospower)/log(100000.0)
  //derp
  specular_roughness = v;
  pblock->SetValue( mtl_reflect_roughness, t, reflect_roughness);
  NotifyChanged();
}
        
Color hydraMaterial::GetAmbient(int mtlNum, BOOL backFace)
{
	//return submtl[0]?submtl[0]->GetAmbient(mtlNum,backFace):Color(0,0,0);
	return diffuse_color * diffuse_mult + reflect_color * reflect_mult * 0.01f;
  ;
}

Color hydraMaterial::GetDiffuse(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
  return diffuse_color * diffuse_mult + reflect_color * reflect_mult * 0.01f;
}

Color hydraMaterial::GetSpecular(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetSpecular(mtlNum,backFace):Color(0,0,0);  
	return reflect_color * reflect_mult;
}

Color hydraMaterial::GetSelfIllumColor(int mtlNum, BOOL backFace)
{   
  return emission_color * emission_mult;
}

float hydraMaterial::GetXParency(int mtlNum, BOOL backFace)
{
  if (transparency_mult > 1.0f)
    transparency_mult = 1.0f;
  return transparency_mult * (0.2126f * transparency_color.r + 0.7152f * transparency_color.g + 0.0722f * transparency_color.b) * 0.9f; // 0.9 - for visible obj in voewport with full transparency
}

float hydraMaterial::GetShininess(int mtlNum, BOOL backFace)
{
  return reflect_gloss;  
}

float hydraMaterial::GetShinStr(int mtlNum, BOOL backFace)
{
  return 1.0f;
}

float hydraMaterial::WireSize(int mtlNum, BOOL backFace)
{
  return 1.0f;
}

 


ClassDesc2* GethydraMaterialDesc() 
{ 
  static hydraMaterialClassDesc hydraMaterialDesc;
  return &hydraMaterialDesc; 
}



FPInterface * hydraMaterialClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * hydraMaterialClassDesc::GetEntryName() const
{
  return const_cast<hydraMaterialClassDesc*>(this)->ClassName();
  
}

const MCHAR * hydraMaterialClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * hydraMaterialClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}
