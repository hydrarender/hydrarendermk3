#include "HydraMtl.h"
#include "../../HydraRender mk3/ImportStructs.h"
#include <math.h> 
#include <fstream> 
#include <cstdlib> // for random
#include "Random.h"

void Lerp(Color& inData1, const Color& inData2, float coeff) // 0 - data1, 1 - data2
{
  if (coeff > 1.0f) coeff = 1.0f;
  else if (coeff < 0.0f) coeff = 0.0f;
  inData1 = inData1 + (inData2 - inData1) * coeff;
}

void Lerp(Point3& inData1, const Point3& inData2, float coeff) // 0 - data1, 1 - data2
{
  if (coeff > 1.0f) coeff = 1.0f;
  else if (coeff < 0.0f) coeff = 0.0f;
  inData1 = inData1 + (inData2 - inData1) * coeff;
}

float Luminance(const Color data)
{
  return (0.2126f * data.r + 0.7152f * data.g + 0.0722f * data.b);
}

void Clamp1(float &a)
{
  if (a > 1.0f) a = 1.0f;
}

void ClampMinMax(float &a)
{
  if (a > 1.0f) a = 1.0f;
  else if (a < 0.0f) a = 0.0f;
}


/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/



void hydraMaterial::Shade(ShadeContext& sc) 
{

  if(gbufID) 
    sc.SetGBufferID(gbufID);

  sc.rayLevel = 2;
  
  //if(sc.mode == SCMODE_SHADOW) // In shadow mode, you are just trying to find out what color the shadow is that is falling on an object. In this case, all you care about is transmitted color.
  //{
  //  sc.out.c = transparency_color*fog_color;
  //  return;
  //}

  bool bumped = false;
  Point3 N, L, reflVect;
  Point3 N0 = sc.Normal(); // source normal  

  Color resultDiffuse(0.0f, 0.0f, 0.0f);
  Color resultSpecular(0.0f, 0.0f, 0.0f);  
  Color resultTransluc(0.0f, 0.0f, 0.0f);

  for (int i = 0; i < sc.nLights; i++) // only 1 light work correct
  {
    LightDesc* l = sc.Light(i);

    if(l != nullptr)
    {
      float nDotL = 0.0f;
      float diffCoeff = 0.0f;      

      Color  lightColor(1.0f, 1.0f, 1.0f);
      
      N = N0;

      if(subTex[NORMALMAP_TEX_SLOT] != nullptr && sc.doMaps)   // perturb normal if we have any such and move all vectors to tangent space
      {
        bumped = true;
        
        Point3 tbnBasis[3];
        sc.DPdUVW(tbnBasis);

        Matrix3 tangentTransform(1);

        tangentTransform.SetRow(0, tbnBasis[0]);
        tangentTransform.SetRow(1, tbnBasis[1]);
        tangentTransform.SetRow(2, tbnBasis[2]);
                
        Point3 reliefNormal = subTex[NORMALMAP_TEX_SLOT]->EvalNormalPerturb(sc);

        if (invertHeightOn)
        {        
          reliefNormal.x *= (-1.0f);
          reliefNormal.y *= (-1.0f);
          reliefNormal.z *= (-1.0f);
          reliefNormal.z = abs(reliefNormal.z);
        }      

        Point3 zero(0.0f, 0.0f, 0.0f);
        Lerp(reliefNormal, zero, 1.0f - bump_amount / 5.0f);
        reliefNormal *= 5.0f; // looks like render and standart mat (bump amount 100).
        N = Normalize(sc.Normal() + (sc.backFace ? -reliefNormal : reliefNormal));
        sc.SetNormal(N);        
      }
      
      l->Illuminate(sc, N, lightColor, L, nDotL, diffCoeff);
      reflVect = sc.ReflectVector();
      
      float power = GetShadeReflectionCosPow(sc); 

      float specularFalloff = powf(max(DotProd(L,reflVect), 0.0f), power);

      Color diffColor, specColor, translucColor;

      diffColor = GetShadeDiffuseColor(sc);
      specColor = GetShadeReflectionColor(sc);
      translucColor = GetShadeTranslucencyColor(sc);
      const float translucCoeff = max(translucColor.r, max(translucColor.g, translucColor.b));
      float nDotLMinus = 0.0f;

      if (nDotL < 0.0f)
      {
        nDotLMinus = nDotL;
        nDotL = 0.0f;
      }
      else
        translucColor = { 0, 0, 0 };

      resultDiffuse += (lightColor * diffColor * nDotL);
      resultSpecular += (lightColor * specColor * specularFalloff);
      resultTransluc += (lightColor * translucColor * abs(nDotLMinus));

      Lerp(resultDiffuse, resultTransluc, translucCoeff);
    }
  }

  // add ambient
  resultDiffuse.ClampMin();
  sc.out.c = resultDiffuse + sc.ambientLight * GetShadeDiffuseColor(sc);     
  

  // Mix with background (transparency)
  Color bg, bgt;
  sc.GetBGColor(bg, bgt, false);
  sc.out.t = GetShadeTransparencyColor(sc);    
  Lerp(sc.out.c, (bg * sc.out.t), max(sc.out.t.r, max(sc.out.t.g, sc.out.t.b)));  
  

  // add reflections and specular  
  Color resultReflects;
  Color reflColor = GetShadeReflectionColor(sc);
  
  float glossiness = 1.0f - GetShadeReflectionGloss(sc);

  if (texmapEnv != nullptr)
  {
    resultReflects += resultSpecular;

    // Glossiness    
    reflVect = sc.ReflectVector();

    const int samples = 128 * glossiness + 1;
    for (int i = 0; i < samples; i++)
    {      
      const float rX = (float)(rand() % 1000) / 1000.0f - 0.5f;
      const float rY = (float)(rand() % 1000) / 1000.0f - 0.5f;
      const float rZ = (float)(rand() % 1000) / 1000.0f - 0.5f;

      const Point3 randVect(rX, rY, rZ);
      Point3 reflVectResult = reflVect + randVect * glossiness;

      resultReflects += (sc.EvalEnvironMap(texmapEnv, reflVectResult) * reflColor);
    }
    resultReflects /= (float)samples;
  }
  else   
    resultReflects += resultSpecular;
  

  // Fresnel
  if (reflect_fresnel_on)
  {
    const float VdotN = abs(DotProd(sc.OrigView(), sc.Normal()));

    float bias = (reflect_ior - 1.0f) / 10.0f;
    ClampMinMax(bias);
    bias = pow(bias, 0.5f);

    const float fresnel = pow(1.0f - VdotN, 5.0f) * (1.0f - bias) + bias;

    resultReflects *= fresnel;

    Lerp(sc.out.c, resultReflects, reflect_mult * fresnel);
  }
  else
    Lerp(sc.out.c, resultReflects, reflect_mult);


  // add emission
  sc.out.c += GetShadeEmissiveColor(sc);


  // Mix with background (Opacity)  
  Lerp(sc.out.c, bg, 1.0f - GetShadeOpacity(sc));
    

  if (bumped) sc.SetNormal(N0); // restore normal
}


//-----------------------------------------------------------------------------------


float hydraMaterial::EvalDisplacement(ShadeContext& sc)
{
  if(subTex[NORMALMAP_TEX_SLOT] != nullptr && sc.doMaps)
  {
    TSTR className;
    subTex[NORMALMAP_TEX_SLOT]->GetClassName(className);

    if(className == L"Normal Bump")
    {
      return 0.0f;
    }
    else
    {
      return bump_amount * subTex[NORMALMAP_TEX_SLOT]->EvalMono(sc);
    }
  }

  return 0.0f;
}

Interval hydraMaterial::DisplacementValidity(TimeValue t)
{
  //Interval iv; iv.SetInfinite();
  //return iv;	
  return FOREVER;
}


Color hydraMaterial::GetShadeDiffuseColor(ShadeContext &sc)
{
  Color res;
  if(subTex[DIFFUSE_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[DIFFUSE_TEX_SLOT]->EvalColor(sc);

    if (diffuse_tint_on) 
    {
      res = texColor * diffuse_color*diffuse_mult;
      res.ClampMinMax();
      return res;
    }
    else 
    {
      res = texColor * diffuse_mult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = diffuse_color * diffuse_mult;
    res.ClampMinMax();
    return res;
  }
}

Color hydraMaterial::GetShadeSpecularColor(ShadeContext &sc)
{
  Color res;

  if(subTex[SPECULAR_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[SPECULAR_TEX_SLOT]->EvalColor(sc);

    if (specular_tint_on)
    {
      res = texColor * specular_color * specular_mult;
      res.ClampMinMax();
      return res;

    }
    else
    {
      res = texColor * specular_mult;
      res.ClampMinMax();
      return res;

    }
  }
  else
  {
    res = specular_color * specular_mult;
    res.ClampMinMax();
    return res;
  }
}

Color hydraMaterial::GetShadeReflectionColor(ShadeContext &sc)
{
  Color res;

  if(subTex[REFLECT_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[REFLECT_TEX_SLOT]->EvalColor(sc);

    if (reflect_tint_on)
    {
      res = texColor * reflect_color * reflect_mult;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * reflect_mult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = reflect_color * reflect_mult;
    res.ClampMinMax();
    return res;
  }
}

Color hydraMaterial::GetShadeEmissiveColor(ShadeContext &sc)
{
  if(subTex[EMISSION_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[EMISSION_TEX_SLOT]->EvalColor(sc);

    if(emission_tint_on)
      return texColor * emission_color * emission_mult;
    else
      return texColor * emission_mult;
  }
  else
    return emission_color * emission_mult;
}

Color hydraMaterial::GetShadeTransparencyColor(ShadeContext &sc)
{
  Color fogTransparency(1,1,1);
  Color res;
  if(!transparency_thin_on)
  {
    Lerp(fogTransparency, fog_color, (fog_multiplier * 0.05f));
  }

  if(subTex[TRANSPAR_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor(1,1,1);
    texColor = subTex[TRANSPAR_TEX_SLOT]->EvalColor(sc);

    if (transparency_tint_on)
    {
      res = texColor * transparency_color * transparency_mult * fogTransparency;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * transparency_mult * fogTransparency;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = transparency_color * transparency_mult * fogTransparency;
    res.ClampMinMax();
    return res;
  }
}

float hydraMaterial::GetShadeOpacity(ShadeContext & sc)
{
  if (subTex[OPACITY_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[OPACITY_TEX_SLOT]->EvalColor(sc);

    float result = max(texColor.r, max(texColor.g, texColor.b));

    Clamp1(result);

    if(opacity_smooth)
      return result;
    else
    {
      if (result < 0.5f) 
        return 0.0f;
      else 
        return 1.0f;
    }
  }
  else
    return 1.0f;
}

Color hydraMaterial::GetShadeTranslucencyColor(ShadeContext & sc)
{
  Color res;

  if (subTex[TRANSLUCENCY_TEX_SLOT] != NULL && sc.doMaps)
  {    
    Color texColor = subTex[TRANSLUCENCY_TEX_SLOT]->EvalColor(sc);

    if (translucency_tint_on)
    {
      res = texColor * translucency_color * translucency_mult;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * translucency_mult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = translucency_color * translucency_mult;
    res.ClampMinMax();
    return res;
  }
}


inline float SpecularCosPowerFromGlosiness(float a_shiness)
{
  float cosPower = GetCosPowerFromMaxShiness(a_shiness);
  if(cosPower < 1.0f) cosPower = 1.0f;
  if(cosPower > 500.0f) cosPower = 500.0f;
  return cosPower;
}

float hydraMaterial::GetShadeSpecularCosPow(ShadeContext &sc)
{
  if(specular_gloss_or_cos == 0)
  {
    float glosiness;

    if(subTex[SPEC_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = specular_gloss*(subTex[SPEC_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = specular_gloss;

    return SpecularCosPowerFromGlosiness(glosiness);
  }
  else
    return specular_cospower;
}

float hydraMaterial::GetShadeReflectionCosPow(ShadeContext &sc)
{
  if(reflect_gloss_or_cos == 0)
  {
    float glosiness;

    if(subTex[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = reflect_gloss*(subTex[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = reflect_gloss;

    return SpecularCosPowerFromGlosiness(glosiness);
  }
  else
    return reflect_cospower;
}

float hydraMaterial::GetShadeReflectionGloss(ShadeContext &sc)
{
  if (reflect_gloss_or_cos == 0)
  {
    float glosiness;

    if (subTex[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = reflect_gloss * (subTex[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = reflect_gloss;

    ClampMinMax(glosiness);

    return glosiness;
  }
  else
    return reflect_cospower;
}

