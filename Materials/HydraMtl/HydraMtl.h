#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include <string>
#include "stdmat.h"
#include <IMaterialBrowserEntryInfo.h>

///////////////////////////////////////////////////////////////////////////////////////


extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define hydraMaterial_CLASS_ID	Class_ID(0xa3f0e60d, 0x941ba693)

#define NSUBMTL		0 // TODO: number of sub-materials supported by this plugin 
#define NSUBTEX 11

#define VERSION_CURRENT 3

//#define MAX2014

static int subTexId[NSUBTEX] = { IDC_DIFFUSE_MAP, IDC_SPECULAR_MAP, IDC_REFLECT_MAP, IDC_EMISSION_MAP, IDC_TRANSP_MAP, IDC_NORMAL_MAP, IDC_SPEC_GL_MAP, IDC_REFL_GL_MAP, IDC_TRANSP_GL_MAP, IDC_OPACITY_MAP, IDC_TRANSLUCENCY_MAP };

#define DIFFUSE_TEX_SLOT      0
#define SPECULAR_TEX_SLOT     1
#define REFLECT_TEX_SLOT      2
#define EMISSION_TEX_SLOT     3
#define TRANSPAR_TEX_SLOT     4
#define NORMALMAP_TEX_SLOT    5
#define SPEC_GLOSS_TEX_SLOT   6
#define REFL_GLOSS_TEX_SLOT   7
#define TRANSP_GLOSS_TEX_SLOT 8
#define OPACITY_TEX_SLOT 9
#define TRANSLUCENCY_TEX_SLOT 10

#define PBLOCK_REF	NSUBMTL+NSUBTEX

//#define EXTRUSION_COMP 0
#define EXTRUSION_STRONG 0
#define EXTRUSION_LUM 1

#ifdef MAX2012
typedef std::string hydraStr;
typedef char hydraChar;
#define paramEnd end 
#else
typedef std::wstring hydraStr;
typedef wchar_t hydraChar;
#define paramEnd p_end 
#endif

class hydraMaterial : public Mtl 
{
  public:

    // Parameter block
    IParamBlock2	*pblock;	//ref 0
    Texmap* subTex[NSUBTEX]; //ref 1-10
   // HBITMAP hBitmap;
   /* Mtl				*submtl[NSUBMTL];  
    BOOL			mapOn[NSUBMTL];
    float			spin;*/

    Color diffuse_color, specular_color, reflect_color, emission_color, transparency_color, fog_color, exit_color, translucency_color;    
    BOOL displacement_on, invertHeightOn, no_ic_records;
    BOOL diffuse_tint_on, specular_tint_on, emission_tint_on, transparency_tint_on, reflect_tint_on, transparency_thin_on, translucency_tint_on, affect_shadows_on;
		BOOL specular_fresnel_on, reflect_fresnel_on, lock_specular, emission_gi, opacity_smooth;    
    float specular_roughness, reflect_roughness, reflect_cospower, specular_cospower, ior, specular_ior, reflect_ior, transparency_cospower, fog_multiplier, displacement_height, diffuse_mult, specular_mult, emission_mult, transparency_mult, reflect_mult;
		float specular_gloss, reflect_gloss, transp_gloss, bump_amount, bump_radius, bump_sigma, translucency_mult, diffuse_roughness;
    int specular_brdf, reflect_brdf, specular_gloss_or_cos, reflect_gloss_or_cos, transp_gloss_or_cos, reflect_extrusion;

    Interval		ivalid;

    // Load bitmap for environment map
    static Interface *ip;
    static BitmapTex *bmapTexEnv;
    static StdUVGen *uvGenEnv;
    static Texmap *texmapEnv;

    ParamDlg *CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
    void Update(TimeValue t, Interval& valid);
    Interval Validity(TimeValue t);
    void Reset();

    void NotifyChanged();

    // From MtlBase and Mtl
    void SetAmbient(Color c, TimeValue t);		
    void SetDiffuse(Color c, TimeValue t);		
    void SetSpecular(Color c, TimeValue t);
    void SetShininess(float v, TimeValue t);
    Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE);
    Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE);
    Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE);
    Color GetSelfIllumColor(int mtlNum = 0, BOOL backFace = FALSE);
    float GetXParency(int mtlNum=0, BOOL backFace=FALSE);
    float GetShininess(int mtlNum=0, BOOL backFace=FALSE);		
    float GetShinStr(int mtlNum=0, BOOL backFace=FALSE);
    float WireSize(int mtlNum=0, BOOL backFace=FALSE);
        

    // Shade and displacement calculation
    //
    void Shade(ShadeContext& sc);
    float EvalDisplacement(ShadeContext& sc); 
    Interval DisplacementValidity(TimeValue t); 	

    // SubMaterial access methods
    int NumSubMtls() {return NSUBMTL;}
    Mtl* GetSubMtl(int i);
    void SetSubMtl(int i, Mtl *m);
    TSTR GetSubMtlSlotName(int i);
    TSTR GetSubMtlTVName(int i);

    // SubTexmap access methods
    int NumSubTexmaps() {return NSUBTEX;}
    Texmap* GetSubTexmap(int i);
    void SetSubTexmap(int i, Texmap *m);
    TSTR GetSubTexmapSlotName(int i);
    TSTR GetSubTexmapTVName(int i);
    
    BOOL SetDlgThing(ParamDlg* dlg);
    hydraMaterial(BOOL loading);
   // ~hydraMaterial(){ DeleteObject(hBitmap);};
    
    
    // Loading/Saving
    IOResult Load(ILoad *iload);
    IOResult Save(ISave *isave);

    //From Animatable
    Class_ID ClassID() {return hydraMaterial_CLASS_ID;}		
    SClass_ID SuperClassID() { return MATERIAL_CLASS_ID; }
    void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

    RefTargetHandle Clone( RemapDir &remap );

#ifdef MAX2014
		RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message);
#else
		virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);
#endif


    int NumSubs() { return 1+NSUBMTL; }
    Animatable* SubAnim(int i); 
    TSTR SubAnimName(int i);

    // TODO: Maintain the number or references here 
    int NumRefs() { return 1+NSUBMTL+NSUBTEX; }
    RefTargetHandle GetReference(int i);
    void SetReference(int i, RefTargetHandle rtarg);
		/*void PostLoadCallback( ILoad* iload )
		{
			IParamBlock2PostLoadInfo* postLoadInfo = (IParamBlock2PostLoadInfo*)
				pblock->GetInterface( IPARAMBLOCK2POSTLOADINFO_ID );

			// Check for specific obsolete version
			if(postLoadInfo!=NULL && postLoadInfo->GetVersion() < VERSION_CURRENT)
			{
				qqq = 3;
			}
		}*/


    int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
    IParamBlock2* GetParamBlock(int i) { return pblock; } // return i'th ParamBlock
    IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

    void DeleteThis() { delete this; }

    protected:

       Color GetShadeDiffuseColor(ShadeContext &sc);
       Color GetShadeReflectionColor(ShadeContext &sc);
       Color GetShadeSpecularColor(ShadeContext &sc);
       Color GetShadeTransparencyColor(ShadeContext &sc);
       float GetShadeOpacity(ShadeContext &sc);
       Color GetShadeTranslucencyColor(ShadeContext &sc);
       Color GetShadeEmissiveColor(ShadeContext &sc);

       float GetShadeSpecularCosPow(ShadeContext &sc);
       float GetShadeReflectionCosPow(ShadeContext &sc);
       float GetShadeReflectionGloss(ShadeContext &sc);

};



class hydraMaterialClassDesc : public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  virtual int IsPublic() 							{ return TRUE; }
  virtual void* Create(BOOL loading = FALSE) 		{ return new hydraMaterial(loading); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  virtual SClass_ID SuperClassID() 				{ return MATERIAL_CLASS_ID; }
  virtual Class_ID ClassID() 						{ return hydraMaterial_CLASS_ID; }
  virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }
  virtual const TCHAR* InternalName() 			{ return _T("hydraMaterial"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
  
  // For entry category
  virtual FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  virtual const MCHAR* GetEntryName() const override;
  virtual const MCHAR* GetEntryCategory() const override;
  virtual Bitmap* GetEntryThumbnail() const override;
};



class hydraMaterialDlgProc : public ParamMap2UserDlgProc {
public:
  hydraMaterial * hM;
  HWND thishWnd;

  ICustButton *mEmission;
  ICustButton *mDiffuse;
  ICustButton *mSpecular;
  ICustButton *mReflect;
  ICustButton *mTransparency;
  ICustButton *mNormal;
  ICustButton *mSpecGloss;
  ICustButton *mReflectGloss;
  ICustButton *mTranspGloss;
  ICustButton *mOpacity;
  ICustButton *mTranslucency;

  ICustEdit *spec_gloss_edit;
  ICustEdit *spec_cos_edit;
  ICustEdit *spec_mult_edit;
  ICustEdit *spec_ior_edit;
  ICustEdit *refl_gloss_edit;
  ICustEdit *refl_cos_edit;
  ICustEdit *transp_gloss_edit;
  ICustEdit *transp_cos_edit;
  ICustEdit *bump_amt_edit;
  ICustEdit *diffuse_roughness_edit;

  ISpinnerControl *spec_gloss_spin;
  ISpinnerControl *spec_cos_spin;
  ISpinnerControl *spec_mult_spin;
  ISpinnerControl *spec_ior_spin;
  ISpinnerControl *refl_gloss_spin;
  ISpinnerControl *refl_cos_spin;
  ISpinnerControl *transp_gloss_spin;
  ISpinnerControl *transp_cos_spin;
  ISpinnerControl *bump_amt_spin;
  ISpinnerControl *diffuse_roughness_spin;

  IColorSwatch *spec_color;

  bool additional_init;

  hydraMaterialDlgProc(hydraMaterial *cb);
  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  void lockSpec(bool lock);
  void bumpAmountLock();
  void texBtnText();
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() { delete this; }
};


ClassDesc2* GethydraMaterialDesc(); 

enum { hydramaterial_params };


//TODO: Add enums for various parameters
enum { 
  pb_spin,
  mtl_mat1,
  mtl_mat1_on,
  mtl_diffuse_color,
  mtl_diffuse_map,
  mtl_diffuse_tint_on,
  mtl_diffuse_mult,
  mtl_specular_color,
  mtl_specular_map,
  mtl_specular_tint_on,
  mtl_specular_mult,
  mtl_specular_brdf,
  mtl_specular_roughness,
	mtl_specular_cospower,
	mtl_specular_ior,
  mtl_reflect_color,
  mtl_reflect_map,
  mtl_reflect_tint_on,
  mtl_reflect_mult,
  mtl_reflect_brdf,
  mtl_reflect_cospower,
  mtl_reflect_roughness,
	mtl_reflect_ior,
  mtl_emission_color,
  mtl_emission_map,
  mtl_emission_tint_on,
  mtl_emission_mult,
  mtl_transparency_color,
  mtl_transparency_map,
  mtl_transparency_tint_on,
  mtl_transparency_mult,
  mtl_transparency_thin_on,
  mtl_ior,
  mtl_transparency_cospower,
  mtl_fog_color,
  mtl_fog_multiplier,
  mtl_exit_color,
  mtl_displacement_on,
  mtl_displacement_height,
  mtl_normal_map,
  mtl_displacement_invert_height_on,
	mtl_no_ic_records,
	mtl_spec_gloss,
	mtl_spec_gl_map,
	mtl_spec_gloss_or_cos,
	mtl_spec_fresnel_on,
	mtl_refl_gloss,
	mtl_refl_gl_map,
	mtl_refl_gloss_or_cos,
	mtl_refl_fresnel_on,
	mtl_transp_gloss,
	mtl_transp_gl_map,
	mtl_transp_gloss_or_cos,
	mtl_lock_specular,
	mtl_bump_amount,
	mtl_bump_radius,
	mtl_bump_sigma,
	mtl_emission_gi,
	mtl_opacity_map,
	mtl_opacity_smooth,
	mtl_translucency_color,
	mtl_translucency_mult,
	mtl_translucency_map,
	mtl_translucency_tint_on,
	mtl_reflect_extrusion,
  mtl_roughness_mult
};


