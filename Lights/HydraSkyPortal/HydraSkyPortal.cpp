//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "HydraSkyPortal.h"


#define INITINTENS 255


// Class variables of GeneralLight
bool SkyPortal::inCreate = false;
short SkyPortal::meshBuilt = 0;
int SkyPortal::dlgRed = INITINTENS;
int SkyPortal::dlgGreen = INITINTENS;
int SkyPortal::dlgBlue = INITINTENS;

float SkyPortal::dlgMultiplier = 1.0f;
float SkyPortal::dlgLength = 100.0f;
float SkyPortal::dlgWidth = 100.0f;
Color24 SkyPortal::dlgTint(255, 255, 255);

short SkyPortal::dlgShape = RECT_LIGHT;

const Class_ID stdShadMap = Class_ID(STD_SHADOW_MAP_CLASS_ID, 0);		// ID for shadow map generator

Class_ID SkyPortal::dlgShadType = GetMarketDefaults()->GetClassID(LIGHT_CLASS_ID, Class_ID(OMNI_LIGHT_CLASS_ID, 0), _T("ShadowGenerator"), stdShadMap, MarketDefaults::CheckNULL);
//short SkyPortal::dlgUseGlobalShadowParams = GetMarketDefaults()->GetInt(LIGHT_CLASS_ID, Class_ID(OMNI_LIGHT_CLASS_ID, 0), _T("useGlobalShadowSettings"), 0) != 0;

// Global shadow settings
short SkyPortal::globShadowType = 1;  // 0: ShadowMap   1: RayTrace
short SkyPortal::globAbsMapBias = 0;
float SkyPortal::globMapBias = 1.0f;
float SkyPortal::globMapRange = 4.0f;
int   SkyPortal::globMapSize = 512;
float SkyPortal::globRayBias = 0.2f;
short SkyPortal::globAtmosShadows = false;
float SkyPortal::globAtmosOpacity = 1.0f;
float SkyPortal::globAtmosColamt = 1.0f;
//
SkyPortal * SkyPortal::currentEditLight = nullptr;
HWND SkyPortal::hSkyPortal = nullptr;

void resetLightParams()
{
  SkyPortal::dlgMultiplier = 1.0f;
  SkyPortal::dlgLength = 1.0f;
  SkyPortal::dlgWidth = 1.0f;
  SkyPortal::dlgTint.r = 255;
  SkyPortal::dlgTint.g = 255;
  SkyPortal::dlgTint.b = 255;
  SkyPortal::dlgShape = RECT_LIGHT;


  SkyPortal::dlgShadType = GetMarketDefaults()->GetClassID(LIGHT_CLASS_ID, Class_ID(OMNI_LIGHT_CLASS_ID, 0), _T("ShadowGenerator"), stdShadMap, MarketDefaults::CheckNULL);

  HWND hl = GetDlgItem(SkyPortal::hSkyPortal, IDC_TINT);
  if (hl)
    InvalidateRect(hl, NULL, TRUE);
}

IObjParam *SkyPortal::iObjParams;

ISpinnerControl *SkyPortal::multiplierSpin;
ISpinnerControl *SkyPortal::lengthSpin;
ISpinnerControl *SkyPortal::widthSpin;
IColorSwatch    *SkyPortal::tintColorSwatch;



static SkyPortalClassDesc HydraSkyPortalDesc;
ClassDesc* GetHydraSkyPortalDesc() { return &HydraSkyPortalDesc; }



template <void (SkyPortal::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Restore(int undo)
{
  (_light->*set)(_undo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

template <void (SkyPortal::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Redo()
{
  (_light->*set)(_redo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

template <void (SkyPortal::*set)(int), bool notify>
int SimpleLightUndo<set, notify>::Size()
{
  return sizeof(*this);
}

template <void (SkyPortal::*set)(int), bool notify>
TSTR SimpleLightUndo<set, notify>::Description()
{
  return _T("SimpleLightUndo");
}

static ShadowType *GetShadowTypeForIndex(int n) {
  SubClassList *scList = GetCOREInterface()->GetDllDir().ClassDir().GetClassList(SHADOW_TYPE_CLASS_ID);
  int count = 0;
  for (long i = 0; i < scList->Count(ACC_ALL); ++i) {
    if ((*scList)[i].IsPublic()) {
      ClassDesc* pClassD = (*scList)[i].CD();
      if (count == n) {
        HoldSuspend hs; // LAM - 6/12/04 - defect 571821
        return (ShadowType*)(GetCOREInterface()->CreateInstance(pClassD->SuperClassID(), pClassD->ClassID()));
      }
      count++;
    }
  }
  return nullptr;
}

void SkyPortal::UpdateUICheckbox(HWND hwnd, int dlgItem, TCHAR *name, int val)
{
  if (hwnd)
  {
    // spit out macro recorder stuff
    if (name && !theHold.RestoreOrRedoing())
      macroRec->SetSelProperty(name, mr_bool, val);

    // update the checkbox
    macroRec->Disable();
    CheckDlgButton(hwnd, dlgItem, val);
    macroRec->Enable();
  }
}

int SkyPortal::HitTest(TimeValue t, INode * inode, int type, int crossing, int flags, IPoint2 * p, ViewExp * vpt)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return false;
  }

  HitRegion hitRegion;
  DWORD savedLimits;
  int res = 0;
  Matrix3 m;
  if (!enable)
    return 0;
  GraphicsWindow *gw = vpt->getGW();
  Material *mtl = gw->getMaterial();
  MakeHitRegion(hitRegion, type, crossing, 4, p);
  gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~(GW_ILLUM | GW_BACKCULL));
  GetMat(t, inode, *vpt, m);
  gw->setTransform(m);
  // if we get a hit on the mesh, we're done
  res = mesh->select(gw, mtl, &hitRegion, flags & HIT_ABORTONHIT);  // CRASH!
  // if not, check the target line, and set the pair flag if it's hit
  if (!res) {
    // this special case only works with point selection of targeted lights
    if ((type != HITTYPE_POINT) || !inode->GetTarget())
      return 0;
    // don't let line be active if only looking at selected stuff and target isn't selected
    if ((flags & HIT_SELONLY) && !inode->GetTarget()->Selected())
      return 0;

    gw->clearHitCode();

    if (res != 0)
      inode->SetTargetNodePair(1);
  }
  gw->setRndLimits(savedLimits);
  return res;
}

static void GenericSnap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt)
{
  // Make sure the vertex priority is active and at least as important as the best snap so far
  if (snap->vertPriority > 0 && snap->vertPriority <= snap->priority) {
    Matrix3 tm = inode->GetObjectTM(t);
    GraphicsWindow *gw = vpt->getGW();

    gw->setTransform(tm);

    Point2 fp = Point2((float)p->x, (float)p->y);
    IPoint3 screen3;
    Point2 screen2;
    Point3 vert(0.0f, 0.0f, 0.0f);

    gw->wTransPoint(&vert, &screen3);

    screen2.x = (float)screen3.x;
    screen2.y = (float)screen3.y;

    // Are we within the snap radius?
    int len = (int)Length(screen2 - fp);
    if (len <= snap->strength) {
      // Is this priority better than the best so far?
      if (snap->vertPriority < snap->priority) {
        snap->priority = snap->vertPriority;
        snap->bestWorld = vert * tm;
        snap->bestScreen = screen2;
        snap->bestDist = len;
      }
      // Closer than the best of this priority?
      else if (len < snap->bestDist) {
        snap->priority = snap->vertPriority;
        snap->bestWorld = vert * tm;
        snap->bestScreen = screen2;
        snap->bestDist = len;
      }
    }
  }
}

void SkyPortal::Snap(TimeValue t, INode * inode, SnapInfo * snap, IPoint2 * p, ViewExp * vpt)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return;
  }

  GenericSnap(t, inode, snap, p, vpt);
}

void SkyPortal::SetExtendedDisplay(int flags)
{
  extDispFlags = flags;
}

int SkyPortal::Display(TimeValue t, INode * inode, ViewExp * vpt, int flags)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }

  Matrix3 m;

  if (!enable)
    return 0;

  GraphicsWindow *gw = vpt->getGW();
  GetMat(t, inode, *vpt, m);
  gw->setTransform(m);
  DWORD rlim = gw->getRndLimits();
  gw->setRndLimits(GW_WIREFRAME | GW_EDGES_ONLY | GW_BACKCULL | (gw->getRndMode() & GW_Z_BUFFER));
  
  if (inode->Selected())
    gw->setColor(LINE_COLOR, GetSelColor());
  else if (!inode->IsFrozen() && !inode->Dependent()) {
    if (useLight)
    {
      // 6/22/01 2:16pm --MQM--
      // use the wire-color to draw the light,
      // instead of COLOR_LIGHT_OBJ
      //! NH - Add code here to change the color of the light depending on the Viewport Shading
      Color color = GetIViewportShadingMgr()->GetLightIconColor(*inode);
      gw->setColor(LINE_COLOR, color);
    }
    // I un-commented this line DS 6/11/99
    else
      gw->setColor(LINE_COLOR, 0.0f, 0.0f, 0.0f);
  }
  mesh->render(gw, gw->getMaterial(), (flags&USE_DAMAGE_RECT) ? &vpt->GetDammageRect() : NULL, COMP_ALL);

  gw->setRndLimits(rlim);
  return 0;
}

// Parameter block indices
#define PB_MULTIPLIER	0
#define PB_TINT 		  1	   
#define PB_LENGTH	  	2
#define PB_WIDTH      3


// description for version 0
static ParamBlockDescID descV0[] = {
  { TYPE_FLOAT, NULL, TRUE, 1 },		// descV0[PB_MULTIPLIER] 
  { TYPE_RGBA,  NULL, TRUE, 2 },	  // descV0[PB_TINT] 		   
  { TYPE_FLOAT, NULL, TRUE, 3 },		// descV0[PB_LENGTH]	   
  { TYPE_FLOAT, NULL, TRUE, 4 } };	// descV0[PB_WIDTH]


#define LIGHT_VERSION   0		// current version

ParamBlockDescID *descs[LIGHT_VERSION + 1] = {
  descV0,
};

// number of parameters light 
static int pbDim1[1] = { 4 };

static int *pbdims[LIGHT_VERSION + 1] = { pbDim1 };

static int GetDim(int vers, int type) {
  return pbdims[vers][type];
}

static  ParamBlockDescID* GetDesc(int vers, int type) {
  return descs[vers];
}


INT_PTR CALLBACK SkyPortalParamDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  int id;
  SkyPortal *sp = DLGetWindowLongPtr<SkyPortal *>(hDlg);
  if (!sp && message != WM_INITDIALOG)
    return FALSE;
  if (sp->iObjParams == NULL)
    return FALSE;
  TimeValue t = sp->iObjParams->GetTime();

  switch (message)
  {
  case WM_INITDIALOG:
    sp = (SkyPortal *)lParam;

    DLSetWindowLongPtr(hDlg, sp);
    SetDlgFont(hDlg, sp->iObjParams->GetAppHFont());
    CheckDlgButton(hDlg, IDC_ON, sp->GetUseLight());
    CheckDlgButton(hDlg, IDC_VISIBLE, sp->GetVisibleLight());
    return FALSE;

  case WM_DESTROY:
    // these spinners no longer exist in the updated UI.
    RELEASE_SPIN(multiplierSpin);
    RELEASE_SPIN(lengthSpin);
    RELEASE_SPIN(widthSpin);
    RELEASE_COLSW(tintColorSwatch);
    return FALSE;

  case WM_SET_TYPE:
    theHold.Begin();
    sp->SetType(FSPOT_LIGHT);
    theHold.Accept(GetString(IDS_DS_SETTYPE));
    return FALSE;

  case WM_SET_SHADTYPE:
    theHold.Begin();
    sp->SetShadowGenerator(GetShadowTypeForIndex(wParam));
    theHold.Accept(GetString(IDS_DS_SETSHADTYPE));

    // 5/15/01 11:48am --MQM-- 
    macroRec->SetSelProperty(_T("raytracedShadows"), mr_bool, wParam);
    // 3/29/11 | Zane | viewport now may regard the shadow type 
    sp->iObjParams->RedrawViews(t, REDRAW_INTERACTIVE);
    return FALSE;

  case CC_SPINNER_CHANGE:
  {
    int id = LOWORD(wParam);
    if (!theHold.Holding()) theHold.Begin();
    bool redraw = true;

    switch (id)
    {
    case IDC_MULT_SPIN:
      sp->SetIntensity(t, sp->multiplierSpin->GetFVal());
      sp->SetSpinnerBracket(sp->multiplierSpin, PB_MULTIPLIER);
      redraw = false;
      break;
    case IDC_LENGTH_SPIN:
      sp->SetLength(t, sp->lengthSpin->GetFVal());
      sp->SetSpinnerBracket(sp->lengthSpin, PB_LENGTH);
      sp->BuildRectMesh(t);
      break;
    case IDC_WIDTH_SPIN:
      sp->SetWidth(t, sp->widthSpin->GetFVal());
      sp->SetSpinnerBracket(sp->widthSpin, PB_WIDTH);
      sp->BuildRectMesh(t);
      break;
    }
    if (redraw)
      sp->iObjParams->RedrawViews(sp->iObjParams->GetTime(), REDRAW_INTERACTIVE);
  }
  return true;

  case CC_SPINNER_BUTTONDOWN:
    theHold.Begin();
    return true;

  case CC_COLOR_CHANGE:
    id = LOWORD(wParam);
    if (HIWORD(wParam)) 
    {	// button up
      DWORD rgb;
      theHold.Begin();
      switch (id)
      {
      case IDC_TINT:
        rgb = sp->tintColorSwatch->GetColor();
        sp->SetRGBColor(t, Point3(GetRValue(rgb) / 255.0f, GetGValue(rgb) / 255.0f, GetBValue(rgb) / 255.0f));
        break;
      }
      theHold.Accept(GetString(IDS_DS_PARAMCHG));
      sp->iObjParams->RedrawViews(sp->iObjParams->GetTime(), REDRAW_END);
      sp->UpdateUI(t);
    }
    return true;

  case WM_CUSTEDIT_ENTER:
  case CC_SPINNER_BUTTONUP:
    if (HIWORD(wParam) || message == WM_CUSTEDIT_ENTER)
      theHold.Accept(GetString(IDS_DS_PARAMCHG));
    else
      theHold.Cancel();
    sp->iObjParams->RedrawViews(sp->iObjParams->GetTime(), REDRAW_END);
    return true;

  case WM_MOUSEACTIVATE:
    sp->iObjParams->RealizeParamPanel();
    return false;

  case WM_LBUTTONDOWN:
  case WM_LBUTTONUP:
  case WM_RBUTTONDOWN:
  case WM_RBUTTONUP:
  case WM_MOUSEMOVE:
    sp->iObjParams->RollupMouseMessage(hDlg, message, wParam, lParam);
    return false;

  case WM_COMMAND:
    switch (id = LOWORD(wParam))
    {
    case IDC_ON:
      theHold.Begin();
      sp->SetUseLight(IsDlgButtonChecked(hDlg, IDC_ON));
      sp->iObjParams->RedrawViews(t);
      theHold.Accept(GetString(IDS_DS_PARAMCHG));
      break;
    case IDC_VISIBLE:
      theHold.Begin();
      sp->SetVisibleLight(IsDlgButtonChecked(hDlg, IDC_VISIBLE));
      sp->iObjParams->RedrawViews(t);
      theHold.Accept(GetString(IDS_DS_PARAMCHG));
      break;
    }
      return FALSE;
    default:
      return FALSE;
  }  
}


#define SET_QUAD(face, v0, v1, v2, v3) \
	rectMesh.faces[face].setVerts(v0, v1, v2); \
	rectMesh.faces[face].setEdgeVisFlags(1,1,0); \
	rectMesh.faces[face+1].setVerts(v0, v2, v3); \
	rectMesh.faces[face+1].setEdgeVisFlags(0,1,1);


void SkyPortal::BuildRectMesh(TimeValue t)
{
  // Non scale viewport mesh
  const int nverts = 5;
  const int nfaces = 3;

  rectMesh.setNumVerts(nverts);
  rectMesh.setNumFaces(nfaces);
        
  float length, width;
  pblock->GetValue(PB_LENGTH, t, length, ivalid);
  pblock->GetValue(PB_WIDTH, t, width, ivalid);

  const float halfLength = length / 2.0f;
  const float halfWidth = width / 2.0f;
  // Quad
  rectMesh.setVert(0, Point3(-halfLength, -halfWidth, 0.0f));
  rectMesh.setVert(1, Point3(halfLength, -halfWidth, 0.0f));
  rectMesh.setVert(2, Point3(halfLength, halfWidth, 0.0f));
  rectMesh.setVert(3, Point3(-halfLength, halfWidth, 0.0f));
  //SET_QUAD(0, 0, 1, 2, 3);
  rectMesh.faces[0].setVerts(0, 1, 2); 
  rectMesh.faces[0].setEdgeVisFlags(1, 1, 0); 
  rectMesh.faces[1].setVerts(0, 2, 3); 
  rectMesh.faces[1].setEdgeVisFlags(0, 1, 1);

  // Direct line
  const float lengthDirLine = (halfLength + halfWidth) / 3.0f;

  rectMesh.setVert(4, Point3(0.0f, 0.0f, 0.0f));
  rectMesh.setVert(5, Point3(0.0f, 0.0f, -lengthDirLine));
  rectMesh.faces[2].setVerts(0, 4, 5);
  rectMesh.faces[2].setEdgeVisFlags(0, 1, 0);

  for (int i = 0; i < nfaces; i++) 
    rectMesh.faces[i].setSmGroup(i);
    
  //rectMesh.buildNormals();
  //rectMesh.EnableEdgeList(1);
  rectMesh.InvalidateGeomCache();
}


void SkyPortal::UpdateUI(TimeValue t)
{
  Point3 color;

  if (hSkyPortal && !waitPostLoad &&
    GetWindowLongPtr(hSkyPortal, GWLP_USERDATA) == (LONG_PTR)this && pblock)
  {
    color = GetRGBColor(t);
    tintColorSwatch->SetColor(RGB(FLto255i(color.x), FLto255i(color.y), FLto255i(color.z)));

    UpdateColBrackets(t);

    CheckDlgButton(hSkyPortal, IDC_ON, GetUseLight());
    CheckDlgButton(hSkyPortal, IDC_VISIBLE, GetVisibleLight());

    multiplierSpin->SetValue(GetIntensity(t), FALSE);
    multiplierSpin->SetKeyBrackets(pblock->KeyFrameAtTime(PB_MULTIPLIER, t));

    lengthSpin->SetValue(GetLength(t), FALSE);
    lengthSpin->SetKeyBrackets(pblock->KeyFrameAtTime(PB_LENGTH, t));

    widthSpin->SetValue(GetWidth(t), FALSE);
    widthSpin->SetKeyBrackets(pblock->KeyFrameAtTime(PB_WIDTH, t));

    // DS 8/28/00
    // added: DC 08/03/2001
    // reflects simpler selection of type ui
    //if (!uiConsistency)
    //  SendMessage(GetDlgItem(hGeneralLight, IDC_LIGHT_TYPE), CB_SETCURSEL, idFromType(type), (LPARAM)0);
    //else
    //  SendMessage(GetDlgItem(hGeneralLight, IDC_LIGHT_TYPE), CB_SETCURSEL, idFromTypeSimplified(type), (LPARAM)0);

    //aszabo|Nov.09.01 - disable param-wired, script or expression controlled params

    // DC - Nov 30, 2001 moved this call into this block so that a NULL pblock is checked
    // bug 313434
    UpdateControlledUI();
  }  // closes initial update check
}

void SkyPortal::SetSpinnerBracket(ISpinnerControl * spin, int pbid)
{
  spin->SetKeyBrackets(pblock->KeyFrameAtTime(pbid, iObjParams->GetTime()));
}

void SkyPortal::SetVisibleLight(bool onOff)
{
  SimpleLightUndo< &SkyPortal::SetUseLight, false >::Hold(this, onOff, useLight);
  visible = onOff;
  if (currentEditLight == this)  // LAM - 8/13/02 - defect 511609
    UpdateUICheckbox(hSkyPortal, IDC_VISIBLE, _T("visible"), onOff); // 5/15/01 11:00am --MQM-- maxscript fix
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

//void SkyPortal::SetLength(TimeValue t, float f)
//{
//  pblock->SetValue(PB_LENGTH, t, f);
//  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
//}

//float SkyPortal::GetLength(TimeValue t, Interval & valid)
//{
//  float f;
//  pblock->GetValue(PB_LENGTH, t, f, valid);
//  return f;
//}

//void SkyPortal::SetWidth(TimeValue t, float f)
//{
//  pblock->SetValue(PB_WIDTH, t, f);
//  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
//}

//float SkyPortal::GetWidth(TimeValue t, Interval & valid)
//{
//  float f;
//  pblock->GetValue(PB_WIDTH, t, f, valid);
//  return f;
//}

void SkyPortal::UpdateColBrackets(TimeValue t)
{
  bool isKey = pblock->KeyFrameAtTime(PB_TINT, t);
  tintColorSwatch->SetKeyBrackets(isKey);
}

RefTargetHandle SkyPortal::Clone(RemapDir & remap)
{
  SkyPortal* newob = new SkyPortal();
  newob->enable = enable;
  newob->useLight = useLight;
  newob->shape = shape;
  newob->shadow = shadow;
  newob->shadowType = shadowType;
  newob->exclList = exclList;
  newob->ReplaceReference(PBLOCK_REF, remap.CloneRef(pblock));
  if (shadType) newob->ReplaceReference(SHADTYPE_REF, remap.CloneRef(shadType));
  newob->ivalid.SetEmpty();
  BaseClone(this, newob, remap);
  return(newob);
}

RefResult SkyPortal::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
  case REFMSG_NODE_NAMECHANGE:
  case REFMSG_CHANGE:
    if (iObjParams)
      UpdateUI(iObjParams->GetTime());
    break;

  case REFMSG_NODE_HANDLE_CHANGED:
  {
    IMergeManager* imm = (IMergeManager*)partID;
    if (imm)
    {
      exclList.OnMerge(imm);
    }
    return REF_STOP;
  }
  case REFMSG_GET_PARAM_DIM:
  {
    GetParamDim *gpd = (GetParamDim*)partID;
    if (hTarget == (RefTargetHandle)pblock)
    {
      switch (gpd->index)
      {
      case PB_TINT:
        gpd->dim = stdColor255Dim;
        break;
      case PB_MULTIPLIER:
      case PB_LENGTH:
      case PB_WIDTH:
        gpd->dim = defaultDim; //or stdWorldDim
        break;
      }
      return REF_HALT;
    }
    return REF_STOP;
  }

  case REFMSG_GET_PARAM_NAME:
  {
    GetParamName *gpn = (GetParamName*)partID;
    if (hTarget == (RefTargetHandle)pblock)
    {
      switch (gpn->index)
      {
      case PB_TINT: gpn->name = GetString(IDS_DB_COLOR); break;
      case PB_MULTIPLIER: gpn->name = GetString(IDS_DB_MULTIPLIER); break;
      case PB_LENGTH:  gpn->name = GetString(IDS_DS_LENGTH); break;
      case PB_WIDTH:  gpn->name = GetString(IDS_DS_WIDTH); break;
      }
    }
    return REF_HALT;
  }
  return REF_STOP;
  }
  return REF_SUCCEED;
}

static void RemoveScaling(Matrix3 &tm) {
  AffineParts ap;
  decomp_affine(tm, &ap);
  tm.IdentityMatrix();
  tm.SetRotate(ap.q);
  tm.SetTrans(ap.t);
}

void SkyPortal::GetMat(TimeValue t, INode * inode, ViewExp & vpt, Matrix3 & tm)
{
  if (!vpt.IsAlive())
  {
    tm.Zero();
    return;
  }

  tm = inode->GetObjectTM(t);

  // This light source is real size, so everything scaleFactor is not needed.
  //RemoveScaling(tm);
  //float scaleFactor = vpt.NonScalingObjectSize() * vpt.GetVPWorldWidth(tm.GetTrans()) / 360.0f;
  //tm.Scale(Point3(scaleFactor, scaleFactor, scaleFactor));
}


SkyPortal::SkyPortal()
{
  // Disable hold, macros and ref msgs.
  SuspendAll	xDisable(TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE);
  //type = 0;
  pblock = nullptr;
  shadType = nullptr;

  ReplaceReference(0, CreateParameterBlock(GetDesc(LIGHT_VERSION, 0), GetDim(LIGHT_VERSION, 0), LIGHT_VERSION));

#ifdef USE_DLG_COLOR
  SetRGBColor(TimeValue(0), Point3(dlgRed / 255.0f, dlgGreen / 255.0f, dlgBlue / 255.0f));
#else
  SetRGBColor(TimeValue(0), Point3(float(INITINTENS) / 255.0f, float(INITINTENS) / 255.0f, float(INITINTENS) / 255.0f));
#endif
  SetIntensity(TimeValue(0), dlgMultiplier);
  SetLength(TimeValue(0), dlgLength);
  SetWidth(TimeValue(0), dlgWidth);


  shape = RECT_LIGHT;
  useLight = true;
  enable = false;
  visible = true;

  shadow = GetMarketDefaults()->GetInt(LIGHT_CLASS_ID, Class_ID(OMNI_LIGHT_CLASS_ID, 0), _T("CastShadows"), 0) != 0;

  shadowType = 1; // raytrace
  shadType = nullptr;

  void* s = CreateInstance(SHADOW_TYPE_CLASS_ID, dlgShadType);
  if (s == NULL)
    s = CreateInstance(SHADOW_TYPE_CLASS_ID, stdShadMap);
  ReplaceReference(SHADTYPE_REF, (ShadowType *)s);


  BuildMeshes();

  // this call determines whether the ui is consistent with what is expected
  // mainly this just checks if all the expected light types exist and are in the 
  // right position
  //CheckUIConsistency();

}

SkyPortal::~SkyPortal()
{
  DeleteAllRefsFromMe();
  pblock = nullptr;

  //if (shadType && !shadType->SupportStdMapInterface()) 
  //  ReplaceReference(AS_SHADTYPE_REF, nullptr);
}

void SkyPortal::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
  iObjParams = ip;
  TimeValue t = ip->GetTime();
  Point3 color;
  inCreate = (flags&BEGIN_EDIT_CREATE) ? 1 : 0;

  //projDADMgr.Init(this);

  currentEditLight = this;

  if (!hSkyPortal)
    InitGeneralParamDialog(t, ip, flags, prev);
  else
  {
    // Plug in new values to UI 
    DLSetWindowLongPtr(hSkyPortal, this);

    color = GetRGBColor(t);
    //color = GetHSVColor(t);
    multiplierSpin->SetValue(GetIntensity(t), FALSE);
  }
  // change made to reflect simpler type selection
  //if (!uiConsistency)
  //  SendMessage(GetDlgItem(hSkyPortal, IDC_LIGHT_TYPE), CB_SETCURSEL, idFromType(type), (LPARAM)0);
  //else
  //  SendMessage(GetDlgItem(hSkyPortal, IDC_LIGHT_TYPE), CB_SETCURSEL, idFromTypeSimplified(type), (LPARAM)0);


  //if (!inCreate)
  //  ip->AddSFXRollupPage(0);
}


void SkyPortal::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
  Point3 color;
  TimeValue t = ip->GetTime();

  color = GetRGBColor(t);
  //color = GetHSVColor(t);
  dlgMultiplier = GetIntensity(t);
  dlgLength = GetLength(t);
  dlgWidth = GetWidth(t);

  Point3 col = GetShadColor(t);
  //dlgShadType = shadType->ClassID();

  currentEditLight = NULL;

  if (flags&END_EDIT_REMOVEUI)
  {
    HWND hwgl = hSkyPortal;
    hSkyPortal = nullptr; // This keeps UpdateUI from jumping in
    //simpDialInitialized = FALSE;

    ip->UnRegisterDlgWnd(hwgl);
    ip->DeleteRollupPage(hwgl);

    //ip->DeleteSFXRollupPage();
  }

  iObjParams = nullptr;
}

const TCHAR * SkyPortal::GetObjectName()
{
  return GetString(IDS_DB_HYDRA_SKY_PORTAL);
}

void SkyPortal::GetWorldBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  int nv;
  Matrix3 tm;
  GetMat(t, inode, *vpt, tm);
  Point3 loc = tm.GetTrans();
  nv = mesh->getNumVerts();
  box.Init();
  if (!(extDispFlags & EXT_DISP_ZOOM_EXT))
    box.IncludePoints(mesh->verts, nv, &tm);
  else
    box += loc;
  tm = inode->GetObjectTM(t);
  //BoxLight(t, inode, box, &tm);
}

void SkyPortal::GetLocalBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  // This light source is real size, so everything scaleFactor is not needed.

  //Point3 loc = inode->GetObjectTM(t).GetTrans();
  //float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(loc) / 360.0f;
  box = mesh->getBoundingBox();//same as GetDeformBBox
  //box.Scale(scaleFactor);

  //JH 6/13/03 careful, the mesh extents in z are (0, ~-20)
  //thus the scale alone is wrong
  //move the top of the box back to the object space origin

  box.Translate(Point3(0.0f, 0.0f, -box.pmax.z));

  //this adds the target point, cone, etc.
  //BoxLight(t, inode, box, NULL);
}

void SkyPortal::GetDeformBBox(TimeValue t, Box3 & box, Matrix3 * tm, BOOL useSel)
{
  box = mesh->getBoundingBox(tm);
}

void SkyPortal::InitNodeName(TSTR & s)
{
  s = GetString(IDS_DB_HYDRASKYPORTAL);
}

Interval SkyPortal::ObjectValidity(TimeValue time)
{
  Interval valid;
  valid.SetInfinite();

  if (!waitPostLoad) 
  {
    GetIntensity(time, valid);
    GetRGBColor(time, valid);
    GetLength(time, valid);
    GetWidth(time, valid);

    UpdateUI(time);
  }
  return valid;
}


static SkyPortalCreateCallBack skyPortalCreateCB;

CreateMouseCallBack * SkyPortal::GetCreateMouseCallBack()
{
  skyPortalCreateCB.SetObj(this);
  return(&skyPortalCreateCB);
}

ObjectState SkyPortal::Eval(TimeValue t)
{
  UpdateUI(t);
  return ObjectState(this);
}

GenLight * SkyPortal::NewLight(int type)
{
  return new SkyPortal();
}

RefResult SkyPortal::EvalLightState(TimeValue t, Interval & valid, LightState * ls)
{
  if (useLight)
    ls->color = GetRGBColor(t, valid);
  else
    ls->color = Point3(0, 0, 0);
  ls->on = useLight;
  ls->intens = GetIntensity(t, valid);
  ls->hotsize = GetHotspot(t, valid);
  ls->fallsize = GetFallsize(t, valid);
#if 0
  if (ls->useAtten = GetUseAtten()) {
    ls->attenStart = GetAtten(t, ATTEN_START, valid);
    ls->attenEnd = GetAtten(t, ATTEN_END, valid);
  }
  else
    ls->attenStart = ls->attenEnd = 0.0f;
#endif

  ls->useNearAtten = GetUseAttenNear();
  ls->nearAttenStart = GetAtten(t, ATTEN1_START, valid);
  ls->nearAttenEnd = GetAtten(t, ATTEN1_END, valid);

  ls->useAtten = GetUseAtten();
  ls->attenStart = GetAtten(t, ATTEN_START, valid);
  ls->attenEnd = GetAtten(t, ATTEN_END, valid);
  if (ls->extra == 0x0100) 
  {
    // Special code for the interactive viewport in order to handle
    // DecayType.  If extra is set to 0x0100, we assume that we are called
    // from interactive viewport code.
    //
    // Set bits on the extra field to pass on the type of attenuation in
    // use.
    //
    // Note that attenStart contains the DecayRadius if useAtten is not
    // DECAY_NONE, i.e. either DECAY_INV or DECAY_INVSQ.  The GFX code will
    // use this to handle these two types of attenuation.
    //
    // Please note that attenEnd always contains the attenEnd value of the Far
    // Attenuation.
    //
    int decay = GetDecayType();
    if (decay > 0) 
      ls->attenStart = GetDecayRadius(t, valid);

    switch (decay) 
    {
    case DECAY_NONE:
    default:
      ls->extra |= GW_ATTEN_NONE;
      break;
    case DECAY_INV:
      ls->extra |= GW_ATTEN_LINEAR;
      break;
    case DECAY_INVSQ:
      ls->extra |= GW_ATTEN_QUAD;
      break;
    }
    if (ls->useAtten) 
      ls->extra |= GW_ATTEN_END;

  }
  ls->shape = RECT_LIGHT;
  ls->aspect = GetAspect(t, valid);
  ls->overshoot = false;
  ls->shadow = true;
  ls->ambientOnly = false;
  ls->affectDiffuse = true;
  ls->affectSpecular = true;

  //if (type == OMNI_LIGHT)
  //  ls->type = OMNI_LGT;
  //else if (type == DIR_LIGHT || type == TDIR_LIGHT)
  //  ls->type = DIRECT_LGT;
  //else
  //  ls->type = SPOT_LGT;

  ls->type = SPOT_LGT;

  return REF_SUCCEED;
}

int SkyPortalCreateCallBack::proc(ViewExp * vpt, int msg, int point, int flags, IPoint2 m, Matrix3 & mat)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }


#ifdef _OSNAP
  if (msg == MOUSE_FREEMOVE)
  {
#ifdef _3D_CREATE
    vpt->SnapPreview(m, m, NULL, SNAP_IN_3D);
#else
    vpt->SnapPreview(m, m, NULL, SNAP_IN_PLANE);
#endif
  }
#endif

  if (msg == MOUSE_POINT || msg == MOUSE_MOVE)
  {

    // 6/19/01 11:37am --MQM-- wire-color changes
    // since we're now allowing the user to set the color of
    // the light wire-frames, we need to set the initial light 
    // color to yellow, instead of the default random object color.
    ULONG handle;
    INode * node;

    ob->NotifyDependents(FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE);
    node = GetCOREInterface()->GetINodeByHandle(handle);
    if (node)
    {
      Point3 color = GetUIColor(COLOR_LIGHT_OBJ);	// yellow wire color
      node->SetWireColor(RGB(color.x*255.0f, color.y*255.0f, color.z*255.0f));
    }

    switch (point)
    {
    case 0: // only happens with MOUSE_POINT msg
      p0 = vpt->SnapPoint(m, m, NULL, SNAP_IN_3D);
      mat.SetTrans(p0);
      ob->enable = true;
      break;
    case 1:
    {
      p1 = vpt->SnapPoint(m, m, NULL, SNAP_IN_PLANE);

      const float dragLength = p1.x - p0.x;
      const float dragWidth = p1.y - p0.y;

      if (abs(dragLength) > 1.0f && abs(dragWidth) > 1.0f)
      {
        ob->pblock->SetValue(PB_LENGTH, ob->iObjParams->GetTime(), abs(dragLength));
        ob->pblock->SetValue(PB_WIDTH, ob->iObjParams->GetTime(), abs(dragWidth));

        Point3 center;
        center.Set((float)p0.x + dragLength / 2.0f, (float)p0.y + dragWidth / 2.0f, (float)p0.z + (p1.z - p0.z) / 2.0f);
        mat.SetTrans(center);
        ob->enable = true;
      }

      if (msg == MOUSE_POINT)
        return CREATE_STOP;
      break;
    }
    case 2:
      return CREATE_STOP;
      break;
    }
  }
  else
  {
    if (msg == MOUSE_ABORT)
      return CREATE_ABORT;
  }

  return TRUE;
}

BOOL SkyPortal::IsSpot()
{
  return true;
}

BOOL SkyPortal::IsDir()
{
  return false;
}

void SkyPortal::SetUseLight(int onOff)
{
  SimpleLightUndo< &SkyPortal::SetUseLight, false >::Hold(this, onOff, useLight);
  useLight = onOff;
  if (currentEditLight == this)  // LAM - 8/13/02 - defect 511609
    UpdateUICheckbox(hSkyPortal, IDC_ON, _T("enabled"), onOff); // 5/15/01 11:00am --MQM-- maxscript fix
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}


void SkyPortal::SetSpotShape(int s)
{
  SimpleLightUndo< &SkyPortal::SetSpotShape, false >::Hold(this, s, shape);
  shape = s;

    if (currentEditLight == this) 
      UpdateUI(GetCOREInterface()->GetTime());

    NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}

int SkyPortal::GetSpotShape(void)
{
  return shape;
}

void SkyPortal::SetHotspot(TimeValue time, float f)
{
}

float SkyPortal::GetHotspot(TimeValue t, Interval & valid)
{
  return 175.0f;
}

void SkyPortal::SetFallsize(TimeValue time, float f)
{
}

float SkyPortal::GetFallsize(TimeValue t, Interval & valid)
{
  return 175.0f;
}

void SkyPortal::SetAtten(TimeValue time, int which, float f)
{
}

float SkyPortal::GetAtten(TimeValue t, int which, Interval & valid)
{
  float f;
  f = /*PB_ATTENSTART1*/ 7 + which; // whith - maybe it`s : 0 - not, 1 - inverse, 2 - inverse square
  return f;
}

void SkyPortal::SetTDist(TimeValue time, float f)
{
}

float SkyPortal::GetTDist(TimeValue t, Interval & valid)
{
  return 10.0f;
}

ObjLightDesc * SkyPortal::CreateLightDesc(INode * inode, BOOL forceShadowBuf)
{
  return new BaseObjLight(inode, forceShadowBuf);
  //return nullptr;
}

void SkyPortal::SetRGBColor(TimeValue t, Point3 & rgb)
{
  pblock->SetValue(PB_TINT, t, rgb);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

Point3 SkyPortal::GetRGBColor(TimeValue t, Interval & valid)
{
  Point3 rgb;
  pblock->GetValue(PB_TINT, t, rgb, valid);
  return rgb;
}

void SkyPortal::SetHSVColor(TimeValue t, Point3 & hsv)
{
}

Point3 SkyPortal::GetHSVColor(TimeValue t, Interval & valid)
{
  return Point3();
}

void SkyPortal::SetIntensity(TimeValue t, float f)
{
  pblock->SetValue(PB_MULTIPLIER, t, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float SkyPortal::GetIntensity(TimeValue t, Interval & valid)
{
  float f;
  pblock->GetValue(PB_MULTIPLIER, t, f, valid);
  return f;
}

void SkyPortal::SetContrast(TimeValue time, float f)
{
}

float SkyPortal::GetContrast(TimeValue t, Interval & valid)
{
  return 0.0f;
}

void SkyPortal::SetAspect(TimeValue t, float f)
{
}

float SkyPortal::GetAspect(TimeValue t, Interval & valid)
{
  return 1.0f;
}

void SkyPortal::SetConeDisplay(int s, int notify)
{
}

BOOL SkyPortal::GetConeDisplay(void)
{
  return false;
}


void SkyPortal::SetUseAtten(int s)
{
}

BOOL SkyPortal::GetUseAtten(void)
{
  return false;
}

void SkyPortal::SetAttenDisplay(int s)
{
}

BOOL SkyPortal::GetAttenDisplay(void)
{
  return false;
}

void SkyPortal::SetUseAttenNear(int s)
{
}

BOOL SkyPortal::GetUseAttenNear(void)
{
  return false;
}

void SkyPortal::SetAttenNearDisplay(int s)
{
}

BOOL SkyPortal::GetAttenNearDisplay(void)
{
  return false;
}


void SkyPortal::SetMapBias(TimeValue t, float f)
{
}

float SkyPortal::GetMapBias(TimeValue t, Interval & valid)
{
  return 1.0f;
}

void SkyPortal::SetMapRange(TimeValue t, float f)
{
}

float SkyPortal::GetMapRange(TimeValue t, Interval & valid)
{
  return 4.0f;
}

void SkyPortal::SetMapSize(TimeValue t, int f)
{
}

int SkyPortal::GetMapSize(TimeValue t, Interval & valid)
{
  return 0;
}

void SkyPortal::SetRayBias(TimeValue t, float f)
{
}

float SkyPortal::GetRayBias(TimeValue t, Interval & valid)
{
  return 0.0f;
}

int SkyPortal::GetUseGlobal()
{
  return 1;
}

void SkyPortal::SetUseGlobal(int a)
{
}


void SkyPortal::SetShadow(int a)
{
  SimpleLightUndo< &SkyPortal::SetShadow, true >::Hold(this, a, shadow);
  shadow = a;
}

static int ShadTypeFromGen(ShadowType *s) {
  if (s) {
    if (s->ClassID() == Class_ID(STD_SHADOW_MAP_CLASS_ID, 0))
      return 0;
    if (s->ClassID() == Class_ID(STD_RAYTRACE_SHADOW_CLASS_ID, 0))
      return 1;
    else
      return 0xFFFF;
  }
  return -1;
}

int SkyPortal::GetShadowType()
{
  return useGlobalShadowParams ? ShadTypeFromGen(GetCOREInterface()->GetGlobalShadowGenerator()) :
    ShadTypeFromGen(shadType);
}

void SkyPortal::SetShadowType(int a)
{
  ShadowType *s = a ? NewDefaultRayShadowType() : NewDefaultShadowMapType();
  if (useGlobalShadowParams) {
    GetCOREInterface()->SetGlobalShadowGenerator(s);
    globShadowType = a;
  }
  else {
    SetShadowGenerator(s);
    shadowType = a;
  }

  // 5/15/01 11:48am --MQM-- 
  //macroRec->SetSelProperty(_T("raytracedShadows"), mr_bool, a);
  //macroRec->Disable();
  //CheckRadioButton(hSkyPortal, IDC_SHADOW_MAPS, IDC_RAY_TRACED_SHADOWS, IDC_SHADOW_MAPS + GetShadowType());
  //macroRec->Enable();
}

int SkyPortal::GetAbsMapBias()
{
  return 0;
}

void SkyPortal::SetAbsMapBias(int a)
{
}

int SkyPortal::GetOvershoot()
{
  return 0;
}

void SkyPortal::SetOvershoot(int a)
{
}

ExclList & SkyPortal::GetExclusionList()
{
  // TODO: insert return statement here
  return exclList;
}

void SkyPortal::SetExclusionList(ExclList & list)
{
}

BOOL SkyPortal::SetHotSpotControl(Control * c)
{
  return false;
}

BOOL SkyPortal::SetFalloffControl(Control * c)
{
  return false;
}

BOOL SkyPortal::SetColorControl(Control * c)
{
  pblock->SetController(PB_TINT, c);
  return true;
}

Control * SkyPortal::GetHotSpotControl()
{
  return nullptr;
}

Control * SkyPortal::GetFalloffControl()
{
  return nullptr;
}

Control * SkyPortal::GetColorControl()
{
  return	pblock->GetController(PB_TINT);
}

void SkyPortal::BuildMeshes(bool isnew)
{
  if (isnew)
  {
    SetIntensity(TimeValue(0), dlgMultiplier);
    SetLength(TimeValue(0), dlgLength);
    SetWidth(TimeValue(0), dlgWidth);

    shape = dlgShape;
  }
  BuildRectMesh(TimeValue(0));
  mesh = &rectMesh;
}


#define ON_OFF_CHUNK 0x2580

IOResult SkyPortal::Load(ILoad *iload)
{
  ULONG nb;
  IOResult res;
  enable = true;

  while (IO_OK == (res = iload->OpenChunk())) 
  {
    switch (iload->CurChunkID()) 
    {
    case ON_OFF_CHUNK:
      res = iload->Read(&useLight, sizeof(useLight), &nb);
      break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }
  waitPostLoad++;
  iload->RegisterPostLoadCallback(new LightPostLoad(this));
  //iload->RegisterPostLoadCallback(new ParamBlockPLCB(emitterVersions, numEmitterVersions, &curEmitterVersion, this, EMITTER_REF));
  return IO_OK;

}

IOResult SkyPortal::Save(ISave *isave)
{
  ULONG nb;

  isave->BeginChunk(ON_OFF_CHUNK);
  isave->Write(&useLight, sizeof(useLight), &nb);
  isave->EndChunk();

  return IO_OK;
}



//MaxSDK::Graphics::Utilities::MeshEdgeKey LightMeshKey;

//bool SkyPortal::PrepareDisplay(const MaxSDK::Graphics::UpdateDisplayContext & prepareDisplayContext)
//{
//  LightMeshKey.SetFixedSize(false);
//  return true;
//}
//
//bool SkyPortal::UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext & updateDisplayContext, MaxSDK::Graphics::UpdateNodeContext & nodeContext, MaxSDK::Graphics::IRenderItemContainer & targetRenderItemContainer)
//{
//  INode* pNode = nodeContext.GetRenderNode().GetMaxNode();
//  MaxSDK::Graphics::Utilities::MeshEdgeRenderItem* pMeshItem = new MaxSDK::Graphics::Utilities::MeshEdgeRenderItem(mesh, false, false);
//  if (pNode->Dependent())
//    pMeshItem->SetColor(Color(ColorMan()->GetColor(kViewportShowDependencies)));
//  else if (pNode->Selected())
//    pMeshItem->SetColor(Color(GetSelColor()));
//  else if (pNode->IsFrozen())
//    pMeshItem->SetColor(Color(GetFreezeColor()));
//  else
//  {
//    if (useLight)
//    {
//      Color color = GetIViewportShadingMgr()->GetLightIconColor(*pNode);
//      pMeshItem->SetColor(color);
//    }
//    else
//    {
//      pMeshItem->SetColor(Color(0, 0, 0));
//    }
//  }
//
//  MaxSDK::Graphics::CustomRenderItemHandle meshHandle;
//  meshHandle.Initialize();
//  meshHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
//  meshHandle.SetCustomImplementation(pMeshItem);
//  MaxSDK::Graphics::ConsolidationData data;
//  data.Strategy = &MaxSDK::Graphics::Utilities::MeshEdgeConsolidationStrategy::GetInstance();
//  data.Key = &LightMeshKey;
//  meshHandle.SetConsolidationData(data);
//  targetRenderItemContainer.AddRenderItem(meshHandle);
//
//  return true;
//}


void SkyPortal::InitGeneralParamDialog(TimeValue t, IObjParam * ip, ULONG flags, Animatable * prev)
{
  hSkyPortal = ip->AddRollupPage(
    hInstance,
    /*type == DIR_LIGHT ? MAKEINTRESOURCE(IDD_DIRLIGHTPARAM) :*/
    // note:  there are two different dialogs depending on whether the user
    //			 is in create mode or not
    MAKEINTRESOURCE(IDD_PANEL),
    SkyPortalParamDialogProc,
    GetString(IDS_PARAMS),
    (LPARAM)this);

  ip->RegisterDlgWnd(hSkyPortal);


  multiplierSpin = GetISpinner(GetDlgItem(hSkyPortal, IDC_MULT_SPIN));
  multiplierSpin->SetLimits(0.0f, 1000.0f, FALSE);
  multiplierSpin->SetValue(GetIntensity(t), FALSE);
  multiplierSpin->SetScale(0.1f);
  multiplierSpin->LinkToEdit(GetDlgItem(hSkyPortal, IDC_MULT_EDIT), EDITTYPE_FLOAT);

  Point3 color;
  color = GetRGBColor(t);
  tintColorSwatch = GetIColorSwatch(GetDlgItem(hSkyPortal, IDC_TINT),
    RGB(FLto255i(color.x), FLto255i(color.y), FLto255i(color.z)), GetString(IDS_DS_TINT));
  tintColorSwatch->ForceDitherMode(true);

  lengthSpin = GetISpinner(GetDlgItem(hSkyPortal, IDC_LENGTH_SPIN));
  lengthSpin->SetLimits(0.0f, 1000.0f, FALSE);
  lengthSpin->SetValue(GetIntensity(t), FALSE);
  lengthSpin->SetScale(0.1f);
  lengthSpin->LinkToEdit(GetDlgItem(hSkyPortal, IDC_LENGTH_EDIT), EDITTYPE_FLOAT);

  widthSpin = GetISpinner(GetDlgItem(hSkyPortal, IDC_WIDTH_SPIN));
  widthSpin->SetLimits(0.0f, 1000.0f, FALSE);
  widthSpin->SetValue(GetIntensity(t), FALSE);
  widthSpin->SetScale(0.1f);
  widthSpin->LinkToEdit(GetDlgItem(hSkyPortal, IDC_WIDTH_EDIT), EDITTYPE_FLOAT);
}

void SkyPortal::UpdateControlledUI()
{
  //aszabo|Nov.09.01 - mimic the functionality in the parammaps
  //This will disable controls that are driven by scripted, wire, expression controllers

  // should exit from function if pblock does not exist.
  // leaving the DbgAssert as a warning
  // bug 313434
  DbgAssert(pblock != nullptr);
  if (pblock == nullptr)
    return;

  Control* c;

  if (multiplierSpin != nullptr)
  {
    c = pblock->GetController(PB_MULTIPLIER);
    if (c != nullptr)
      multiplierSpin->Enable(c->IsKeyable());
    else
      multiplierSpin->Enable(true);
  }

  if (lengthSpin != nullptr)
  {
    c = pblock->GetController(PB_LENGTH);
    if (c != nullptr)
      lengthSpin->Enable(c->IsKeyable());
    else
      lengthSpin->Enable(true);
  }

  if (widthSpin != nullptr)
  {
    c = pblock->GetController(PB_WIDTH);
    if (c != nullptr)
      widthSpin->Enable(c->IsKeyable());
    else
      widthSpin->Enable(true);
  }

  if (tintColorSwatch != nullptr)
  {
    c = pblock->GetController(PB_TINT);
    if (c != nullptr)
      tintColorSwatch->Enable(c->IsKeyable());
  }

  BuildRectMesh(TimeValue(0));
}



// From LightscapeLight

void SkyPortal::SetType(int type)
{
  type = AREA_TYPE;
}

void SkyPortal::SetType(const MCHAR * name)
{
  //name = "Free_Area";
}

int SkyPortal::Type()
{
  return AREA_TYPE; 
}

const MCHAR * SkyPortal::TypeName()
{
  return L"Free_Area";
}

void SkyPortal::SetDistribution(DistTypes dist)
{
  dist = DIFFUSE_DIST;
}

SkyPortal::DistTypes SkyPortal::GetDistribution() const
{
  return DIFFUSE_DIST;
}

void SkyPortal::SetIntensityAt(float f)
{
  f = 1.0f;
}

float SkyPortal::GetIntensityAt()
{
  return 1.0f;
}

void SkyPortal::SetIntensityType(IntensityType t)
{
  t = CANDELAS;
}

SkyPortal::IntensityType SkyPortal::GetIntensityType()
{
  return CANDELAS;
}

void SkyPortal::SetFlux(TimeValue t, float flux)
{
  flux = 12.566370f; // 4.0f * PI - Flux of an isotropic light is always 4PI times the intensity (4PI is the area of a unit sphere).
}

float SkyPortal::GetFlux(TimeValue t, Interval & valid) const
{
  return 12.566370f; // 4.0f * PI
}

void SkyPortal::SetRGBFilter(TimeValue t, Point3 & rgb)
{
  rgb.Set(1.0f, 1.0f, 1.0f);
}

Point3 SkyPortal::GetRGBFilter(TimeValue t, Interval & valid)
{
  return { 1.0f, 1.0f, 1.0f };
}

void SkyPortal::SetHSVFilter(TimeValue t, Point3 & hsv)
{
}

Point3 SkyPortal::GetHSVFilter(TimeValue t, Interval & valid)
{
  return Point3();
}

ShadowType * SkyPortal::ActiveShadowType()
{
  if (useGlobalShadowParams) {
    Interface *ip = GetCOREInterface();
    return ip->GetGlobalShadowGenerator();
  }
  else
    return shadType;
}

ShadowType * SkyPortal::GetShadowGenerator()
{
  return ActiveShadowType();
}

const MCHAR * SkyPortal::GetShadowGeneratorName()
{  
  return L"raytracedShadows"; // not sure what's right.
}

void SkyPortal::SetShadowGenerator(ShadowType * s)
{
  if (s == NULL)
    return;
  Interface *ip = GetCOREInterface();
  //if (theHold.Holding())
  //  theHold.Put(new ShadTypeRest(this, s));
  //theHold.Suspend();

  if (hSkyPortal && (currentEditLight == this)) { // LAM - 8/13/02 - defect 511609
    if (!inCreate)
      ip->DeleteSFXRollupPage();
    //shadParamDlg->DeleteThis();
  }
  if (useGlobalShadowParams)
    ip->SetGlobalShadowGenerator(s);
  else
    ReplaceReference(SHADTYPE_REF, s);
  if (hSkyPortal && (currentEditLight == this)) { // LAM - 8/13/02 - defect 511609

    //shadParamDlg = ActiveShadowType()->CreateShadowParamDlg(ip);
    if (!inCreate)
      ip->AddSFXRollupPage();
    //UpdtShadowTypeSel();
  }
  shadowType = ShadTypeFromGen(shadType);
  NotifyDependents(FOREVER, 0, REFMSG_SUBANIM_STRUCTURE_CHANGED);
  theHold.Resume();
}

void SkyPortal::SetShadowGenerator(const MCHAR * name)
{
}

void SkyPortal::SetUseShadowColorMap(TimeValue t, int onOff)
{
}

int SkyPortal::GetUseShadowColorMap(TimeValue t)
{
  return 0;
}

void SkyPortal::SetInclude(BOOL onOff)
{
}

void SkyPortal::UpdateTargDistance(TimeValue t, INode * inode)
{
}

BOOL SkyPortal::SetKelvinControl(Control * kelvin)
{
  return 0;
}

BOOL SkyPortal::SetFilterControl(Control * filter)
{
  return 0;
}

Control * SkyPortal::GetKelvinControl()
{
  return nullptr;
}

Control * SkyPortal::GetFilterControl()
{
  return nullptr;
}

float SkyPortal::GetKelvin(TimeValue t, Interval & v)
{
  return 6500.0f;
}

void SkyPortal::SetKelvin(TimeValue t, float kelvin)
{
  kelvin = 6500.0f;
}

BOOL SkyPortal::GetUseKelvin()
{
  return false;
}

void SkyPortal::SetUseKelvin(BOOL useKelvin)
{
  useKelvin = false;
}

MaxSDK::AssetManagement::AssetUser SkyPortal::GetWebFile() const
{
  return MaxSDK::AssetManagement::AssetUser();
}

const MCHAR * SkyPortal::GetFullWebFileName() const
{
  return nullptr;
}

void SkyPortal::SetWebFile(const MaxSDK::AssetManagement::AssetUser & file)
{
}

float SkyPortal::GetWebRotateX() const
{
  return 0.0f;
}

void SkyPortal::SetWebRotateX(float degrees)
{
}

float SkyPortal::GetWebRotateY() const
{
  return 0.0f;
}

void SkyPortal::SetWebRotateY(float degrees)
{
}

float SkyPortal::GetWebRotateZ() const
{
  return 0.0f;
}

void SkyPortal::SetWebRotateZ(float degrees)
{
}

float SkyPortal::GetDimmerValue(TimeValue t, Interval & valid) const
{
  return 0.0f;
}

void SkyPortal::SetDimmerValue(TimeValue t, float dimmer)
{
}

BOOL SkyPortal::GetUseMultiplier() const
{
  return true;
}

void SkyPortal::SetUseMultiplier(BOOL on)
{
  on = true;
}

BOOL SkyPortal::IsColorShiftEnabled() const
{
  return false;
}

void SkyPortal::EnableColorShift(BOOL on)
{
  on = false;
}

float SkyPortal::GetResultingIntensity(TimeValue t, Interval & valid) const
{
  float f;
  pblock->GetValue(PB_MULTIPLIER, t, f, valid);
  return f;
}

float SkyPortal::GetResultingFlux(TimeValue t, Interval & valid) const
{
  return GetResultingIntensity(t, valid) * GetFlux(t, valid);
}

Point3 SkyPortal::GetCenter() const
{
  return Point3();
}

float SkyPortal::GetRadius(TimeValue t, Interval & valid) const
{
  return 0.0f;
}

void SkyPortal::SetRadius(TimeValue t, float radius)
{
}

float SkyPortal::GetLength(TimeValue t, Interval & valid) const
{
  float f;
  pblock->GetValue(PB_LENGTH, t, f, valid);
  return f;
}

void SkyPortal::SetLength(TimeValue t, float f)
{
  pblock->SetValue(PB_LENGTH, t, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float SkyPortal::GetWidth(TimeValue t, Interval & valid) const
{
  float f;
  pblock->GetValue(PB_WIDTH, t, f, valid);
  return f;
}

void SkyPortal::SetWidth(TimeValue t, float f)
{
  pblock->SetValue(PB_WIDTH, t, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void SkyPortal::SetShape(int count, const Point3 * pointsIn)
{
  count = 4;
}

int SkyPortal::GetShape(Point3 * pointsOut, int bufSize) const
{
  bufSize = 4;

  float length, width;

  Interval valid = Interval(0, 0);
  pblock->GetValue(PB_LENGTH, TimeValue(0), length, valid);
  pblock->GetValue(PB_WIDTH, TimeValue(0), width, valid);

  const float halfLength = length / 2.0f;
  const float halfWidth = width / 2.0f;

  // Quad
  pointsOut[0].Set(-halfLength, -halfWidth, 0.0f);
  pointsOut[1].Set(halfLength, -halfWidth, 0.0f);
  pointsOut[2].Set(halfLength, halfWidth, 0.0f);
  pointsOut[3].Set(-halfLength, halfWidth, 0.0f);

  return bufSize;
}

float SkyPortal::GetOriginalFlux() const
{
  return 12.566370f; // 4.0f * PI
}

void SkyPortal::SetOriginalFlux(float flux)
{
  flux = 12.566370f; // 4.0f * PI
}

float SkyPortal::GetOriginalIntensity() const
{
  return 1.0f;
}

void SkyPortal::SetOriginalIntensity(float candelas)
{
  candelas = 1.0f;
}

SkyPortal::PresetLightColor SkyPortal::GetColorPreset() const
{
  return D65_ILLUMINANT_REFERENCE_WHITE;
}

void SkyPortal::SetColorPreset(PresetLightColor presetID)
{
  presetID = D65_ILLUMINANT_REFERENCE_WHITE;
}


//----------------------------------------------------------------------
//  Light descriptors for rendering
//----------------------------------------------------------------------

#define COS_45 0.7071067f
#define COS_45_2X 1.4142136f

static float stepFactor[] = { 50.0f,80.0f,140.0f };
#define MAXSTEPS 1000


BaseObjLight::BaseObjLight(INode *n, BOOL forceShadowBuf) : ObjLightDesc(n) 
{
  ObjectState os = n->EvalWorldState(TimeValue(0));
  assert(os.obj->SuperClassID() == LIGHT_CLASS_ID);
  sp = (os.obj->GetInterface(I_MAXSCRIPTPLUGIN) != NULL) ? (SkyPortal*)os.obj->GetReference(0) : (SkyPortal*)os.obj;  // JBW 4/7/99
  exclList = *(sp->GetExclList());
  forceShadowBuffer = forceShadowBuf;
  isDefaultShadowMap = ActiveShadowType()->SupportStdMapInterface();
}

static Color blackCol(0, 0, 0);

int BaseObjLight::Update(TimeValue t, const RendContext& rc, RenderGlobalContext * rgc, BOOL shadows, BOOL shadowGeomChanged)
{
  ObjLightDesc::Update(t, rc, rgc, shadows, shadowGeomChanged);
  intensCol = ls.intens*ls.color*rc.GlobalLightLevel();
  ObjectState os = inode->EvalWorldState(t);
  SkyPortal* lob = (SkyPortal*)os.obj;
  contrast = lob->GetContrast(t);
  diffSoft = lob->GetDiffuseSoft(t) / 100.0f;
  decayType = lob->GetDecayType();
  //shadColor= lob->GetShadColor(t)*lob->GetShadMult(t);
  shadColor = lob->GetShadColor(t);
  shadMult = lob->GetShadMult(t);
  ambOnly = lob->GetAmbientOnly();
  decayRadius = lob->GetDecayRadius(t);
  doingShadColor = (shadColor == Color(0, 0, 0)) ? 0 : 1;
  shadProjMap = lob->GetUseShadowColorMap(t) ? lob->GetShadowProjMap() : NULL;
  if (shadProjMap) {
    Interval v;
    shadProjMap->Update(t, v);
  }
  ltAfctShad = lob->GetLightAffectsShadow();
  ambOnly = false;
  float a = contrast / 204.0f + 0.5f; // so "a" varies from .5 to .99
  kA = (2.0f - 1.0f / a);
  kB = 1.0f - kA;
  diffSoften = true;
  //dontNeedShadBuf = (intensCol==Color(0,0,0))?1:0;   		

  // if light is black and don't have a shadowProjMap or a non-black shadow color,
  // then don't need a shadow buffer
  dontNeedShadBuf = ((intensCol == blackCol) && (!(shadProjMap || (!(shadColor == blackCol))))) ? 1 : 0;


  atmosShadows = /*lob->GetShadow()&&*/shadows&&lob->GetAtmosShadows(t);
  atmosOpacity = lob->GetAtmosOpacity(t);
  atmosColAmt = lob->GetAtmosColAmt(t);

  return 1;
}

void BaseObjLight::ApplyAtmosShadows(ShadeContext & sc, Point3 lpos, Color & color, float & shadAtten)
{
}

inline void BaseObjLight::ApplyShadowColor(Color & color, const float & shadAtten, const Color & scol)
{
}

inline void BaseObjLight::ApplyShadowColor(IlluminateComponents & illumComp, const float & shadAtten)
{
}

float BaseObjLight::ApplyDistanceAtten(float dist)
{
  float atten = 1.0f;
  if (ls.useNearAtten) {
    if (dist<ls.nearAttenStart)		// Beyond range 
      return 0;
    if (dist<ls.nearAttenEnd) {
      // in attenuated range
      float u = (dist - ls.nearAttenStart) / (ls.nearAttenEnd - ls.nearAttenStart);
      atten = u*u*(3 - 2 * u);  /* smooth cubic curve */
    }
  }
  if (ls.useAtten) {
    if (dist>ls.attenEnd)		/* Beyond range */
      return 0;
    if (dist>ls.attenStart) {
      /* Outside full-intensity range */
      float u = (ls.attenEnd - dist) / (ls.attenEnd - ls.attenStart);
      atten *= u*u*(3 - 2 * u);  /* smooth cubic curve */
    }
  }

  //	atten = Decay(atten,dist,ls.nearAttenEnd); // old way, changed by dan
  atten = Decay(atten, dist, decayRadius);
  return atten;
}

void BaseObjLight::ApplySpecularDiffuseAtten(Color & lightClr, IlluminateComponents & illumComp)
{
  Color black(0, 0, 0);

  illumComp.lightAmbientColor = black;
  illumComp.ambientAtten = 0.0f;
  illumComp.lightDiffuseColor = lightClr;
  illumComp.diffuseAtten = 1.0f;
  illumComp.lightSpecularColor = lightClr;
  illumComp.specularAtten = 1.0f;
}

void BaseObjLight::TraverseVolume(ShadeContext & sc, const Ray & ray, int samples, float tStop, float attenStart, float attenEnd, DWORD flags, LightRayTraversal * proc)
{
}

int BaseObjLight::IntersectRayMultiple(Ray & ray, float t0, float t1, float * t)
{
  t[0] = t0;
  t[1] = t1;
  return 2;
}

BOOL BaseObjLight::UseAtten()
{
  return 0;
}

int BaseObjLight::LightType()
{
  return 0;
}

ShadowType * BaseObjLight::ActiveShadowType()
{
  //if (forceShadowBuffer) 
  //{
  //  if (sp->ASshadType == nullptr) 
  //  {
  //    if (sp->shadType->SupportStdMapInterface())
  //      sp->ReplaceReference(AS_SHADTYPE_REF, sp->shadType);
  //    else
  //      sp->ReplaceReference(AS_SHADTYPE_REF, NewDefaultShadowMapType());
  //  }
  //  return sp->ASshadType;
  //}
  return sp->ActiveShadowType();
}

//void ShadTypeRest::Restore(int isUndo) {
//  theHold.Suspend();
//  theLight->SetShadowGenerator(oldtype);
//  theHold.Resume();
//}
//
//void ShadTypeRest::Redo() {
//  theHold.Suspend();
//  theLight->SetShadowGenerator(newtype);
//  theHold.Resume();
//}


//SpotLight::SpotLight(INode * inode, BOOL forceShadowBuf)
//{
//  shadow = GetMarketDefaults()->GetInt(LIGHT_CLASS_ID, Class_ID(OMNI_LIGHT_CLASS_ID, 0), _T("CastShadows"), 0) != 0;
//
//  rect = overshoot = shadowRay = FALSE;
//  shadGen = nullptr;
//
//  if (sp->GetShadow())
//    shadGen = ActiveShadowType()->CreateShadowGenerator(sp, this, 0);
//
//  //	projMap = NULL;
//}
//
//#define MAX(a,b) (((a)>(b))?(a):(b))
//
//int SpotLight::Update(TimeValue t, const RendContext & rc, RenderGlobalContext * rgc, BOOL shadows, BOOL shadowGeomChanged)
//{
//  int res = 1;
//  BaseObjLight::Update(t, rc, rgc, shadows, shadowGeomChanged);
//
//  float hs = DegToRad(ls.hotsize);
//  float fs = DegToRad(ls.fallsize);
//  fall_tan = (float)tan(fs / 2.0f);
//  hot_cos = (float)cos(hs / 2.0f);
//  fall_cos = (float)cos(fs / 2.0f);
//  fall_sin = (float)sin(fs / 2.0f);
//  hotpct = ls.hotsize / ls.fallsize;
//  ihotpct = 1.0f - hotpct;
//
//  ObjectState os = inode->EvalWorldState(t);
//  LightObject* lob = (LightObject *)os.obj;
//  assert(os.obj->SuperClassID() == LIGHT_CLASS_ID);
//  SkyPortal* sp = (lob->GetInterface(I_MAXSCRIPTPLUGIN) != NULL) ? (SkyPortal*)lob->GetReference(0) : (SkyPortal*)lob;  // JBW 4/7/99
//
//                                                                                                                                 //bank = ls.bank;
//  rect = sp->GetSpotShape() == RECT_LIGHT ? 1 : 0;
//  overshoot = sp->GetOvershoot();
//  shadow = sp->GetShadow()&shadows;
//  if (ambOnly) shadow = FALSE;
//  shadowRay = sp->GetShadowType();
//  projector = sp->GetProjector();
//  fov = MAX(fs, hs);
//
//  float aspect = rect ? sp->GetAspect(t) : 1.0f;
//  shadsize = ActiveShadowType()->MapSize(t);
//  if (shadsize <= 0) shadsize = 10;
//  sz2 = .5f*(float)shadsize;
//
//  fov = 2.0f* (float)atan(tan(fov*0.5f)*sqrt(aspect));
//  zfac = -sz2 / (float)tan(0.5*(double)fov);
//  xscale = zfac;
//  yscale = -zfac*aspect;
//  curve = (float)fabs(1.0f / xscale);
//
//  //rectv0.y = float(tan(fs*0.5) * sqrt(aspect));
//  //rectv1.y = rectv0.y/aspect;
//  rectv0.y = fall_sin * (float)sqrt(aspect);
//  rectv1.y = fall_sin / (float)sqrt(aspect);
//
//  rectv0.x = rectv1.x = fall_cos;
//  rectv0 = Normalize(rectv0);
//  rectv1 = Normalize(rectv1);
//
//  Interval v;
//
//  if (shadow)
//  {
//    if (dontNeedShadBuf)
//    {
//      FreeShadGenBuffers();
//    }
//    else
//    {
//      float clipDist = ls.useAtten ? ls.attenEnd : DONT_CLIP;
//      if (shadGen && shadowGeomChanged) // mjm - 06.08.00 - added check for non-NULL shadGen
//        res = shadGen->Update(t, rc, rgc, lightToWorld, aspect, fov, clipDist);
//    }
//  }
//  shadow = shadow&&shadGen;
//  if (dontNeedShadBuf) shadow = FALSE;
//  return res;
//}
//
//int SpotLight::UpdateViewDepParams(const Matrix3 & worldToCam)
//{
//  BaseObjLight::UpdateViewDepParams(worldToCam);
//  // get light direction in cam space
//  lightDir = -FNormalize(lightToCam.GetRow(2));
//  if (shadGen)
//    shadGen->UpdateViewDepParams(worldToCam);
//  return 1;
//}
//
//BOOL SpotLight::Illuminate(ShadeContext & sc, Point3 & normal, Color & color, Point3 & dir, float & dot_nl, float & diffuseCoef)
//{
//  if (ls.intens == 0.0f)
//    return FALSE;
//
//  /* SPOTlight : compute light value */
//  float atten, angle, conelimit, dist;
//  Point3 p = sc.P();
//  Point3 L = lightPos - p;  // vector from light to p in camera space
//  Point3 plt, LL;
//
//  if (!ambOnly) {
//    // quick test for light behind surface
//    if (DotProd(normal, L) <= 0.0f)
//      return 0;
//  }
//  plt = camToLight * p;
//  dist = FLength(plt);
//  if (dist == 0.0f) return 0;
//  LL = -plt / dist;
//  atten = 1.0f;
//
//  // Handle light distance attenuation 
//  if (ls.useNearAtten) {
//    if (dist<ls.nearAttenStart)		// Beyond range 
//      return 0;
//    if (dist<ls.nearAttenEnd) {
//      // in attenuated range
//      float u = (dist - ls.nearAttenStart) / (ls.nearAttenEnd - ls.nearAttenStart);
//      atten = u*u*(3 - 2 * u);  /* smooth cubic curve */
//    }
//  }
//
//  if (ls.useAtten) {
//    if (dist>ls.attenEnd)		// Beyond range 
//      return 0;
//    if (dist>ls.attenStart) {
//      /* Outside full-intensity range */
//      float u = (ls.attenEnd - dist) / (ls.attenEnd - ls.attenStart);
//      atten *= u*u*(3 - 2 * u);  // smooth cubic curve 
//    }
//  }
//
//  //	atten = Decay(atten,dist,ls.nearAttenEnd);
//  atten = Decay(atten, dist, decayRadius);
//
//  conelimit = rect ? (fall_cos / COS_45_2X) : fall_cos;
//  color = intensCol;
//
//  L = FNormalize(L);
//
//  if (ambOnly) {
//    dot_nl = diffuseCoef = 1.0f;
//  }
//  else {
//    if (uniformScale) {
//      dot_nl = DotProd(normal, L);
//    }
//    else {
//      Point3	N = FNormalize(VectorTransform(camToLight, normal));
//      // Is light is in front of surface? 
//      if ((dot_nl = DotProd(N, LL)) <= 0.0f)
//        return 0;
//    }
//    diffuseCoef = ContrastFunc(dot_nl);
//  }
//  float shadAtten = 1.0f;
//
//  // Is point in light cone? 
//  if ((angle = LL.z) < conelimit) {
//    if (!overshoot)
//      return 0;
//    goto overshoot_bypass;
//  }
//
//  atten *= SampleMaps(sc, p, normal, plt, color, shadAtten);
//  if ((!rect) && (!overshoot) && (angle < hot_cos)) {
//    float u = (angle - fall_cos) / (hot_cos - fall_cos);
//    atten *= u*u*(3.0f - 2.0f*u);  // smooth cubic curve 
//  }
//overshoot_bypass:
//  if (atten == 0.0f)
//    return 0;
//  dir = L;  // direction in camera space
//
//            // ATMOSPHERIC SHADOWS
//  if (sp->GetShadow() && atmosShadows)
//    ApplyAtmosShadows(sc, lightPos, color, shadAtten);
//
//  if (shadAtten != 1.0f) {
//    Color scol = shadColor;
//    if (shadProjMap) {
//      float x = xscale*plt.x / plt.z + sz2;
//      float y = yscale*plt.y / plt.z + sz2;
//      scol = SampleProjMap(sc, plt, sc.DP(), x, y, shadProjMap);
//    }
//    ApplyShadowColor(color, shadAtten, scol);
//  }
//  if (atten != 1.0f) color *= atten;
//  return 1;
//}
//
//BOOL SpotLight::Illuminate(ShadeContext & sc, Point3 & N, IlluminateComponents & illumComp)
//{
//  if (ls.intens == 0.0f)
//    return FALSE;
//
//  /* SPOTlight : compute light value */
//  float atten, angle, conelimit, dist;
//  Point3 p = sc.P();
//  Point3 L = lightPos - p;  // vector from light to p in camera space
//  Point3 plt, LL;
//
//  if (!ambOnly) {
//    // quick test for light behind surface
//    if (DotProd(N, L) <= 0.0f)
//      return 0;
//  }
//  // prepare point & dist in light space
//  plt = camToLight * p;
//  dist = FLength(plt);
//  if (dist == 0.0f) return 0;
//  LL = -plt / dist;  // normalized lightspace L
//
//                     // get the distance atten
//  atten = illumComp.distanceAtten = ApplyDistanceAtten(dist);
//
//  conelimit = rect ? (fall_cos / COS_45_2X) : fall_cos;
//  Color color = intensCol;
//
//
//  // > 11/11/02 - 6:19pm --MQM-- do same thing as omni for "rawColor"
//  Color c2 = intensCol;
//  //if (sp->projMap && projector)
//  //{
//  //  float x = xscale * plt.x / plt.z + sz2;
//  //  float y = yscale * plt.y / plt.z + sz2;
//  //  c2 *= SampleProjMap(sc, plt, sc.DP(), x, y, gl->projMap);
//  //}
//  illumComp.rawColor = c2;
//
//
//  L = FNormalize(L);
//
//  if (ambOnly) {
//    illumComp.NL = illumComp.geometricAtten = 1.0f;
//  }
//  else {
//    if (uniformScale) {
//      illumComp.NL = DotProd(N, L);
//    }
//    else {
//      Point3	Nl = FNormalize(VectorTransform(camToLight, N));
//      // Is light is in front of surface? 
//      if ((illumComp.NL = DotProd(Nl, LL)) <= 0.0f)
//        return 0;
//    }
//    illumComp.geometricAtten = ContrastFunc(illumComp.NL);
//  }
//
//  float shadAtten = 1.0f;
//  illumComp.shapeAtten = 1.0f;
//
//  // Is point in light cone? 
//  if ((angle = LL.z) < conelimit) {
//    if (!overshoot)
//      return 0;
//    goto overshoot_bypass;
//  }
//
//  // is there a shadow or projector map?
//  //if (shadow || rect || (sp->projMap&&projector)) {
//    atten *= SampleMaps(sc, p, N, plt, color, shadAtten, &illumComp);
////  }
//
//  // handle the cone falloff
//  if ((!rect) && (!overshoot) && (angle < hot_cos)) {
//    float u = (angle - fall_cos) / (hot_cos - fall_cos);
//    illumComp.shapeAtten = u*u*(3.0f - 2.0f*u);  // smooth cubic curve 
//    atten *= illumComp.shapeAtten;
//  }
//
//overshoot_bypass:
//  if (atten == 0.0f)
//    return 0;
//
//  illumComp.L = L;  // direction in camera space
//
//                    // ATMOSPHERIC SHADOWS
//  if (sp->GetShadow() && atmosShadows)
//    ApplyAtmosShadows(sc, lightPos, color, shadAtten);
//
//  // > 11/11/02 - 6:06pm --MQM-- take this out...mimic omni (look about 30 lines above too)
//  illumComp.filteredColor = color;
//  //	illumComp.rawColor = illumComp.filteredColor = color; // never any filtering for shadow maps
//
//  if (shadAtten != 1.0f) {
//    illumComp.shadowColor = shadColor;
//    illumComp.shadowAtten = shadAtten;
//    if (shadProjMap) {
//      float x = xscale * plt.x / plt.z + sz2;
//      float y = yscale * plt.y / plt.z + sz2;
//      illumComp.shadowColor = SampleProjMap(sc, plt, sc.DP(), x, y, shadProjMap);
//    }
//    ApplyShadowColor(illumComp, shadAtten);
//
//  }
//  else {
//    illumComp.shadowColor.Black();
//    illumComp.shadowAtten = 1.0f;
//    illumComp.lightDiffuseColor = illumComp.finalColor = illumComp.finalColorNS = color;
//  }
//
//  if (atten != 1.0f) {
//    illumComp.finalColor *= atten;
//    illumComp.finalColorNS *= atten;
//    illumComp.lightDiffuseColor *= atten;
//    illumComp.shadowColor *= atten;
//  }
//
//  ApplySpecularDiffuseAtten(illumComp.lightDiffuseColor, illumComp);
//  return 1;
//}
//
//float SpotLight::SampleMaps(ShadeContext & sc, Point3 pin, Point3 norm, Point3 plt, Color & lcol, float & shadAtten, IlluminateComponents * pIllumComp)
//{
//  float mx, my, x, y, atten;
//  Point3 pout, n;
//  Color col;
//  atten = 1.0f;
//  if (plt.z<0.0f) {
//    mx = xscale*plt.x / plt.z;
//    my = yscale*plt.y / plt.z;
//    x = sz2 + mx;
//    y = sz2 + my;
//
//    /* If it falls outside the rectangle, bail out */
//    if ((float)fabs(mx)>sz2 || (float)fabs(my)>sz2) {
//      if (pIllumComp) {
//        pIllumComp->rawColor = pIllumComp->filteredColor = lcol;
//        pIllumComp->shapeAtten = (overshoot) ? 1.0f : 0.0f;
//      }
//      if (overshoot)
//        return(1.0f);	/* No attenuation */
//      else
//        return(0.0f);
//    }
//
//    //if (gl->projMap&&projector)
//    //  lcol *= SampleProjMap(sc, plt, sc.DP(), x, y, gl->projMap);
//
//    if (pIllumComp) pIllumComp->rawColor = lcol;
//
//    if (sc.shadow&&shadow) {
//      if (isDefaultShadowMap) {
//        Point3 n = VectorTransform(camToLight, norm);
//        float k = plt.z*plt.z / (zfac*DotProd(n, plt));
//        shadAtten = shadGen->Sample(sc, x, y, plt.z, -n.x*k, n.y*k);
//        if (shadAtten == 0.0f) return atten;
//      }
//      else {
//        shadAtten = shadGen->Sample(sc, norm, lcol);
//        if (shadAtten == 0.0f) return atten;
//      }
//    }
//    if (pIllumComp) pIllumComp->filteredColor = lcol;
//
//    // Calculate rectangular dropoff if outside the hotspot area 
//    if (rect && !(overshoot)) {
//      float u = 0.0f;
//      float ux = (float)fabs(mx) / sz2;
//      float uy = (float)fabs(my) / sz2;
//      int inflag = 0;
//      if (ux>hotpct) {
//        inflag = 1;
//        ux = (ux - hotpct) / ihotpct;
//      }
//      if (uy>hotpct) {
//        inflag |= 2;
//        uy = (uy - hotpct) / ihotpct;
//      }
//      switch (inflag) {
//      case 1:	u = 1.0f - ux;			break;
//      case 2:	u = 1.0f - uy;			break;
//      case 3:	u = (1.0f - ux)*(1.0f - uy); break;
//      case 0:	goto noAtten;
//      }
//      float a = u*u*(3.0f - 2.0f*u);  /* smooth cubic curve */
//      if (pIllumComp) pIllumComp->shapeAtten = a;
//      atten *= a;  /* smooth cubic curve */
//    }
//  }
//noAtten:
//  return(atten);
//}
//
//#define Dx (ray.dir.x)
//#define Dy (ray.dir.y)
//#define Dz (ray.dir.z)
//#define Px (ray.p.x)
//#define Py (ray.p.y)
//#define Pz (ray.p.z)
//
//BOOL SpotLight::IntersectCone(Ray & ray, float tanAng, float & t0, float & t1)
//{
//  double A, Dx2, Dy2, Dz2, den, k, root, z0, z1;
//
//  Dx2 = Dx*Dx;
//  Dy2 = Dy*Dy;
//  Dz2 = Dz*Dz;
//  A = tanAng*tanAng;
//
//  den = 2.0f*(Dx2 + Dy2 - A*Dz2);
//  if (den == 0.0f) return FALSE;
//
//  k = -2.0f*Dx*Px - 2.0f*Dy*Py + 2.0f*A*Dz*Pz;
//  root = k*k - 2.0f*den*(Px*Px + Py*Py - A*Pz*Pz);
//  if (root<0.0f) return FALSE;
//  root = (float)sqrt(root);
//
//  t0 = (float)((k - root) / den);
//  t1 = (float)((k + root) / den);
//  if (t0 > t1) { float temp = t0; t0 = t1; t1 = temp; }
//
//  // t0 and t1 may be the iintersection with the reflected cone in the
//  // positive Z direction.
//  z0 = ray.p.z + ray.dir.z*t0;
//  z1 = ray.p.z + ray.dir.z*t1;
//  BOOL out0 = z0 > 0.0f ? TRUE : FALSE;
//  BOOL out1 = z1 > 0.0f ? TRUE : FALSE;
//  if (out0 && out1) {
//    return FALSE;
//  }
//
//  if (out0) {
//    // Must be in the reflected cone, shooting out and hitting the real cone.
//
//    // Do an extra check to make sure we really are inside the
//    // cone and it's not just round-off.
//    if (t0>0) {
//      FLOAT l = (Px*Px + Py*Py) / A;
//      if (Pz*Pz < l) return FALSE;
//    }
//
//    t0 = t1;
//    t1 = float(1.0E30);
//  }
//
//  if (out1) {
//    // Must be inside the cone, shooting out and hitting the reflected cone.
//    t1 = t0;
//    t0 = 0.0f;
//  }
//
//  return TRUE;
//}
//
//static BOOL IntersectConeSide(Ray &ray, int X, int Y, float cs, float sn, float tn, float &t)
//{
//  Point2 p(ray.p[X], ray.p.z), v(cs, sn), c(ray.dir[X], ray.dir.z);
//  float cv = -c.DotProd(v);
//  if (cv != 0.0f) {
//    float pv = p.DotProd(v);
//    t = pv / cv;
//    if (t>0.0f) {
//      Point3 pt = ray.p + ray.dir*t;
//      if (pt.z<0.0f) {
//        if ((float)fabs(pt[Y] / pt.z) <= tn) {
//          return TRUE;
//        }
//      }
//    }
//  }
//  return FALSE;
//}
//
//BOOL SpotLight::IntersectRectCone(Ray & ray, float & t0, float & t1)
//{
//  float sn0 = rectv0.y;
//  float sn1 = rectv1.y;
//  float cs0 = rectv0.x;
//  float cs1 = rectv1.x;
//  float tn0 = (float)fabs(sn0 / cs0);
//  float tn1 = (float)fabs(sn1 / cs1);
//  int hit = 0;
//  float t;
//  BOOL in = FALSE;
//
//  if (IntersectConeSide(ray, 0, 1, cs0, sn0, tn1, t)) {
//    t0 = t1 = t;
//    hit++;
//  }
//
//  if (IntersectConeSide(ray, 1, 0, cs1, sn1, tn0, t)) {
//    if (hit) {
//      if (t<t0) t0 = t;
//      if (t>t1) t1 = t;
//    }
//    else {
//      t0 = t1 = t;
//    }
//    hit++;
//  }
//
//  if (IntersectConeSide(ray, 0, 1, -cs0, sn0, tn1, t)) {
//    if (hit) {
//      if (t<t0) t0 = t;
//      if (t>t1) t1 = t;
//    }
//    else {
//      t0 = t1 = t;
//    }
//    hit++;
//  }
//
//  if (IntersectConeSide(ray, 1, 0, cs1, -sn1, tn0, t)) {
//    if (hit) {
//      if (t<t0) t0 = t;
//      if (t>t1) t1 = t;
//    }
//    else {
//      t0 = t1 = t;
//    }
//    hit++;
//  }
//
//  float xz = float(fabs(ray.p.x) / fabs(ray.p.z));
//  float yz = float(fabs(ray.p.y) / fabs(ray.p.z));
//  if (ray.p.z < 0.0f && xz<tn0 && yz<tn1) {
//    in = TRUE;
//  }
//
//  if (hit == 0) {
//    if (in) {
//      t0 = 0.0f;
//      t1 = float(1.0E30);
//      return TRUE;
//    }
//    else {
//      return FALSE;
//    }
//  }
//
//  if (hit == 1) {
//    if (in) t0 = 0.0f;
//    else t1 = float(1.0E30);
//  }
//
//  return TRUE;
//}
//
//BOOL SpotLight::IntersectRay(Ray & ray, float & t0, float & t1, AttenRanges & ranges)
//{
//  BOOL res = rect ? IntersectRectCone(ray, t0, t1) : IntersectCone(ray, fall_tan, t0, t1);
//  if (ls.useAtten && (ray.dir.z != 0.0f)) {
//
//    // intersect ray with plane at z = -ranges.aFarEnd;
//    float zf = -ranges.aEnd;
//    float t = (zf - ray.p.z) / ray.dir.z;
//    if (ray.p.z<zf) {
//      // p beyond far end plane
//      if (ray.dir.z>0) {
//        // ray pointing toward light
//        if (t>t0)
//          t0 = t;
//      }
//    }
//    if (ray.p.z>zf) {
//      // p on same side of far end plane as light
//      if (ray.dir.z<0) {
//        // ray pointing away from light, may hit far end plane
//        if (t<t1)
//          t1 = t;
//      }
//    }
//  }
//  return res;
//}
//
//Color SpotLight::AttenuateIllum(ShadeContext & sc, Point3 p, Color & colStep, Point3 & dp, int filt, float ldp, float & distAtten, AttenRanges & ranges)
//{
//  float mx, my, x, y;
//  float atten = 1.0f, cosang;
//  float lv = FLength(p);
//  if (lv == 0.0f)
//    return colStep;
//
//  // Handle light distance attenuation 
//  distAtten = 1.0f;
//  if (ls.useNearAtten) {
//    if (lv<ranges.aNearStart) { // beyond range
//      distAtten = 0.0f;
//      return Color(0.0f, 0.0f, 0.0f);
//    }
//    if (lv<ranges.aNearEnd) {
//      // in attenuated range
//      float u = (lv - ranges.aNearStart) / (ranges.aNearEnd - ranges.aNearStart);
//      atten = distAtten = u*u*(3 - 2 * u);  // smooth cubic curve 
//    }
//  }
//  if (ls.useAtten) {
//    if (lv>ranges.aEnd) {		// Beyond range
//      distAtten = 0.0f;
//      return Color(0, 0, 0);
//    }
//    if (lv>ranges.aStart) {
//      // in attenuated range
//      float u = (ranges.aEnd - lv) / (ranges.aEnd - ranges.aStart);
//      atten = distAtten *= u*u*(3 - 2 * u); /* smooth cubic curve */
//    }
//  }
//
//  //	atten = Decay(atten, lv, ranges.aNearEnd);
//  atten = Decay(atten, lv, ranges.decayRadius);
//
//  cosang = (float)fabs(p.z / lv);
//  if ((!rect) && (!overshoot) && (cosang<hot_cos) && (hot_cos != fall_cos)) {
//    if (cosang<fall_cos) {
//      // RB: point is outside of falloff cone
//      return Color(0.0f, 0.0f, 0.0f);
//    }
//    float u = (cosang - fall_cos) / (hot_cos - fall_cos);
//    atten *= u*u*(3.0f - 2.0f*u);  /* smooth cubic curve */
//  }
//
//  // ---DS -- 4/8/96 added ||projMap
//
//  if ((p.z<0.0f && (sc.shadow && shadow)) || rect) {
//    mx = xscale*p.x / p.z;
//    my = yscale*p.y / p.z;
//    x = sz2 + mx;
//    y = sz2 + my;
//
//    if (sc.shadow && shadow) {
//      if (filt) {
//        if (filt == 1) {
//          atten *= shadGen->FiltSample(int(x), int(y), p.z, filt);
//        }
//        else if (filt == 2) {
//          Point3 p2 = p + dp;
//          if (p2.z<0.0f) {
//            int x2 = int(sz2 + xscale*p2.x / p2.z);
//            int y2 = int(sz2 + yscale*p2.y / p2.z);
//            atten *= shadGen->LineSample(
//              int(x), int(y), p.z, x2, y2, p2.z);
//          }
//        }
//        else if (filt == 3) {
//          // Use sample range
//          atten *= shadGen->Sample(sc, x, y, p.z, 0.0f, 0.0f);
//        }
//        if (atten == 0.0f) return Color(0.0f, 0.0f, 0.0f);
//      }
//      else {
//        if (!shadGen->QuickSample(int(x), int(y), p.z)) {
//          return Color(0.0f, 0.0f, 0.0f);
//        }
//      }
//    }
//    //if (gl->projMap&&projector) {
//    //  Point3 pcam = lightToCam * p;
//    //  float d = sc.ProjType() ? sc.RayDiam() : sc.RayConeAngle()*fabs(pcam.z);
//    //  AColor pc = SampleProjMap(sc, p, Point3(d, d, d), x, y, gl->projMap);
//    //  return atten*colStep*Color(pc.r, pc.g, pc.b);
//    //}
//
//
//    if (rect && !(overshoot)) {
//      float u = 0.0f;
//      float ux = (float)fabs(mx) / sz2;
//      float uy = (float)fabs(my) / sz2;
//      if (ux>1.0f || uy>1.0f) {
//        atten = 0.0f;
//        goto skipAtten;
//      }
//      int inflag = 0;
//      if (ux>hotpct) {
//        inflag = 1;
//        ux = (ux - hotpct) / ihotpct;
//      }
//      if (uy>hotpct) {
//        inflag |= 2;
//        uy = (uy - hotpct) / ihotpct;
//      }
//      switch (inflag) {
//      case 1:	u = 1.0f - ux;			break;
//      case 2:	u = 1.0f - uy;			break;
//      case 3:	u = (1.0f - ux)*(1.0f - uy); break;
//      case 0:	goto skipAtten;
//      }
//      atten *= u*u*(3.0f - 2.0f*u);  /* smooth cubic curve */
//    }
//
//  }
//
//
//skipAtten:
//  return colStep * atten;
//}
//
//AColor SpotLight::SampleProjMap(ShadeContext & sc, Point3 plt, Point3 dp, float x, float y, Texmap * map)
//{
//  //SCLight scspot;
//  //float ifs = 1.0f / (float)shadsize;
//  //scspot.origsc = &sc;
//  //scspot.projType = 0; // perspective
//  //scspot.curtime = sc.CurTime();
//  //scspot.curve = (float)fabs(1.0f / xscale);
//  //scspot.ltPos = plt;
//  //scspot.view = FNormalize(plt);
//  //scspot.uv.x = x*ifs;
//  //scspot.uv.y = 1.0f - y*ifs;
//
//  //scspot.scrpos.x = (int)(x + 0.5);
//  //scspot.scrpos.y = (int)(y + 0.5);
//  //scspot.filterMaps = sc.filterMaps;
//  //scspot.mtlNum = sc.mtlNum;
//
//  //// antialiasing for 2D textures:
//  //float d = MaxAbs(dp.x, dp.y, dp.z);
//
//  //float duv = 0.0f;
//  //if (plt.z != 0.0f)
//  //  duv = fabs(xscale*d*ifs / plt.z);
//  //if (duv>.9f) duv = .9f;
//  //scspot.duv.x = scspot.duv.y = duv;
//
//  //// clip 
//  //float hd = duv*0.5f;
//  //if (scspot.uv.x + hd >0.999999) scspot.uv.x = .9999999 - hd;
//  //if (scspot.uv.x - hd <0.0f) scspot.uv.x = hd;
//  //if (scspot.uv.y + hd >0.999999) scspot.uv.y = .9999999 - hd;
//  //if (scspot.uv.y - hd <0.0f) scspot.uv.y = hd;
//
//  //// antialiasing for 3D textures: 
//  //scspot.dp = Point3(d, d, d);
//
//  //return map->EvalColor(scspot);
//  return{ 1, 1, 1, 0 };
//}
//
//BOOL SpotLight::IsFacingLight(Point3 & dir)
//{
//  return dir.z>0.0f;
//}
