#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

//#include "resource.h"
//#include <istdplug.h>
//#include <iparamb2.h>
//#include <iparamm2.h>
//#include <maxtypes.h>
//SIMPLE TYPE
//#include <mouseman.h>    
#include "prim.h"        // on
//#include "target.h"
//#include "hsv.h"
//#include "render.h"
#include "shadgen.h"    // on
#include "notify.h"     // on
//#include "genlight.h"
//#include <bmmlib.h>
#include "macrorec.h"    // on
#include "decomp.h"      // on
#include <marketDefaults.h>  // on
//#include <INodeGIProperties.h>
//#include <INodeShadingProperties.h>
#include <IViewportShadingMgr.h>        // on
#include <Graphics/Utilities/MeshEdgeRenderItem.h>  // on
#include <Graphics/Utilities/SplineRenderItem.h>
#include <Graphics/CustomRenderItemHandle.h>
//#include <Graphics/RenderNodeHandle.h>
//#include "MouseCursors.h"
#include "3dsmaxport.h" // on
#include <lslights.h> // on

////////////////////////////////////////////////////////////////////////////////////

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HydraSkyPortal_CLASS_ID	Class_ID(0x69729747, 0x8c40aaf7)

#define PBLOCK_REF	0
#define SHADTYPE_REF 	1

#define RELEASE_SPIN(x)  if (sp->x) { ReleaseISpinner(sp->x); sp->x = nullptr;}
#define RELEASE_COLSW(x) if (sp->x) { ReleaseIColorSwatch(sp->x); sp->x = nullptr;}
#define RELEASE_BUT(x)   if (sp->x) { ReleaseICustButton(sp->x); sp->x = nullptr;}


//from 3dswin\src\maxsdk\samples\systems\sunlight\sunclass.h
// The unique 32-bit Class IDs of the slave controller
#define USE_DLG_COLOR

#define WM_SET_TYPE		WM_USER + 0x04002
#define WM_SET_SHADTYPE		WM_USER + 0x04003

#define OMNI_MESH		0
#define DIR_MESH		1


static int waitPostLoad = 0;

#define FLto255i(x)	((int)((x)*255.0f))
#define FLto255f(x)	((float)((x)*255.0f))


class SkyPortal : public LightscapeLight {
public:
  friend class 	GeneralLightCreateCallBack;
  friend class 	LightPostLoad;
  friend class 	TSpotLightClassDesc;
  friend class 	ProjMapDADMgr;
  friend class 	SetTypeRest;
  friend class 	BaseObjLight;
  friend class 	SpotLight;
  friend class  LightConeItem;
  friend class  LightTargetLineItem;

  friend INT_PTR CALLBACK  SkyPortalParamDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

  friend void 	resetLightParams();

  // Class vars
  static bool 	inCreate;
  static short 	meshBuilt;
  static SkyPortal *currentEditLight;
  static HWND 	hSkyPortal;
  //static HWND 	hSpotLight;
  //static HWND 	hShadow;
  static IObjParam *iObjParams;

  static int 		dlgRed;
  static int 		dlgGreen;
  static int 		dlgBlue;

  static float 	 dlgMultiplier;
  static float 	 dlgLength;
  static float 	 dlgWidth;
  static Color24 dlgTint;
  static short 	dlgShape;
  static Class_ID dlgShadType;

  static short 	globShadowType;
  static short 	globAbsMapBias;
  static float 	globMapRange;
  static float 	globMapBias;
  static int   	globMapSize;
  static float 	globRayBias;
  static short 	globAtmosShadows;
  static float 	globAtmosOpacity;
  static float 	globAtmosColamt;

  static ISpinnerControl *multiplierSpin;
  static ISpinnerControl *lengthSpin;
  static ISpinnerControl *widthSpin;
  static IColorSwatch    *tintColorSwatch;

  // Object parameters
  IParamBlock	*pblock;	//ref 0
  ShadowType 	*shadType;    		// a reference
  Interval ivalid;

  short 	enable;           // maybe it's some kind of visibility.
  short   useLight;					// on/off toggle
  bool    visible;
  short 	shape;
  short 	shadow;
  short 	useGlobalShadowParams;
  short 	shadowType;  			// 0: ShadowMap   1: RayTrace
  int   	extDispFlags;
  Mesh 	  rectMesh;
  Mesh 	  *mesh;           // this is independent of the scale of the mesh in the viewport. Not use for real rectangle mesh.
  ExclList exclList;
  void  BuildRectMesh(TimeValue t);
  void 	UpdateUI(TimeValue t);


  // Loading/Saving
  IOResult Load(ILoad *iload);
  IOResult Save(ISave *isave);


  // internal routines
  void 	SetSpinnerBracket(ISpinnerControl *spin, int pbid);
  bool  GetVisibleLight() { return visible; }
  void  SetVisibleLight(bool onOff);
  void 	UpdateColBrackets(TimeValue t);


  //From Animatable
  Class_ID ClassID() { return HydraSkyPortal_CLASS_ID; }
  SClass_ID SuperClassID() { return LIGHT_CLASS_ID; }
  void GetClassName(TSTR& s) { s = GetString(IDS_CLASS_NAME); }
  void 	DeleteThis() { delete this; }  


  Animatable * SubAnim(int i)
  {
    switch (i)
    {
    case PBLOCK_REF: 	 return (Animatable *)pblock;
    default: 				   return nullptr;
    }
  }

  TSTR SubAnimName(int i)
  {
    switch (i)
    {
    case PBLOCK_REF: return GetString(IDS_PARAMS); 
    default: 				return _T(""); 
    }
  }

  RefTargetHandle Clone(RemapDir &remap);
  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);

  void GetMat(TimeValue t, INode* inode, ViewExp &vpt, Matrix3& mat);

  int NumSubs() { return 1; }

  //Constructor/Destructor
  SkyPortal();
  ~SkyPortal();

  // fixes for macrorecorder
  static void UpdateUICheckbox(HWND hwnd, int dlgItem, TCHAR *name, int val);

  // From BaseObject
  int 	HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
  void 	Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt);
  void 	SetExtendedDisplay(int flags);
  int 	Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
  //void 	UpdtShadowTypeList(HWND hwndDlg);
  //void 	UpdtShadowTypeSel();
  void 	BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev);
  void 	EndEditParams(IObjParam *ip, ULONG flags, Animatable *next);
  const TCHAR * GetObjectName();
  

  // From Object
  void 		 InitNodeName(TSTR& s);
  Interval ObjectValidity(TimeValue t);
  BOOL 		 UsesWireColor() { return true; }		// 6/18/01 2:51pm --MQM-- now we can set object color
  int 		 DoOwnSelectHilite() { return 1; }
  void     GetWorldBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box);
  void     GetLocalBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box);
  void     GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel);
  BOOL     HasViewDependentBoundingBox() { return false; }


  // Inherited via GenLight
  CreateMouseCallBack * GetCreateMouseCallBack();
  ObjectState Eval(TimeValue t);
  GenLight * NewLight(int type);
  RefResult EvalLightState(TimeValue t, Interval & valid, LightState * ls);
  ObjLightDesc * CreateLightDesc(INode * inode, BOOL forceShadowBuf = false);

  
  BOOL  IsSpot();
  BOOL  IsDir();
  void  SetUseLight(int onOff);
  BOOL 	GetUseLight(void) { return useLight; }
  void  SetSpotShape(int s);
  int   GetSpotShape();
  void  SetHotspot(TimeValue time, float f);
  float GetHotspot(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetFallsize(TimeValue time, float f);
  float GetFallsize(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetAtten(TimeValue time, int which, float f);
  float GetAtten(TimeValue t, int which, Interval & valid = Interval(0, 0));
  void  SetTDist(TimeValue time, float f);
  float GetTDist(TimeValue t, Interval & valid = Interval(0, 0));
  void   SetRGBColor(TimeValue t, Point3 & rgb);
  Point3 GetRGBColor(TimeValue t, Interval & valid = Interval(0, 0));
  void   SetHSVColor(TimeValue t, Point3 & hsv);
  Point3 GetHSVColor(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetIntensity(TimeValue t, float f);
  float GetIntensity(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetContrast(TimeValue time, float f);
  float GetContrast(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetAspect(TimeValue t, float f) ;
  float GetAspect(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetConeDisplay(int s, int notify = TRUE);
  BOOL  GetConeDisplay();
  void  SetUseAtten(int s);
  BOOL  GetUseAtten();
  void  SetAttenDisplay(int s);
  BOOL  GetAttenDisplay();
  void  SetUseAttenNear(int s);
  BOOL  GetUseAttenNear();
  void  SetAttenNearDisplay(int s);
  BOOL  GetAttenNearDisplay();
  void  Enable(int enab) { enable = enab; }
  void  SetMapBias(TimeValue t, float f);
  float GetMapBias(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetMapRange(TimeValue t, float f);
  float GetMapRange(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetMapSize(TimeValue t, int f);
  int   GetMapSize(TimeValue t, Interval & valid = Interval(0, 0));
  void  SetRayBias(TimeValue t, float f);
  float GetRayBias(TimeValue t, Interval & valid = Interval(0, 0));
  int   GetUseGlobal();
  void  SetUseGlobal(int a);
  int   GetShadow() { return shadow; }
  void  SetShadow(int a);
  int   GetShadowType();
  void  SetShadowType(int a);
  int   GetAbsMapBias();
  void  SetAbsMapBias(int a);
  int   GetOvershoot();
  void  SetOvershoot(int a);
  ExclList & GetExclusionList();
  void SetExclusionList(ExclList & list);
  BOOL SetHotSpotControl(Control * c);
  BOOL SetFalloffControl(Control * c);
  BOOL SetColorControl(Control * c);
  Control * GetHotSpotControl();
  Control * GetFalloffControl();
  Control * GetColorControl();

  void 	BuildMeshes(bool isnew = true);

  //virtual unsigned long GetObjectDisplayRequirement() const { return 0; }
  //virtual bool PrepareDisplay(const MaxSDK::Graphics::UpdateDisplayContext& prepareDisplayContext);
  //virtual bool UpdatePerNodeItems(
  //  const MaxSDK::Graphics::UpdateDisplayContext& updateDisplayContext,
  //  MaxSDK::Graphics::UpdateNodeContext& nodeContext,
  //  MaxSDK::Graphics::IRenderItemContainer& targetRenderItemContainer);


  RefTargetHandle GetReference(int i)
  {
    switch (i)
    {
    case PBLOCK_REF: 	 
      return (RefTargetHandle)(pblock);
    default:				   
      return nullptr;
    }
  }

private:
  virtual void SetReference(int i, RefTargetHandle rtarg)
  {
    switch (i)
    {
    case PBLOCK_REF:
      pblock = (IParamBlock*)rtarg; 
      break;
    }
  }


  // Inherited via LightscapeLight
  virtual void SetType(int type) override;
  virtual void SetType(const MCHAR * name) override;
  virtual int  Type() override;
  virtual const MCHAR * TypeName() override;
  virtual void SetDistribution(DistTypes dist) override;
  virtual DistTypes GetDistribution() const override;
  virtual void SetIntensityAt(float f) override; // The distance at which the light intensity is measured in system units. Must be greater than 0.
  virtual float GetIntensityAt() override;
  virtual void SetIntensityType(IntensityType t) override;
  virtual IntensityType GetIntensityType() override;
  virtual void SetFlux(TimeValue t, float flux) override;
  virtual float GetFlux(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual void SetRGBFilter(TimeValue t, Point3 & rgb) override;
  virtual Point3 GetRGBFilter(TimeValue t, Interval & valid = Interval(0, 0)) override;
  virtual void SetHSVFilter(TimeValue t, Point3 & hsv) override;
  virtual Point3 GetHSVFilter(TimeValue t, Interval & valid = Interval(0, 0)) override;
  virtual ShadowType * ActiveShadowType() override;
  virtual ShadowType * GetShadowGenerator() override;
  virtual const MCHAR * GetShadowGeneratorName() override;
  virtual void SetShadowGenerator(ShadowType * s) override;
  virtual void SetShadowGenerator(const MCHAR * name) override;
  virtual void SetUseShadowColorMap(TimeValue t, int onOff) override;
  virtual int GetUseShadowColorMap(TimeValue t) override;
  virtual void SetInclude(BOOL onOff) override;
  virtual void UpdateTargDistance(TimeValue t, INode * inode) override;
  virtual BOOL SetKelvinControl(Control * kelvin) override;
  virtual BOOL SetFilterControl(Control * filter) override;
  virtual Control * GetKelvinControl() override;
  virtual Control * GetFilterControl() override;
  virtual float GetKelvin(TimeValue t, Interval & v = Interval(0, 0)) override;
  virtual void SetKelvin(TimeValue t, float kelvin) override;
  virtual BOOL GetUseKelvin() override;
  virtual void SetUseKelvin(BOOL useKelvin) override;
  virtual MaxSDK::AssetManagement::AssetUser GetWebFile() const override;
  virtual const MCHAR * GetFullWebFileName() const override;
  virtual void SetWebFile(const MaxSDK::AssetManagement::AssetUser & file) override;
  virtual float GetWebRotateX() const override;
  virtual void SetWebRotateX(float degrees) override;
  virtual float GetWebRotateY() const override;
  virtual void SetWebRotateY(float degrees) override;
  virtual float GetWebRotateZ() const override;
  virtual void SetWebRotateZ(float degrees) override;
  virtual float GetDimmerValue(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual void SetDimmerValue(TimeValue t, float dimmer) override;
  virtual BOOL GetUseMultiplier() const override;
  virtual void SetUseMultiplier(BOOL on) override;
  virtual BOOL IsColorShiftEnabled() const override;
  virtual void EnableColorShift(BOOL on) override;
  virtual float GetResultingIntensity(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual float GetResultingFlux(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual Point3 GetCenter() const override;
  virtual float GetRadius(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual void SetRadius(TimeValue t, float radius) override;
  virtual float GetLength(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual void SetLength(TimeValue 	t, float 	length) override;
  virtual float GetWidth(TimeValue t, Interval & valid = Interval(0, 0)) const override;
  virtual void 	SetWidth(TimeValue t, float f) override;
  virtual void SetShape(int count, const Point3 * pointsIn) override;
  virtual int GetShape(Point3 * pointsOut, int bufSize) const override;
  virtual float GetOriginalFlux() const override;
  virtual void SetOriginalFlux(float flux) override;
  virtual float GetOriginalIntensity() const override;
  virtual void SetOriginalIntensity(float candelas) override;
  virtual PresetLightColor GetColorPreset() const override;
  virtual void SetColorPreset(PresetLightColor presetID) override;

  protected:
    void InitGeneralParamDialog(TimeValue t, IObjParam *ip, ULONG flags, Animatable *prev);
    void UpdateControlledUI();
};



class SkyPortalClassDesc : public ClassDesc
{
public:
  virtual int IsPublic() { return TRUE; }
  virtual void* Create(BOOL /*loading = FALSE*/) { return new SkyPortal(); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
  virtual SClass_ID SuperClassID() { return LIGHT_CLASS_ID; }
  virtual Class_ID ClassID() { return HydraSkyPortal_CLASS_ID; }
  virtual const TCHAR* Category() { return GetString(IDS_CATEGORY); }

  virtual const TCHAR* InternalName() { return _T("HydraSkyPortal"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() { return hInstance; }					// returns owning module handle
};


//Class for interactive creation of the object using the mouse
class SkyPortalCreateCallBack : public CreateMouseCallBack
{
  SkyPortal *ob;  
  Point3 p0;                // First point in world coordinates
  Point3 p1;                // Second point in world coordinates
  
  // added for exersice 6 to keep track of previous point.
  IPoint2 m_mousecallback_pt2_prev;

public:
  int proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3& mat);
  void SetObj(SkyPortal *obj) { ob = obj; }
};


template <void (SkyPortal::*set)(int), bool notify>
class SimpleLightUndo : public RestoreObj {
public:
  virtual void Restore(int undo);
  virtual void Redo();
  virtual int Size();
  virtual TSTR Description();

  static void Hold(SkyPortal* light, int newVal, int oldVal)
  {
    if (theHold.Holding())
      theHold.Put(new SimpleLightUndo<set, notify>(light, newVal, oldVal));
  }

private:
  SimpleLightUndo(SkyPortal* light, int newVal, int oldVal)
    : _light(light), _redo(newVal), _undo(oldVal) {}

  SkyPortal*	_light;
  int					_redo;
  int					_undo;
};

enum { hydraskyportal_params };


//TODO: Add enums for various parameters
enum {
  pb_spin,
};


class LightPostLoad : public PostLoadCallback 
{
public:
  SkyPortal *sp;
  Interval valid;
  LightPostLoad(SkyPortal *l) { sp = l; }

  void proc(ILoad *iload) 
  {
    //gl->FixOldVersions(iload);
    waitPostLoad--;
    delete this;
  }
};


class AttenRanges
{
public:
  float aStart, aEnd;	// Attenuation start and end and hot spot scaling for volume shading		
  float aNearStart, aNearEnd;	// Near Attenuation start and end and hot spot scaling for volume shading		
  float decayRadius;
};


class BaseObjLight : public ObjLightDesc
{
public:
  ExclList exclList;
  Color intensCol;   	// intens*color	
                      // RB: put these into a different structure and pass them to
                      // AttenIllum to avoid threading problems.
                      //float aStart, aEnd;	// Attenuation start and end and hot spot scaling for volume shading		
                      //float aNearStart, aNearEnd;	// Near Attenuation start and end and hot spot scaling for volume shading		
  Color shadColor;
  float shadMult;
  float contrast, kA, kB, diffSoft;
  int decayType;
  Texmap *shadProjMap;
  BOOL ltAfctShad;
  BOOL diffSoften;
  BOOL dontNeedShadBuf;
  int atmosShadows;
  float atmosOpacity;
  float atmosColAmt;
  BOOL doingShadColor;
  BOOL ambOnly;
  BOOL isDefaultShadowMap;
  float decayRadius;
  SkyPortal* sp;
  BOOL forceShadowBuffer;

  BaseObjLight(INode *n, BOOL forceShadowBuf);
  void DeleteThis() { delete this; }
  ExclList* GetExclList() { return &exclList; }
  int Update(TimeValue t, const RendContext& rc, RenderGlobalContext * rgc, BOOL shadows, BOOL shadowGeomChanged);
  void UpdateGlobalLightLevel(Color globLightLevel)
  {
    intensCol = ls.intens*ls.color*globLightLevel;
  }
  virtual void ApplyAtmosShadows(ShadeContext &sc, Point3 lpos, Color  &color, float &shadAtten);
  inline void ApplyShadowColor(Color &color, const float &shadAtten, const Color &scol);
  inline void ApplyShadowColor(IlluminateComponents& illumComp, const float &shadAtten);
  float ApplyDistanceAtten(float dist);
  void ApplySpecularDiffuseAtten(Color& lightClr, IlluminateComponents& illumComp);

  void TraverseVolume(ShadeContext& sc, const Ray &ray, int samples, float tStop, float attenStart, float attenEnd, DWORD flags, LightRayTraversal *proc);
  virtual BOOL IntersectRay(Ray &ray, float &t0, float &t1, AttenRanges &ranges) { return FALSE; }
  virtual int  IntersectRayMultiple(Ray &ray, float t0, float t1, float *t);
  virtual Color AttenuateIllum(ShadeContext& sc, Point3 p, Color &colStep, Point3 &dp, int filt, float ldp, float &distAtten, AttenRanges &ranges) { return Color(0, 0, 0); }
  virtual BOOL UseAtten();
  virtual BOOL IsFacingLight(Point3 &dir) { return FALSE; }
  virtual int LightType();

  ShadowType * ActiveShadowType();

  inline float ContrastFunc(float nl)
  {
    if (diffSoft != 0.0f)
    {
      float p = nl*nl*(2.0f - nl);	// based on Hermite interpolant 
      nl = diffSoft*p + (1.0f - diffSoft)*nl;
    }
    return (contrast == 0.0f) ? nl :
      nl / (kA*nl + kB);  //  the "Bias" function described in Graphics Gems IV, pp. 401ff
  }
  inline float Decay(float att, float dist, float r0)
  {
    float s;
    if (decayType == DECAY_NONE || dist == 0.0f) return att;
    if ((s = r0 / dist) >= 1.0f) return att;
    return  (decayType == DECAY_INVSQ) ? s*s*att : s*att;
  }
};


//class SpotLight : public BaseObjLight, IIlluminationComponents 
//{
//  friend class SCLight;
//  Point3 lightDir;  // light direction in render space
//  ShadowGenerator *shadGen;
//  BOOL rect;
//  BOOL overshoot, projector, shadowRay;
//  BOOL shadow;
//  int shadsize;
//  float hot_cos, fall_cos, fall_tan, fall_sin;
//  float hotpct, ihotpct;
//  float zfac, xscale, yscale, fov, sz2, curve;
//  float out_range, in_range, range_span;
//  Point2 rectv0, rectv1;
//  //	Texmap* projMap;
//public:
//  SpotLight(INode *inode, BOOL forceShadowBuf);
//  ~SpotLight() { FreeShadGens(); }
//  void FreeShadGens() { if (shadGen) { shadGen->DeleteThis();	shadGen = NULL; } }
//  void FreeShadGenBuffers() { if (shadGen)  shadGen->FreeBuffer(); }
//  int Update(TimeValue t, const RendContext& rc, RenderGlobalContext *rgc, BOOL shadows, BOOL shadowGeomChanged);
//  int UpdateViewDepParams(const Matrix3& worldToCam);
//  BOOL Illuminate(ShadeContext &sc, Point3& normal, Color& color, Point3 &dir, float& dot_nl, float &diffuseCoef);
//  BOOL Illuminate(ShadeContext &sc, Point3& N, IlluminateComponents& illumComp);
//  float SampleMaps(ShadeContext& sc, Point3 pin, Point3 norm, Point3 plt, Color& lcol, float &shadAtten, IlluminateComponents* pIllumComp = NULL);
//  BOOL IntersectCone(Ray &ray, float tanAng, float &t0, float &t1);
//  BOOL IntersectRectCone(Ray &ray, float &t0, float &t1);
//  BOOL IntersectRay(Ray &ray, float &t0, float &t1, AttenRanges &ranges);
//  Color AttenuateIllum(ShadeContext& sc, Point3 p, Color &colStep, Point3 &dp, int filt, float ldp, float &distAtten, AttenRanges &ranges);
//  AColor SampleProjMap(ShadeContext& sc, Point3 plt, Point3 dp, float x, float y, Texmap *map);
//  BOOL UseAtten() { return ls.useAtten; }
//  BOOL IsFacingLight(Point3 &dir);
//  int LightType() { return FSPOT_LIGHT; }
//  BaseInterface *GetInterface(Interface_ID id) {
//    if (id == IID_IIlluminationComponents)
//      return (IIlluminationComponents*)this;
//    else
//      return BaseObjLight::GetInterface(id);
//  }
//};


//class ShadTypeRest : public RestoreObj, public ReferenceMaker {
//public:
//  SkyPortal *theLight;
//  ShadowType *oldtype, *newtype;
//  ShadTypeRest(SkyPortal *lt, ShadowType *s) {
//    oldtype = newtype = nullptr;
//    theLight = lt;
//    theHold.Suspend();
//    ReplaceReference(0, lt->GetShadowGenerator());
//    ReplaceReference(1, s);
//    theHold.Resume();
//  }
//  ~ShadTypeRest() {
//    DeleteAllRefsFromMe();
//  }
//  void Restore(int isUndo);
//  void Redo();
//  int Size() { return 1; }
//  TSTR Description() { return _T("Set Shadow Type"); }
//
//  virtual void GetClassName(MSTR& s) { s = _M("ShadTypeRest"); }
//
//  // ReferenceMaker 
//  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget,
//    PartID& partID, RefMessage message, BOOL propagate) {
//    if (message == REFMSG_TARGET_DELETED) {
//      if (hTarget == oldtype)
//        oldtype = NULL;
//      if (hTarget == newtype)
//        newtype = NULL;
//    }
//    return REF_SUCCEED;
//  }
//  int NumRefs() { return 2; }
//  RefTargetHandle GetReference(int i) { return i == 0 ? oldtype : newtype; }
//private:
//  virtual void SetReference(int i, RefTargetHandle rtarg) {
//    if (i == 0) oldtype = (ShadowType *)rtarg; else newtype = (ShadowType *)rtarg;
//  }
//public:
//  BOOL CanTransferReference(int i) { return FALSE; }
//};
