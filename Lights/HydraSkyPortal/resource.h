//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HydraSkyPortal.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_MULT                        5
#define IDS_DB_HYDRA_SKY_PORTAL         6
#define IDS_DB_HYDRASKYPORTAL           7
#define IDD_PANEL                       101
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_ON                          1001
#define IDC_VISIBLE                     1002
#define IDC_TINT                        1003
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_MULT_EDIT                   1490
#define IDC_LENGTH_EDIT                 1491
#define IDC_WIDTH_EDIT                  1492
#define IDC_SPIN                        1496
#define IDC_MULT_SPIN                   1496
#define IDC_LENGTH_SPIN                 1497
#define IDC_WIDTH_SPIN                  1498
#define IDS_DS_TINT                     10175
#define IDS_DB_MULTIPLIER               20315
#define IDS_DS_LENGTH                   20316
#define IDS_DS_WIDTH                    20317
#define IDS_DB_COLOR                    20320
#define IDS_RB_PARAMETERS               30028
#define IDS_DB_GENERAL_PARAMS           30064
#define IDS_DS_PARAMCHG                 31316
#define IDS_DS_SETTYPE                  31343
#define IDS_DS_SETSHADTYPE              31376

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
