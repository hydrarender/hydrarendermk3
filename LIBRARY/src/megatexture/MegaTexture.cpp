#include "MegaTexture.h"
#include <algorithm>
/////////////////////////////////////////////////////////////////////
MegaTexture::MegaTexture(Texture input)
{
	m_size = input;
}
/////////////////////////////////////////////////////////////////////
TEXTURE_ID MegaTexture::AddTexture(Texture texture)
{
	int id=m_sizes.size();
	texture.m_id=id;
	texture.width+=INTERPOLATION_GAP*2+2;
	texture.height+=INTERPOLATION_GAP*2+2;
	m_sizes.push_back(texture);
	return id;
}
/////////////////////////////////////////////////////////////////////
bool MegaTexture::CalcMegaTexture()
{
	std::sort(m_sizes.begin(),m_sizes.end());

	m_freeRects.clear();
	rect r;
	r.bottom = 0;
	r.left = 0;
	r.right = m_size.width - 1;
	r.top = m_size.height - 1;
	r.valid = true;
	m_freeRects.push_back(r);

	m_offsetsSrt.resize(m_sizes.size());
	m_usedTextures.clear();
	m_usedTextures.resize(m_sizes.size(), false);

	for (int i=0; i<(int)m_sizes.size(); i++)
	{
		if (!FindPlace(m_sizes[i]))
			return false;
	}

	return true;
}
/////////////////////////////////////////////////////////////////////
bool MegaTexture::GetOffsetById(std::pair<int,int>& out_offset, TEXTURE_ID in_id)
{
	if (in_id>=(int)m_sizes.size()||in_id<0)
		return false;
	out_offset=m_offsetsSrt[in_id];
	out_offset.first+=INTERPOLATION_GAP;
	out_offset.second+=INTERPOLATION_GAP;
	return true;
}
/////////////////////////////////////////////////////////////////////
bool MegaTexture::FindPlace(Texture in_texture)
{
	//bool found = false;

	for (int i = 0; i < signed(m_freeRects.size()); i++)
	{
		if (!m_freeRects[i].valid) continue;
		if (in_texture.width <= m_freeRects[i].right - m_freeRects[i].left + 1 &&
			in_texture.height <= m_freeRects[i].top - m_freeRects[i].bottom + 1)
		{
			//intersection
			m_freeRects[i].valid = false;
			m_offsets.push_back(std::pair<int,int>(m_freeRects[i].left,m_freeRects[i].bottom));
			m_offsetsSrt[in_texture.m_id]=std::pair<int,int>(m_freeRects[i].left,m_freeRects[i].bottom);


			if (in_texture.width < m_freeRects[i].right - m_freeRects[i].left + 1)
			{
				rect r;
				r.left = m_freeRects[i].left + in_texture.width;
				r.right = m_freeRects[i].right;
				r.bottom = m_freeRects[i].bottom;
				r.top = m_freeRects[i].top;
				r.valid = true;
				m_freeRects.push_back(r);
			}

			if (in_texture.height < m_freeRects[i].top - m_freeRects[i].bottom + 1)
			{
				rect r;
				r.left = m_freeRects[i].left;
				r.right = m_freeRects[i].left + in_texture.width - 1;
				r.bottom = m_freeRects[i].bottom + in_texture.height;
				r.top = m_freeRects[i].top;
				r.valid = true;
				m_freeRects.push_back(r);
			}
		   /*
			if (in_texture.height < m_freeRects[i].top - m_freeRects[i].bottom + 1 &&
				in_texture.width < m_freeRects[i].right - m_freeRects[i].left + 1)
			{
				rect r;
				r.left = m_freeRects[i].left + in_texture.width;
				r.right = m_freeRects[i].right;
				r.bottom = m_freeRects[i].bottom + in_texture.height;
				r.top = m_freeRects[i].top;
				r.valid = true;
				m_freeRects.push_back(r);
			}
			 */
			m_usedTextures[in_texture.m_id] = true;
			return true;
		}
	}
	/*
	for (int i=0; i<m_size.height; i++)
	{
		if (m_size.width-m_maskX[i]<in_texture.width)
			continue;

		if (m_size.height-i<in_texture.height)
			break;
		//int offsetX=m_maskX[i];

		for (int offsetX=0; offsetX<m_size.width-in_texture.width; offsetX++)
		{
			//test if we can add rectangle
			int offsetY=i;
			bool intersection=false;
			for (int j=0; j<(int)m_offsets.size(); j++)
			{
				if (m_offsets[j].first<=offsetX&&m_offsets[j].first+m_sizes[j].width-1>=offsetX||
					m_offsets[j].first<=offsetX+in_texture.width-1&&m_offsets[j].first+m_sizes[j].width-1>=offsetX+in_texture.width-1)
				{
					//x intersection
					if (m_offsets[j].second<=offsetY&&m_offsets[j].second+m_sizes[j].height-1>=offsetY||
						m_offsets[j].second<=offsetY+in_texture.height-1&&m_offsets[j].second+m_sizes[j].height-1>=offsetY+in_texture.height-1)
					{
						//y intersection
						intersection=true;
						break;
					}
				}
			}

			if (intersection)
				continue;
			else
			{
				m_offsets.push_back(std::pair<int,int>(offsetX,offsetY));
				m_offsetsSrt[in_texture.m_id]=std::pair<int,int>(offsetX,offsetY);
				for (int k=0; k<in_texture.height; k++)
					m_maskX[k+offsetY]+=in_texture.width;
				return true;
			}
		}
	}  */
	return false;
}
