#ifndef MEGATEX_GUARDIAN
#define MEGATEX_GUARDIAN

#include <vector>

typedef int TEXTURE_ID;

#define INTERPOLATION_GAP 1

struct rect
{
	int bottom, top, left, right;
	bool valid;
};

class Texture
{
	friend class MegaTexture;

public:
	Texture()
	{
		width=100;
		height=100;
	}
	Texture(int w, int h)
	{
		width=w;
		height=h;
	}
	int width;
	int height;

  inline bool operator<(const Texture& tex) const { return (this->width)>(tex.width); }

private:
	TEXTURE_ID m_id;
};

class MegaTexture
{
public:
	//MegaTexture() {}
	MegaTexture(Texture input);

	///@return texture id
	TEXTURE_ID AddTexture(Texture texture);

	bool CalcMegaTexture();

	bool GetOffsetById(std::pair<int,int>& out_offset, TEXTURE_ID in_id);

private:

	bool FindPlace(Texture in_texture);

private:
	std::vector<Texture> m_sizes;
	std::vector< std::pair<int,int> > m_offsets;
	std::vector< std::pair<int,int> > m_offsetsSrt;

	std::vector<bool> m_usedTextures;
	//std::vector<int> m_maskX;
	std::vector<rect> m_freeRects;
	Texture m_size;
};

#endif // MEGATEX_GUARDIAN
