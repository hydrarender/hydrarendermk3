#ifndef MEGATEX_MANAGER_GUARDIAN
#define MEGATEX_MANAGER_GUARDIAN

#include <math.h>
#include <algorithm>
#include <vector>
#include "MegaTexture.h"



using std::pair;

template <class T, int channels>
class MegaTextureManager
{

public:
  typedef std::vector<std::pair<int, int>> SizeArray;
  typedef std::vector<std::pair<int,std::pair<int, int>>> OffsetsArray;
  typedef std::vector<const T*> ImagesArray;
  typedef pair<int, pair<int,int> > LUTElem;

  MegaTextureManager() : m_offsetsWasAlreadyCalculated(false),
  m_blackAreaPercent(0), m_usedBytes(0), m_totalArea(0) {}

  bool CalculateOffsets(SizeArray& sizes, int* outWidth, int* outHeight)
  {
	  m_offsetsWasAlreadyCalculated = CalculateMegaTextures(sizes, m_lastCalculatedOffsets, m_megaTextureSizes);
    m_inputTexturesSizes = sizes;

	  *outWidth  = m_megaTextureSizes[0].first;
	  *outHeight = m_megaTextureSizes[0].second;

	  m_usedBytes = *outWidth * *outHeight * sizeof(T) * channels;
	  m_blackAreaPercent = 100 * (*outWidth * *outHeight - m_totalArea) / (*outWidth * *outHeight);

	  if(!m_offsetsWasAlreadyCalculated)
		  throw std::runtime_error("MegaTextureManager::CalculateOffsets failed!");

	  return m_offsetsWasAlreadyCalculated;
  }

  bool BuildImage(ImagesArray& images, SizeArray& sizes, T *outputImage, int a_width, int a_height)
  {

    if(sizes.size() == 1)
    {
       memcpy(outputImage, images[0], a_width*a_height*sizeof(T)*channels);
       return true;
    }

	  OffsetsArray outputOffsets;
	  SizeArray megaTextureSizes;

	  T* ptr = NULL;
	  if(m_offsetsWasAlreadyCalculated)
	  {
		  megaTextureSizes = m_megaTextureSizes;
		  outputOffsets    = m_lastCalculatedOffsets;
		  ptr = outputImage;
	  }
	  else
		  throw std::runtime_error("MegaTextureManager::BuildImage failed, call CalculateOffsets first!");

	  if (!ptr)
		  throw std::runtime_error("MegaTextureManager::BuildImage failed, outputImage == NULL!");

	  for (int i = 0; i < (int) sizes.size(); i++)
	  {
		  int sizeX = sizes[i].first;
		  int sizeY = sizes[i].second;

		  int offsetX = outputOffsets[i].second.first;
		  int offsetY = outputOffsets[i].second.second;

		  //������ ������
		  memcpy(&ptr[channels * offsetX + (offsetY) * channels * a_width + channels], images[i], sizeX * channels * sizeof(T));

		  for (int j = 0; j < sizeY; j++)
		  {
			  memcpy(&ptr[channels * offsetX + (offsetY + j + 1) * channels * a_width + channels], &(images[i][channels * sizeX * j]), sizeX * channels * sizeof(T));
		  }

		  for (int j = 0; j < sizeY; j++)
		  {
			  memcpy(&ptr[channels * offsetX + (offsetY + j + 1) * channels * a_width], &(images[i][channels * sizeX * j]), channels * sizeof(T));
			  memcpy(&ptr[channels * offsetX + (offsetY + j + 1) * channels * a_width + channels * sizeX + channels], &(images[i][channels * sizeX * j + channels* (sizeX - 1)]), channels * sizeof(T));
		  }

		  //��������� ������
		  memcpy(&ptr[channels * offsetX + (offsetY + sizeY + 1) * channels * a_width + channels], &(images[i][channels * sizeX * (sizeY - 1)]), sizeX * channels * sizeof(T));

		  memcpy(&ptr[channels * offsetX + offsetY * channels * a_width], images[i], channels * sizeof(T));
		  memcpy(&ptr[channels * offsetX + (offsetY + sizeY + 1) * channels * a_width], &(images[i][channels * sizeX * (sizeY - 1)]), channels * sizeof(T));
		  memcpy(&ptr[channels * (offsetX + sizeX + 1) + offsetY * channels * a_width], &(images[i][channels * sizeX - channels]), channels * sizeof(T));
		  memcpy(&ptr[channels * (offsetX + sizeX + 1) + (offsetY + sizeY + 1) * channels * a_width], &(images[i][channels * sizeX * sizeY - channels]), channels * sizeof(T));

	  }
	  return true;
  }

  int GetBlackAreasPercent()
  {
	  return m_blackAreaPercent;
  }

  long GetUsedBytes()
  {
	  return m_usedBytes;
  }

  const OffsetsArray& GetOffsetsTable() const { return m_lastCalculatedOffsets;}

  const std::vector<float>& GetLookUpTable(int megaTexId = 0)
  {
    m_LookUpTable.resize(4*m_lastCalculatedOffsets.size());

    float width  = float(m_megaTextureSizes[megaTexId].first);
    float height = float(m_megaTextureSizes[megaTexId].second);

    for(int index=0; index<m_lastCalculatedOffsets.size(); index++)
    {
      pair<int,int> texSize = m_inputTexturesSizes[index];

      m_LookUpTable[index*4 + 0] = float(texSize.first)/width;    // scaleX
      m_LookUpTable[index*4 + 1] = float(texSize.second)/height;  // scaleY
      m_LookUpTable[index*4 + 2] = float(m_lastCalculatedOffsets[index].second.first  + 1)/width;    // offsetX
      m_LookUpTable[index*4 + 3] = float(m_lastCalculatedOffsets[index].second.second + 1)/height;  // offsetY

      if(texSize.first == 1 && texSize.second == 1)
      {
        m_LookUpTable[index*4 + 0] = 0.0f;  // scaleX
        m_LookUpTable[index*4 + 1] = 0.0f;  // scaleY
        //m_LookUpTable[index*4 + 2] = float(m_lastCalculatedOffsets[index].second.first+0.5)/width;    // offsetX
        //m_LookUpTable[index*4 + 3] = float(m_lastCalculatedOffsets[index].second.second+0.5)/height;  // offsetY
      }

    }

    return m_LookUpTable;
  }

protected:

  bool CalculateMegaTextures(SizeArray& inputSizes, OffsetsArray& outputOffsets, SizeArray& megaTextureSizes)
  {

    if(inputSizes.size() == 1)
    {
      outputOffsets.push_back(LUTElem(0, pair<int,int>(0,0)));
      megaTextureSizes.push_back(pair<int,int>(1,1));
      return true;
    }

	  m_totalArea = 0;
	  int maxW = -1;
	  int maxH = -1;
	  for (int i = 0; i < (int) inputSizes.size(); i++)
	  {
		  m_totalArea += (inputSizes[i].first + 2) * (inputSizes[i].second + 2);

		  if (inputSizes[i].first + 2 > maxW)
			  maxW = inputSizes[i].first + 2;

		  if (inputSizes[i].second + 2 > maxH)
			  maxH = inputSizes[i].second + 2;
	  }

	  int linSize = (int) sqrt(m_totalArea);

	  Texture sizeTotal;
	  sizeTotal.width  = std::max(linSize, maxW);
	  sizeTotal.height = std::max(linSize, maxH);

	  std::vector<Texture> textures;
	  MegaTexture mega(sizeTotal);
	  for (int i = 0; i < (int) inputSizes.size(); i++)
	  {
		  Texture size;
		  size.width = inputSizes[i].first;
		  size.height = inputSizes[i].second;
		  textures.push_back(size);
		  mega.AddTexture(size);
	  }

	  bool res = mega.CalcMegaTexture();

	  bool chW = true;

	  while (!res)
	  {
		  if (chW)
			  sizeTotal.width *= 1.1f;
		  else
			  sizeTotal.height *= 1.1f;

		  chW = !chW;

		  MegaTexture mega(sizeTotal);
		  for (int i = 0; i < (int) inputSizes.size(); i++)
		  {
			  Texture size;
			  size.width = inputSizes[i].first;
			  size.height = inputSizes[i].second;
			  textures.push_back(size);
			  mega.AddTexture(size);
		  }
		  res = mega.CalcMegaTexture();

		  if (res)
		  {
			  outputOffsets.resize(inputSizes.size());

			  int maxX = -1;
			  int maxY = -1;
			  for (int i = 0; i < (int) inputSizes.size(); i++)
			  {
				  outputOffsets[i].first = 0;
				  mega.GetOffsetById(outputOffsets[i].second, i);

				  int x = outputOffsets[i].second.first + inputSizes[i].first + 2;
				  int y = outputOffsets[i].second.second + inputSizes[i].second + 2;
				  if (x > maxX) maxX = x;
				  if (y > maxY) maxY = y;
			  }

			  megaTextureSizes.push_back(std::pair<int,int>(std::min(sizeTotal.width,maxX), std::min(sizeTotal.height,maxY)));
			  return true;
		  }
	  }

	  outputOffsets.resize(inputSizes.size());

	  int maxX = -1;
	  int maxY = -1;
	  for (int i = 0; i < (int) inputSizes.size(); i++)
	  {
		  outputOffsets[i].first = 0;
		  mega.GetOffsetById(outputOffsets[i].second, i);

		  int x = outputOffsets[i].second.first + inputSizes[i].first;
		  int y = outputOffsets[i].second.second + inputSizes[i].second;
		  if (x > maxX) maxX = x;
		  if (y > maxY) maxY = y;
	  }

	  megaTextureSizes.push_back(std::pair<int,int>(std::min(sizeTotal.width,maxX), std::min(sizeTotal.height,maxY)));
	  return true;
  }

  OffsetsArray m_lastCalculatedOffsets;
  SizeArray    m_megaTextureSizes;
  SizeArray    m_inputTexturesSizes;
  bool         m_offsetsWasAlreadyCalculated;
  std::vector<float> m_LookUpTable;
  int m_blackAreaPercent;
  long m_usedBytes;
  float m_totalArea;
};

#endif

