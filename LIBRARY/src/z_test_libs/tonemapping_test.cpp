#include <string>

#include "../PostProcessEngine/image.h"
#include "../PostProcessEngine/filters/tonemappingfilter.h"
#include "../PostProcessEngine/filters/blurfilter.h"
#include "../PostProcessEngine/filters/boxfilter.h"

#include "..\il\il.h"
#include "..\il\ilu.h"

#pragma comment(lib, "devil.lib")
#pragma comment(lib, "ilu.lib")
#pragma comment(lib, "ilut.lib")

int main(char **argv, int argc)
{
	ilInit();
	iluInit();
	PP_FILTERS::Image src, dst;
	PP_FILTERS::ToneMappingFilter filter;

  src.Load("c:\\Users\\andrew\\Downloads\\boxes\\hydra_3.93664.hdr");
	
  dst.CreateCopy(&src);
  filter.Apply(&dst, &src);	
  dst.Save("c:\\Users\\andrew\\Downloads\\boxes\\dst.png");
  src.Save("c:\\Users\\andrew\\Downloads\\boxes\\src.png");

  filter.Apply(&dst, &src);
  dst.Save("c:\\Users\\andrew\\Downloads\\boxes\\dst2.png");
}

	