#include "MegaTexture.h"
#include <windows.h>
#include "gl/GL.h"
#include <gl/freeglut.h>
#include <time.h>
#include <fstream>

Texture size2(2500,1300);
MegaTexture mega(size2);
std::vector<Texture> textures;

#define SIZES 9
int possibleSizes[SIZES]={20,50,128,256,300,400,512,600,1024};

double Xmin = -1.0, Xmax = size2.width+1;
double Ymin = -1.0, Ymax = size2.height+1;

double windowXmin, windowXmax, windowYmin, windowYmax;

int gW, gH;

void drawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f( 1.0, 0, 0 );
	glBegin(GL_LINE_STRIP);
	glVertex2f(0,0);
	glVertex2f(0+size2.width,0);
	glVertex2f(0+size2.width,0+size2.height);
	glVertex2f(0,0+size2.height);
	glVertex2f(0,0);
	glEnd();

	glColor3f( 1.0, 1.0, 1.0 );		

	for (int i=0; i<(int)textures.size(); i++)
	{
		std::pair<int,int> offset;
		mega.GetOffsetById(offset,i);
		glBegin(GL_QUADS);
		glVertex2f(offset.first,offset.second);
		glVertex2f(offset.first+textures[i].width,offset.second);
		glVertex2f(offset.first+textures[i].width,offset.second+textures[i].height);
		glVertex2f(offset.first,offset.second+textures[i].height);
		glEnd();
	}

	glFlush();
}

void initRendering()
{
	glEnable ( GL_DEPTH_TEST );
}

void resizeWindow(int w, int h)
{
	double scale, center;
	
	glViewport( 0, 0, w, h );

	gW = w = (w==0) ? 1 : w;
	gH = h = (h==0) ? 1 : h;
	if ( (Xmax-Xmin)/w < (Ymax-Ymin)/h ) {
		scale = ((Ymax-Ymin)/h)/((Xmax-Xmin)/w);
		center = (Xmax+Xmin)/2;
		windowXmin = center - (center-Xmin)*scale;
		windowXmax = center + (Xmax-center)*scale;
		windowYmin = Ymin;
		windowYmax = Ymax;
	}
	else {
		scale = ((Xmax-Xmin)/w)/((Ymax-Ymin)/h);
		center = (Ymax+Ymin)/2;
		windowYmin = center - (center-Ymin)*scale;
		windowYmax = center + (Ymax-center)*scale;
		windowXmin = Xmin;
		windowXmax = Xmax;
	}

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( windowXmin, windowXmax, windowYmin, windowYmax, -1, 1 );
}

void myKeyboardFunc( unsigned char key, int x, int y )
{
	switch ( key ) {

	case 27:									
		exit(1);
		break;
	}
}

void GenerateTextures()
{
  unsigned int seed = time(0);
	srand(seed);

  std::ofstream fout("MegaTexInfo.txt");

  fout << seed << std::endl;
  fout.close();

  for (int i=0; i<15; i++)
	{
		Texture size;
		size.width=possibleSizes[rand()%SIZES];
		size.height=possibleSizes[rand()%SIZES];
		textures.push_back(size);
		mega.AddTexture(size);
	}



	/*
	Texture size;
	size.width=1024;
	size.height=1024;
	textures.push_back(size);
	mega.AddTexture(size);

	for (int i=0; i<2; i++)
	{
		size.width=500;
		size.height=600;
		textures.push_back(size);
		mega.AddTexture(size);
	}

	for (int i=0; i<3; i++)
	{
		size.width=200;
		size.height=300;
		textures.push_back(size);
		mega.AddTexture(size);
	}

	for (int i=0; i<4; i++)
	{
		size.width=128;
		size.height=128;
		textures.push_back(size);
		mega.AddTexture(size);
	}

  size.width=640;
	size.height=400;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=2;
	size.height=2;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=1;
	size.height=1;
	textures.push_back(size);
	mega.AddTexture(size);	   */
}

int main ( int argc, char** argv )
{
	/*
	Texture size;
	size.width=100;
	size.height=50;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=80;
	size.height=70;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=90;
	size.height=30;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=40;
	size.height=70;
	textures.push_back(size);
	mega.AddTexture(size);

	size.width=30;
	size.height=150;
	textures.push_back(size);
	mega.AddTexture(size);
	  */

	GenerateTextures();

	bool res=mega.CalcMegaTexture();

	if (!res)
	{
		printf("Too big and too many textures\n");
		return -1;
	}


	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH );

	glutInitWindowPosition( 20, 60 );
	glutInitWindowSize( 600, 600 );
	glutCreateWindow( "Memory view" );

	initRendering();


	glutKeyboardFunc( myKeyboardFunc );			
	glutReshapeFunc( resizeWindow );
	glutDisplayFunc( drawScene );

	glutMainLoop(  );
}