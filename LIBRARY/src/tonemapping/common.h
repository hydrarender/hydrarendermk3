#pragma once

#define PE_OK                 1
#define PE_UNKNOWN_ERROR      0
#define PE_BAD_ARGUMENTS      -1
#define PE_NOT_ENOUGH_MEMORY  -2
#define PE_UNSUPPORTED        -3

#define PE_SUCCEEDED(x) ((x)==0)
#define PE_FAILED(x) ((x)<=0)