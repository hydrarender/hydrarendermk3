#include <string.h>
#include <iostream>

#include <FreeImage.h>

#include "common.h"
#include "image.h"

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::Image::Image(void) : m_ptr(0), m_donotdelete(false)
{
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::Image::~Image(void)
{
  Dispose();
}
////////////////////////////////////////////////////////////////////////////////

void PP_FILTERS::Image::Dispose()
{
  if (!m_donotdelete && m_ptr)
  {
    delete[] m_ptr;
  }
  m_donotdelete = false;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::CreateStandard(int width, int height, int channels)
{
  Dispose();
  m_ptr = new float[width*height*channels];
  if (!m_ptr)
    return PE_NOT_ENOUGH_MEMORY;
  m_width = width;
  m_height = height;
  m_stride = width*channels;
  m_channels = channels;
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::CreateLocked(float* ptr, int width, int height, int stride, int channels)
{
  Dispose();
  if (!ptr)
    return PE_BAD_ARGUMENTS;
  if (stride < width*channels)
    return PE_BAD_ARGUMENTS;
  m_ptr = ptr;
  m_width = width;
  m_height = height;
  m_stride = stride;
  m_channels = channels;
  m_donotdelete = true;
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::CreateCopy(Image* image)
{
  Dispose();
  m_width = image->GetWidth();
  m_height = image->GetHeight();
  m_stride = image->GetStride();
  m_channels = image->GetChannels();
  int size = m_height*m_stride;
  m_ptr = new float[size];
  memcpy(m_ptr, image->GetPointer(), size*sizeof(float));
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

float* PP_FILTERS::Image::GetPointer()
{
  return m_ptr;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::GetWidth()
{
  return m_width;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::GetHeight()
{
  return m_height;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::GetStride()
{
  return m_stride;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Image::GetChannels()
{
  return m_channels;
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::Image* PP_FILTERS::Image::GetSubImage(int left, int top, int right, int bottom)
{
  Image* image = new Image();
  int res = image->CreateLocked(m_ptr + top*m_stride + left*m_channels, right - left, bottom - top, m_stride, m_channels);
  if (PE_FAILED(res))
  {
    delete image;
    return 0;
  }
  return image;
}
////////////////////////////////////////////////////////////////////////////////
int PP_FILTERS::Image::Load( const std::string& path )
{
  std::cerr << " PP_FILTERS::Image::Load is not implemented!" << std::endl;

  /*
	ILuint imageName;
	ilGenImages(1, &imageName);
	ilBindImage(imageName);

	//std::wstring path1( path.begin(), path.end() );
	if(!ilLoadImage(path.c_str()))
		return PE_BAD_ARGUMENTS;

	if(!ilConvertImage(IL_RGB, IL_FLOAT))
		return PE_BAD_ARGUMENTS;

	//ilOriginFunc( IL_ORIGIN_UPPER_LEFT );
	ILinfo ImageInfo;
	iluGetImageInfo(&ImageInfo);
	if( ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT )
	{
		iluFlipImage();
	}

	int w = ilGetInteger(IL_IMAGE_WIDTH);
	int h = ilGetInteger(IL_IMAGE_HEIGHT);

	CreateStandard(w, h);

	int size = w * h * 3;

	ilCopyPixels(0, 0, 0, w, h, 3, IL_RGB, IL_FLOAT, m_ptr);
	ilDeleteImages(1, &imageName);
  */

	return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////
int PP_FILTERS::Image::Save( const std::string& path )
{
  FIBITMAP* dib;

  dib = FreeImage_Allocate(m_width, m_height, 32);
  BYTE* bits = FreeImage_GetBits(dib);

  //typedef unsigned char uchar;

  for(int i=0;i<m_width*m_height;i++)
  {
    bits[4*i+0] = BYTE(m_ptr[3*i+2]*255.0f);
    bits[4*i+1] = BYTE(m_ptr[3*i+1]*255.0f);
    bits[4*i+2] = BYTE(m_ptr[3*i+0]*255.0f);
    bits[4*i+3] = 255;
  }

  if(!FreeImage_Save(FIF_PNG, dib, path.c_str()) )
    std::cerr << "Image::Save(): FreeImage_Save error on " << path.c_str() << std::endl;

  FreeImage_Unload(dib);

  /*
	ILenum Error;
	while ((Error = ilGetError()) != IL_NO_ERROR);

	ILuint imageID = ilGenImage();
	ilBindImage(imageID);

	ilTexImage(m_width, m_height, 1, 3, IL_RGB, IL_FLOAT, m_ptr);

	if(!ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE))
		return PE_BAD_ARGUMENTS;

	ilEnable(IL_FILE_OVERWRITE);
	//std::wstring path1( path.begin(), path.end() );
	ilSave(IL_PNG, path.c_str());

	while ((Error = ilGetError()) != IL_NO_ERROR)
		return PE_BAD_ARGUMENTS;

	ilDeleteImages(1, &imageID);
	ilDisable(IL_FILE_OVERWRITE);
  */

	return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////
