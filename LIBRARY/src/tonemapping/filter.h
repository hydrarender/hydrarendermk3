#pragma once

#include "image.h"

namespace PP_FILTERS
{


typedef bool (*PROGRESSHANDLER)(int progress, void* param);

class Filter
{
public:
  Filter(void);
  virtual ~Filter(void);
  
  // Apply - apply filter to the image not in-place
  // use "precalculate=false" to precalculate once for processing multiple crops or if you called PreCalculate manually
  virtual int Apply(Image* image_dst, Image* image_src, bool precalculate = true);

  // Apply - apply filter to the image in-place
  // use "precalculate=false" to precalculate once for processing multiple crops or if you called PreCalculate manually
  int ApplyInPlace(Image* image_dst, bool precalculate = true);


  // PreCalculate - do necessary precalculations (once for one source image)
  // calling to "PreCalculate" is optional, you can do it to use GetResultSize
  int PreCalculate(Image* image_src, int full_width = 0, int full_height = 0);

  // GetResultSize - get the required destination image size, a call is valid after PreCalculate
  void GetResultSize(int* out_width, int* out_height);

  // SetProgressHandler - setting a callback for progress report
  void SetProgressHandler(PROGRESSHANDLER handler, void* param);

private:

  // Members
protected:
  bool m_precalculated;
  int m_precalculated_width, m_precalculated_height;
  
  PROGRESSHANDLER m_progress_handler;
  void* m_progress_param;
};


}
