#pragma once
#include "..\filter.h"

namespace PP_FILTERS
{


class ToneMappingFilter : public Filter
{
public:
  ToneMappingFilter(void);
  virtual ~ToneMappingFilter(void);

  struct Presets
  {
    Presets() : strength(1.0f), phi(16.0f), whitePoint(1.0f) {}
    float strength;
    float phi;
    float whitePoint;
  };

  virtual void SetPresets(const Presets& a_presets)
  {
    m_strength   = a_presets.strength;
    m_phi        = a_presets.phi;
    m_whitePoint = a_presets.whitePoint;
  }

  virtual int Apply(Image* image_dst, Image* image_src, bool precalculate = true);

  /// Sets the range of luminance to apply tonemapping
  /// Params are in [0..1]
  //virtual void SetLuminanceRange(float minLum, float maxLum);

private:

  int CalcLumLogAverageImage(Image* image_dst, Image* image_src);

  int CalcCorrectedLum(Image* image_dst, Image* image_src);

private:

	int m_range;
	float m_delta;
	float m_eps;
	int m_phi;
	float m_strength;
	float m_minimumLum, m_maximumLum;
	float m_minimumImageLum, m_maximumImageLum;
	float m_whitePoint;
};
}

