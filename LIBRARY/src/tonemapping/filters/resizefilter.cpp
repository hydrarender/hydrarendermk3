#include "..\common.h"
#include "resizefilter.h"
#include "commonfilter.h"
#include <string.h>

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::ResizeFilter::ResizeFilter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::ResizeFilter::~ResizeFilter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::ResizeFilter::Apply(Image* image_dst, Image* image_src, bool precalculate)
{
  int res = Filter::Apply(image_dst, image_src, precalculate);
  if (PE_FAILED(res))
    return res;

  if (image_dst==image_src)
	  return PE_OK;

  if (image_src->GetChannels() != image_dst->GetChannels())
    return PE_BAD_ARGUMENTS;

  int height = image_src->GetHeight();
  int width = image_src->GetWidth();
  int height_dest = image_dst->GetHeight();
  int width_dest = image_dst->GetWidth();
  int stride_src = image_src->GetStride();
  int stride_dst = image_dst->GetStride();


  float* ptr_src = image_src->GetPointer();
  float* ptr_dst = image_dst->GetPointer();
  if (!ptr_src || !ptr_dst)
    return PE_BAD_ARGUMENTS;

  if (height_dest < 1 || width_dest < 1)
    return PE_BAD_ARGUMENTS;
  
  float * temp = (ptr_src==ptr_dst)? new float [height*stride_dst]: ptr_dst;

  if (!temp)
	  return PE_NOT_ENOUGH_MEMORY;

  ApplyAllChannels(ptr_src, temp, width, height, width_dest, height_dest, stride_src, stride_dst, image_src->GetChannels());
 
  if (ptr_src==ptr_dst)
  {
	  memcpy(ptr_dst, temp, height*stride_dst);
	  delete [] temp;
  }

  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////
int PP_FILTERS::ResizeFilter::ApplyAllChannels(float* ptr_src, float* ptr_dst, int width, int height, int width_dest, int height_dest, int stride_src, int stride_dst, int channels)
{
  int x, y, xP, yP, yP2, xP2;

  float * pSrc, * pSrc2, * pDst, * pColY1, * pColY2, * pColX1, * pColX2;

  int t, z, z2, iz2;
  int w1, w2, w3, w4;

  xP2 = ((width - 1) << 15) / max(1, width_dest - 1);
  yP2 = ((height - 1) << 15) / max(1, height_dest - 1);

  yP = 0;

  for (y = 0; y < height_dest; y++)
  {
    xP = 0;

	int coord = yP >> 15;
    pSrc = &ptr_src[coord*stride_src];


    if (coord < height - 1)
      pSrc2 = &ptr_src[(coord + 1)*stride_src];
    else
      pSrc2 = pSrc;

    pDst = &ptr_dst[y*stride_dst];
    z2 = (yP & 0x7FFF);
    iz2 = 0x8000 - z2;

    for (x = 0; x < width_dest; x++)
    {
      t = xP >> 15;
      z = xP & 0x7FFF;
      w2 = (z * iz2) >> 15;
      w1 = iz2 - w2;
      w4 = (z * z2) >> 15;
      w3 = z2 - w4;

      pColY1 = &pSrc[t * channels];
      pColY2 = &pSrc2[t * channels];

      pColX1 = pColY1 + channels * (t < width - 1);
      pColX2 = pColY2 + channels * (t < width - 1);

      for (int i = 0; i < channels; i++)
        pDst[i] = ((pColY1[i] * w1 + pColX1[i] * w2 + pColY2[i] * w3 + pColX2[i] * w4) / 4); 

      pDst += channels;

      xP += xP2;
    }
    yP += yP2;
    
  }
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

