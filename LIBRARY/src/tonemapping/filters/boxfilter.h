#pragma once
#include "..\filter.h"

namespace PP_FILTERS
{



class BoxFilter : public Filter
{
public:
  BoxFilter(void);
  virtual ~BoxFilter(void);

  virtual int Apply(Image* image_dst, Image* image_src, bool precalculate = true);

  void SetRadius(int in_iRadius);

private:
  int ApplyOneChannel(float* ptr_src, float* ptr_dst, float* temp,int width, int height, 
	  int stride_src, int stride_dst, int channelNumber, int channels);

private:
  int m_radius;
};

}
