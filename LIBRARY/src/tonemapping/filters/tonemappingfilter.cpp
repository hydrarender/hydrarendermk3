#include "..\common.h"
#include "blurfilter.h"
#include "tonemappingfilter.h"
#include "commonfilter.h"
#include <math.h>

float a;

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::ToneMappingFilter::ToneMappingFilter(void)
{
	m_range = 8;
	m_delta = 0.01f;
	m_eps = 0.05f;
	m_phi = 16;
	m_strength = 0.18f;
	m_whitePoint = 1.0f;
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::ToneMappingFilter::~ToneMappingFilter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::ToneMappingFilter::Apply(Image* image_dst, Image* image_src, bool precalculate)
{
  int res = Filter::Apply(image_dst, image_src, precalculate);
  if (PE_FAILED(res))
    return res;

  if (image_src->GetChannels() != image_dst->GetChannels())
    return PE_BAD_ARGUMENTS;

  int height = image_src->GetHeight();
  int width  = image_src->GetWidth();
  int stride_src = image_src->GetStride();
  int stride_dst = image_dst->GetStride();

  if (image_dst->GetHeight() != height || image_dst->GetWidth() != width)
    return PE_BAD_ARGUMENTS;

  float* ptr_src = image_src->GetPointer();
  float* ptr_dst = image_dst->GetPointer();
  if (!ptr_src || !ptr_dst)
    return PE_BAD_ARGUMENTS;


  float * lum = new float [height*width];

  if (!lum)
	  return PE_NOT_ENOUGH_MEMORY;

  float * lum2 = new float [height*width];

  if (!lum2)
  {
	  delete [] lum;
	  return PE_NOT_ENOUGH_MEMORY;
  }

  Image imageLum;
  imageLum.CreateLocked(lum, width, height, width, 1);
  Image imageLum2;
  imageLum2.CreateLocked(lum2, width, height, width, 1);

  CalcLumLogAverageImage(&imageLum, image_src);

  CalcCorrectedLum(&imageLum2, &imageLum);

  for (int i=0; i<width; i++)
  {
	  for (int j=0; j<height; j++)
	  {
		  float lum=(27.0f*ptr_src[3*i+j*stride_src]+67.0f*ptr_src[3*i+j*stride_src+1]+6.0f*ptr_src[3*i+j*stride_src+2])/100.0f;
		  ptr_dst[3*i+j*stride_dst] = Clamper<float,float>::Clamp(ptr_src[3*i+j*stride_src]*lum2[i+j*width]/lum,0.0f,1.0f);
		  ptr_dst[3*i+j*stride_dst+1]=Clamper<float,float>::Clamp(ptr_src[3*i+j*stride_src+1]*lum2[i+j*width]/lum,0.0f,1.0f);
		  ptr_dst[3*i+j*stride_dst+2]=Clamper<float,float>::Clamp(ptr_src[3*i+j*stride_src+2]*lum2[i+j*width]/lum,0.0f,1.0f);
	  }
  }

  delete [] lum;
  delete [] lum2;

  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::ToneMappingFilter::CalcLumLogAverageImage(Image* image_dst, Image* image_src)
{
  int stride_src = image_src->GetStride();
  int stride_dst = image_dst->GetStride();

  float* ptr_src = image_src->GetPointer();
  float* ptr_dst = image_dst->GetPointer();

  int width=image_src->GetWidth();
  int height=image_src->GetHeight();

  if (image_dst->GetChannels() != 1 || image_src->GetChannels() != 3)
    return PE_BAD_ARGUMENTS;

  float sum=0.0f;

  for (int i=0; i<width; i++)
	for (int j=0; j<height; j++)
	{
		float lum=(27.0f*ptr_src[3*i+j*stride_src]+67.0f*ptr_src[3*i+j*stride_src+1]+6.0f*ptr_src[3*i+j*stride_src+2])/100.0f;
		ptr_dst[i+j*stride_dst]=lum;//Clamper<float,float>::Clamp(lum,0.0f,1.0f);
		sum += log(lum + m_delta);
	}

  sum /= width * height;
  sum = exp(sum);

  a = /*sum */ m_strength;//0.18f*2;

  for (int i=0; i<width; i++)
	for (int j=0; j<height; j++)
	{
	   ptr_dst[i+j*stride_dst] = a * ptr_dst[i+j*stride_dst] / sum;//Clamper<float,float>::Clamp(a*ptr_dst[i+j*stride_dst]/sum,0.0f,1.0f);
	}
  return PE_OK;
}

////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::ToneMappingFilter::CalcCorrectedLum(Image* image_dst, Image* image_src)
{
  int stride_src = image_src->GetStride();
  int stride_dst = image_dst->GetStride();

  float* ptr_src = image_src->GetPointer();
  float* ptr_dst = image_dst->GetPointer();

  int width=image_src->GetWidth();
  int height=image_src->GetHeight();

  if (image_dst->GetChannels() != 1 || image_src->GetChannels() != 1)
    return PE_BAD_ARGUMENTS;

  BlurFilter filter;

  float * blurredStack=new float [width*height*m_range];
  if (!blurredStack)
	 return PE_NOT_ENOUGH_MEMORY;

  float curRadius=1.0f;
  const float multCoeff=2.0f;
  for (int i=0; i<m_range; i++)
  {
	  Image imageBlurred;
      imageBlurred.CreateLocked(blurredStack+i*width*height, width, height, width, 1);
	  filter.SetRadius((int)curRadius);
	  filter.Apply(&imageBlurred,image_src);
	  //curRadius*=multCoeff;
	  curRadius+=1;
  }
//  const float a=0.18f*2;

  float norm = pow(2.0f, m_phi);

  for (int i=0; i<width; i++)
  {
    for (int j=0; j<height; j++)
    {
      curRadius=1.0f;
      for (int s=0; s<m_range-1; s++)
      {
        float diff=(blurredStack[s*width*height+j*width+i]-blurredStack[(s+1)*width*height+j*width+i]);
        float res=diff/(norm*a/curRadius/curRadius+blurredStack[s*width*height+j*width+i]);

        if (res < m_eps || s == m_range - 1)
        {
          ptr_dst[i+j*stride_dst]=(ptr_src[i+j*stride_src]/(1+blurredStack[s*width*height+j*width+i])) *
			  (1 + ptr_src[i+j*stride_src] / m_whitePoint / m_whitePoint);
          break;
        }
        //curRadius*=multCoeff;
        curRadius+=1;
      }
    }
  }

  delete [] blurredStack;

  return PE_OK;
}
