#include "..\common.h"
#include "boxfilter.h"
#include <string.h>

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::BoxFilter::BoxFilter(void)
{
	m_radius=10; //default radius
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::BoxFilter::~BoxFilter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

void PP_FILTERS::BoxFilter::SetRadius(int in_iRadius)
{
	m_radius=in_iRadius;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::BoxFilter::Apply(Image* image_dst, Image* image_src, bool precalculate)
{
  int res = Filter::Apply(image_dst, image_src, precalculate);
  if (PE_FAILED(res))
    return res;

  if (image_src->GetChannels() != image_dst->GetChannels())
    return PE_BAD_ARGUMENTS;

  int height = image_src->GetHeight();
  int width = image_src->GetWidth();
  int stride_src = image_src->GetStride();
  int stride_dst = image_dst->GetStride();

  if (image_dst->GetHeight() != height || image_dst->GetWidth() != width)
    return PE_BAD_ARGUMENTS;

  float* ptr_src = image_src->GetPointer();
  float* ptr_dst = image_dst->GetPointer();
  if (!ptr_src || !ptr_dst)
    return PE_BAD_ARGUMENTS;

  float * temp = (ptr_src==ptr_dst)? new float [height*stride_dst]: ptr_dst;

  if (!temp)
	  return PE_NOT_ENOUGH_MEMORY;

  float * temp2 = new float [width];

  if (!temp2)
  {
	  delete [] temp;
	  return PE_NOT_ENOUGH_MEMORY;
  }

  for (int ch=0; ch<image_src->GetChannels(); ch++)
  {
	  ApplyOneChannel(ptr_src, temp, temp2, width, height, stride_src, stride_dst, ch, image_src->GetChannels());
  }

  if (ptr_src==ptr_dst)
  {
	  memcpy(ptr_dst, temp, height*stride_dst*sizeof(float));
	  delete [] temp;
  }

  delete [] temp2;

  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////
int PP_FILTERS::BoxFilter::ApplyOneChannel(float* ptr_src, float* ptr_dst, float * temp, int width, int height, int stride_src, int stride_dst, int channelNumber, int channels)
{
	const int normalization=(2*m_radius+1)*(2*m_radius+1);

	//1	
	for (int j=0; j<width; j++) 
	{
		float sum = 0;
		for (int i=1; i<=m_radius; i++)
			sum += ptr_src[i*stride_src+j*channels+channelNumber];
		temp[j] = (sum * 2) + ptr_src[j*channels+channelNumber];
	}

	float *pDstRows, *pDst;

	float sum = 0;
	for (int j=1; j<=m_radius; j++)
		sum += temp[j];

	sum = (sum * 2) + temp[0];

	pDstRows=ptr_dst;
	pDst=&pDstRows[channelNumber];

	pDstRows[0] = sum/normalization;
	pDst+=channels;
	
	for (int j=1; j<=m_radius; j++, pDst+=channels) 
	{
		sum += temp[j+m_radius] - temp[m_radius-j+1];
		pDst[0] = sum/normalization;
	} 

	for (int j=m_radius+1; j<width-m_radius; j++, pDst+=channels) 
	{
		sum += temp[j+m_radius] - temp[j-m_radius-1];
		pDst[0] = sum/normalization;
	} 

	for (int j=width-m_radius; j<width; j++, pDst+=channels) 
	{
		sum += temp[2*width-2-j-m_radius] - temp[j-m_radius-1];
		pDst[0] = sum/normalization;
	} 
	
	
	//2
	for (int i=1; i<=m_radius; i++) 
	{
		for (int j=0; j<width; j++) 
		{
			temp[j] += ptr_src[(i+m_radius)*stride_src+j*channels+channelNumber] - ptr_src[(m_radius-i+1)*stride_src+j*channels+channelNumber];
		}
		
		float sum = 0;
		for (int j=1; j<=m_radius; j++)
			sum += temp[j];

		sum = (sum * 2) + temp[0];

		pDstRows=ptr_dst+i*stride_dst;
		pDst=&pDstRows[channelNumber];

		pDstRows[0] = sum/normalization;
		pDst+=channels;

		
		for (int j=1; j<=m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[m_radius-j+1];
			pDst[0] = sum/normalization;
		} 

		for (int j=m_radius+1; j<width-m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 

		
		for (int j=width-m_radius; j<width; j++, pDst+=channels) 
		{
			sum += temp[2*width-2-j-m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 
	}
	
	//3 
	for (int i=m_radius+1; i<height-m_radius; i++) 
	{
		for (int j=0; j<width; j++) 
		{
			temp[j] += ptr_src[(i+m_radius)*stride_src+j*channels+channelNumber] - ptr_src[(i-m_radius-1)*stride_src+j*channels+channelNumber];
		}
		
		float sum = 0;
		for (int j=1; j<=m_radius; j++)
			sum += temp[j];

		sum = (sum * 2) + temp[0];

		pDstRows=ptr_dst+i*stride_dst;
		pDst=&pDstRows[channelNumber];

		pDstRows[0] = sum/normalization;
		pDst+=channels;
	
		for (int j=1; j<=m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[m_radius-j+1];
			pDst[0] = sum/normalization;
		} 

		for (int j=m_radius+1; j<width-m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 

		for (int j=width-m_radius; j<width; j++, pDst+=channels) 
		{
			sum += temp[2*width-2-j-m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 
	}

	//4
	for (int i=height-m_radius; i<height; i++) 
	{
		for (int j=0; j<width; j++) 
		{
			temp[j] += ptr_src[(2*height-2-i-m_radius)*stride_src+j*channels+channelNumber] - ptr_src[(i-m_radius-1)*stride_src+j*channels+channelNumber];
		}

		float sum = 0;
		for (int j=1; j<=m_radius; j++)
			sum += temp[j];

		sum = (sum * 2) + temp[0];

		pDstRows=ptr_dst+i*stride_dst;
		pDst=&pDstRows[channelNumber];

		pDstRows[0] = sum/normalization;
		pDst+=channels;

		for (int j=1; j<=m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[m_radius-j+1];
			pDst[0] = sum/normalization;
		} 

		for (int j=m_radius+1; j<width-m_radius; j++, pDst+=channels) 
		{
			sum += temp[j+m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 

		for (int j=width-m_radius; j<width; j++, pDst+=channels) 
		{
			sum += temp[2*width-2-j-m_radius] - temp[j-m_radius-1];
			pDst[0] = sum/normalization;
		} 
	}
	
	return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

