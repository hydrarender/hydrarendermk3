#include "commonfilter.h"
#include <math.h>

void PP_FILTERS::ColorConverter::RGB2HSL(unsigned char r, unsigned char g, unsigned char b, int & h, int & s, int & l)
{
	int R=(int)r;
	int G=(int)g;
	int B=(int)b;
	l=(R+G+B)/3;
	int u=R-(G+B)/2;
    int v=866025*(G-B)/1000000;

	//fixme - lut?
	h=(int)(57.2957795f*atan2((float)v,(float)u));

	if (h<0) 
		h=h+360;

	int t=173205*u/100000;
    if (v>=0)
	{
		if (t>v) 
			s= u+57735*v/100000;
		else
			if (-t>v)
				s=-u+57735*v/100000;
			else
				s=1154701*v/1000000;
	}
	else
	{
		if (t>-v) 
			s = u-57735*v/100000;
		else
			if (-t>-v)  
				s=-u-57735*v/100000;
			else
				s=-1154701*v/1000000;

	}
}

void PP_FILTERS::ColorConverter::HSL2RGB(int h, int s, int l, unsigned char& r, unsigned char& g, unsigned char& b)
{
	float si=sin(h*0.0174532925f);
	float co=cos(h*0.0174532925f);

	int h0=30+60*floor(h/60.0f);

	float si2=sin(h0*0.0174532925f);
	float co2=cos(h0*0.0174532925f);
	
	float D=s*0.866025f/(co*co2+si*si2);
	int U=((int)(D*co))/3; 
	int V=(int)(0.577350f*D*si);
	
	r=Clamper<int,unsigned char>::Clamp(l+U+U,0,255); 
	g=Clamper<int,unsigned char>::Clamp(l-U+V,0,255);
	b=Clamper<int,unsigned char>::Clamp(l-U-V,0,255);
}

