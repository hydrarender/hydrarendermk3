#pragma once
#include "..\filter.h"

namespace PP_FILTERS
{


class ResizeFilter : public Filter
{
public:
  ResizeFilter(void);
  virtual ~ResizeFilter(void);

  virtual int Apply(Image* image_dst, Image* image_src, bool precalculate = true);

private:
  int ApplyAllChannels(float* ptr_src, float* ptr_dst, int width, int height, 
	  int width_dest, int height_dest, int stride_src, int stride_dst, int channels);

};

}
