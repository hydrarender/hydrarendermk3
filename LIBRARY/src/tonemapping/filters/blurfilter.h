#pragma once
#include "..\filter.h"

namespace PP_FILTERS
{



class BlurFilter : public Filter
{
public:
  BlurFilter(void);
  virtual ~BlurFilter(void);

  virtual int Apply(Image* image_dst, Image* image_src, bool precalculate = true);

  void SetRadius(int in_iRadius);

private:

private:
  int m_radius;
};


}
