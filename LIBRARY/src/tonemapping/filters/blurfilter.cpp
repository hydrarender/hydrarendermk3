#include "..\common.h"
#include "boxfilter.h"
#include "blurfilter.h"

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::BlurFilter::BlurFilter(void)
{
	m_radius=10; //default radius
}
////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::BlurFilter::~BlurFilter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

void PP_FILTERS::BlurFilter::SetRadius(int in_iRadius)
{
	m_radius=in_iRadius;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::BlurFilter::Apply(Image* image_dst, Image* image_src, bool precalculate)
{
  int res = Filter::Apply(image_dst, image_src, precalculate);
  if (PE_FAILED(res))
    return res;

  Image imageTemp;
  imageTemp.CreateCopy(image_src);
  BoxFilter filter;
  filter.SetRadius(m_radius);
  res=filter.Apply(&imageTemp, image_src);
  if (PE_FAILED(res))
    return res;

  res=filter.Apply(image_dst, &imageTemp);
  if (PE_FAILED(res))
    return res;

  return PE_OK;
}