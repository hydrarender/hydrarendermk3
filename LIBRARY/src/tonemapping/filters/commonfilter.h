#pragma once

#define max(A,B) ((A)>(B) ? (A) : (B))

namespace PP_FILTERS
{


class ColorConverter
{
public:

  //h: 0..360
  //s: 0..255
  //l: 0..255
  static void RGB2HSL(unsigned char r, unsigned char g, unsigned char b,
	  int& h, int& s, int& l);

  static void HSL2RGB(int h, int s, int l,
	  unsigned char& r, unsigned char& g, unsigned char& b);

};

template <class InputT, class OutputT>
class Clamper
{
public:

  static OutputT Clamp(InputT val, InputT valMin, InputT valMax)
  {
	  if (val>valMax)
		  return (OutputT)valMax;
	  if (val<valMin)
		  return (OutputT)valMin;
	  return (OutputT)val;
  }

};

}

