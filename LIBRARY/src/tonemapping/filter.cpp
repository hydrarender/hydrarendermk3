#include "common.h"
#include "filter.h"

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::Filter::Filter(void) : m_progress_handler(0), m_precalculated(false), m_precalculated_width(0), m_precalculated_height(0)
{
}

////////////////////////////////////////////////////////////////////////////////

PP_FILTERS::Filter::~Filter(void)
{
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Filter::PreCalculate(Image* image_src, int full_width, int full_height)
{
  m_precalculated = true;
  if (full_width != 0 && full_height != 0)
  {
    m_precalculated_width = full_width;
    m_precalculated_height = full_height;
  }
  else
  {
    m_precalculated_width = image_src->GetWidth();
    m_precalculated_height = image_src->GetHeight();
  }
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

void PP_FILTERS::Filter::GetResultSize(int* out_width, int* out_height)
{
  *out_width = m_precalculated_width;
  *out_height = m_precalculated_height;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Filter::Apply(Image* image_dst, Image* image_src, bool precalculate)
{
  if (precalculate || !m_precalculated)
  {
    PreCalculate(image_src);
  }
  return PE_OK;
}
////////////////////////////////////////////////////////////////////////////////

int PP_FILTERS::Filter::ApplyInPlace(Image* image_dst, bool precalculate)
{
  return Apply(image_dst, image_dst, precalculate);
}
////////////////////////////////////////////////////////////////////////////////

void PP_FILTERS::Filter::SetProgressHandler(PROGRESSHANDLER handler, void* param)
{
  m_progress_handler = handler;
  m_progress_param = param;
}
////////////////////////////////////////////////////////////////////////////////
