#pragma once
#include <string>

namespace PP_FILTERS
{


class Image
{
public:
  Image(void);
  virtual ~Image(void);

  // Creation
  int CreateStandard(int width, int height, int channels = 3);
  int CreateLocked(float* ptr, int width, int height, int stride, int channels = 3);
  int CreateCopy(Image* image);

  // Access
  float* GetPointer();
  int GetWidth();
  int GetHeight();
  int GetStride();
  int GetChannels();
  Image* GetSubImage(int left, int top, int right, int bottom);

  int Load(const std::string& path);
  int Save(const std::string& path);

private:
  void Dispose();

  // Members
private:
  float* m_ptr;
  int m_width;
  int m_height;
  int m_stride;
  int m_channels;
  bool m_donotdelete;
};


}

