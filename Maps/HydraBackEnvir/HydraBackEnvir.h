#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>
#include <IMaterialBrowserEntryInfo.h>

#include <stdmat.h>
#include <imtl.h>
#include <macrorec.h>

#include "3dsmaxport.h"


extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HydraBackEnvir_CLASS_ID	Class_ID(0xe954bddf, 0x4a0184fd)

#define NSUBMTL		0 // TODO: number of sub-materials supported by this plugin 
#define NSUBTEX		2 // TODO: number of sub-textures supported by this plugin
// Reference Indexes
//#define COORD_REF  0

#define BACK_TEX_SLOT   0
#define ENVIR_TEX_SLOT 1

#define PBLOCK_REF	NSUBMTL+NSUBTEX

typedef std::wstring hydraStr;
typedef wchar_t hydraChar;

//class HydraBackEnvir;

class HydraBackEnvir : public Texmap
{
public:
  //Constructor/Destructor
  HydraBackEnvir();
  virtual ~HydraBackEnvir();

  // References
  XYZGen*          xyzGen;          // ref 0
  IParamBlock2*    pblock;          // ref 1
  Texmap*          subtex[NSUBTEX]; // Reference array of sub-materials

                                    //static ParamDlg* xyzGenDlg;
  Interval         ivalid;

  Color backColor, envirColor; 

  //From MtlBase
  virtual ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
  virtual BOOL      SetDlgThing(ParamDlg* dlg);
  virtual void      Update(TimeValue t, Interval& valid);
  virtual void      Reset();
  virtual Interval  Validity(TimeValue t);
  virtual ULONG     LocalRequirements(int subMtlNum);

  void NotifyChanged();

  //TODO: Return the number of sub-textures
  virtual int NumSubTexmaps() { return NSUBTEX; }
  //TODO: Return the pointer to the 'i-th' sub-texmap
  virtual Texmap* GetSubTexmap(int i) { return subtex[i]; }
  virtual void SetSubTexmap(int i, Texmap *m);
  virtual TSTR GetSubTexmapSlotName(int i);

  //From Texmap
  virtual RGBA EvalColor(ShadeContext& sc);
  virtual float EvalMono(ShadeContext& sc);
  virtual Point3 EvalNormalPerturb(ShadeContext& sc);

  virtual XYZGen *GetTheXYZGen() { return xyzGen; }

  //TODO: Return anim index to reference index
  virtual int SubNumToRefNum(int subNum) { return subNum; }

  //TODO: If your class is derived from Tex3D then you should also
  //implement ReadSXPData for 3D Studio/DOS SXP texture compatibility
  virtual void ReadSXPData(TCHAR* /*name*/, void* /*sxpdata*/) { }

  // Loading/Saving
  virtual IOResult Load(ILoad *iload);
  virtual IOResult Save(ISave *isave);

  //From Animatable
  virtual Class_ID ClassID() { return HydraBackEnvir_CLASS_ID; }
  virtual SClass_ID SuperClassID() { return TEXMAP_CLASS_ID; }
  virtual void GetClassName(TSTR& s) { s = GetString(IDS_CLASS_NAME); }

  virtual int NumSubs() { return 2 + NSUBTEX; }
  virtual Animatable* SubAnim(int i);
  virtual TSTR SubAnimName(int i);

  // TODO: Maintain the number or references here
  virtual int NumRefs() { return 2 + NSUBTEX; }
  virtual RefTargetHandle GetReference(int i);

  virtual RefTargetHandle Clone(RemapDir &remap);
  virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);

  virtual int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
  virtual IParamBlock2* GetParamBlock(int /*i*/) { return pblock; } // return i'th ParamBlock
  virtual IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

  virtual void DeleteThis() { delete this; }
protected:
  virtual void SetReference(int i, RefTargetHandle rtarg);

};


class HydraBackEnvirClassDesc : public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  virtual int IsPublic() { return TRUE; }
  virtual void* Create(BOOL /*loading = FALSE*/) { return new HydraBackEnvir(); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
  virtual SClass_ID SuperClassID() { return TEXMAP_CLASS_ID; }
  virtual Class_ID ClassID() { return HydraBackEnvir_CLASS_ID; }
  virtual const TCHAR* Category() { return GetString(IDS_CATEGORY); }
  virtual const TCHAR* InternalName() { return _T("HydraBack/Envir"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() { return hInstance; }					// returns owning module handle

                                                              // For entry category
  virtual FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  virtual const MCHAR* GetEntryName() const override;
  virtual const MCHAR* GetEntryCategory() const override;
  virtual Bitmap* GetEntryThumbnail() const override;
};


class hydraBackEnvirDlgProc : public ParamMap2UserDlgProc {
public:
  HydraBackEnvir *hM;
  HWND thishWnd;

  ICustButton *gui_backMap;
  ICustButton *gui_envirMap;  

  IColorSwatch *gui_back_color;
  IColorSwatch *gui_envir_color;

  bool additional_init;

  hydraBackEnvirDlgProc(HydraBackEnvir *cb);
  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() { delete this; }
};

ClassDesc2* GetHydraBackEnvirDesc();

enum { hydraBackEnvir_params };

//TODO: Add enums for various parameters
enum {
  pb_backMap,
  pb_backColor,
  pb_envirMap,
  pb_envirColor
};
