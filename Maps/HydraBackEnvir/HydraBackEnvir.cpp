//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR:
//***************************************************************************/

#include "HydraBackEnvir.h"


static ParamBlockDesc2 hydraBackEnvir_param_blk(hydraBackEnvir_params, _T("params"), 0, GetHydraBackEnvirDesc(),
  P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  // params
  pb_backMap, _T("pb_backMap"), TYPE_TEXMAP, P_OWNERS_REF, IDS_BACK_MAP,
    p_refno, 0,
    p_subtexno, 0,
    p_ui, TYPE_TEXMAPBUTTON, IDC_BACK_MAP,
    p_end,
  pb_backColor, _T("pb_backColor"), TYPE_RGBA, P_ANIMATABLE, IDS_BACK_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_BACK_COLOR,
    p_end,
  pb_envirMap, _T("pb_envirMap"), TYPE_TEXMAP, P_OWNERS_REF, IDS_ENVIR_MAP,
    p_refno, 1,
    p_subtexno, 1,
    p_ui, TYPE_TEXMAPBUTTON, IDC_ENVIR_MAP,
    p_end,
  pb_envirColor, _T("pb_envirColor"), TYPE_RGBA, P_ANIMATABLE, IDS_ENVIR_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_ENVIR_COLOR,
    p_end,
  p_end
);


ClassDesc2* GetHydraBackEnvirDesc() {
  static HydraBackEnvirClassDesc HydraAODesc;
  return &HydraAODesc;
}


hydraBackEnvirDlgProc::hydraBackEnvirDlgProc(HydraBackEnvir * cb)
{
  hM = cb;
  additional_init = true;

  gui_backMap = nullptr;
  gui_envirMap = nullptr;

  gui_back_color = nullptr;
  gui_envir_color = nullptr;
}

INT_PTR hydraBackEnvirDlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd = hWnd;
  hydraBackEnvirDlgProc *dlg = DLGetWindowLongPtr<hydraBackEnvirDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
  case WM_INITDIALOG:
  {
    //dlg = (hydraMaterialDlgProc *)lParam;
    //DLSetWindowLongPtr(hWnd, dlg);

    if (!gui_backMap) gui_backMap = GetICustButton(GetDlgItem(hWnd, IDC_BACK_MAP));
    if (!gui_envirMap) gui_envirMap = GetICustButton(GetDlgItem(hWnd, IDC_ENVIR_MAP));

    gui_back_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_BACK_COLOR));
    gui_envir_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_ENVIR_COLOR));

    DragAcceptFiles(GetDlgItem(hWnd, IDC_BACK_MAP), true);
    DragAcceptFiles(GetDlgItem(hWnd, IDC_ENVIR_MAP), true);

    return TRUE;
  }
  //case WM_COMMAND:
  //  switch (LOWORD(wParam))
  //  {
  //  case IDC_BACK_MAP:
  //  case IDC_ENVIR_MAP:
  //  break;
  //  }
  case WM_DESTROY:
    ReleaseICustButton(gui_backMap);
    ReleaseICustButton(gui_envirMap);   

    ReleaseIColorSwatch(gui_back_color);
    ReleaseIColorSwatch(gui_envir_color);

    break;
  case WM_DROPFILES:
    POINT pt;
    WORD numFiles;
    HWND dropTarget;
    int dropTargetID;

    hydraChar lpszFile[80];

    DragQueryPoint((HDROP)wParam, &pt);

    numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

    if (numFiles == 0)
    {
      DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
      dropTarget = RealChildWindowFromPoint(hWnd, pt);
      dropTargetID = GetDlgCtrlID(dropTarget);
      DropFileInMapSlot(dropTargetID, lpszFile);
    }

    DragFinish((HDROP)wParam);

    break;
  default:
    return FALSE;
  }
  return TRUE;
}

void hydraBackEnvirDlgProc::DropFileInMapSlot(int mapControlID, hydraChar * filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap *tex = (Texmap *)btex;

  switch (mapControlID)
  {
  case IDC_BACK_MAP:
    hM->SetSubTexmap(BACK_TEX_SLOT, tex);
    hM->subtex[BACK_TEX_SLOT] = tex;
    break;
  case IDC_ENVIR_MAP:
    hM->SetSubTexmap(ENVIR_TEX_SLOT, tex);
    hM->subtex[ENVIR_TEX_SLOT] = tex;
    break;
  default:
    break;
  }
}

//ParamDlg* HydraAO::xyzGenDlg;

//--- HydraAO -------------------------------------------------------
HydraBackEnvir::HydraBackEnvir()
  : xyzGen(nullptr)
  , pblock(nullptr)
{
  for (int i = 0; i < NSUBTEX; i++)
    subtex[i] = nullptr;
  //TODO: Add all the initializing stuff
  GetHydraBackEnvirDesc()->MakeAutoParamBlocks(this);
  Reset();
}

HydraBackEnvir::~HydraBackEnvir()
{

}

//From MtlBase
void HydraBackEnvir::Reset()
{
  ivalid.SetEmpty();
  //if (xyzGen) 
  //	xyzGen->Reset();
  //else 
  //	ReplaceReference( COORD_REF, GetNewDefaultXYZGen());

  //TODO: Reset texmap back to its default values
  for (int i = NSUBMTL; i<NSUBTEX + NSUBMTL; i++)
  {
    if (subtex[i])
    {
      DeleteReference(i);
      subtex[i] = NULL;
    }
  }

  GetHydraBackEnvirDesc()->MakeAutoParamBlocks(this);

}

void HydraBackEnvir::Update(TimeValue t, Interval& valid)
{
  //TODO: Add code to evaluate anything prior to rendering
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(pb_backColor, t, backColor, ivalid);
    pblock->GetValue(pb_envirColor, t, envirColor, ivalid);

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (int i = 0; i<NSUBTEX; i++)
    {
      if (subtex[i])
        subtex[i]->Update(t, ivalid);
    }
  }

  valid &= ivalid;
}

Interval HydraBackEnvir::Validity(TimeValue t)
{
  //TODO: Update ivalid here
  Interval valid = FOREVER;

  for (int i = NSUBMTL; i<NSUBTEX; i++)
  {
    if (subtex[i])
      valid &= subtex[i]->Validity(t);
  }

  return ivalid;
}

ParamDlg* HydraBackEnvir::CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)
{
  IAutoMParamDlg* masterDlg = GetHydraBackEnvirDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  //xyzGenDlg = xyzGen->CreateParamDlg(hwMtlEdit, imp);
  //masterDlg->AddDlg(xyzGenDlg);
  //TODO: Set the user dialog proc of the param block, and do other initialization

  //hydraao_param_blk.SetUserDlgProc(new HydraAODlgProc(this));
  return masterDlg;
}

BOOL HydraBackEnvir::SetDlgThing(ParamDlg* dlg)
{
  //if (dlg == xyzGenDlg)
  //	xyzGenDlg->SetThing(xyzGen);
  //else
  //	return FALSE;
  //return TRUE;
  return FALSE;
}

void HydraBackEnvir::SetSubTexmap(int i, Texmap *m)
{
  //ReplaceReference(i + 2, m);
  ReplaceReference(i, m);
  //TODO Store the 'i-th' sub-texmap managed by the texture

  switch (i)
  {
  case 0:
    hydraBackEnvir_param_blk.InvalidateUI(pb_backMap);
    pblock->SetValue(pb_backMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 1:
    hydraBackEnvir_param_blk.InvalidateUI(pb_envirMap);
    pblock->SetValue(pb_envirMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  default:
    break;
  }

  NotifyChanged();

}

TSTR HydraBackEnvir::GetSubTexmapSlotName(int i)
{
  //TODO: Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_BACK_MAP);
  case 1:  return GetString(IDS_ENVIR_MAP);
  default: return _T("");
  }

  //return TSTR(_T(""));
}


//From ReferenceMaker
RefTargetHandle HydraBackEnvir::GetReference(int i)
{
  //TODO: Return the references based on the index
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBLOCK_REF: return pblock;
  //	default: return subtex[i-2];
  //	}

  if (i < NSUBTEX)
    return subtex[i];
  else
    return pblock;

}

void HydraBackEnvir::SetReference(int i, RefTargetHandle rtarg)
{
  //TODO: Store the reference handle passed into its 'i-th' reference
  //switch(i) {
  //	case COORD_REF:  xyzGen = (XYZGen *)rtarg; break;
  //	case PBLOCK_REF: pblock = (IParamBlock2 *)rtarg; break;
  //	default: subtex[i-2] = (Texmap *)rtarg; break;
  //}

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NSUBTEX && s != L"ParamBlock2")
    subtex[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

//From ReferenceTarget
RefTargetHandle HydraBackEnvir::Clone(RemapDir &remap)
{
  HydraBackEnvir *mnew = new HydraBackEnvir();
  *((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff

                                         //TODO: Add other cloning stuff

  mnew->ReplaceReference(NSUBMTL, remap.CloneRef(pblock));

  mnew->backColor = backColor;
  mnew->envirColor = envirColor;

  mnew->ivalid.SetEmpty();

  for (int i = 0; i<NSUBTEX; i++)
  {
    mnew->subtex[i] = NULL;
    if (subtex[i])
      mnew->ReplaceReference(i, remap.CloneRef(subtex[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}


Animatable* HydraBackEnvir::SubAnim(int i)
{
  //TODO: Return 'i-th' sub-anim
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBLOCK_REF: return pblock;
  //	default: return subtex[i-2];
  //	}

  if (i < NSUBTEX)
    return subtex[i];
  else
    return pblock;

}

TSTR HydraBackEnvir::SubAnimName(int i)
{
  //TODO: Return the sub-anim names
  //switch (i) {
  //	//case COORD_REF: return GetString(IDS_COORDS);
  //	case PBLOCK_REF: return GetString(IDS_PARAMS);
  //	default: return GetSubTexmapTVName(i-1);
  //	}

  return _T("");
}

RefResult HydraBackEnvir::NotifyRefChanged(const Interval& /*changeInt*/, RefTargetHandle hTarget, PartID& /*partID*/, RefMessage message, BOOL /*propagate*/)
{
  switch (message)
  {
  case REFMSG_TARGET_DELETED:
  {
    if (hTarget == xyzGen) { xyzGen = nullptr; }
    else if (hTarget == pblock) { pblock = nullptr; }
    else
    {
      for (int i = 0; i < NSUBTEX; i++)
      {
        if (subtex[i] == hTarget)
        {
          subtex[i] = nullptr;
          break;
        }
      }
    }
  }
  break;
  }
  return(REF_SUCCEED);
}

/*===========================================================================*\
|	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraBackEnvir::Save(ISave* isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res != IO_OK) return res;
  isave->EndChunk();

  isave->BeginChunk(HYDRA_CHUNK);
  isave->Write(&backColor, sizeof(Color), &nb);
  isave->Write(&envirColor, sizeof(Color), &nb);
  isave->EndChunk();

  return IO_OK;
}

IOResult HydraBackEnvir::Load(ILoad* iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
    case MTL_HDR_CHUNK:
      res = MtlBase::Load(iload);
      break;
    case HYDRA_CHUNK:
      res = iload->Read(&backColor, sizeof(Color), &nb);
      res = iload->Read(&envirColor, sizeof(Color), &nb);
      break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}

AColor HydraBackEnvir::EvalColor(ShadeContext& /*sc*/)
{
  //TODO: Evaluate the color of texture map for the context.
  return AColor(0.0f, 0.0f, 0.0f, 0.0f);
}

float HydraBackEnvir::EvalMono(ShadeContext& sc)
{
  //TODO: Evaluate the map for a "mono" channel
  return Intens(EvalColor(sc));
}

Point3 HydraBackEnvir::EvalNormalPerturb(ShadeContext& /*sc*/)
{
  //TODO: Return the perturbation to apply to a normal for bump mapping
  return Point3(0, 0, 0);
}

ULONG HydraBackEnvir::LocalRequirements(int subMtlNum)
{
  //TODO: Specify various requirements for the material
  //return xyzGen->Requirements(subMtlNum);
  return 0;
}

void HydraBackEnvir::NotifyChanged()
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

FPInterface * HydraBackEnvirClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraBackEnvirClassDesc::GetEntryName() const
{
  return const_cast<HydraBackEnvirClassDesc*>(this)->ClassName();
}

const MCHAR * HydraBackEnvirClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Maps\\Hydra";
  return str;
}

Bitmap * HydraBackEnvirClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}

