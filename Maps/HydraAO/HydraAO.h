#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

//#include "3dsmaxsdk_preinclude.h"
#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>
#include <IMaterialBrowserEntryInfo.h>

#include <stdmat.h>
#include <imtl.h>
#include <macrorec.h>

#include "3dsmaxport.h"


extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;


#define HydraAO_CLASS_ID	Class_ID(0x423c85a3, 0x83d993c6)

#define NSUBMTL		0 // TODO: number of sub-materials supported by this plugin 
#define NSUBTEX		3 // TODO: number of sub-textures supported by this plugin
// Reference Indexes
//#define COORD_REF  0

#define OCCLUDED_TEX_SLOT   0
#define UNOCCLUDED_TEX_SLOT 1
#define DISTANCE_TEX_SLOT   2

#define PBLOCK_REF	NSUBMTL+NSUBTEX

typedef std::wstring hydraStr;
typedef wchar_t hydraChar;

class HydraAO;

class HydraAO : public Texmap
{
public:
  //Constructor/Destructor
  HydraAO();
  virtual ~HydraAO();

  // References
  XYZGen*          xyzGen;          // ref 0
  IParamBlock2*    pblock;          // ref 1
  Texmap*          subtex[NSUBTEX]; // Reference array of sub-materials

                                    //static ParamDlg* xyzGenDlg;
  Interval         ivalid;

  Color occludedColor, unoccludedColor;
  float distanceMult, falloffMult;
  int calculateFor;
  BOOL onlyForThisOn;


  //From MtlBase
  virtual ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
  virtual BOOL      SetDlgThing(ParamDlg* dlg);
  virtual void      Update(TimeValue t, Interval& valid);
  virtual void      Reset();
  virtual Interval  Validity(TimeValue t);
  virtual ULONG     LocalRequirements(int subMtlNum);

  void NotifyChanged();

  //TODO: Return the number of sub-textures
  virtual int NumSubTexmaps() { return NSUBTEX; }
  //TODO: Return the pointer to the 'i-th' sub-texmap
  virtual Texmap* GetSubTexmap(int i) { return subtex[i]; }
  virtual void SetSubTexmap(int i, Texmap *m);
  virtual TSTR GetSubTexmapSlotName(int i);

  //From Texmap
  virtual RGBA EvalColor(ShadeContext& sc);
  virtual float EvalMono(ShadeContext& sc);
  virtual Point3 EvalNormalPerturb(ShadeContext& sc);

  virtual XYZGen *GetTheXYZGen() { return xyzGen; }

  //TODO: Return anim index to reference index
  virtual int SubNumToRefNum(int subNum) { return subNum; }

  //TODO: If your class is derived from Tex3D then you should also
  //implement ReadSXPData for 3D Studio/DOS SXP texture compatibility
  virtual void ReadSXPData(TCHAR* /*name*/, void* /*sxpdata*/) { }

  // Loading/Saving
  virtual IOResult Load(ILoad *iload);
  virtual IOResult Save(ISave *isave);

  //From Animatable
  virtual Class_ID ClassID() { return HydraAO_CLASS_ID; }
  virtual SClass_ID SuperClassID() { return TEXMAP_CLASS_ID; }
  virtual void GetClassName(TSTR& s) { s = GetString(IDS_CLASS_NAME); }

  virtual int NumSubs() { return 2 + NSUBTEX; }
  virtual Animatable* SubAnim(int i);
  virtual TSTR SubAnimName(int i);

  // TODO: Maintain the number or references here
  virtual int NumRefs() { return 2 + NSUBTEX; }
  virtual RefTargetHandle GetReference(int i);

  virtual RefTargetHandle Clone(RemapDir &remap);
  virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);

  virtual int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
  virtual IParamBlock2* GetParamBlock(int /*i*/) { return pblock; } // return i'th ParamBlock
  virtual IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

  virtual void DeleteThis() { delete this; }
protected:
  virtual void SetReference(int i, RefTargetHandle rtarg);

};


class HydraAOClassDesc : public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  virtual int IsPublic() { return TRUE; }
  virtual void* Create(BOOL /*loading = FALSE*/) { return new HydraAO(); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
  virtual SClass_ID SuperClassID() { return TEXMAP_CLASS_ID; }
  virtual Class_ID ClassID() { return HydraAO_CLASS_ID; }
  virtual const TCHAR* Category() { return GetString(IDS_CATEGORY); }

  virtual const TCHAR* InternalName() { return _T("HydraAO"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() { return hInstance; }					// returns owning module handle

                                                              // For entry category
  virtual FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  virtual const MCHAR* GetEntryName() const override;
  virtual const MCHAR* GetEntryCategory() const override;
  virtual Bitmap* GetEntryThumbnail() const override;
};


class hydraAODlgProc : public ParamMap2UserDlgProc {
public:
  HydraAO *hM;
  HWND thishWnd;

  ICustButton *gui_occludedMap;
  ICustButton *gui_unoccludedMap;
  ICustButton *gui_distanceMap;

  IColorSwatch *gui_occluded_color;
  IColorSwatch *gui_unoccluded_color;

  ICustEdit *gui_distance_edit;
  ICustEdit *gui_falloff_edit;

  ISpinnerControl *gui_distance_spin;
  ISpinnerControl *gui_falloff_spin;

  bool additional_init;

  hydraAODlgProc(HydraAO *cb);
  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() { delete this; }
};

ClassDesc2* GetHydraAODesc();

enum { hydraao_params };

//TODO: Add enums for various parameters
enum {
  pb_occludedMap,
  pb_occludedColor,
  pb_unoccludedMap,
  pb_unoccludedColor,
  pb_distanceMap,
  pb_distanceMult,
  pb_calculateFor,
  pb_falloffMult,
  pb_onlyForThis,
};
