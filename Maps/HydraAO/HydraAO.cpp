//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
//
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR:
//***************************************************************************/

#include "HydraAO.h"


static ParamBlockDesc2 hydraao_param_blk ( hydraao_params, _T("params"),  0, GetHydraAODesc(), 
	P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF, 
	//rollout
	IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
	// params
  pb_occludedMap, _T("mtl_occluded_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OCCLUDED_MAP,
    p_refno, 0,
    p_subtexno, 0,
    p_ui, TYPE_TEXMAPBUTTON, IDC_OCCLUDED_MAP,
    p_end,
  pb_occludedColor, _T("mtl_occluded_color"), TYPE_RGBA, P_ANIMATABLE, IDS_OCCLUDED_COLOR,
    p_default, Color(0.0, 0.0, 0.0),
    p_ui, TYPE_COLORSWATCH, IDC_OCCLUDED_COLOR,
    p_end,
  pb_unoccludedMap, _T("mtl_unoccluded_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_UNOCCLUDED_MAP,
    p_refno, 1,
    p_subtexno, 1,
    p_ui, TYPE_TEXMAPBUTTON, IDC_UNOCCLUDED_MAP,
    p_end,
  pb_unoccludedColor, _T("mtl_unoccluded_color"), TYPE_RGBA, P_ANIMATABLE, IDS_UNOCCLUDED_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_UNOCCLUDED_COLOR,
    p_end,
  pb_distanceMap, _T("mtl_distance_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_DISTANCE_MAP,
    p_refno, 2,
    p_subtexno, 2,
    p_ui, TYPE_TEXMAPBUTTON, IDC_DISTANCE_MAP,
    p_end,
  pb_distanceMult, _T("mtl_distance_mult"), TYPE_FLOAT, P_ANIMATABLE, IDC_DISTANCE_MULT_SPIN,
  	p_default, 0.1f, 
  	p_range, 0.0f, 1000.0f, 
  	p_ui, TYPE_SPINNER,	EDITTYPE_FLOAT, IDC_DISTANCE_MULT, IDC_DISTANCE_MULT_SPIN, 0.1f,
  	p_end,
  pb_calculateFor, _T("mtl_calculate_for"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_DISTRIB,
    p_ui, TYPE_INT_COMBOBOX, IDC_DISTRIB_COMBO, 3, IDS_CONCAVE, IDS_CONVEX, IDS_BOTH,
    p_tooltip, IDS_DISTRIB,
    p_end,
  pb_falloffMult, _T("mtl_falloff_mult"), TYPE_FLOAT, P_ANIMATABLE, IDC_FALLOFF_MULT_SPIN,
    p_default, 1.0f,
    p_range, 0.1f, 10.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_FALLOFF_MULT, IDC_FALLOFF_MULT_SPIN, 0.1f,
    p_end,
  pb_onlyForThis, _T("mtl_only_for_this_on"), TYPE_BOOL, 0, IDS_ONLY_FOR_THIS,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_ONLY_FOR_THIS_ON,
    p_end,
	p_end
	);


ClassDesc2* GetHydraAODesc() {
  static HydraAOClassDesc HydraAODesc;
  return &HydraAODesc;
}


hydraAODlgProc::hydraAODlgProc(HydraAO * cb)
{
  hM = cb;
  additional_init = true;

  gui_occludedMap = nullptr;
  gui_unoccludedMap = nullptr;
  gui_distanceMap = nullptr;

  gui_occluded_color = nullptr;
  gui_unoccluded_color = nullptr;

  gui_distance_edit = nullptr;
  gui_falloff_edit = nullptr;

  gui_distance_spin = nullptr;
  gui_falloff_spin = nullptr;

}

INT_PTR hydraAODlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd = hWnd;
  hydraAODlgProc *dlg = DLGetWindowLongPtr<hydraAODlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
  case WM_INITDIALOG:
  {
    //dlg = (hydraMaterialDlgProc *)lParam;
    //DLSetWindowLongPtr(hWnd, dlg);

    if (!gui_distance_edit) gui_distance_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DISTANCE_MULT));
    if (!gui_falloff_edit) gui_falloff_edit = GetICustEdit(GetDlgItem(hWnd, IDC_FALLOFF_MULT));

    if (!gui_distance_spin) gui_distance_spin = GetISpinner(GetDlgItem(hWnd, IDC_DISTANCE_MULT_SPIN));
    if (!gui_falloff_spin) gui_falloff_spin = GetISpinner(GetDlgItem(hWnd, IDC_FALLOFF_MULT_SPIN));

    if (!gui_occludedMap) gui_occludedMap = GetICustButton(GetDlgItem(hWnd, IDC_OCCLUDED_MAP));
    if (!gui_unoccludedMap) gui_unoccludedMap = GetICustButton(GetDlgItem(hWnd, IDC_UNOCCLUDED_MAP));
    if (!gui_distanceMap) gui_distanceMap = GetICustButton(GetDlgItem(hWnd, IDC_DISTANCE_MAP));

    gui_occluded_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_OCCLUDED_COLOR));
    gui_unoccluded_color = GetIColorSwatch(GetDlgItem(hWnd, IDC_UNOCCLUDED_COLOR));

    DragAcceptFiles(GetDlgItem(hWnd, IDC_OCCLUDED_MAP), true);
    DragAcceptFiles(GetDlgItem(hWnd, IDC_UNOCCLUDED_MAP), true);
    DragAcceptFiles(GetDlgItem(hWnd, IDC_DISTANCE_MAP), true);

    return TRUE;
  }
  case WM_COMMAND:
    switch (LOWORD(wParam))
    {
    case IDC_OCCLUDED_MAP:
    case IDC_UNOCCLUDED_MAP:
    case IDC_DISTANCE_MAP:
      if (HIWORD(wParam) == BN_BUTTONUP || HIWORD(wParam) == BN_RIGHTCLICK || HIWORD(wParam) == BN_BUTTONDOWN)
      {
      }
      break;
    }
    break;
  case WM_DESTROY:
    ReleaseICustButton(gui_occludedMap);
    ReleaseICustButton(gui_unoccludedMap);
    ReleaseICustButton(gui_distanceMap);

    ReleaseISpinner(gui_distance_spin);
    ReleaseISpinner(gui_falloff_spin);

    ReleaseICustEdit(gui_distance_edit);
    ReleaseICustEdit(gui_falloff_edit);

    ReleaseIColorSwatch(gui_occluded_color);
    ReleaseIColorSwatch(gui_unoccluded_color);

    break;
  case WM_DROPFILES:
    POINT pt;
    WORD numFiles;
    HWND dropTarget;
    int dropTargetID;

    hydraChar lpszFile[80];

    DragQueryPoint((HDROP)wParam, &pt);

    numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

    if (numFiles == 0)
    {
      DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
      dropTarget = RealChildWindowFromPoint(hWnd, pt);
      dropTargetID = GetDlgCtrlID(dropTarget);
      DropFileInMapSlot(dropTargetID, lpszFile);
    }

    DragFinish((HDROP)wParam);

    break;
  default:
    return FALSE;
  }
  return TRUE;
}

void hydraAODlgProc::DropFileInMapSlot(int mapControlID, hydraChar * filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap *tex = (Texmap *)btex;

  switch (mapControlID)
  {
  case IDC_OCCLUDED_MAP:
    hM->SetSubTexmap(OCCLUDED_TEX_SLOT, tex);
    hM->subtex[OCCLUDED_TEX_SLOT] = tex;
    break;
  case IDC_UNOCCLUDED_MAP:
    hM->SetSubTexmap(UNOCCLUDED_TEX_SLOT, tex);
    hM->subtex[UNOCCLUDED_TEX_SLOT] = tex;
    break;
  case IDC_DISTANCE_MAP:
    hM->SetSubTexmap(DISTANCE_TEX_SLOT, tex);
    hM->subtex[DISTANCE_TEX_SLOT] = tex;
    break;
  default:
    break;
  }
}

//ParamDlg* HydraAO::xyzGenDlg;

//--- HydraAO -------------------------------------------------------
HydraAO::HydraAO()
	: xyzGen(nullptr)
	, pblock(nullptr)
{
	for (int i = 0; i < NSUBTEX; i++)
		subtex[i] = nullptr;
	//TODO: Add all the initializing stuff
	GetHydraAODesc()->MakeAutoParamBlocks(this);
	Reset();
}

HydraAO::~HydraAO()
{

}

//From MtlBase
void HydraAO::Reset()
{
	ivalid.SetEmpty();
	//if (xyzGen) 
	//	xyzGen->Reset();
	//else 
	//	ReplaceReference( COORD_REF, GetNewDefaultXYZGen());

	//TODO: Reset texmap back to its default values
  for (int i = NSUBMTL; i<NSUBTEX + NSUBMTL; i++)
  {
    if (subtex[i])
    {
      DeleteReference(i);
      subtex[i] = NULL;
    }
  }

  GetHydraAODesc()->MakeAutoParamBlocks(this);

}

void HydraAO::Update(TimeValue t, Interval& valid) 
{
	//TODO: Add code to evaluate anything prior to rendering
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(pb_occludedColor, t, occludedColor, ivalid);
    pblock->GetValue(pb_unoccludedColor, t, unoccludedColor, ivalid);
    pblock->GetValue(pb_distanceMult, t, distanceMult, ivalid);
    pblock->GetValue(pb_calculateFor, t, calculateFor, ivalid);
    pblock->GetValue(pb_falloffMult, t, falloffMult, ivalid);
    pblock->GetValue(pb_onlyForThis, t, onlyForThisOn, ivalid);

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (int i = 0; i<NSUBTEX; i++)
    {
      if (subtex[i])
        subtex[i]->Update(t, ivalid);
    }
  }

  valid &= ivalid;
}

Interval HydraAO::Validity(TimeValue t)
{
	//TODO: Update ivalid here
  Interval valid = FOREVER;

  for (int i = NSUBMTL; i<NSUBTEX; i++)
  {
    if (subtex[i])
      valid &= subtex[i]->Validity(t);
  }

	return ivalid;
}

ParamDlg* HydraAO::CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)
{
	IAutoMParamDlg* masterDlg = GetHydraAODesc()->CreateParamDlgs(hwMtlEdit, imp, this);
	//xyzGenDlg = xyzGen->CreateParamDlg(hwMtlEdit, imp);
	//masterDlg->AddDlg(xyzGenDlg);
	//TODO: Set the user dialog proc of the param block, and do other initialization
  
  //hydraao_param_blk.SetUserDlgProc(new HydraAODlgProc(this));
	return masterDlg;
}

BOOL HydraAO::SetDlgThing(ParamDlg* dlg)
{
	//if (dlg == xyzGenDlg)
	//	xyzGenDlg->SetThing(xyzGen);
	//else
	//	return FALSE;
	//return TRUE;
  return FALSE;
}

void HydraAO::SetSubTexmap(int i, Texmap *m)
{
	//ReplaceReference(i + 2, m);
  ReplaceReference(i, m);
	//TODO Store the 'i-th' sub-texmap managed by the texture

  switch (i)
  {
  case 0:
    hydraao_param_blk.InvalidateUI(pb_occludedMap);
    pblock->SetValue(pb_occludedMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 1:
    hydraao_param_blk.InvalidateUI(pb_unoccludedMap);
    pblock->SetValue(pb_unoccludedMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  case 2:
    hydraao_param_blk.InvalidateUI(pb_distanceMap);
    pblock->SetValue(pb_distanceMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  default:
    break;
  }

  NotifyChanged();

}

TSTR HydraAO::GetSubTexmapSlotName(int i)
{
	//TODO: Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_OCCLUDED_MAP);
  case 1:  return GetString(IDS_UNOCCLUDED_MAP);
  case 2:  return GetString(IDS_DISTANCE_MAP);
  default: return _T("");
  }

	//return TSTR(_T(""));
}


//From ReferenceMaker
RefTargetHandle HydraAO::GetReference(int i)
{
	//TODO: Return the references based on the index
	//switch (i) {
	//	case COORD_REF: return xyzGen;
	//	case PBLOCK_REF: return pblock;
	//	default: return subtex[i-2];
	//	}

  if (i < NSUBTEX)
    return subtex[i];
  else
    return pblock;

}

void HydraAO::SetReference(int i, RefTargetHandle rtarg)
{
	//TODO: Store the reference handle passed into its 'i-th' reference
	//switch(i) {
	//	case COORD_REF:  xyzGen = (XYZGen *)rtarg; break;
	//	case PBLOCK_REF: pblock = (IParamBlock2 *)rtarg; break;
	//	default: subtex[i-2] = (Texmap *)rtarg; break;
	//}

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NSUBTEX && s != L"ParamBlock2")
    subtex[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

//From ReferenceTarget
RefTargetHandle HydraAO::Clone(RemapDir &remap)
{
	HydraAO *mnew = new HydraAO();
	*((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff
	
  //TODO: Add other cloning stuff

  mnew->ReplaceReference(NSUBMTL, remap.CloneRef(pblock));

  mnew->occludedColor = occludedColor;
  mnew->unoccludedColor = unoccludedColor;
  mnew->distanceMult = distanceMult;
  mnew->calculateFor = calculateFor;
  mnew->falloffMult = falloffMult;
  mnew->onlyForThisOn = onlyForThisOn;

  mnew->ivalid.SetEmpty();

  for (int i = 0; i<NSUBTEX; i++)
  {
    mnew->subtex[i] = NULL;
    if (subtex[i])
      mnew->ReplaceReference(i, remap.CloneRef(subtex[i]));
  }

	BaseClone(this, mnew, remap);
	return (RefTargetHandle)mnew;
}


Animatable* HydraAO::SubAnim(int i)
{
	//TODO: Return 'i-th' sub-anim
	//switch (i) {
	//	case COORD_REF: return xyzGen;
	//	case PBLOCK_REF: return pblock;
	//	default: return subtex[i-2];
	//	}
  
  if (i < NSUBTEX) 
    return subtex[i];
  else 
    return pblock;

}

TSTR HydraAO::SubAnimName(int i)
{
	//TODO: Return the sub-anim names
	//switch (i) {
	//	//case COORD_REF: return GetString(IDS_COORDS);
	//	case PBLOCK_REF: return GetString(IDS_PARAMS);
	//	default: return GetSubTexmapTVName(i-1);
	//	}

  return _T("");
}

RefResult HydraAO::NotifyRefChanged(const Interval& /*changeInt*/, RefTargetHandle hTarget,PartID& /*partID*/, RefMessage message, BOOL /*propagate*/ )
{
	switch (message)
	{
	case REFMSG_TARGET_DELETED:
		{
			if      (hTarget == xyzGen) { xyzGen = nullptr; }
			else if (hTarget == pblock) { pblock = nullptr; }
			else
			{
				for (int i = 0; i < NSUBTEX; i++)
				{
					if (subtex[i] == hTarget)
					{
						subtex[i] = nullptr;
						break;
					}
				}
			}
		}
		break;
	}
	return(REF_SUCCEED);
}

/*===========================================================================*\
|	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraAO::Save(ISave* isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  if (res != IO_OK) return res;
  isave->EndChunk();

  isave->BeginChunk(HYDRA_CHUNK);
  isave->Write(&occludedColor, sizeof(Color), &nb);
  isave->Write(&unoccludedColor, sizeof(Color), &nb);
  isave->Write(&distanceMult, sizeof(float), &nb);
  isave->Write(&falloffMult, sizeof(float), &nb);
  isave->Write(&calculateFor, sizeof(int), &nb);
  isave->Write(&onlyForThisOn, sizeof(bool), &nb);
  isave->EndChunk();

	return IO_OK;
}

IOResult HydraAO::Load(ILoad* iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
    case MTL_HDR_CHUNK:
      res = MtlBase::Load(iload);
      break;
    case HYDRA_CHUNK:
      res = iload->Read(&occludedColor, sizeof(Color), &nb);
      res = iload->Read(&unoccludedColor, sizeof(Color), &nb);
      res = iload->Read(&distanceMult, sizeof(float), &nb);
      res = iload->Read(&calculateFor, sizeof(int), &nb);
      res = iload->Read(&falloffMult, sizeof(float), &nb);
      res = iload->Read(&onlyForThisOn, sizeof(bool), &nb);
      break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}

AColor HydraAO::EvalColor(ShadeContext& /*sc*/)
{
	//TODO: Evaluate the color of texture map for the context.
	return AColor (0.0f, 0.0f, 0.0f, 0.0f);
}

float HydraAO::EvalMono(ShadeContext& sc)
{
	//TODO: Evaluate the map for a "mono" channel
	return Intens(EvalColor(sc));
}

Point3 HydraAO::EvalNormalPerturb(ShadeContext& /*sc*/)
{
	//TODO: Return the perturbation to apply to a normal for bump mapping
	return Point3(0, 0, 0);
}

ULONG HydraAO::LocalRequirements(int subMtlNum)
{
	//TODO: Specify various requirements for the material
	//return xyzGen->Requirements(subMtlNum);
  return 0;
}

void HydraAO::NotifyChanged()
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

FPInterface * HydraAOClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraAOClassDesc::GetEntryName() const
{
  return const_cast<HydraAOClassDesc*>(this)->ClassName();
} 

const MCHAR * HydraAOClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Maps\\Hydra";
  return str;
}

Bitmap * HydraAOClassDesc::GetEntryThumbnail() const
{
  return nullptr;
}

