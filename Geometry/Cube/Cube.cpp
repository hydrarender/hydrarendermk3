//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: 
//***************************************************************************/

#include "Cube.h"


ClassDesc2* GetCubeDesc() { return &cubeDesc; }


// ���� ���������� ������� IDD_KBRD
static ParamBlockDesc2 cube_kbrd_blk (cube_kbrd, _T("cubeKbrd"), 0, &cubeDesc, P_CLASS_PARAMS + P_AUTO_UI,
  IDD_KBRD, IDS_KBRD, BEGIN_EDIT_CREATE, APPENDROLL_CLOSED, NULL,
  cube_kb_size, _T("kbSize"), TYPE_FLOAT, 0, IDS_CB_SIZE,
    p_default, 40.0, p_range, 0.0f, 100.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_KBSZ, IDC_KBSZSPIN, 0.1f,
    p_end,
  p_end
);
// ���� ���������� ������� IDD_PARAMS
static ParamBlockDesc2 cube_param_blk (cube_params, _T("params"), 0, &cubeDesc, P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
  IDD_PARAMS, IDS_PARAMS, 0, 0, NULL,
  cube_size, _T("size"), TYPE_FLOAT, P_ANIMATABLE, IDS_CB_SIZE,
    p_default, 0.0f, p_range, 0.0f, 100.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_SZ, IDC_SZSPIN, 0.1f,
    p_end,
  p_end
);



//--- cube -------------------------------------------------------

// ������������� ������� ip � kbrdCreate ������ cube
IObjParam *cube::ip = NULL;
BOOL cube::kbrdCreate = FALSE;

// ������� ������ � P_AUTO_CONSTRUCT-������ ����������
// ������ IDD_KBRD ����� ������ ��� ��������� cubeDesc.BeginEditParams
cube::cube() { cubeDesc.MakeAutoParamBlocks(this); }

IOResult cube::Load(ILoad *iload) { return IO_OK; }
IOResult cube::Save(ISave *isave) { return IO_OK; }


// ������������ �������� ���� ����� ������� �� ������ Create ������� IDD_KBRD
INT_PTR cubeKBDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
  if (msg != WM_COMMAND) return FALSE;
  if (LOWORD(wParam) != IDC_CREATE) return FALSE;

  float cbSize = cube_kbrd_blk.GetFloat(cube_kb_size);
  if (cbSize == 0.0) return TRUE;
  
  // ������������� IDC_SZ = IDC_KBSZ
  if (ob->TestAFlag(A_OBJ_CREATING)) ob->pblock2->SetValue(cube_size, 0, cbSize);

  // ���� ������� �����
  ob->kbrdCreate = TRUE;

  // ��������� ��������� �������
  Matrix3 tm(1);

  // ������������� � ������� ������ Translate (�����������) �������� ��������������
  tm.SetTrans(Point3(0, 0, 0));
  ob->suspendSnap = FALSE;

  // ��������� ���
  ob->ip->NonMouseCreate(tm);
  return TRUE;
}


// ���������� ��� �������� ���������� ���������� ���� (�� ������ Animatable)
void cube::BeginEditParams(IObjParam* ip, ULONG flags, Animatable* prev)
{
	SimpleObject2::BeginEditParams(ip,flags,prev);
  this->ip = ip;
  
  if (kbrdCreate)
  {
    // ���� ����� ��� �������� ������ ����, �� ������������� IDC_SZ = IDC_KBSZ
    pblock2->SetValue(cube_size, 0, cube_kbrd_blk.GetFloat(cube_kb_size));
    kbrdCreate = FALSE;
  }
	
  GetCubeDesc()->BeginEditParams(ip, this, flags, prev);

  // ��������� ���������������� ���������, ��������������� � ������ ���������� cube_kbrd_blk
  cube_kbrd_blk.SetUserDlgProc(new cubeKBDlgProc(this));
}

void cube::EndEditParams( IObjParam* ip, ULONG flags, Animatable* next )
{
	//TODO: Save plugin parameter values into class variables, if they are not hosted in ParamBlocks. 
	SimpleObject2::EndEditParams(ip,flags,next);
	GetCubeDesc()->EndEditParams(ip, this, flags, next);

  // ������ ������ �������� ������ ���������� ip ������ ����� BeginEditParams � EndEditParams
  this->ip = NULL;
}

//From Object
// ������� ���� ������� � ������� UVW-���������.
BOOL cube::HasUVW() { return TRUE; }

// ����� ��������������, ������ �� ����� �������.
void cube::SetGenUVW(BOOL sw) 
{
	if (sw == HasUVW()) 
		return;
	//TODO: Set the plugin's internal value to sw
}


int cubeCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat )
{
	//TODO: Implement the mouse creation code here
	if ( ! vpt || ! vpt->IsAlive() )
	{
		// why are we here
		DbgAssert(!_T("Invalid viewport!"));
		return FALSE;
	}

	if (msg == MOUSE_POINT || msg == MOUSE_MOVE) 
  {
		switch(point) 
    {
		case 0: // only happens with MOUSE_POINT msg
			ob->suspendSnap = TRUE;
      // m - ������� ���� � ������� �����������.
      // p0 - ������� ���� � ������� �����������.
			sp0 = m;
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE);
      // ������ Translate (�����������) �������� �������������� �������.
			mat.SetTrans(p0);
      // �������� �������� ��������� cube_size ������� IDD_PARAMS.
      ob->pblock2->SetValue(cube_size, ob->ip->GetTime(), 0.0f);
			break;
		//TODO: �������� ��� ��������� �����.
    case 1: {
      ob->suspendSnap = TRUE;
      // p1 - ����� ������� ���� � ������� �����������.
      p1 = vpt->SnapPoint(m, m, NULL, SNAP_IN_PLANE);
      // ��������� �������� ���� � ����������� �� ��������� ����.
      ob->pblock2->SetValue(cube_size, ob->ip->GetTime(), 0.5f * Length(p1 - p0));
      // ������� � ���������� ��� � ������� �����.
      cube_param_blk.InvalidateUI();
      break;
    }
		case 2:
      return CREATE_STOP;
		}
	} 
  else {
		if (msg == MOUSE_ABORT) return CREATE_ABORT;
	}

	return TRUE;
}

static cubeCreateCallBack cubeCreateCB;

//From BaseObject
CreateMouseCallBack* cube::GetCreateMouseCallBack() 
{
	cubeCreateCB.SetObj(this);
	return (&cubeCreateCB);
}

//From SimpleObject
// �������� ������ ����, ������ ��� ��� � ��������� ������ �����������.
void cube::BuildMesh(TimeValue t)
{
  // ���������� ��� ��� ���������� ������������� ����� �������, ��������� ��� ��������� 
  // ���������� � ������ ��������. ������������ ������ ������ ������������ ����� �������� ������ 
  // ��� �������� ����������� �����. ���� simpleobject ivalid ������ ���� ��������. ��� ����� 
  // ������� ��� ���������� �������� �� pblock2.
  
  float size, h;
  ivalid = FOREVER;
  pblock2->GetValue(cube_size, t, size, ivalid);
  h = 0.5 * size;
  

  // ����� ������ � ������ � ���.
  mesh.setNumVerts(8);
  mesh.setNumFaces(12);
  
  // ���������� ������.
  mesh.setVert(0, Point3(-h, -h, -h));
  mesh.setVert(1, Point3(h, -h, -h));
  mesh.setVert(2, Point3(-h, h, -h));
  mesh.setVert(3, Point3(h, h, -h));
  mesh.setVert(4, Point3(-h, -h, h));
  mesh.setVert(5, Point3(h, -h, h));
  mesh.setVert(6, Point3(-h, h, h));
  mesh.setVert(7, Point3(h, h, h));

  // ������ ������.
  mesh.faces[0].setVerts(0, 2, 3);
  mesh.faces[1].setVerts(3, 1, 0);
  mesh.faces[2].setVerts(4, 5, 7);
  mesh.faces[3].setVerts(7, 6, 4);
  mesh.faces[4].setVerts(0, 1, 5);
  mesh.faces[5].setVerts(5, 4, 0);
  mesh.faces[6].setVerts(1, 3, 7);
  mesh.faces[7].setVerts(7, 5, 1);
  mesh.faces[8].setVerts(3, 2, 6);
  mesh.faces[9].setVerts(6, 7, 3);
  mesh.faces[10].setVerts(2, 0, 4);
  mesh.faces[11].setVerts(4, 6, 2);

  // ������ �����������.
  mesh.faces[0].setSmGroup(2);
  mesh.faces[1].setSmGroup(2);
  mesh.faces[2].setSmGroup(4);
  mesh.faces[3].setSmGroup(4);
  mesh.faces[4].setSmGroup(8);
  mesh.faces[5].setSmGroup(8);
  mesh.faces[6].setSmGroup(16);
  mesh.faces[7].setSmGroup(16);
  mesh.faces[8].setSmGroup(32);
  mesh.faces[9].setSmGroup(32);
  mesh.faces[10].setSmGroup(64);
  mesh.faces[11].setSmGroup(64);

  // ������������� ��������� ������� ����� ������ (������������ ����� �� �����).
  for (int k = 0; k < 12; k++)
  {
    mesh.faces[k].setEdgeVisFlags(1, 1, 0);
    mesh.faces[k].setMatID(1);
  }

  // ��������� UVW-����������.
  Matrix3 tm(1);
  tm.Scale(Point3(h, h, h));
  tm = Inverse(tm);
  mesh.ApplyUVWMap(MAP_BOX, 1.0f, 1.0f, 1.0f, 0, 0, 0, 0, tm);
  mesh.InvalidateTopologyCache();
}

// �������� ��� �������� ������������ ��������� size.
BOOL cube::OKtoDisplay(TimeValue t)
{
  float size;
  pblock2->GetValue(cube_size, t, size, FOREVER);
  return (size <= 0.0f) ? FALSE : TRUE;
}

void cube::InvalidateUI() 
{
  cube_param_blk.InvalidateUI(pblock2->LastNotifyParamID());
	// Hey! Update the param blocks
	//pblock2->GetDesc()->InvalidateUI();
}

// From Object
// ������� ����� ����������� ���� ray � ������������ (��. ����� Ray)
// � ������� � ����������� � ���� �����.
int cube::IntersectRay(TimeValue t, Ray& ray, float& at, Point3& norm)
{
  return SimpleObject::IntersectRay(t, ray, at, norm);
  //TODO: Return TRUE after you implement this method
  //return FALSE;
}

// ������, �������������� �������������� � ����������� �������.
Object* cube::ConvertToType(TimeValue t, Class_ID obtype)
{
  // ���� ������ ����� �������������� � nurbs - ����������� ����� ���������, 
  // �� ��������� �� obtype == EDITABLE_SURF_CLASS_ID � �������������� ������� ��� nurbs
  // - ����������� � ������� ������, ���� ��� ������������ �������������� ���������� �������
  // ��� ����������.
	
	return SimpleObject::ConvertToType(t,obtype);
}

int cube::CanConvertToType(Class_ID obtype)
{
  if (obtype == defObjectClassID || obtype == triObjectClassID)
    return 1;
  else
    return SimpleObject::CanConvertToType(obtype);

  // ��������� ������� �������������� �����, �������������� ��������.
  // ���� ����������� �������������� �� ���������, ������� ��� ����������.
	//return SimpleObject::CanConvertToType(obtype);
}

void cube::GetCollapseTypes(Tab<Class_ID>& clist,Tab<TSTR*>& nlist)
{
	Object::GetCollapseTypes(clist, nlist);
	//TODO: Append any any other collapse type the plugin supports
}

// From ReferenceTarget
RefTargetHandle cube::Clone(RemapDir& remap) 
{
	cube* newob = new cube();	
	// �������� ����� ���� ������, � ����� ����������� ��� ������.
	newob->ReplaceReference(0,remap.CloneRef(pblock2));
	newob->ivalid.SetEmpty();
	BaseClone(this, newob, remap);
	return(newob);
}
