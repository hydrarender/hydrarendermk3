//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Contains the Dll Entry stuff
// AUTHOR: 
//***************************************************************************/

#include "Cube.h"

extern ClassDesc2* GetCubeDesc();

HINSTANCE hInstance;
int controlsInit = FALSE;

// ��� ������� ���������� Windows ��� �������� ���������� DLL.
// ��� ������� ����� ����� ���� ������ ��������� ��� �� ����� ����������� ��������, 
// ����� ��� ���������. ������� ������������ ������ ���� ���������, ��� ��� ������ ������ 
// ���� �������. � ����������� ���� ���� �������� ��������, ��� ����� �������� ���������� 
// DLL � ������ ��� ����������� ������ ��������� ����������.
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID /*lpvReserved*/)
{
	if( fdwReason == DLL_PROCESS_ATTACH )
	{
		MaxSDK::Util::UseLanguagePackLocale();
		// Hang on to this DLL's instance handle.
		hInstance = hinstDLL;
		DisableThreadLibraryCalls(hInstance);
		// DO NOT do any initialization here. Use LibInitialize() instead.
	}
	return(TRUE);
}

// ��� ������� ���������� ������, ����������� dll � ��� ������������ ����� ���������� dll,
// ���� � ��� ��� ���.
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

// ���������� ����� ������� �������.
// ���������� �������� ��� ����� ��� ���������� ������ ������.
// � ����� ������ DLL ��������� ����������� ����� ������� Cube.
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

// ���������� �������� i-�� ������ �������
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: return GetCubeDesc();
		default: return 0;
	}
}

// ��� ������� ���������� ���������������� ���������, ����������� ������ �������,
// � ������� ��� ���� ��������������. �� ������������, ����� ��������� ������� �������
// ���������� ���������� dll.
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

// ���������� ���� ��� ��� �������� ������� � 3ds Max.
// ������� TRUE, ���� �� ��������, ��� ��� ������ ������� ��������, ��� FALSE � ��������� ������. 
// ���� � �������� ���������� ������� FALSE, �� ������� �� ����� ��������� ������,
// � DLL ����� ������������������ ��� ��������� ����������, � �������� ��� ���������.
__declspec( dllexport ) int LibInitialize(void)
{
	#pragma message(TODO("Perform initialization here."))
	return TRUE;
}

// This function is called once, just before the plugin is unloaded. 
// Perform one-time plugin un-initialization in this method."
// The system doesn't pay attention to a return value.

// ���������� ���� ��� ��� �������� ������� �� 3ds Max
// ������������ ��������� ����������� �� ������������.
__declspec( dllexport ) int LibShutdown(void)
{
	#pragma message(TODO("Perform un-initialization here."))
	return TRUE;
}

// ���������� ������ ������� �������� �������.
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, _countof(buf)) ? buf : NULL;
	return NULL;
}

