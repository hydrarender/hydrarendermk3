#pragma once

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: 
//***************************************************************************/

//#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <Simpobj.h>




extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;


#define cube_CLASS_ID	Class_ID(0xaf68a1bf, 0xa862bd5b)

#define PBLOCK_REF	0


class cube : public SimpleObject2
{
public:
  // ������ �� ���������
  static IObjParam *ip;

  // ���� ������� (�� ������ Create) ����� ���������
  static BOOL kbrdCreate;

  //Constructor/Destructor
  cube();
  virtual ~cube() {}

  // From BaseObject
  virtual CreateMouseCallBack* GetCreateMouseCallBack();

  // From Object
  BOOL HasUVW();
  void SetGenUVW(BOOL sw);
  int CanConvertToType(Class_ID obtype);
  Object* ConvertToType(TimeValue t, Class_ID obtype);
  void GetCollapseTypes(Tab<Class_ID>& clist, Tab<TSTR*>& nlist);

  // �� ������ GeomObject
  int IntersectRay(TimeValue t, Ray& ray, float& at, Point3& norm);

  // ���������� ��������� ObjectState
  ObjectState Eval(TimeValue t) { return ObjectState(this); };

  // From Animatable
  void BeginEditParams(IObjParam  *ip, ULONG flags, Animatable *prev);
  void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next);

  Class_ID ClassID() { return cube_CLASS_ID; }
  SClass_ID SuperClassID() { return GEOMOBJECT_CLASS_ID; }
  void GetClassName(TSTR& s) { s = GetString(IDS_CLASS_NAME); }
  void DeleteThis() { delete this; }

  RefTargetHandle Clone(RemapDir& remap);

  // From SimpleObject
  // ������ ���
  void BuildMesh(TimeValue t);
  // ��������� ������������ ������� ���������� �������
  BOOL OKtoDisplay(TimeValue t);
  // ��������� ���������������� ���������
  void InvalidateUI();

  // �������� � ���������� ������ �������
  IOResult Load(ILoad *iload);
  IOResult Save(ISave *isave);

  // �������� �� ������� �������� ��� ������
  const TCHAR *GetObjectName() { return GetString(IDS_CLASS_NAME); }

  // ����� ������ ����������
  int NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
  // ���������� ���� ���������� �� ��� ������
  IParamBlock2* GetParamBlock(int i) { return pblock2; } // return i'th ParamBlock
  // ���������� ���� ���������� �� ��� ��������������
  IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock2->ID() == id) ? pblock2 : NULL; } // return id'd ParamBlock
};



class cubeClassDesc : public ClassDesc2
{
public:
  int IsPublic() { return TRUE; }
  void* Create(BOOL /*loading = FALSE*/) { return new cube(); }
  const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
  SClass_ID SuperClassID() { return GEOMOBJECT_CLASS_ID; }
  Class_ID ClassID() { return cube_CLASS_ID; }
  const TCHAR* Category() { return GetString(IDS_CATEGORY); }

  const TCHAR* InternalName() { return _T("cube"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE HInstance() { return hInstance; }					// returns owning module handle
};



class cubeKBDlgProc : public ParamMap2UserDlgProc
{
public:
  cube *ob;
  cubeKBDlgProc(cube *cb) { ob = cb; }
  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  void DeleteThis() { delete this; }
};



// ����� ��� �������������� �������� ������� � ������� ����.
class cubeCreateCallBack : public CreateMouseCallBack
{
  cube* ob;       //Pointer to the object 
  Point3 p0;      //First point in world coordinates
  Point3 p1;      // ������ ����� � ������� ������� ���������
  IPoint2 sp0;    //First point in screen coordinates
public:
  int proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3& mat);
  void SetObj(cube *obj) { ob = obj; }
};



static cubeClassDesc cubeDesc;


// ����� �������� IDD_KBRD � IDD_PARAMS
enum { cube_kbrd, cube_params };

// ����� ������������ � ����������� ���������� ��������
// �������� IDC_KBSZ, IDC_KBSZSPIN
enum { cube_kb_size };

// �������� IDC_SZ, IDC_SZSPIN
enum { cube_size };
