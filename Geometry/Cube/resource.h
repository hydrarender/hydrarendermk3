//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Cube.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_KBRD                        5
#define IDS_CB_SIZE                     6
#define IDD_KBRD                        101
#define IDD_PARAMS                      102
#define IDC_KBSTATIC                    1001
#define IDC_CREATE                      1002
#define IDC_SZSTATIC                    1003
#define IDC_SZ                          1004
#define IDC_SZSPIN                      1005
#define IDC_KBSZ                        1490
#define IDC_KBSZSPIN                    1496

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
