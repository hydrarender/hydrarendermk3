#include "hydraRender mk3.h"
#include "3dsmaxport.h"
#include "gamma.h"


HydraRenderParams::HydraRenderParams()
{
  SetDefaults();

  envMap = NULL;
  atmos = NULL;
  rendType = RENDTYPE_NORMAL;
  nMinx = 0;
  nMiny = 0;
  nMaxx = 0;
  nMaxy = 0;
  nNumDefLights = 0;
  scrDUV = Point2(0.0f, 0.0f);
  pDefaultLights = NULL;
  pFrp = NULL;
  bVideoColorCheck = 0;
  bForce2Sided = FALSE;
  bRenderHidden = FALSE;
  bSuperBlack = FALSE;
  bRenderFields = FALSE;
  bNetRender = FALSE;

  renderer = NULL;
  projType = PROJ_PERSPECTIVE;
  devWidth = 0;
  devHeight = 0;
  xscale = 0;
  yscale = 0;
  xc = 0;
  yc = 0;
  antialias = FALSE;
  nearRange = 0;
  farRange = 0;
  devAspect = 0;
  frameDur = 0;
  time = 0;
  wireMode = FALSE;
  inMtlEdit = FALSE;
  fieldRender = FALSE;
  first_field = FALSE;
  field_order = FALSE;
  objMotBlur = FALSE;
  nBlurFrames = 0;


  rendTimeType = REND_TIMESINGLE;
  rendStart = GetCOREInterface()->GetAnimRange().End() / GetTicksPerFrame();
  rendEnd = GetCOREInterface()->GetAnimRange().End() / GetTicksPerFrame();
  nthFrame = 1;
  rendSaveFile = FALSE;

}

void HydraRenderParams::SetDefaults()
{
  // Main
  renderQuality = 50.0f;
  primary = 0;
  secondary = 0;
  tertiary = 0;
  enableCaustics = false;
  enableTextureResize = false;
  productionMode = false;
  timeLimitOn = false;
  timeLimit = 0;
  useHydraGUI = false;
  enableDOF = false;
  noRandomLightSel = false;
  enableLog = false;
  focalplane = 8;
  lensradius = 1.0f;
  debugOn = false;
  allSecondary = false;
  allTertiary = false;
  primaryMLFilter = false;
  secondaryMLFilter = false;
  writeToDisk = false;
  engine_type = 0;
  device_id.clear();
  liteMode = false;
  useSeparateSwap   = false;
  specNoiseFilterOn = false;
  specNoiseFilter   = 2.0f;
	preset = 4; //*******
	rememberDevice = false;

  // Path tracing
  seed        = 1;
  causticRays = 1;
  minrays     = 256;
  maxrays     = 4156; // 50% quality slider
  raybounce   = 8;
  diffbounce  = 4;
  relative_error = 2.5f;
  guided         = false;
  bsdf_clamp_on  = false;
  bsdf_clamp     = 100.0f;
  env_clamp      = 5.0f;
  estimateLDR    = false;

  // MLT
  mlt_plarge = 0.5f;
  mlt_iters_mult = 2;
  mlt_burn_iters = 256;
  mlt_enable_median_filter = false;
  mlt_median_filter_threshold = 0.4;


  // SPPM Caustics
  maxphotons_c = 1000;
  caustic_power = 1;
  retrace_c = 4;
  initial_radius_c = 4;
  visibility_c = false;
  alpha_c = 0.67;

  // SPPM Diffuse
  maxphotons_d = 1000;
  retrace_d = 10;
  initial_radius_d = 10;
  visibility_d = false;
  alpha_d = 0.67;
  irr_map = false;

  // Irradiance cache
  ic_eval = 0;
  maxpass = 16;
  fixed_rays = 1024;
  ws_err = 40;
  ss_err4 = 16;
  ss_err2 = 4;
  ss_err1 = 1;
  ic_relative_error = 10;

  // Multi-Layer
  filterPrimary = false;
  r_sigma = 2.5;
  s_sigma = 10.0f;
  layerNum = 1;
	spp = 256;
  //saveComposeLayers = false;

  // Override
  env_mult = 1.0f;
  overrideGrayMatOn = false;

  // Post process
  postProcOn      = false;
  numThreads      = 1;
  exposure        = 1.0f;
  compress        = 0.0f;
  contrast        = 1.0f;
  saturation      = 1.0f;
  whiteBalance    = 0.0f;
  whitePointColor = Color(0.0, 0.0, 0.0);
  uniformContrast = 0.0f;
  normalize       = 0.0f;
  chromAberr      = 0.0f;
  vignette        = 0.0f;
  sharpness       = 0.0f;
  sizeStar        = 0.0f;
  numRay          = 8;
  rotateRay       = 10;
  randomAngle     = 0.1f;
  sprayRay        = 0.0f;

  bloom           = false;
	bloom_radius    = 7.0f;
	bloom_str       = 0.5f;
  icBounce        = 0;
  //hydraFrameBuf   = false;
  filter_id       = 0;
	mlaa            = true;

  // Denoise
  use_denoiser = false;
  noiseLvl = 0.5f;

	// Debug
  debugCheckBoxes[L"DEBUG"]                         = false;
  debugCheckBoxes[L"LITE_CORE"]                     = false;
	debugCheckBoxes[L"PURE_CL_FOR_CPU_TRACE"]         = false;
	debugCheckBoxes[L"NORMAL_PRIORITY_FOR_CPU_TRACE"] = false;
  debugCheckBoxes[L"FORCE_GPU_FRAMEBUFFER"]         = false;
  debugCheckBoxes[L"BLACK_IMAGE"] = false;

  // Render elements
  renElemAlphaOn        = true;
  renElemColorOn        = false;
  renElemCoordOn        = false;
  renElemCoverOn        = false;
  renElemDepthOn        = false;
  renElemInstIdOn       = false;
  renElemMatIdOn        = false;
  renElemNormalsOn      = false;
  renElemObjIdOn        = false;
  renElemShadowOn       = false;


  rendTimeType = REND_TIMESINGLE;
  rendStart = GetCOREInterface()->GetAnimRange().End() / GetTicksPerFrame();
  rendEnd = GetCOREInterface()->GetAnimRange().End() / GetTicksPerFrame();
  nthFrame = 1;
  rendSaveFile = FALSE;


}

#define VIEW_DEFAULT_WIDTH ((float)400.0)

void HydraRenderParams::ComputeViewParams(const ViewParams&vp)
{
	worldToCam = vp.affineTM;
	camToWorld = Inverse(worldToCam);

	xc = devWidth / 2.0f;
	yc = devHeight / 2.0f;

	scrDUV.x = 1.0f / (float)devWidth;
	scrDUV.y = 1.0f / (float)devHeight;

	projType = vp.projType;

	if (projType == PROJ_PERSPECTIVE) {
		float fac = -(float)(1.0 / tan(0.5*(double)vp.fov));
		xscale = fac*xc;
		yscale = -devAspect*xscale;
	}
	else {
		xscale = (float)devWidth / (VIEW_DEFAULT_WIDTH*vp.zoom);
		yscale = -devAspect*xscale;
	}
}

Point3 HydraRenderParams::RayDirection(float sx, float sy)
{
  Point3 p;
  p.x = -(sx - xc) / xscale;
  p.y = -(sy - yc) / yscale;
  p.z = -1.0f;
  return Normalize(p);
}

int HydraRenderParams::NumRenderInstances()
{

  return 0;
}

RenderInstance* HydraRenderParams::GetRenderInstance(int i)
{

  return NULL;
}
