﻿#include "PostProcessEngine.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <tuple>

#include <ctime>
#include <functional>

#include <gutil.h>
#include <omp.h>


HMODULE PostProcessPipeline::mlaa_dll_handle = NULL;
PCREATEFUN_T PostProcessPipeline::mlaa_create_fun_ptr = nullptr;

HMODULE PostProcessPipeline::free_img_dll_handle = NULL;

P_FreeImage_AllocateT PostProcessPipeline::freeImage_AllocateT = nullptr;
P_FreeImage_GetBits   PostProcessPipeline::freeImage_GetBits   = nullptr;
P_FreeImage_SaveU     PostProcessPipeline::freeImage_SaveU     = nullptr;
P_FreeImage_Unload    PostProcessPipeline::freeImage_Unload    = nullptr;

std::wstring HydraInstallPathW();

PostProcessPipeline::PostProcessPipeline(FilterChainPresets a_presets, ImageZ* a_pInput, Bitmap* a_bmap_out, std::ostream* pLog) : m_ldrPassNumber(0)
{
  pErrLog = pLog;
  // resize(a_pInput->Width(), a_pInput->Height());

  if (a_presets.enableMLT)
    m_pCopyFromZFilter = std::shared_ptr<IFilter2D>(new CopyFromZFilterMLT(a_pInput, pErrLog), std::mem_fn(&IFilter2D::Release));
  else 
    m_pCopyFromZFilter = std::shared_ptr<IFilter2D>(new CopyFromZFilter(a_pInput), std::mem_fn(&IFilter2D::Release)); 
  
  m_pAlphaFromGBuff  = std::shared_ptr<IFilter2D>(new PutAlphaChannelFilter(&m_alphaImage), std::mem_fn(&IFilter2D::Release)); 

  if(a_presets.enableMedian)
    m_pMedianFilter = std::shared_ptr<IFilter2D>(new MedianFilter, std::mem_fn(&IFilter2D::Release));

  //m_pToneMapFilter = std::shared_ptr<IFilter2D>(new ToneMappingSSE(calcPresets()), std::mem_fn(&IFilter2D::Release));
  
  // if (a_presets.toneMapFilterId == 0)
  //   m_pToneMapFilter = std::shared_ptr<IFilter2D>(new NaiveToneMappingFilter, std::mem_fn(&IFilter2D::Release));
  // else if(a_presets.toneMapFilterId == 1)
  //   m_pToneMapFilter = std::shared_ptr<IFilter2D>(new ToneMappingSSE(calcPresets()), std::mem_fn(&IFilter2D::Release));
  // else if (a_presets.toneMapFilterId == 2)
  //   m_pToneMapFilter = std::shared_ptr<IFilter2D>(new AndrewToneMappingFilter(), std::mem_fn(&IFilter2D::Release));
  // else
  //   m_pToneMapFilter = std::shared_ptr<IFilter2D>(new NaiveToneMappingFilter, std::mem_fn(&IFilter2D::Release));

  if (a_presets.hdrFrameBuff)
    m_pToneMapFilter = std::shared_ptr<IFilter2D>(new NaiveToneMappingFilter, std::mem_fn(&IFilter2D::Release));
  else
    m_pToneMapFilter = std::shared_ptr<IFilter2D>(new TestToneMapping, std::mem_fn(&IFilter2D::Release));

  if (mlaa_dll_handle == NULL)
  {
    std::wstring mlaaPath = HydraInstallPathW() + L"\\bin\\mlaa_cs_11.dll";
    mlaa_dll_handle = LoadLibraryW(mlaaPath.c_str());
  }
    //mlaa_dll_handle = LoadLibraryW(L"D:\\PROG\\[Graphics]\\gpu_rt4_git\\pp_mlaa_dx11\\x64\\DebugDLL\\mlaa_cs_11.dll");

  if (mlaa_create_fun_ptr == nullptr && mlaa_dll_handle != NULL)
    mlaa_create_fun_ptr = (PCREATEFUN_T)GetProcAddress(mlaa_dll_handle, "CreateFilter2D");

  if (mlaa_create_fun_ptr != nullptr)
    m_pAAFilter = std::shared_ptr<IFilter2D>(mlaa_create_fun_ptr("default"), std::mem_fn(&IFilter2D::Release));
  else
    m_pAAFilter = nullptr;

  if (a_presets.enableNLM)
    m_pNLMFilterLDR = std::shared_ptr<IFilter2D>(new NonLocalMeansLDR(a_bmap_out), std::mem_fn(&IFilter2D::Release));
  else
    m_pNLMFilterLDR = nullptr;

  if (a_presets.enableBloom)
  {
    m_pBloomPrePass = std::shared_ptr<IFilter2D>(new BloomGetBrightPixels(), std::mem_fn(&IFilter2D::Release));
    m_pBloomFinPass = std::shared_ptr<IFilter2D>(new BloomBlurAndBlend2(), std::mem_fn(&IFilter2D::Release));
  }
  else
  {
    m_pBloomPrePass = nullptr;
    m_pBloomFinPass = nullptr;
  }

  m_pShowError = std::shared_ptr<IFilter2D>(new ShowErrorFromZFilter(a_pInput, a_bmap_out), std::mem_fn(&IFilter2D::Release));
  m_pDrawTiles = std::shared_ptr<IFilter2D>(new ShowTiles(a_pInput, a_bmap_out), std::mem_fn(&IFilter2D::Release));

  if (free_img_dll_handle == NULL)
  {
    std::wstring freeImagePath = HydraInstallPathW() + L"\\bin\\FreeImage.dll";
    free_img_dll_handle = LoadLibraryW(freeImagePath.c_str());
  }

  if (free_img_dll_handle != NULL && freeImage_AllocateT == nullptr)
  {
    freeImage_AllocateT = (P_FreeImage_AllocateT)GetProcAddress(free_img_dll_handle, "FreeImage_AllocateT");
    freeImage_GetBits   = (P_FreeImage_GetBits  )GetProcAddress(free_img_dll_handle, "FreeImage_GetBits");
    freeImage_SaveU     = (P_FreeImage_SaveU    )GetProcAddress(free_img_dll_handle, "FreeImage_SaveU");
    freeImage_Unload    = (P_FreeImage_Unload   )GetProcAddress(free_img_dll_handle, "FreeImage_Unload");
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // add all succesfully created filters to list
  //
  if (m_pShowError != nullptr && a_presets.enableTiles)
    m_filtersList.push_back(m_pShowError);

    
  if (m_pCopyFromZFilter != nullptr)          //
    m_filtersList.push_back(m_pCopyFromZFilter);   //

  MakeCommonChain(a_presets, a_bmap_out);

  m_layersWasLoaded = false;
  m_avgB = 0.0f;
  m_avgBCounter = 0;

  m_lastPresets = a_presets;
}

void PostProcessPipeline::MakeCommonChain(FilterChainPresets a_presets, Bitmap* a_bmap_out)
{
  if (m_pMedianFilter != nullptr)
    m_filtersList.push_back(m_pMedianFilter);

  if (a_presets.hdrFrameBuff && m_pAlphaFromGBuff != nullptr)
    m_filtersList.push_back(m_pAlphaFromGBuff);

  // (1 add aux copy filter, after got unprocessed image)
  m_filtersList.push_back(std::shared_ptr<IFilter2D>(new CopyHDRToMaxBitMapFilter(a_bmap_out), std::mem_fn(&IFilter2D::Release))); // copy to max hdr buffer to implement hdr image save

  if (m_pBloomPrePass != nullptr)
    m_filtersList.push_back(m_pBloomPrePass);

  if (m_pToneMapFilter != nullptr)
    m_filtersList.push_back(m_pToneMapFilter);

  if (m_pNLMFilterLDR != nullptr)
    m_filtersList.push_back(m_pNLMFilterLDR);

  if (m_pAAFilter != nullptr && a_presets.enableMLAA)
    m_filtersList.push_back(m_pAAFilter);

  if (m_pBloomFinPass != nullptr)
    m_filtersList.push_back(m_pBloomFinPass);

  if (m_pDrawTiles != nullptr && a_presets.enableTiles)
    m_filtersList.push_back(m_pDrawTiles);

  if (!a_presets.hdrFrameBuff && m_pAlphaFromGBuff != nullptr)
    m_filtersList.push_back(m_pAlphaFromGBuff);
}

void PostProcessPipeline::InitInteractivePPFilterChain(Bitmap* a_bmap_out, FilterChainPresets a_presets)
{
  if (a_presets.enableBloom)
  {
    m_pBloomPrePass = std::shared_ptr<IFilter2D>(new BloomGetBrightPixels(), std::mem_fn(&IFilter2D::Release));
    m_pBloomFinPass = std::shared_ptr<IFilter2D>(new BloomBlurAndBlend2(),   std::mem_fn(&IFilter2D::Release));
  }
  else
  {
    m_pBloomPrePass = nullptr;
    m_pBloomFinPass = nullptr;
  }

  m_pNLMFilterLDR = std::shared_ptr<IFilter2D>(new NonLocalMeansLDR(a_bmap_out), std::mem_fn(&IFilter2D::Release));

  if (a_presets.hdrFrameBuff)
    m_pToneMapFilter = std::shared_ptr<IFilter2D>(new NaiveToneMappingFilter, std::mem_fn(&IFilter2D::Release));
  else
    m_pToneMapFilter = std::shared_ptr<IFilter2D>(new TestToneMapping, std::mem_fn(&IFilter2D::Release));

  m_image = m_imageHDRCopy;
  m_filtersList.clear();
	
  MakeCommonChain(a_presets, a_bmap_out);
}

void PostProcessPipeline::AccumMLTAvgBrightness(float a_brightness)
{
  float alpha = 1.0f / float(m_avgBCounter + 1);
  m_avgB = a_brightness*alpha + m_avgB*(1.0f - alpha);
}

void PostProcessPipeline::freeAuxMemory()
{
  //ml_data.clear();
}


PostProcessPipeline::~PostProcessPipeline()
{

}

void PostProcessPipeline::resize(int a_width, int a_height)
{
  if (m_image.width() == a_width && m_image.height() == a_height)
    return;

  m_image.resize(a_width, a_height);

}


void PostProcessPipeline::runFilterChain(FilterChainCustom a_presets) // const HydraRenderParams* a_pRenderParams
{
  float* pHDRData = m_image.data();
  if (pHDRData == nullptr)
    return;

  int m_width  = m_image.width();
  int m_height = m_image.height();

  char errMsg[1024];
  memset(errMsg, 0, 1024);

  int id = 0;
  int totalMs = 0;

  if (m_pCopyFromZFilter != nullptr)
  {
    std::stringstream presets;
    presets << "avgBrightness " << m_avgB << " mlt_enable_median_filter " << a_presets.mlt_enableMedianFilter << " mlt_median_threshold " << a_presets.mlt_medianFilterThreshold;
    m_pCopyFromZFilter->SetPresets(presets.str().c_str());
  }

  if (m_pToneMapFilter != nullptr)
  {
    std::stringstream presets;
   // presets << "intelKLow " << a_presets.tmLow << " intelKHigh " << a_presets.tmHigh << " intelExp " << a_presets.tmExp << " intelDefog " << a_presets.tmDefog;
    presets << "exposure " << a_presets.exposure << " compress " << a_presets.compress 
            << " contrast " << a_presets.contrast << " saturation " << a_presets.saturation
            << " whiteBalance " << a_presets.whiteBalance << " uniformContrast " << a_presets.uniformContrast 
            << " normalize " << a_presets.normalize << " chromAberr " << a_presets.chromAberr 
            << " vignette " << a_presets.vignette;

    m_pToneMapFilter->SetPresets(presets.str().c_str());
  }

  HDRImage4f bloomBrightPixels;

  if (m_pBloomFinPass != nullptr)
  {
    std::stringstream presets;
    presets << "bloomRadius " << a_presets.bloomRadius << " bloomStrength " << a_presets.bloomStrength;
    m_pBloomFinPass->SetPresets(presets.str().c_str());
    bloomBrightPixels.resize(m_width, m_height);
  }

  if (m_pMedianFilter != nullptr)
  {
    std::stringstream presets;
    presets << "medianThreshold " << a_presets.medianThreshold;
    m_pMedianFilter->SetPresets(presets.str().c_str());
  }

  if (m_pNLMFilterLDR != nullptr)
  {
    std::stringstream presets;
    presets << "noiseLvl " << a_presets.noiseLevel << " finalRun " << int(a_presets.finalPass);
    m_pNLMFilterLDR->SetPresets(presets.str().c_str());
  }

  for (int id = 0; id < m_filtersList.size();id++)
  {
    auto& filter = m_filtersList[id];

    std::clock_t c_start = std::clock();  // timer start

    BloomGetBrightPixels* pBloom1 = dynamic_cast<BloomGetBrightPixels*>(filter.get());
    BloomBlurAndBlend*    pBloom2 = dynamic_cast<BloomBlurAndBlend*>(filter.get());
    ShowTiles*            pDrawT  = dynamic_cast<ShowTiles*>(filter.get());

    if (pBloom1 != nullptr)
    {
      filter->SetInput_f4(pHDRData, m_width, m_height, "");
      filter->SetOutput_f4(bloomBrightPixels.data(), m_width, m_height);
    }
    else if (pBloom2 != nullptr)
    {
      filter->SetInput_f4(bloomBrightPixels.data(), m_width, m_height, "bright_pixels");
      filter->SetInput_f4(pHDRData, m_width, m_height, "");
      filter->SetOutput_f4(pHDRData, m_width, m_height);
    }
    else
    {
      filter->SetInput_f4(pHDRData, m_width, m_height, "");
      filter->SetOutput_f4(pHDRData, m_width, m_height);
    }

    FilterCommon* pFilter2 = dynamic_cast<FilterCommon*>(filter.get());
    if (pFilter2 != nullptr)
    {
      pFilter2->SetInput(&m_image);
      pFilter2->SetOutput(&m_image);
    };

    filter->SetProgressBar(a_presets.pProgressBar);

    if (pDrawT != nullptr && a_presets.finalPass) // don't draw tiles in final pass
      break;

    if (!filter->Eval() && pErrLog != nullptr)
    {
      filter->GetLastError(errMsg);
      (*pErrLog) << "image filter error: " << errMsg << std::endl;
      memset(errMsg, 0, 1024);
    }

    if ((void*)(filter.get()) == (void*)(m_pCopyFromZFilter.get())) // save HDR image for further usage
      m_imageHDRCopy = m_image;

    std::clock_t c_end = std::clock(); // timer end

    int msCurr = int((1000.0 * double(c_end - c_start)) / double(CLOCKS_PER_SEC));

    //if(pErrLog != nullptr)
      //(*pErrLog) << "im_filter[" << id << "] time = " << msCurr << " ms" << std::endl;
    totalMs += msCurr;
  }

  //(*pErrLog) << "total filter time = " << totalMs << " ms" << std::endl;

  if (pErrLog != nullptr)
    (*pErrLog) << std::endl;

}


void PostProcessPipeline::SaveHDR(const std::wstring& folderSceneName, Bitmap* from_bm)
{
  std::wstring outPath = folderSceneName; // + L".hdr";

  float* pHDRData = m_image.data();
  if (pHDRData == nullptr)
    return;

  int m_width = m_image.width();
  int m_height = m_image.height();

  if (m_pCopyFromZFilter != nullptr)
  {
    m_pCopyFromZFilter->SetInput_f4(pHDRData, m_width, m_height, "");
    m_pCopyFromZFilter->SetOutput_f4(pHDRData, m_width, m_height);
    m_pCopyFromZFilter->Eval();
  }
  else
    return;

 
  // save real hdr data with freeImage
  //
  if (freeImage_AllocateT != nullptr)
  {
    FIBITMAP* dib = freeImage_AllocateT(FIT_RGBF, m_width, m_height,8,0,0,0);

    float* bits = (float*)freeImage_GetBits(dib);
    
    for (int i = 0; i < m_width*m_height; i++)
    {
      bits[i * 3 + 0] = pHDRData[i * 4 + 0];
      bits[i * 3 + 1] = pHDRData[i * 4 + 1];
      bits[i * 3 + 2] = pHDRData[i * 4 + 2];
    }

    //FreeImage_SetOutputMessage(FreeImageErrorHandler);

    if (!freeImage_SaveU(FIF_HDR, dib, outPath.c_str(), 0))
      MessageBoxW(NULL, outPath.c_str(), L"FreeImage save error", NULL);

    freeImage_Unload(dib);
  }

  std::wstring outPathDepth = folderSceneName + L"_depth.hdr";

  // save depth image, save other images if needed, please use 1 channel exr or something like that
  //

  /*
  if (m_depthBuffer.size() != 0)
  {
    FIBITMAP* dib = freeImage_AllocateT(FIT_RGBF, m_width, m_height, 8, 0, 0, 0);

    float* bits = (float*)freeImage_GetBits(dib);

    for (int i = 0; i < m_width*m_height; i++)
    {
      bits[i * 3 + 0] = m_depthBuffer[i];
      bits[i * 3 + 1] = m_depthBuffer[i];
      bits[i * 3 + 2] = m_depthBuffer[i];
    }
  
    if (!freeImage_SaveU(FIF_HDR, dib, outPathDepth.c_str(), 0))
      MessageBoxW(NULL, outPathDepth.c_str(), L"FreeImage save error", NULL);

    freeImage_Unload(dib);
  }
  */
}


namespace HydraGBuffer
{
  struct float3 { float x, y, z; };
  struct float4 { float x, y, z, w; };

  typedef unsigned int uint;
  typedef unsigned short ushort;

  inline int   __float_as_int(float x) { return *((int*)&x); }
  inline float __int_as_float(int x) { return *((float*)&x); }

  inline int   as_int(float x) { return __float_as_int(x); }
  inline float as_float(int x) { return __int_as_float(x); }

  float3 make_float3(float x, float y, float z)
  {
    float3 res;
    res.x = x;
    res.y = y;
    res.z = z;
    return res;
  }

  inline uint RealColorToUint32(float4 real_color)
  {
    float  r = real_color.x*255.0f;
    float  g = real_color.y*255.0f;
    float  b = real_color.z*255.0f;
    float  a = real_color.w*255.0f;

    unsigned char red = (unsigned char)r;
    unsigned char green = (unsigned char)g;
    unsigned char blue = (unsigned char)b;
    unsigned char alpha = (unsigned char)a;

    return red | (green << 8) | (blue << 16) | (alpha << 24);
  }

  inline uint encodeNormal(float3 n)
  {
    short x = (short)(n.x*32767.0f);
    short y = (short)(n.y*32767.0f);

    ushort sign = (n.z >= 0) ? 0 : 1;

    int sx = ((int)(x & 0xfffe) | sign);
    int sy = ((int)(y & 0xfffe) << 16);

    return (sx | sy);
  }

  inline float3 decodeNormal(uint a_data)
  {
    const float divInv = 1.0f / 32767.0f;

    short a_enc_x, a_enc_y;

    a_enc_x = (short)(a_data & 0x0000FFFF);
    a_enc_y = (short)((int)(a_data & 0xFFFF0000) >> 16);

    float sign = (a_enc_x & 0x0001) ? -1.0f : 1.0f;

    float x = (short)(a_enc_x & 0xfffe)*divInv;
    float y = (short)(a_enc_y & 0xfffe)*divInv;
    float z = sign*sqrt(fmax(1.0f - x*x - y*y, 0.0f));

    return make_float3(x, y, z);
  }


  typedef struct GBuffer1T
  {
    float  depth;
    float3 norm;
    float4 rgba;
    unsigned int matId;

  } GBuffer1;


  inline float4 packGBuffer1(GBuffer1 a_input)
  {
    float4 resColor;

    unsigned int packedRGBX = RealColorToUint32(a_input.rgba);

    resColor.x = a_input.depth;
    resColor.y = as_float(encodeNormal(a_input.norm));
    resColor.z = as_float(a_input.matId);
    resColor.w = as_float(packedRGBX);

    return resColor;
  }


  inline GBuffer1 unpackGBuffer1(float4 a_input)
  {
    GBuffer1 res;

    res.depth = a_input.x;
    res.norm  = decodeNormal(as_int(a_input.y));
    res.matId = as_int(a_input.z);

    unsigned int rgba = as_int(a_input.w);
    res.rgba.x = (rgba & 0x000000FF)*(1.0f / 255.0f);
    res.rgba.y = ((rgba & 0x0000FF00) >> 8)*(1.0f / 255.0f);
    res.rgba.z = ((rgba & 0x00FF0000) >> 16)*(1.0f / 255.0f);
    res.rgba.w = ((rgba & 0xFF000000) >> 24)*(1.0f / 255.0f);

    return res;
  }

};

using HydraGBuffer::float4;
using HydraGBuffer::float3;
using HydraGBuffer::GBuffer1;

void PostProcessPipeline::LoadGBuffer(const std::string& a_folderPath, Bitmap* tobm)
{
  m_layersWasLoaded = true;

  if (tobm == nullptr)
    return;

  int m_width  = m_image.width();
  int m_height = m_image.height();

  HDRImage4f gbuffImg;
  gbuffImg.loadFromImage4f(a_folderPath + "gbuffer1.image4f");

  if (m_width != gbuffImg.width() && m_height != gbuffImg.height())
    return;

  m_alphaImage.resize(m_width*m_height);

  int wh[2] = { gbuffImg.width(), gbuffImg.height() };

  // zbuff
  //
  ULONG ctype;
  float* zbuffer = (float *)tobm->GetChannel(BMM_CHAN_Z, ctype);

  if (zbuffer == nullptr)
  {
    tobm->CreateChannels(BMM_CHAN_Z);
    zbuffer = (float *)tobm->GetChannel(BMM_CHAN_Z, ctype);
  }

  // normals
  //
  DWORD* nbuffer = (DWORD*)tobm->GetChannel(BMM_CHAN_NORMAL, ctype);
  if (nbuffer == nullptr)
  {
    tobm->CreateChannels(BMM_CHAN_NORMAL);
    nbuffer = (DWORD*)tobm->GetChannel(BMM_CHAN_NORMAL, ctype);
  }

  // tex color
  //
  Color24* texcolors = (Color24 *)tobm->GetChannel(BMM_CHAN_COLOR, ctype);

  if (texcolors == nullptr)
  {
    tobm->CreateChannels(BMM_CHAN_COLOR);
    texcolors = (Color24 *)tobm->GetChannel(BMM_CHAN_COLOR, ctype);
  }

  // alpha
  //
  const float4* data = (const float4*)gbuffImg.data();
  
  for (int y = 0; y < wh[1]; y++)
  {
    int offset1 = y*m_width;
    int offset2 = (m_height - y - 1)*m_width;
    
    for (int x = 0; x < wh[0]; x++)
    {
      GBuffer1 buffData = unpackGBuffer1(data[offset1 + x]);

      uchar r = (uchar)(255.0f*buffData.rgba.x);
      uchar g = (uchar)(255.0f*buffData.rgba.y);
      uchar b = (uchar)(255.0f*buffData.rgba.z);
      uchar a = (uchar)(255.0f*buffData.rgba.w);

      zbuffer  [offset2 + x] = buffData.depth;
      nbuffer  [offset2 + x] = CompressNormal(Point3(buffData.norm.x, buffData.norm.y, buffData.norm.z));
      texcolors[offset2 + x] = Color24(r, g, b);

      m_alphaImage[offset1 + x] = (uint8_t)(a);
    }

  }
 

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MedianFilter::MedianFilter() : m_threshold (0.4f) {}
MedianFilter::~MedianFilter() {}

void MedianFilter::Release(){}

void MedianFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) {}

void MedianFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data = a_output;
  m_width = w;
  m_height = h;
}

bool MedianFilter::Eval()
{
  if (m_pImageIn != nullptr)
  {
    m_pImageIn->medianFilterInPlace(m_threshold, 0.0f);
    return true;
  }
  else
    return false;
}

void MedianFilter::SetPresets(const char* a_presetsStr)
{
  std::istringstream iss(a_presetsStr);

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == "medianThreshold")
      m_threshold = atof(val.c_str());
  
  } while (iss);

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace ZImageUtils
{
  unsigned int IndexZB(int x, int y, int pitch);
};


CopyFromZFilter::CopyFromZFilter(ImageZ* a_pInput) { m_pInput = a_pInput; }
CopyFromZFilter::~CopyFromZFilter() {}

void CopyFromZFilter::Release(){}

void CopyFromZFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) {}

void CopyFromZFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data   = a_output;
  m_width  = w;
  m_height = h;
}


bool CopyFromZFilter::Eval()
{
  if (m_pInput == nullptr)
    return false;

  int zimageWidth  = m_pInput->Width();
  int zimageHeight = m_pInput->Height();

  if (zimageWidth != m_width || zimageHeight != m_height)
  {
    strncpy(m_err, "CopyFromZFilter: zimage and pp image resolusion are not match", 1024);
    return false;
  }

  float* color = m_pInput->ColorPtr();

  if (color == nullptr || m_data == nullptr)
  {
    strncpy(m_err, "CopyFromZFilter: null zimage or m_data", 1024);
    return false;
  }

  // go
  //
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth); // zimageHeight - y - 1

      float r = color[indexSrc * 4 + 0];
      float g = color[indexSrc * 4 + 1];
      float b = color[indexSrc * 4 + 2];
      float a = color[indexSrc * 4 + 3];

      int index1 = y*m_width + x;

      m_data[index1 * 4 + 0] = r;
      m_data[index1 * 4 + 1] = g;
      m_data[index1 * 4 + 2] = b;
      m_data[index1 * 4 + 3] = a;
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CopyFromZFilterMLT::CopyFromZFilterMLT(ImageZ* a_pInput, std::ostream* pLog) : m_avgB(1.0f), m_enableMedianFilter(false), m_medianthreshold(0.4f), m_pLog(pLog) { m_pInput = a_pInput; }
CopyFromZFilterMLT::~CopyFromZFilterMLT() {}                             

void CopyFromZFilterMLT::Release(){}

void CopyFromZFilterMLT::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) {}

void CopyFromZFilterMLT::SetOutput_f4(float* a_output, int w, int h)
{
  m_data = a_output;
  m_width = w;
  m_height = h;
}

void CopyFromZFilterMLT::SetPresets(const char* a_presetsStr)
{
  std::istringstream iss(a_presetsStr);  

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == "avgBrightness")
      m_avgB = atof(val.c_str());

    if (name == "mlt_enable_median_filter")
      m_enableMedianFilter = (bool)atoi(val.c_str());

    if (name == "mlt_median_threshold")
      m_medianthreshold = atof(val.c_str());

  } while (iss);
}

inline float contribFunc(double color[4])
{
  return (float)(fmax(0.33334f*(color[0] + color[1] + color[2]), 0.0));
}


bool CopyFromZFilterMLT::Eval()
{
  int zimageWidth  = m_pInput->Width();
  int zimageHeight = m_pInput->Height();

  if (zimageWidth != m_width || zimageHeight != m_height)
  {
    strncpy(m_err, "CopyFromZFilterMLT: zimage and pp image resolusion are not match", 1024);
    return false;
  }

  float* colorP = m_pInput->ColorPtr(0);
  float* colorS = m_pInput->ColorPtr(1);

  if (colorP == nullptr || colorS == nullptr || m_data == nullptr)
  {
    strncpy(m_err, "CopyFromZFilterMLT: null zimage or m_data", 1024);
    return false;
  }

  float4* colorP2 = (float4*)colorP;
  float4* colorS2 = (float4*)colorS;

  double avgB4[4] = {0,0,0,0};

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      float4 col = colorS2[indexSrc];
      
      if (isfinite(col.x)) avgB4[0] += col.x;
      if (isfinite(col.y)) avgB4[1] += col.y;
      if (isfinite(col.z)) avgB4[2] += col.z;
      if (isfinite(col.w)) avgB4[3] += col.w;
    }
  }

  avgB4[0] *= (1.0 / double(m_width*m_height));
  avgB4[1] *= (1.0 / double(m_width*m_height));
  avgB4[2] *= (1.0 / double(m_width*m_height));
  avgB4[3] *= (1.0 / double(m_width*m_height));

  const double avgbPlugin = contribFunc(avgB4);

  // if (avgbPlugin == 0.0f)
  // {
  //   std::vector<float4> data(m_width*m_height);
  //   for (int y = 0; y < m_height; y++)
  //   {
  //     for (int x = 0; x < m_width; x++)
  //       data[y*m_width + x] = colorS2[ZImageUtils::IndexZB(x, y, zimageWidth)];
  //   }
  // 
  //   std::ofstream fout("C:\\[Hydra]\\temp\\bad_image.image4f", std::ios::binary);
  //   fout.write((char*)&m_width, sizeof(int));
  //   fout.write((char*)&m_height, sizeof(int));
  //   fout.write((char*)&data[0], sizeof(float4)*m_width*m_height);
  // }

  const float normConst = (float)(double(m_avgB) / avgbPlugin);

  (*m_pLog) << std::endl;
  (*m_pLog) << "[MLT]: m_avgB(render) = " << m_avgB << std::endl;
  (*m_pLog) << "[MLT]: m_avgB(plugin) = " << avgbPlugin << std::endl;
  (*m_pLog) << "[MLT]: m_avgB(normc)  = " << normConst << std::endl;

  // median filter secondary light
  //
  if (m_tempImage.width() != m_width || m_tempImage.height() != m_height)
    m_tempImage.resize(m_width, m_height);

  float4* pImgData = (float4*)m_tempImage.data();
  
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth);
      int index1   = y*m_width + x;
  
      float4 indirect = colorS2[indexSrc];
      indirect.x *= normConst;
      indirect.y *= normConst;
      indirect.z *= normConst;
      indirect.w *= normConst;
      pImgData[index1] = indirect;
    }
  }
  
  if (m_enableMedianFilter)
    m_tempImage.medianFilterInPlace(m_medianthreshold, m_avgB);

  // final pass, get image
  //
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth); 
  
      float4 primary   = colorP2[indexSrc];
      float4 secondary = colorS2[indexSrc];
      
      int index1 = y*m_width + x;
  
      // m_data[index1 * 4 + 0] = primary.x + normConst*secondary.x;
      // m_data[index1 * 4 + 1] = primary.y + normConst*secondary.y;
      // m_data[index1 * 4 + 2] = primary.z + normConst*secondary.z;
      // m_data[index1 * 4 + 3] = primary.w + normConst*secondary.w;

      float4 secondary2 = pImgData[index1];

      m_data[index1 * 4 + 0] = primary.x + secondary2.x;
      m_data[index1 * 4 + 1] = primary.y + secondary2.y;
      m_data[index1 * 4 + 2] = primary.z + secondary2.z;
      m_data[index1 * 4 + 3] = primary.w + secondary2.w;
    }
  }


  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void PutAlphaChannelFilter::Release(){}

void PutAlphaChannelFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) {}

void PutAlphaChannelFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data   = a_output;
  m_width  = w;
  m_height = h;
}

bool PutAlphaChannelFilter::Eval()
{
  if (m_pInputImage == nullptr || m_data == nullptr)
  {
    strcpy(m_err,"null input image");
    return false;
  }

  if (m_pInputImage->size() < m_width*m_height)
  {
    strcpy(m_err,"PutAlphaChannelFilter: bad input image size");
    return false;
  }

  for (int i = 0; i < m_width*m_height; i++)
  {
    uint8_t a = m_pInputImage->at(i);
    m_data[i * 4 + 3] = float(a)*(1.0f / 255.0f);
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NaiveToneMappingFilter::NaiveToneMappingFilter() { }
NaiveToneMappingFilter::~NaiveToneMappingFilter() {}

void NaiveToneMappingFilter::Release(){}

void NaiveToneMappingFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) 
{
  //m_data   = a_input;
  m_width  = w;
  m_height = h;
}

void NaiveToneMappingFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data   = a_output;
  m_width  = w;
  m_height = h;
}

bool NaiveToneMappingFilter::Eval()
{
  int size = m_width*m_height;

  float* data = &m_data[0];

  const register __m128 one = _mm_set_ps1(1.0f);

  if (uintptr_t(data) % 16 == 0)
  {
    for (int i = 0; i < size; i++)
    {
      float* p = data + i * 4;
      _mm_store_ps(p, _mm_min_ps(_mm_load_ps(p), one));
    }
  }
  else
  {
    for (int i = 0; i < size; i++)
    {
      float* p = data + i * 4;
      _mm_storeu_ps(p, _mm_min_ps(_mm_loadu_ps(p), one));
    }
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TestToneMapping::TestToneMapping() { }
TestToneMapping::~TestToneMapping() {}

void TestToneMapping::Release(){}

void TestToneMapping::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName)
{
  //m_data   = a_input;
  m_width = w;
  m_height = h;
}

void TestToneMapping::SetOutput_f4(float* a_output, int w, int h)
{
  m_data = a_output;
  m_width = w;
  m_height = h;
}

// ---------- Function for Post processing ----------


template <typename T>
void ConvertSrgbToXyz(T &data)
{
  const float R = data.x;
  const float G = data.y;
  const float B = data.z;

  data.x = R * 0.4124564f + G * 0.3575761f + B * 0.1804375f;
  data.y = R * 0.2126729f + G * 0.7151522f + B * 0.0721750f;
  data.z = R * 0.0193339f + G * 0.1191920f + B * 0.9503041f;
}
void ConvertXyzToSrgb(float4 &data)
{
  const float X = data.x;
  const float Y = data.y;
  const float Z = data.z;

  data.x = X *  3.2404542f + Y * -1.5371385f + Z * -0.4985314f;
  data.y = X * -0.9692660f + Y *  1.8760108f + Z *  0.0415560f;
  data.z = X *  0.0556434f + Y * -0.2040259f + Z *  1.0572252f;
}
template <typename T>
void MatrixCat02(T &data)
{
  // Преобразование в колбочковые ответы. Mcat02 in CAM02. 

  const float R = 0.7328f * data.x + 0.4296f * data.y + -0.1624f * data.z;
  const float G = -0.7036f * data.x + 1.6975f * data.y + 0.0061f * data.z;
  const float B = 0.0030f * data.x + 0.0136f * data.y + 0.9834f * data.z;

  data.x = R;
  data.y = G;
  data.z = B;
}
template <typename T>
void InverseMatrixCat02(T &data)
{
  // Inverse Mcat02 in CAM02. 

  const float X = 1.096124f * data.x + -0.278869f * data.y + 0.182745f * data.z;
  const float Y = 0.454369f * data.x + 0.473533f * data.y + 0.072098f * data.z;
  const float Z = -0.009628f * data.x + -0.005698f * data.y + 1.015326f * data.z;

  data.x = X;
  data.y = Y;
  data.z = Z;
}
template <typename T>
void MatrixHpe(T &data)
{
  // конверт в колбочки. Matrix HPE in CAM02. (Hunt-Pointer-Estevez)
  const float L = 0.38971f * data.x + 0.68898f * data.y + -0.07868f * data.z; //Kuo modify −0.07869
  const float M = -0.22981f * data.x + 1.18340f * data.y + 0.04641f * data.z; //            0.04642f ?
  const float S = data.z;

  data.x = L;
  data.y = M;
  data.z = S;
}
template <typename T>
void InverseMatrixHpe(T &data)
{
  // Inverse matrix HPE in CAM02. 
  const float R = 1.910197f * data.x + -1.112124f * data.y + 0.201908f * data.z;
  const float G = 0.370950f * data.x + 0.629054f * data.y + -0.000008f * data.z;
  const float B = data.z;

  data.x = R;
  data.y = G;
  data.z = B;
}
template <typename T>
void ConvertXyzToLms(T &data)
{
  const float L = 0.4002f * data.x + 0.7075f * data.y + -0.0807f * data.z;
  const float M = -0.2280f * data.x + 1.1500f * data.y + 0.0612f * data.z;
  const float S = 0.9184f * data.z;

  //const float a = 0.43f;
  //
  //if (L >= 0.0f) L = pow(L, a);
  //else if (L <  0.0f) L = -pow(-L, a);
  //if (M >= 0.0f) M = pow(M, a);
  //else if (M <  0.0f) M = -pow(-M, a);
  //if (S >= 0.0f) S = pow(S, a);
  //else if (S <  0.0f) S = -pow(-S, a);

  data.x = L;
  data.y = M;
  data.z = S;
}
void ConvertLmsToIpt(float4 &data)
{
  const float I = 0.4000f * data.x + 0.4000f * data.y + 0.2000f * data.z;
  const float P = 4.4550f * data.x + -4.8510f * data.y + 0.3960f * data.z;
  const float T = 0.8056f * data.x + 0.3572f * data.y + -1.1628f * data.z;

  data.x = I;
  data.y = P;
  data.z = T;
}
void ConvertLmsToXyz(float4 &data)
{
  const float L = data.x;
  const float M = data.y;
  const float S = data.z;

  //const float a = 2.3255819f; //  = 1.0f / 0.43f;
  //
  //if (L >= 0.0f) L = pow(L, a);
  //else if (L <  0.0f) L = -pow(-L, a);
  //if (M >= 0.0f) M = pow(M, a);
  //else if (M <  0.0f) M = -pow(-M, a);
  //if (S >= 0.0f) S = pow(S, a);
  //else if (S <  0.0f) S = -pow(-S, a);

  data.x = 1.8493f * L + -1.1383f * M + 0.2381f * S;
  data.y = 0.3660f * L + 0.6444f * M + -0.010f  * S;
  data.z = 1.0893f * S;
}
void ConvertIptToLms(float4 &data)
{
  const float L = 0.9999f * data.x + 0.0970f * data.y + 0.2053f * data.z;
  const float M = 0.9999f * data.x + -0.1138f * data.y + 0.1332f * data.z;
  const float S = 0.9999f * data.x + 0.0325f * data.y + -0.6768f * data.z;

  data.x = L;
  data.y = M;
  data.z = S;
}
void ChromAdaptCam02(float4 &data, const float3 &dataW, float D)
{
  MatrixCat02(data);

  D *= 0.5063f; // chrom adapt for display

  const float Rc = (D / dataW.x + (1.0f - D)) * data.x;
  const float Gc = (D / dataW.y + (1.0f - D)) * data.y;
  const float Bc = (D / dataW.z + (1.0f - D)) * data.z;

  data.x = Rc;
  data.y = Gc;
  data.z = Bc;

  InverseMatrixCat02(data);
}
void InverseChromAdaptCam02(float4 &data, const float3 &dataW, const float Fl, const float D, const float c)
{
  MatrixCat02(data);

  const float Rc = data.x;
  const float Gc = data.y;
  const float Bc = data.z;

  const float R = Rc / (100.0f * D / dataW.x + 1.0f - D);
  const float G = Gc / (100.0f * D / dataW.y + 1.0f - D);
  const float B = Bc / (100.0f * D / dataW.z + 1.0f - D);

  data.x = R;
  data.y = G;
  data.z = B;

  InverseMatrixCat02(data); // Return XYZ
}

void Blend(float &inData1, const float &inData2, const float coeff)
{
  inData1 = inData1 + (inData2 - inData1) * coeff;
}
template <typename T>
void Normalize(T &data, const float inMin, const float inMax, const float outMin, const float outMax)
{
  // Формула нормализации y = (inCurrent - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
  // inCurrent - значение, подлежащее нормализации
  // [Xmin, Xmax] - интервал значений х
  // [outMin, outMax] - Интервал, к которому будет приведено значение x
  data = (data - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}
void OffsetCenter(float &data, const float inMin, const float inMax, const float coef)
{
  // Ф-ция смещения центра диапазона на coef
  Normalize(data, inMin, inMax, 0, 1);  // Нормализуем в 0 - 1

  data = pow(data, coef);

  Normalize(data, 0, 1, inMin, inMax);  // Нормализуем обратно
}
void ContrastField(float &data)
{
  if (data > 0.0f && data < 1.0f)
  {
    const float a = data * 1.5707f;
    data = sin(a) * sin(a);
  }
}
void Compress(float &data, const float maxRgb)
{
  data = (data * (1.0f + data / (maxRgb * maxRgb))) / (1.0f + data);
}
void CompressHsv(float4 &hsvData)
{
  // Сжимаем яркость 
  hsvData.z = hsvData.z / (1.0f + hsvData.z);


  // Смещение тона по аддитивному закону в зависимости от яркости.
  float coefOffsetHue = 1.0f - pow(hsvData.z, 10.0f);
  //coefOffsetHue = 1.0f; // turned off

  // От красного (360) до пурпурного (300) сжимаются к пурпурному.
  if (hsvData.x < 360 && hsvData.x >= 300)
    OffsetCenter(hsvData.x, 360, 300, coefOffsetHue);

  // От пурпурного (300) до синего (240) сжимаются к пурпурному.
  else if (hsvData.x < 300 && hsvData.x >= 240)
    OffsetCenter(hsvData.x, 240, 300, coefOffsetHue);

  // От синего (240) до бирюзового (180) сжимаются к бирюзовому
  else if (hsvData.x < 240 && hsvData.x >= 180)
    OffsetCenter(hsvData.x, 240, 180, coefOffsetHue);

  // От бирюзового (180) до зелёного (120) сжимаются к бирюзовому
  else if (hsvData.x < 180 && hsvData.x >= 120)
    OffsetCenter(hsvData.x, 120, 180, coefOffsetHue);

  // От зелёного (120) до жёлтого (60) сжимаются к жёлтому.
  else if (hsvData.x < 120 && hsvData.x >= 60)
    OffsetCenter(hsvData.x, 120, 60, coefOffsetHue);

  // От красного (0) до жёлтого (60) сжимаются к жёлтому
  else if (hsvData.x < 60 && hsvData.x >= 0)
    OffsetCenter(hsvData.x, 0, 60, coefOffsetHue);
}
void CalculateHistogram(const float &data, float histogram[], const int histogramBin, const float iterrHistogramBin)
{
  if (data <= 1.0f)
  {
    // Current bin.      
    const int currentHistogramBin = (int)round(data * float(histogramBin - 1));

    // Increase count currents bin.
    if (currentHistogramBin >= 0 && currentHistogramBin < histogramBin)
      histogram[currentHistogramBin] += iterrHistogramBin;
  }
}
void UniformHistogram(float histogram[], const int histogramBin)
{
  // Recalculate histogramm
  for (int i = 1; i < histogramBin; i++)
  {
    histogram[i] = histogram[i - 1] + histogram[i]; // cummulative uniform
  }
}
void NormalizeHistogram(float histogram[], const int histogramBin)
{
  float histMin = 999999.9f;
  float histMax = 0.0f;

  // Calculate min and max value
#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
  {
    if (histogram[i] < histMin && histogram[i] != 0)
      histMin = histogram[i];

    if (histogram[i] > histMax)
      histMax = histogram[i];
  }

  // Normalize to 0 - 1
#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
  {
    Normalize(histogram[i], histMin, histMax, 0.0f, 1.0f);
  }
}
void UniformContrastRgb(float data[], const int sizeImage, const int histogramBin)
{
  float histMin = 9999999.9f;
  float histMax = 0.0f;

  const float iterrHistogramBin = 1.0f / (float)(sizeImage * 3);

  std::vector<float> histogram(histogramBin);

  // --- Cчитаем гистограмму ---
  //#pragma omp parallel for
  for (int i = 0; i < sizeImage * 3; i++)
  {
    if (data[i] <= 1.0f)
    {
      // Текущая корзина.      
      const int currentHistogramBin = (int)round(data[i] * float(histogramBin - 1));

      // Увеличиваем счётчик текущей корзины.
      if (currentHistogramBin >= 0 && currentHistogramBin < histogramBin)
        histogram[currentHistogramBin] += iterrHistogramBin;
    }
  }

  // Пересчитываем гистограмму (перераспределение)
  for (int i = 1; i < histogramBin; i++)
    histogram[i] = histogram[i - 1] + histogram[i];

  // // Находим мин и макс значение гистограммы
  //#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
  {
    if (histogram[i] < histMin && histogram[i] != 0)
      histMin = histogram[i];

    if (histogram[i] > histMax)
      histMax = histogram[i];
  }

  // Нормализуем гистограмму к 0 - 1 и выводим
#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
    Normalize(histogram[i], histMin, histMax, 0.0f, 1.0f);

  // Присваиваем гистограмму яркости.
#pragma omp parallel for
  for (int i = 0; i < sizeImage * 3; i++)
  {
    if (data[i] <= 1.0f)
    {
      const int currentHistogramBin = (int)round(data[i] * float(histogramBin - 1));

      if (currentHistogramBin >= 0 && currentHistogramBin < histogramBin)
        data[i] = pow(histogram[currentHistogramBin], 2.2f);
      else
        data[i] = 0.0f;
    }
  }
}
void AssignHistogramToData(float &data, const float histogram[], const int histogramBin)
{
  if (data <= 1.0f)
  {
    // Assign the histogram to the data.
    const int currentHistogramBin = (int)round(data * float(histogramBin - 1));

    if (currentHistogramBin >= 0 && currentHistogramBin < histogramBin)
      data = pow(histogram[currentHistogramBin], 2.2f);
    else
      data = 0.0f;
  }
}
void UniformContrastField0(float4 data[], const int sizeImage, const int histogramBin)
{
#define MAX3(x,y,z)  ((y) >= (z) ? ((x) >= (y) ? (x) : (y)) : ((x) >= (z) ? (x) : (z)))

  float histMin = 10000000.0f;
  float histMax = 0.0f;

  const float iterrHistogramBin = 1.0f / (float)(sizeImage);

  std::vector<float> histogram(histogramBin);

  // --- Cчитаем гистограмму ---
  //#pragma omp parallel for
  for (int i = 0; i < sizeImage; i++)
  {
    data[i].x = data[i].x / (1.0f + data[i].x);

    // Текущая корзина.      
    const int currentHistogramBin = (int)round(data[i].x * float(histogramBin - 1));

    // Увеличиваем счётчик текущей корзины.
    if (currentHistogramBin >= 0 && currentHistogramBin < histogram.size())
      histogram[currentHistogramBin] += iterrHistogramBin;
  }

  // Пересчитываем гистограмму (перераспределение)
  for (int i = 1; i < histogramBin; i++)
    histogram[i] = histogram[i - 1] + histogram[i];

  // Находим мин и макс значение гистограммы
#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
  {
    if (histogram[i] < histMin && histogram[i] != 0)
      histMin = histogram[i];

    if (histogram[i] > histMax)
      histMax = histogram[i];
  }

  // Нормализуем гистограмму к 0 - 1 и выводим
#pragma omp parallel for
  for (int i = 0; i < histogramBin; i++)
    Normalize(histogram[i], histMin, histMax, 0.0f, 1.0f);

  // Присваиваем гистограмму яркости.
#pragma omp parallel for
  for (int i = 0; i < sizeImage; i++)
  {
    const int currentHistogramBin = (int)round(data[i].x * float(histogramBin - 1));

    if (currentHistogramBin >= 0 && currentHistogramBin < histogram.size())
      data[i].x = pow(histogram[currentHistogramBin], 2.2f);
    else
      data[i].x = 0.0f;
  }
}
float Distance(const float x1, const float y1, const float x2, const float y2)
{
  float differenceX = x1 - x2;
  float differenceY = y1 - y2;

  float distance = abs(sqrt(differenceX * differenceX + differenceY * differenceY));
  return distance;
}
void AntialiasArray(const float Brightness, float outData[], int x, int y, float newPosX, float newPosY, const int width, const int height)
{
  const int sizeArray = width * height;

  newPosX += 1.0f;
  newPosY += 1.0f;
  x += 1;
  y += 1;

  int dxy1 = (int)floor(newPosY)       * width + (int)floor(newPosX);
  int dxy2 = (int)floor(newPosY)       * width + (int)floor(newPosX) + 1;
  int dxy3 = (int)(floor(newPosY) + 1) * width + (int)floor(newPosX);
  int dxy4 = (int)(floor(newPosY) + 1) * width + (int)floor(newPosX) + 1;

  float dx = newPosX - x;
  float dy = newPosY - y;

  if (dx < 0) dx = 1 - abs(dx);
  if (dy < 0) dy = 1 - abs(dy);

  float multBrightness1 = (1 - dx) * (1 - dy);
  float multBrightness2 = dx * (1 - dy);
  float multBrightness3 = dy * (1 - dx);
  float multBrightness4 = dx * dy;

  int PosXy = y * width + x;

  if (newPosX < 1.0f)
  {
    multBrightness1 = 0;
    multBrightness3 = 0;
  }
  else if (newPosX > width)
  {
    multBrightness2 = 0;
    multBrightness4 = 0;
  }

  if (newPosY < 1.0f)
  {
    multBrightness1 = 0;
    multBrightness2 = 0;
  }
  else if (newPosY > height)
  {
    multBrightness3 = 0;
    multBrightness4 = 0;
  }

  outData[dxy1] += Brightness * multBrightness1;
  outData[dxy2] += Brightness * multBrightness2;
  outData[dxy3] += Brightness * multBrightness3;
  outData[dxy4] += Brightness * multBrightness4;
}
void ClampMinusToZero(float4 data4[], const int i)
{
  if (data4[i].x < 0) data4[i].x = 0;
  if (data4[i].y < 0) data4[i].y = 0;
  if (data4[i].z < 0) data4[i].z = 0;
  if (data4[i].w < 0) data4[i].w = 0;
}
void Vignette(float4 data4[], const int m_width, const int m_height, const float vignette, const int x, const int y, const int i)
{
  if (vignette > 0.0f)
  {
    const float radiusImage = (float)sqrt(m_width * m_width + m_height * m_height) / 2.0f;
    const float centerImageCoordX = m_width / 2.0f;
    const float centerImageCoordY = m_height / 2.0f;
    const float distanceFromCenter = Distance((float)x, (float)y, centerImageCoordX, centerImageCoordY) / radiusImage;

    float vignetteIntensity = sqrt(1 - distanceFromCenter * vignette);
    if (vignetteIntensity < 0.0f) vignetteIntensity = 0;

    data4[i].x *= vignetteIntensity;
    data4[i].y *= vignetteIntensity;
    data4[i].z *= vignetteIntensity;
  }
}
void SummValueOnField(const float4 data4[], float3 &summRgb, const int i)
{
#pragma omp atomic
  summRgb.x += data4[i].x;
#pragma omp atomic
  summRgb.y += data4[i].y;
#pragma omp atomic
  summRgb.z += data4[i].z;
}
void ChrommAberr(const float4 data4[], float chromAbberPixelsG[], float chromAbberPixelsB[], const int m_width, const int m_height, const float chromAberr, const int x, const int y, const int i)
{
  if (chromAberr > 0.0f)
  {
    // Генерируем карту велосити.
    // (0, 0) - влево вниз, (0.5, 0.5) - нет движения. (1, 1) - вправо вверх.  Красный < зелёный < синий.
    const float stepVelocityX = (float)x / m_width;
    const float stepVelocityY = (float)y / m_height;
    const float velocityX = stepVelocityX * 2.0f - 1.0f;
    const float velocityY = stepVelocityY * 2.0f - 1.0f;

    const float newPosXg = x + velocityX * 0.5f * chromAberr;
    const float newPosYg = y + velocityY * 0.5f * chromAberr;
    const float newPosXb = x + velocityX * 1.0f * chromAberr;
    const float newPosYb = y + velocityY * 1.0f * chromAberr;

    const float BrightnessG = data4[i].y;
    const float BrightnessB = data4[i].z;

    // Anti-aliasing
    AntialiasArray(BrightnessG, chromAbberPixelsG, x, y, newPosXg, newPosYg, m_width, m_height);
    AntialiasArray(BrightnessB, chromAbberPixelsB, x, y, newPosXb, newPosYb, m_width, m_height);
  }
}
void ComputeWhitePoint(const float3 summRgb, float3 &whitePoint, const int sizeImage)
{
  whitePoint.x = summRgb.x / (float)sizeImage;
  whitePoint.y = summRgb.y / (float)sizeImage;
  whitePoint.z = summRgb.z / (float)sizeImage;
}
void MinMaxRgb(float4 data[], float &minRgb, float &maxRgb, const float thresholdFloor, const float thresholdTop, const int i)
{
  if (data[i].x < minRgb && data[i].x > 0.0f) minRgb = data[i].x;
  if (data[i].x > maxRgb)                     maxRgb = data[i].x;

  if (data[i].y < minRgb && data[i].y > 0.0f) minRgb = data[i].y;
  if (data[i].y > maxRgb)                     maxRgb = data[i].y;

  if (data[i].z < minRgb && data[i].z > 0.0f) minRgb = data[i].z;
  if (data[i].z > maxRgb)                     maxRgb = data[i].z;

  if (minRgb < thresholdFloor) minRgb = thresholdFloor;
  if (maxRgb > thresholdTop)   maxRgb = thresholdTop;

  if (data[i].x < thresholdFloor) data[i].x = thresholdFloor;
  else if (data[i].x > thresholdTop)   data[i].x = thresholdTop;

  if (data[i].y < thresholdFloor) data[i].y = thresholdFloor;
  else if (data[i].y > thresholdTop)   data[i].y = thresholdTop;

  if (data[i].z < thresholdFloor) data[i].z = thresholdFloor;
  else if (data[i].z > thresholdTop)   data[i].z = thresholdTop;
}
void MinMaxArray(float &data, float &min, float &max, const float thresholdFloor, const float thresholdTop)
{
  if (data < min && data > 0.0f)  min = data;
  else if (data > max)                 max = data;

  if (min < thresholdFloor) min = thresholdFloor;
  if (max > thresholdTop)   max = thresholdTop;

  if (data < thresholdFloor) data = thresholdFloor;
  else if (data > thresholdTop)   data = thresholdTop;
}
void DrawColumn(float4 data[], const int width, const int height, const int offsetX, const int ImgWidth)
{
  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      const int a = y * ImgWidth + x + offsetX;

      data[a].x = 0.5f;
      data[a].y = 0.5f;
      data[a].z = 0.5f;
    }
  }
}
void ViewHistorgam(float4 data[], const float histogram[], const int m_width, const int m_height, const int histogramBin)
{
  const int countCol = m_width;
  const int step = histogramBin / countCol;
  const int width = m_width / countCol;

  for (int i = 0; i < countCol; i++)
  {
    const float height = histogram[i * step] * m_height;

    DrawColumn(data, width, (int)height, i * width, m_width);
  }
}
void MinMaxHistBin(const float histogram[], float &minHistBin, float &maxHistBin, const int sizeImage, const int histogramBin)
{
  int i = 0;
  const float floor = (float)sizeImage * 0.00001f; // 0.001% from image resolution

  while (histogram[i] <= floor && i < histogramBin)
  {
    minHistBin = (float)i;
    i++;
  }

  i = histogramBin - 1;

  while (histogram[i] <= floor && i >= 0)
  {
    maxHistBin = (float)i;
    i--;
  }

  minHistBin /= histogramBin;
  maxHistBin /= histogramBin;
}
void Min3(const float value1, const float value2, const float value3, float &outMin)
{
  if (value1 <= value2 && value1 <= value3) outMin = value1;
  else if (value2 <= value1 && value1 <= value3) outMin = value2;
  else                                           outMin = value3;
}
void Max3(const float value1, const float value2, const float value3, float &outMax)
{
  if (value1 >= value2 && value1 >= value3) outMax = value1;
  else if (value2 >= value1 && value1 >= value3) outMax = value2;
  else                                           outMax = value3;
}



// ---------- End function for post processing ----------

void TestToneMapping::SetPresets(const char* a_presetsStr)
{
  std::istringstream iss(a_presetsStr);

  do
  {
    std::string name, val;
    iss >> name >> val;

    if      (name == "exposure")        m_exposure        = atof(val.c_str());
    else if (name == "compress")        m_compress        = atof(val.c_str());
    else if (name == "contrast")        m_contrast        = atof(val.c_str());
    else if (name == "saturation")      m_saturation      = atof(val.c_str());
    else if (name == "whiteBalance")    m_whiteBalance    = atof(val.c_str());
    else if (name == "uniformContrast") m_uniformContrast = atof(val.c_str());
    else if (name == "normalize")       m_normalize       = atof(val.c_str());
    else if (name == "chromAberr")      m_chromAberr      = atof(val.c_str());
    else if (name == "vignette")        m_vignette        = atof(val.c_str());

  } while (iss);

}

bool TestToneMapping::Eval()
{

  float4* data4 = (float4*)m_data;

  // Настройки фильтров
  const float exposure          = m_exposure;
  const float compress          = m_compress;
  const float contrast          = m_contrast;
  const float saturation        = m_saturation;
  const float whiteBalance      = m_whiteBalance;
  const float uniformContrast   = m_uniformContrast;
  const float normalize         = m_normalize;
  const float vignette          = m_vignette;
  const float chromAberr        = m_chromAberr;

  // constant and variables

  const int sizeImage           = m_width * m_height;
  const int histogramBin = 10000;
  const float iterrHistogramBin = 1.0f;
  float3 summRgb = { 0.0f, 0.0f, 0.0f };
  float3 whitePoint = { 1.0f, 1.0f, 1.0f };
  float  coefOffsetHue = 0.0f;
  float  minRgbSource = 10000.0f;
  float  maxRgbSource = 0.0f;
  float  minRgbFinish = 10000.0f;
  float  maxRgbFinish = 0.0f;
  float  minRgbSourceBlur = 10000.0f;
  float  maxRgbSourceBlur = 0.0f;

  float  minAllHistBin = 0.0f;
  float  maxAllHistBin = 1.0f;
  float  minHistRbin = 0.0f;
  float  minHistGbin = 0.0f;
  float  minHistBbin = 0.0f;
  float  maxHistRbin = histogramBin;
  float  maxHistGbin = histogramBin;
  float  maxHistBbin = histogramBin;

  std::vector<float>  chromAbberPixelsG(sizeImage + m_width * 2 + m_height * 2 + 4);
  std::vector<float>  chromAbberPixelsB(sizeImage + m_width * 2 + m_height * 2 + 4);
  std::vector<float>  histogram(histogramBin);
  std::vector<float>  luminance(sizeImage);


  // Устанавливаем желаемое количество потоков
  omp_set_num_threads(2);

  //********************* Loop 1. Analization, precalculate and 3 effects *********************

  //#pragma omp parallel for
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      const int i = y * m_width + x;

      ClampMinusToZero(data4, i);

      // ----- Exposure -----

      if (exposure != 1.0f)
      {
        data4[i].x *= exposure;
        data4[i].y *= exposure;
        data4[i].z *= exposure;
      }

      // Max & Min value and clamp
      MinMaxRgb(data4, minRgbSource, maxRgbSource, 0.0f, 255.0f, i);

      // Collect all the brightness values by channel.
      SummValueOnField(data4, summRgb, i);

      // ----- Vignette -----
      Vignette(data4, m_width, m_height, vignette, x, y, i);

      // ----- Chromatic aberration -----
      ChrommAberr(data4, &chromAbberPixelsG[0], &chromAbberPixelsB[0], m_width, m_height, chromAberr, x, y, i);
    }
  }

  // ----- White point for white balance -----

  if (whiteBalance > 0.0f)
  {
    ComputeWhitePoint(summRgb, whitePoint, sizeImage);

    whitePoint.x /= whitePoint.y;
    whitePoint.z /= whitePoint.y;
    whitePoint.y = 1.0f;

    ConvertSrgbToXyz(whitePoint);
    MatrixCat02(whitePoint);
  }

  // *** Loop 3. Chromm. aberr., exposure, white balance, compress, saturation, contrast. ***

  if (chromAberr > 0.0f || saturation != 1 || whiteBalance > 0.0f || compress > 0.0f || contrast > 1.0f)
  {
    #pragma omp parallel for
    for (int i = 0; i < sizeImage; i++)
    {
      // ----- Chrommatic aberration -----

      if (chromAberr > 0.0f)
      {
        data4[i].y = chromAbberPixelsG[i + m_width + 1];
        data4[i].z = chromAbberPixelsB[i + m_width + 1];
      }

      // ----- White balance, compress -----

      if (compress > 0.0f || whiteBalance > 0.0f)
      {
        float4 iptData = data4[i];

        ConvertSrgbToXyz(iptData);

        // ----- White balance -----

        if (whiteBalance > 0.0f)
        {
          ChromAdaptCam02(iptData, whitePoint, whiteBalance);
        }

        ConvertXyzToLms(iptData);
        ConvertLmsToIpt(iptData);

        // ----- Compress -----

        if (compress > 0.0f && maxRgbSource > 1.0f)
        {
          iptData.y /= (1.0f + iptData.x);
          iptData.z /= (1.0f + iptData.x);

          Compress(iptData.x, maxRgbSource);
        }

        // Convert back to RGB
        ConvertIptToLms(iptData);
        ConvertLmsToXyz(iptData);
        ConvertXyzToSrgb(iptData);

        // Return to main array
        Blend(data4[i].x, iptData.x, compress);
        Blend(data4[i].y, iptData.y, compress);
        Blend(data4[i].z, iptData.z, compress);
      }

      // ----- Saturation  -----

      if (saturation != 1.0f)
      {
        const float luminance = 0.2126f * data4[i].x + 0.7152f * data4[i].y + 0.0722f * data4[i].z;

        // Return to main array
        data4[i].x = luminance + (data4[i].x - luminance) * saturation;
        data4[i].y = luminance + (data4[i].y - luminance) * saturation;
        data4[i].z = luminance + (data4[i].z - luminance) * saturation;

        ClampMinusToZero(data4, i);
      }

      // little compress

      if (compress > 0.0f && maxRgbSource > 1.0f || saturation > 1.0f)
      {
        data4[i].x = pow(data4[i].x, 4.0f);
        data4[i].y = pow(data4[i].y, 4.0f);
        data4[i].z = pow(data4[i].z, 4.0f);

        data4[i].x /= (1.0f + data4[i].x);
        data4[i].y /= (1.0f + data4[i].y);
        data4[i].z /= (1.0f + data4[i].z);

        data4[i].x = pow(data4[i].x, 0.25f);
        data4[i].y = pow(data4[i].y, 0.25f);
        data4[i].z = pow(data4[i].z, 0.25f);
      }

      // ----- Contrast -----

      if (contrast > 1.0f)
      {
        float4 rgbData = data4[i];

        ContrastField(rgbData.x);
        ContrastField(rgbData.y);
        ContrastField(rgbData.z);

        // Return to main array
        Blend(data4[i].x, rgbData.x, contrast - 1);
        Blend(data4[i].y, rgbData.y, contrast - 1);
        Blend(data4[i].z, rgbData.z, contrast - 1);
      }
    }
  }

  // ------------------------ Loop 4. Uniform contrast. ------------------------

  if (uniformContrast > 0.0f)
  {
    std::vector<float> rgbArray(sizeImage * 3);

    #pragma omp parallel for
    for (int i = 0; i < sizeImage; i++)
    {
      // Convert 3 field RGB to linear array.
      rgbArray[i] = data4[i].x;
      rgbArray[sizeImage + i] = data4[i].y;
      rgbArray[sizeImage * 2 + i] = data4[i].z;
    }

    UniformContrastRgb(&rgbArray[0], sizeImage, histogramBin);

    // ------------------------ Loop 5. Return to main array. ------------------------

    #pragma omp parallel for
    for (int i = 0; i < sizeImage; i++)
    {
      // Return to main array
      Blend(data4[i].x, rgbArray[i], uniformContrast);
      Blend(data4[i].y, rgbArray[i + sizeImage], uniformContrast);
      Blend(data4[i].z, rgbArray[i + sizeImage * 2], uniformContrast);
    }
  }

  // ------------------------ Loop 6. Calculate histogram for normalize ------------------------

  if (normalize > 0.0f)
  {
    std::vector<float>  histogramR(histogramBin);
    std::vector<float>  histogramG(histogramBin);
    std::vector<float>  histogramB(histogramBin);

    #pragma omp parallel for
    for (int i = 0; i < sizeImage; i++)
    {
      CalculateHistogram(data4[i].x, &histogramR[0], histogramBin, iterrHistogramBin);
      CalculateHistogram(data4[i].y, &histogramG[0], histogramBin, iterrHistogramBin);
      CalculateHistogram(data4[i].z, &histogramB[0], histogramBin, iterrHistogramBin);
    }

    // ------------------------ Calculate min/max histogram for normalize ------------------------

    MinMaxHistBin(&histogramR[0], minHistRbin, maxHistRbin, sizeImage, histogramBin);
    MinMaxHistBin(&histogramG[0], minHistGbin, maxHistGbin, sizeImage, histogramBin);
    MinMaxHistBin(&histogramB[0], minHistBbin, maxHistBbin, sizeImage, histogramBin);

    Min3(minHistRbin, minHistGbin, minHistBbin, minAllHistBin);
    Max3(maxHistRbin, maxHistGbin, maxHistBbin, maxAllHistBin);

    // ------------------------ Loop 7. Normalize ------------------------

    #pragma omp parallel for
    for (int i = 0; i < sizeImage; i++)
    {
      Normalize(data4[i].x, minAllHistBin, maxAllHistBin, 0.0f, 1.0f);
      Normalize(data4[i].y, minAllHistBin, maxAllHistBin, 0.0f, 1.0f);
      Normalize(data4[i].z, minAllHistBin, maxAllHistBin, 0.0f, 1.0f);
    }
  }

  //********************* Конец применения фильтров *********************


  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
AndrewToneMappingFilter::AndrewToneMappingFilter() 
{
  m_strength   = 0.0f;
  m_whitePoint = 0.0f;
  m_phi        = 0.0f;
}

AndrewToneMappingFilter::~AndrewToneMappingFilter() {}

void AndrewToneMappingFilter::Release(){}

void AndrewToneMappingFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName) {}

void AndrewToneMappingFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data = a_output;
  m_width = w;
  m_height = h;
}

#include "../HDRCore/PostProcessEngine/image.h"
#include "../HDRCore/PostProcessEngine/filters/tonemappingfilter.h"
#include "../HDRCore/PostProcessEngine/filters/blurfilter.h"
#include "../HDRCore/PostProcessEngine/filters/boxfilter.h"

void AndrewToneMappingFilter::SetPresets(const char* a_presetsStr)
{
  std::istringstream iss(a_presetsStr);

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == "strength")
      m_strength = atof(val.c_str());
    else if (name == "whitePoint")
      m_whitePoint = atof(val.c_str());
    else if (name == "phi")
      m_phi = atof(val.c_str());

  } while (iss);

}

bool AndrewToneMappingFilter::Eval()
{
  int renderWidth  = m_width;
  int renderHeight = m_height;

  PP_FILTERS::ToneMappingFilter filter;
  PP_FILTERS::ToneMappingFilter::Presets hdrPresets;
  PP_FILTERS::Image src, dst;

  hdrPresets.strength   = m_strength;
  hdrPresets.phi        = m_phi;
  hdrPresets.whitePoint = m_whitePoint;

  filter.SetPresets(hdrPresets);
  src.CreateLocked(m_data, renderWidth, renderHeight, renderWidth * 4, 4);
 
  dst.CreateStandard(renderWidth, renderHeight, 4);
  filter.Apply(&dst, &src);

  for (int i = 0; i < m_width*m_height; i++)
  {
    m_data[i * 4 + 0] = dst.GetPointer()[i * 4 + 0];
    m_data[i * 4 + 1] = dst.GetPointer()[i * 4 + 1];
    m_data[i * 4 + 2] = dst.GetPointer()[i * 4 + 2];
  }

  return true;
}

*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CopyHDRToMaxBitMapFilter::CopyHDRToMaxBitMapFilter(Bitmap* a_bmap_out)
{
  bmap_out = a_bmap_out;
}

CopyHDRToMaxBitMapFilter::~CopyHDRToMaxBitMapFilter()
{

}

void CopyHDRToMaxBitMapFilter::Release()
{

}

bool CopyHDRToMaxBitMapFilter::Eval()
{
  if (bmap_out == nullptr)
    return false;

  ULONG ctype;
  RealPixel* pGbufRealPix = (RealPixel*)bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype);

  if (pGbufRealPix == nullptr)
  {
    bmap_out->CreateChannels(BMM_CHAN_REALPIX);
    pGbufRealPix = (RealPixel*)bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype);
  }

  const int w = bmap_out->Width();
  const int h = bmap_out->Height();

  std::vector<BMM_Color_fl> line(w);

  const float fhInv = 1.0f / float(h);
  const float fwInv = 1.0f / float(w);

  for (int y = 0; y < h; y++)
  {
    float texCoordY = (float(y) + 0.5f)*fhInv;

    for (int x = 0; x < w; x++)
    {
      int indexSrc    = y*w + x;
      float texCoordX = (float(x) + 0.5f)*fwInv;

      float color[4];
      m_pImageIn->sample(texCoordX, texCoordY, color);

      const float r = color[0];
      const float g = color[1];
      const float b = color[2];
      const float a = color[3];

      if (pGbufRealPix != nullptr) // write non clamped color anyway, even if later we overwrite HDR with LDR
      {
        int gbufIndex = (h - y - 1)*w + x;

        RealPixel rpix;
        rpix = MakeRealPixel(r, g, b);
        pGbufRealPix[gbufIndex] = rpix;
      }

      line[x] = BMM_Color_fl(r, g, b, a);
    }

    bmap_out->PutPixels(0, h - y - 1, m_width, &line[0]);
  }

  return true;
}

void CopyHDRToMaxBitMapFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName)
{
  m_width  = w;
  m_height = h;
}

void CopyHDRToMaxBitMapFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_data   = a_output;
  m_width  = w;
  m_height = h;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline uint32_t IndexZBNumber(int x, int y, int pitch)
{
  return ((x >> 4) + (y >> 4)*(pitch >> 4));
}



std::tuple<float,float> MonteCarloStdErr(float avgColorX, float avgColorY, float avgColorZ, float sqrColor, int nSamples)
{
  float fnSamples = ((float)(nSamples));
  float nSampleInv = 1.0f / fnSamples;

  float maxColor = fmaxf(avgColorX, fmaxf(avgColorY, avgColorZ));

  float variance = sqrt(sqrColor - (maxColor*maxColor));
  float stdError = variance / sqrt(fnSamples);

  return std::make_tuple(stdError, stdError / (fmaxf(maxColor, 0.00001f)));
}

ShowErrorFromZFilter::ShowErrorFromZFilter(ImageZ* a_pInput, Bitmap* a_bmap_out)
{
  bmap_out = a_bmap_out;
  m_pInput = a_pInput;
}


ShowErrorFromZFilter::~ShowErrorFromZFilter()
{

}

void ShowErrorFromZFilter::Release()
{

}

bool ShowErrorFromZFilter::Eval()
{
  if (bmap_out == nullptr)
  {
    strncpy(m_err, "ShowErrorFromZFilter: null bmap_out", 1024);
    return false;
  }

  ULONG ctype;
  Point2* pErr = (Point2*)bmap_out->GetChannel(BMM_CHAN_VELOC, ctype);

  if (pErr == nullptr)
  {
    bmap_out->CreateChannels(BMM_CHAN_VELOC);
    pErr = (Point2*)bmap_out->GetChannel(BMM_CHAN_VELOC, ctype);
  }

  if (pErr == nullptr || m_pInput == nullptr)
  {
    strncpy(m_err, "ShowErrorFromZFilter: null BMM_CHAN_VELOC or m_pInput", 1024);
    return false;
  }

  //
  //
  float* color     = m_pInput->ColorPtr();
  int zimageWidth  = m_pInput->Width();
  int zimageHeight = m_pInput->Height();

  ZBlockT2* pBlocks = m_pInput->ZBlocksPtr();

  if (color == nullptr || pBlocks == nullptr)
  {
    strncpy(m_err, "ShowErrorFromZFilter: null color", 1024);
    return false;
  }

  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth); 
      int zbNum    = IndexZBNumber(x, y, zimageWidth);

      ZBlockT2 zb  = pBlocks[zbNum]; 
      int nSamples = zb.counter;

      float r = color[indexSrc * 4 + 0];
      float g = color[indexSrc * 4 + 1];
      float b = color[indexSrc * 4 + 2];
      float s = color[indexSrc * 4 + 3];

      float errRel  = std::get<1>(MonteCarloStdErr(r, g, b, s, nSamples));

      int gbufIndex = (m_height - y - 1)*m_width + x;
      
      Point2 rpix;
      rpix.x = errRel*100.0f;
      rpix.y = (float)(nSamples);
      pErr[gbufIndex] = rpix;
      
    }
  }

  return true;
}

void ShowErrorFromZFilter::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName)
{
  m_width = w;
  m_height = h;
}

void ShowErrorFromZFilter::SetOutput_f4(float* a_output, int w, int h)
{
  m_width = w;
  m_height = h;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int backetImage[16][16] = {
  { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 },
};

int backetImage2[16][16] = {
  { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
  { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1 },
};



ShowTiles::ShowTiles(ImageZ* a_pInput, Bitmap* a_bmap_out)
{
  bmap_out = a_bmap_out;
  m_pInput = a_pInput;
}


ShowTiles::~ShowTiles()
{

}

void ShowTiles::Release()
{

}

bool ShowTiles::Eval()
{
  if (bmap_out == nullptr)
  {
    strncpy(m_err, "ShowTiles: null bmap_out", 1024);
    return false;
  }

  ULONG ctype;
  Point2* pErr = (Point2*)bmap_out->GetChannel(BMM_CHAN_VELOC, ctype);

  if (pErr == nullptr)
  {
    bmap_out->CreateChannels(BMM_CHAN_VELOC);
    pErr = (Point2*)bmap_out->GetChannel(BMM_CHAN_VELOC, ctype);
  }

  if (pErr == nullptr || m_pInput == nullptr)
  {
    strncpy(m_err, "ShowTiles: null BMM_CHAN_VELOC or m_pInput", 1024);
    return false;
  }

  //
  //
  float* color     = m_pInput->ColorPtr();
  int zimageWidth  = m_pInput->Width();
  int zimageHeight = m_pInput->Height();

  ZBlockT2* pBlocks = m_pInput->ZBlocksPtr();

  if (color == nullptr || pBlocks == nullptr)
  {
    strncpy(m_err, "ShowTiles: null color or colorSqr", 1024);
    return false;
  }

  int zblocksNum = m_pInput->ZBlocksNum();
  int bblocksX   = zimageWidth / 16;
  int bblocksY   = zimageHeight / 16;

  for (int z = 0; z < zblocksNum; z++)
  {
    auto block1 = pBlocks[z];

    int by = z / bblocksX;
    int bx = z - by*bblocksX;
    
    int y0 = by*16;
    int x0 = bx*16;
 
    float colVal = (block1.index2 == 0xFFFFFFFF) ? 0.0f : 1.0f;

    if (colVal > 0.0f && ((x0 + 16) <= m_width) && ((y0 + 16) <= m_height))
    {
      for (int y = 0; y < 16; y++)
      {
        for (int x = 0; x < 16; x++)
        {
          int drawPixel = (z % 2 == 0) ? backetImage[y][x] : backetImage2[y][x];
          if (drawPixel == 1) // && x + x0 < ...
          {
            int indexSrc = (y + y0)*m_width + x + x0;

            float r = m_data[indexSrc * 4 + 0];
            float g = m_data[indexSrc * 4 + 1];
            float b = m_data[indexSrc * 4 + 2];

            r = r*0.5f + 0.5f*colVal;
            g = g*0.5f + 0.5f*colVal;
            b = b*0.5f + 0.5f*colVal*2.0f;

            m_data[indexSrc * 4 + 0] = r;
            m_data[indexSrc * 4 + 1] = g;
            m_data[indexSrc * 4 + 2] = b;
          }
        }
      }
    }
    

  }


  return true;
}

void ShowTiles::SetInput_f4(const float* a_input, int w, int h, const char* a_slotName)
{
  m_width  = w;
  m_height = h;
}

void ShowTiles::SetOutput_f4(float* a_output, int w, int h)
{
  m_width  = w;
  m_height = h;
  m_data   = a_output;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void convertLDRf4ToByte4(std::vector<uint8_t>& a_lbrb, std::vector<float>& a_ldrf, float a_gamma)
{
  if (a_lbrb.size() != a_ldrf.size())
    return;

  for (size_t i = 0; i < a_ldrf.size(); i += 4) // port to SSE
  {
    float r = a_ldrf[i + 0];
    float g = a_ldrf[i + 1];
    float b = a_ldrf[i + 2];
    float a = a_ldrf[i + 3];

    r = pow(r, a_gamma);
    g = pow(g, a_gamma);
    b = pow(b, a_gamma);

    r = r*256.0f - 0.5f;
    g = g*256.0f - 0.5f;
    b = b*256.0f - 0.5f;
    a = a*256.0f - 0.5f;

    r = fmaxf(fminf(r, 255.0f), 0.0f);
    g = fmaxf(fminf(g, 255.0f), 0.0f);
    b = fmaxf(fminf(b, 255.0f), 0.0f);
    a = fmaxf(fminf(a, 255.0f), 0.0f);

    a_lbrb[i + 0] = uint8_t(r);
    a_lbrb[i + 1] = uint8_t(g);
    a_lbrb[i + 2] = uint8_t(b);
    a_lbrb[i + 3] = uint8_t(a);
  }

}

static inline int imax2(int a, int b) { return a > b ? a : b; }

void PostProcessPipeline::EstimateProgressLDR(ImageZ* pImageA, float a_errorOk, int a_minRaysPerPixel)
{
  if (pImageA == nullptr)
    return;

  int m_width  = m_image.width();
  int m_height = m_image.height();


  //
  //
  float* color      = pImageA->ColorPtr();
  int zimageWidth   = pImageA->Width();
  int zimageHeight  = pImageA->Height();

  ZBlockT2* pBlocks = pImageA->ZBlocksPtr();

  if (color == nullptr || pBlocks == nullptr)
    return;

  if (m_tempData.size() != zimageWidth*zimageHeight)
  {
    m_tempData.resize(zimageWidth*zimageHeight*4);
    m_tempLDR1.resize(zimageWidth*zimageHeight*4);
    m_tempLDR2.resize(zimageWidth*zimageHeight*4);
  }

  int zblocksNum = pImageA->ZBlocksNum();
  int bblocksX   = zimageWidth / 16;
  int bblocksY   = zimageHeight / 16;

  

  for (int y = 0; y < zimageHeight; y++)
  {
    for (int x = 0; x < zimageWidth; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, zimageWidth); // zimageHeight - y - 1
      int zbNum    = IndexZBNumber(x, y, zimageWidth);

      ZBlockT2 zb  = pBlocks[zbNum];
      int nSamples = zb.counter;

      float r = color[indexSrc * 4 + 0];
      float g = color[indexSrc * 4 + 1];
      float b = color[indexSrc * 4 + 2];
      float a = color[indexSrc * 4 + 3];
      float s = a;

      // calc +/- deviation
      //
      float e = std::get<0>(MonteCarloStdErr(r, g, b, s, nSamples));

      float resColor[4] = { 0, 0, 0, 0 };
      float plusErr[4]  = { r + e, g + e, b + e, a + e };
      float minusErr[4] = { r - e, g - e, b - e, a - e };

      if (m_ldrPassNumber % 2 == 0)
      {
        resColor[0] = plusErr[0];
        resColor[1] = plusErr[1];
        resColor[2] = plusErr[2];
        resColor[3] = plusErr[3];
      }
      else
      {
        resColor[0] = minusErr[0];
        resColor[1] = minusErr[1];
        resColor[2] = minusErr[2];
        resColor[3] = minusErr[3];
      }

      // store resColor to m_tempData
      //
      int index1 = y*zimageWidth + x;

      m_tempData[index1 * 4 + 0] = resColor[0];
      m_tempData[index1 * 4 + 1] = resColor[1];
      m_tempData[index1 * 4 + 2] = resColor[2];
      m_tempData[index1 * 4 + 3] = resColor[3];
    }
  }

  

  if (m_pToneMapFilter != nullptr) // convert HDR to LDR
  {
    m_pToneMapFilter->SetInput_f4(&m_tempData[0], zimageWidth, zimageHeight, "");
    m_pToneMapFilter->SetOutput_f4(&m_tempData[0], zimageWidth, zimageHeight);
    m_pToneMapFilter->Eval();
  }

  if (m_ldrPassNumber % 2 == 0)
    convertLDRf4ToByte4(m_tempLDR1, m_tempData, 1.0f/2.2f);
  else
    convertLDRf4ToByte4(m_tempLDR2, m_tempData, 1.0f/2.2f);
 
  //
  //
  if ((m_ldrPassNumber % 2 == 0) && (m_ldrPassNumber > a_minRaysPerPixel)) // calc diff(m_tempLDR1,m_tempLDR2)  per pixel, then per block
  {
    for (int z = 0; z < zblocksNum; z++)
    {
      auto block1 = pBlocks[z];

      int by = z / bblocksY;
      int bx = z - by*bblocksY;

      int y0 = by * 16;
      int x0 = bx * 16;

      if (block1.index2 != 0xFFFFFFFF)
      {
        float badPixelsCout     = 0;
        float acceptedBadPixels = 8.0f;

        for (int y = 0; y < 16; y++) // iterate all 16x16 pixels in LDR image
        {
          for (int x = 0; x < 16; x++)
          { 
            int indexSrc = (y + y0)*m_width + x + x0;
            
            uint8_t r1 = m_tempLDR1[indexSrc * 4 + 0];
            uint8_t g1 = m_tempLDR1[indexSrc * 4 + 1];
            uint8_t b1 = m_tempLDR1[indexSrc * 4 + 2];

            uint8_t r2 = m_tempLDR2[indexSrc * 4 + 0];
            uint8_t g2 = m_tempLDR2[indexSrc * 4 + 1];
            uint8_t b2 = m_tempLDR2[indexSrc * 4 + 2];
           
            int eR = abs(r1 - r2);
            int eG = abs(g1 - g2);
            int eB = abs(b1 - b2);

            int maxErr = imax2(eR, imax2(eG, eB));

            float avgColorX = 0.5f*(float(r1) + float(r2));
            float avgColorY = 0.5f*(float(g1) + float(g2));
            float avgColorZ = 0.5f*(float(b1) + float(b2));

            //float maxColor  = fmaxf(avgColorX, fmaxf(avgColorY, avgColorZ));
            //float relError = float(maxErr) / fmaxf(maxColor, 1e-10f);
            float relError = float(maxErr);

            //if (relError >= a_errorOk)
            if (relError >= a_errorOk*100.0f)
            {
              float bpc = ((float)relError / (float)a_errorOk);
              badPixelsCout += bpc*bpc;
            }
          }
        }

        if (badPixelsCout < acceptedBadPixels)
          pBlocks[z].index2 = 0xFFFFFFFF;
      }

    }
  }

 

  m_ldrPassNumber++;

}


