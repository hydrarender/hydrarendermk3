#include "hydraRender mk3.h"
#include <sstream>

#include <genlight.h>
#include <lslights.h>
#include <icustattribcontainer.h>

#include <mentalray/imrPhysicalSunLight.h>
#include <mentalray/imrPhysicalSkyLight.h>
#include <mentalray/imrSkyPortalLight.h>

#include <INodeMentalRayProperties.h>
#include <DaylightSimulation/IPhysicalSunSky.h>
#include <shadgen.h>

void hydraRender_mk3::ExtractIESData(LightObject *light, Matrix3 *lightTransform, pugi::xml_node lightNode)
{
  std::wstring iesPath = FindString(L"Web Filename", light);
  IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();


  if (iesPath != L"")
    iesPath = GetCorrectTexPath2(iesPath.c_str(), pFRM);

  float iesRotX = FindFloat(L"X Rotation", light);
  float iesRotY = FindFloat(L"Y Rotation", light);
  float iesRotZ = FindFloat(L"Z Rotation", light);

  Matrix3 rotU(1), rotV(1), rotW(1);
  Matrix3 mRot(1);
  rotU.RotateX(iesRotX*DEG_TO_RAD);
  rotV.RotateZ(iesRotY*DEG_TO_RAD);
  rotW.RotateY(iesRotZ*DEG_TO_RAD);

  mRot = rotU*rotV*rotW;

 // mRot = (*lightTransform) * mRot;

  float samplerMatrix[16];
  MaxMatrix3ToFloat16V2(mRot, samplerMatrix);

  auto distribution = lightNode.force_child(L"ies");
  distribution.force_attribute(L"data").set_value(iesPath.c_str());

  distribution.force_attribute(L"matrix");
  HydraXMLHelpers::WriteMatrix4x4(distribution, L"matrix", samplerMatrix);
}


bool IsThisFuckingPhotometricVisible(LightObject* light, TimeValue t, Interval valid)
{
  bool isVisible = true;

  LightscapeLight* pMRLight = static_cast<LightscapeLight*>(light);
  ICustAttribContainer* pAttribs = light->GetCustAttribContainer();
  
  if (pAttribs != nullptr)
  {
    int n = pAttribs->GetNumCustAttribs();
    for (int i = 0; i<n; i++)
    {
      CustAttrib* pAttr = pAttribs->GetCustAttrib(i);
      if (pAttr != nullptr)
      {
        hydraStr name = pAttr->GetName();
        if (name.find(_M("Area Light Sampling Custom Attribute")) != hydraStr::npos)
        {
          LightscapeLight::AreaLightCustAttrib* pAreaCustomAttribs = pMRLight->GetAreaLightCustAttrib(pAttr);
          isVisible = pAreaCustomAttribs->IsLightShapeRenderingEnabled(t, &valid);
        }
      }
    }
  }

  return isVisible;
}

float GetFuckingDirectLightAreaShadowsSize(LightObject* light, TimeValue t)
{
  GenLight* pGenLight = static_cast<GenLight*>(light);
  if (pGenLight != nullptr)
  {
    ShadowType* pShadowGen = pGenLight->GetShadowGenerator();
    if (pShadowGen != nullptr)
    {
      IAreaShadowType* area = pShadowGen->GetAreaShadowType();
      if (area != nullptr)
        return 0.1f*0.5f*(area->GetLength(t) + area->GetWidth(t));
    }
  }

  return 0.0f;
}


std::wstring ReadIESLightToString(std::wstring iesPath)
{
  std::wifstream fin(iesPath.c_str());
  std::wostringstream fout;

  if (!fin.is_open())
    return std::wstring(L"");

  std::wstring temp;

  while (!fin.eof())
  {
    std::getline(fin, temp);
    fout << temp.c_str() << std::endl;
  }

  return fout.str();
}

void FixEnvironmentTexOffset(pugi::xml_node a_texNode)
{
  pugi::xml_attribute matrixAttr = a_texNode.attribute(L"matrix");
  if (matrixAttr != nullptr)
  {
    std::wstringstream strIn(matrixAttr.as_string());
    std::wstringstream strOut;
    float matrixData[16];
    
    for (int i = 0; i < 16; i++)
      strIn >> matrixData[i];

    matrixData[3] -= 0.5f;

    for (int i = 0; i < 16; i++)
      strOut << matrixData[i] << L" ";

    const std::wstring mstr = strOut.str();
    matrixAttr.set_value(mstr.c_str());
  }
}

void hydraRender_mk3::ExtractStdLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  ULONG classId = light->ClassID().PartA();
  GenLight* glight = static_cast<GenLight*>(light);
  constexpr ULONG SKY_LIGHT_CLASS_ID_PART_A = 0x7bf61478;

  bool isSpot = false;
  bool isDirectional = false;
  bool isPoint = false;
  bool isSky = false;
  bool isExtra = false;

  struct LightState ls;
  glight->EvalLightState(t, valid, &ls);

  lightNode.force_attribute(L"shape").set_value(L"point");
  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(ls.intens);
  
  const float shadowRadius = GetFuckingDirectLightAreaShadowsSize(light, t);

  switch (classId)
  {
  case FSPOT_LIGHT_CLASS_ID:
  case SPOT_LIGHT_CLASS_ID:
    isSpot = true;
    lightNode.force_attribute(L"type").set_value(L"point");
    lightNode.force_attribute(L"distribution").set_value(L"spot");
    break;

  case DIR_LIGHT_CLASS_ID:
  case TDIR_LIGHT_CLASS_ID:
    isDirectional = true;
    lightNode.force_attribute(L"type").set_value(L"directional");
    lightNode.force_attribute(L"distribution").set_value(L"directional");
    lightNode.force_child(L"shadow_softness").force_attribute(L"val").set_value(1.0f);    
    lightNode.force_child(L"angle_radius").force_attribute(L"val").set_value(shadowRadius);
    break;

  case SKY_LIGHT_CLASS_ID_PART_A:
    isSky = true;
    lightNode.force_attribute(L"type").set_value(L"sky");
    break;

  case OMNI_LIGHT_CLASS_ID:
    isPoint = true;
    lightNode.force_attribute(L"type").set_value(L"point");
    lightNode.force_attribute(L"distribution").set_value(L"uniform");
    break;

  default:
    isExtra = true;
    break;
  }

  bool isTargeted = (classId == SPOT_LIGHT_CLASS_ID || classId == TDIR_LIGHT_CLASS_ID);

  if(isSpot || isDirectional)
  {
    auto sizeNode = lightNode.force_child(L"size");
    sizeNode.force_attribute(L"inner_radius").set_value(toMeters(ls.hotsize));
    sizeNode.force_attribute(L"outer_radius").set_value(toMeters(ls.fallsize));
  }

  if (isSky)
  {  
    const unsigned int PBLOCK_REF = 0;		 // This is a IParamBlock
    const unsigned int PBLOCK_REF_SKY = 0; // This is a IParamBlock2
    auto parametersSky = static_cast<IParamBlock2*>(light->GetReference(PBLOCK_REF_SKY));
    if (parametersSky != nullptr)
    {
      Texmap* colorMap = FindTex(L"Sky Color Map", light);
      float  texAmt = FindFloat(L"Sky Color Map Amt", light);
      int useSeparateSkyLight = FindInt(L"Skylight Mode", light);
      bool useMap = FindBool(L"Sky Color Map On", light);

      if (colorMap != nullptr && useMap && colorMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
      {
        texInfo oneTex = BakeTexture(colorMap);
        if (std::get<0>(oneTex).id != -1)
        {
          intensityNode.force_child(L"color").force_attribute(L"val").set_value(L"1 1 1");
          lightNode.force_attribute(L"distribution").set_value(L"map");

          auto texNode = hrTextureBind(std::get<0>(oneTex), intensityNode.child(L"color"));
          if (std::get<1>(oneTex))
          {
            ExtractSampler(texNode, oneTex);
            FixEnvironmentTexOffset(texNode);
          }
        }
        else
        {
          lightNode.force_attribute(L"distribution").set_value(L"uniform");
        }
      }
    }
  }

  if(isSpot)
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val").set_value(ls.fallsize);
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val").set_value(ls.hotsize);
  }

}

void hydraRender_mk3::ExtractSunParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  BaseInterface* bi = light->GetInterface(IID_MR_PHYSICAL_SUN_LIGHT);
  IMRPhysicalSunLight* mrSun = dynamic_cast<IMRPhysicalSunLight*>(bi);

  float redBlueShift = mrSun->getRedBlueShift(t, valid);
  float shiftT = fabs(redBlueShift);

  Color color = FindColor(L"rgb", light);
  float mult = FindFloat(L"intensity", light);  // 99221.8
  float lMult = FindFloat(L"Multiplier", light); // mrSun->getSkyMultiplier(t, valid); // simple mult
  //float phRad = toMeters(FindFloat(L"Photon Target", light));

  ULONG classId = light->ClassID().PartA();
  bool isTargeted = false;
  Matrix3 targetMatrix;
  INode* targetNode = node->GetTarget();
  if (targetNode != nullptr)
    targetMatrix = toMeters(targetNode->GetObjectTM(t));

  isTargeted = (targetNode != nullptr);

  Matrix3 lightMatrix = toMeters(node->GetObjectTM(t));
  Point3 lightDirPoint = -node->GetNodeTM(t).GetRow(2);

  if(isTargeted)
  {
    Point3 pos, target;

    pos = lightMatrix*Point3(0, 0, 0);
    target = targetMatrix*Point3(0, 0, 0);
    lightDirPoint = (target - pos).Normalize();
  }

  float angleT = sqrtf(fabs(lightDirPoint.z));

  Color colorZenit = color;
  Color colorHorizon = Color(1.0f, 0.4f, 0.05f);

  color = colorZenit*angleT + colorHorizon*(1.0f - angleT);

  if (redBlueShift < 0.0f)
    color = color*(1.0f - shiftT) + Color(0.1f, 0.8f, 1.0f);
  else if (redBlueShift > 0.0f)
    color = color*(1.0f - shiftT) + Color(1.0f, 0.6f, 0.1f);

  lightNode.force_attribute(L"type").set_value(L"directional");
  lightNode.force_attribute(L"shape").set_value(L"point");
  lightNode.force_attribute(L"distribution").set_value(L"directional");

  auto sizeNode = lightNode.force_child(L"size");
  sizeNode.force_attribute(L"inner_radius").set_value(L"0.0");
  sizeNode.force_attribute(L"outer_radius").set_value(L"1000.0");

  auto intensityNode = lightNode.force_child(L"intensity");

  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(color).c_str());
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(1.0f*(mult*lMult / 100000.0f));

  lightNode.force_child(L"shadow_softness").force_attribute(L"val").set_value(mrSun->getShadowSoftness(t, valid));
 
}

void hydraRender_mk3::ExtractPhysSkyParams(LightObject *light, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  BaseInterface* skyInterface = light->GetInterface(MRPHYSSKY_LIGHT_INTERFACECLASS_ID);
  IMRPhysicalSkyInterface* physSkyInterface = static_cast<IMRPhysicalSkyInterface*>(skyInterface);
  if (physSkyInterface != nullptr)
  {
    float skyHaze = physSkyInterface->getHaze(t, valid);
    float skyMult = physSkyInterface->getMultiplier(t, valid);

    lightNode.force_attribute(L"type").set_value(L"sky");
    lightNode.force_attribute(L"distribution").set_value(L"perez");

    lightNode.force_child(L"intensity").force_child(L"color").force_attribute(L"val").set_value(L"1 1 1");
    lightNode.force_child(L"intensity").force_child(L"multiplier").force_attribute(L"val").set_value(skyMult);

    auto sunModel = lightNode.force_child(L"perez");

    sunModel.force_attribute(L"sun_name").set_value(m_sunSkyNameW.c_str());
    sunModel.force_attribute(L"turbidity").set_value(skyHaze + 1.0f);
  }
}

void hydraRender_mk3::ExtractMrSkyPortalParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  BaseInterface* bi = light->GetInterface(IID_MR_SKY_PORTAL_LIGHT);
  IMrSkyPortalLight* mrSkyPortal = dynamic_cast<IMrSkyPortalLight*>(bi);

  Matrix3 lightTransform = toMeters(node->GetObjectTM(t));

  struct LightState ls;
  light->EvalLightState(t, valid, &ls);

  float kf = scaleWithMatrix(1.0f, lightTransform);
  int unitDispType = GetUnitDisplayType();
  float scaleIntensity = (unitDispType == UNITDISP_GENERIC) ? (1.0f / 75.0f) : 1.0f;
  scaleIntensity *= 1.0f / (kf * kf);

  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(scaleIntensity * mrSkyPortal->GetMultiplier(t, valid));

  lightNode.force_attribute(L"type").set_value(L"area");
  lightNode.force_attribute(L"shape").set_value(L"rect");
  lightNode.force_attribute(L"distribution").set_value(L"uniform");
  lightNode.force_attribute(L"visible").set_value(int(mrSkyPortal->GetVisibleInRendering(t, valid)));

  auto sizeNode = lightNode.force_child(L"size");

  // In core "length" = "width" in 3ds max.
  sizeNode.force_attribute(L"half_length").set_value(toMeters(mrSkyPortal->GetWidth(t, valid))*0.5f);
  sizeNode.force_attribute(L"half_width").set_value(toMeters(mrSkyPortal->GetLength(t, valid))*0.5f);
  

  auto portalNode = lightNode.force_child(L"sky_portal");
  portalNode.force_attribute(L"val").set_value(1);
  
  int mode = FindInt(L"Mode", light);

  if (mode == 0)
  {
    portalNode.force_attribute(L"source_id").set_value(m_envRef.id);
  }
  else if (mode == 2)
  {
    portalNode.force_attribute(L"source_id").set_value(-1); //TODO: skylight
  }
  else if (mode == 1)
  {
    Texmap* pCustomMap = mrSkyPortal->GetIlluminationMap();
    BitmapTex* pBmap = static_cast<BitmapTex*>(pCustomMap);

    texInfo oneTex = BakeTexture(pCustomMap);

    if (pCustomMap != nullptr && std::get<0>(oneTex).id != -1)
    {
      auto texNode = hrTextureBind(std::get<0>(oneTex), intensityNode.child(L"color"));
      if (std::get<1>(oneTex))
      {
        ExtractSampler(texNode, oneTex);
        FixEnvironmentTexOffset(texNode);
      }

      std::wstring tempStr = strConverter::ToWideString(pBmap->GetMapName());
      portalNode.force_attribute(L"source_id").set_value(-1); //custom
    }
    else 
      portalNode.force_attribute(L"source_id").set_value(-1);
  }
}


void hydraRender_mk3::ExtractHydraSkyPortalParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  LightscapeLight* pMRLight = static_cast<LightscapeLight*>(light);
  float intensityCd = pMRLight->GetResultingIntensity(t, valid); 
  float sizeX = toMeters(pMRLight->GetLength(t, valid));
  float sizeY = toMeters(pMRLight->GetWidth(t, valid));

  bool isVisible = IsThisFuckingPhotometricVisible(light, t, valid);
  
  lightNode.force_attribute(L"type").set_value(L"area");
  lightNode.force_attribute(L"shape").set_value(L"rect");
  lightNode.force_attribute(L"distribution").set_value(L"uniform");
  lightNode.attribute(L"visible").set_value(isVisible ? 1 : 0);

  Matrix3 lightTransform = toMeters(node->GetObjectTM(t));

  int unitDispType = GetUnitDisplayType();
  float scaleIntensity = (unitDispType == UNITDISP_GENERIC) ? (1.0f / 75.0f) : 1.0f;

  lightTransform.SetRow(3, Point3(0, 0, 0)); // kill translate
  float kf = scaleWithMatrix(1.0f, lightTransform);
  scaleIntensity *= 1.0f / (kf*kf);
  
  struct LightState ls;
  light->EvalLightState(t, valid, &ls);
  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(scaleIntensity*intensityCd);
  
  auto sizeNode = lightNode.force_child(L"size");
  sizeNode.force_attribute(L"half_length").set_value(sizeX * 0.5f);
  sizeNode.force_attribute(L"half_width").set_value(sizeY * 0.5f);

  auto portalNode = lightNode.append_child(L"sky_portal");
  portalNode.append_attribute(L"val").set_value(1);
  portalNode.append_attribute(L"source_id").set_value(m_envRef.id);
}

void hydraRender_mk3::ExtractPhotometricLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  LightscapeLight* pMRLight = static_cast<LightscapeLight*>(light);
  float intensityCd = pMRLight->GetResultingIntensity(t, valid); //pMRLight->GetOriginalIntensity(); //pMRLight->GetResultingIntensity(t, valid);
  float sizeX = toMeters(pMRLight->GetLength(t, valid));
  float sizeY = toMeters(pMRLight->GetWidth(t, valid));
  std::wstring type = L"";
  std::wstring distribution = L"";
  std::wstring shape = L"";
  float sphereRadius = toMeters(pMRLight->GetRadius(t, valid));
  

  LightscapeLight::DistTypes lightDistributionType = pMRLight->GetDistribution();

  bool isVisible = IsThisFuckingPhotometricVisible(light, t, valid);
  
  intensityCd *= GetGlobalLightScale(); // magic mental ray scale; this seems work wrong !!! for some units ... 

  Matrix3 lightTransform = toMeters(node->GetObjectTM(t));

  bool useShadowMapColor = FindBool(L"Use Shadow Color Map", light);
  Texmap* pShadowProjector = FindTex(L"Shadow Projector Map", light);

  //TODO: Light textures 
 /* if (useShadowMapColor && pShadowProjector != nullptr) 
  {
    BitmapTex* pBitMapTex = dynamic_cast<BitmapTex*>(pShadowProjector);

    if (pBitMapTex != nullptr)
    {
      std::wstring tempStr = strConverter::ToWideString(pBitMapTex->GetMapName());
      shadowTexStr = GetCorrectTexPath2(tempStr.c_str(), pFRM);
      shadowTexSamplerStr = ExportTextureSamplerStr(pBitMapTex->GetUVGen(), "sampler", pBitMapTex); // shadowTexStr
    }
  }*/

  switch (pMRLight->Type())
  {
  case  LightscapeLight::TARGET_POINT_TYPE:
  case  LightscapeLight::POINT_TYPE:
    type = L"point";
    shape = L"point";
    sizeX = 1.0f;
    sizeY = 1.0f;

    if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";  
    else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    else
      distribution = L"uniform";
    break;

  case  LightscapeLight::TARGET_SPHERE_TYPE:
  case  LightscapeLight::SPHERE_TYPE:
    intensityCd = intensityCd * (1.0f / (PI * sphereRadius * sphereRadius)); // because using half sizes 
    type = L"area";
    shape = L"sphere";
    sizeX = sphereRadius;
    sizeY = sphereRadius;
    distribution = L"uniform";
    break;

  case  LightscapeLight::TARGET_CYLINDER_TYPE:
  case  LightscapeLight::CYLINDER_TYPE:
    intensityCd = intensityCd * (1.0f / (2.0f * PI * sphereRadius * sphereRadius + 2.0f * PI * sphereRadius * sizeX)); // because using half sizes 
    type = L"area";
    shape = L"cylinder";
    sizeY = sphereRadius;
    distribution = L"diffuse";
    
    break;

  case LightscapeLight::TARGET_DISC_TYPE:
  case LightscapeLight::DISC_TYPE:
    intensityCd = intensityCd * (1.0f / (PI * sphereRadius * sphereRadius)); // because using half sizes 
    type = L"area";
    shape = L"disk";
    sizeX = sphereRadius;
    sizeY = sphereRadius;

    if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";
    else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    else
      distribution = L"diffuse";

    break;

  case LightscapeLight::TARGET_AREA_TYPE:
  case LightscapeLight::AREA_TYPE:
    intensityCd = intensityCd * (1.0f / (sizeX * sizeY)); // because using half sizes 
    type = L"area";
    shape = L"rect";
    sizeX = sizeX*0.5;
    sizeY = sizeY*0.5;

    if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";
    else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    else
      distribution = L"diffuse";
    break;

  default:
    intensityCd = intensityCd*(5.0f / GetGlobalLightScale()); // because using half sizes 
    type = L"point";
    shape = L"point";
    sizeX = 1.0f;
    sizeY = 1.0f;

    if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    else
      distribution = L"uniform";
    break;
  }

  lightNode.force_attribute(L"type").set_value(type.c_str());
  lightNode.force_attribute(L"shape").set_value(shape.c_str());
  lightNode.force_attribute(L"distribution").set_value(distribution.c_str());

  int unitDispType = GetUnitDisplayType();
  float scaleIntensity = (unitDispType == UNITDISP_GENERIC) ? (1.0f / 75.0f) : 1.0f;

  lightTransform.SetRow(3, Point3(0, 0, 0)); // kill translate
  float kf = scaleWithMatrix(1.0f, lightTransform);
  scaleIntensity *= 1.0f / (kf*kf);

  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(scaleIntensity*intensityCd);

  struct LightState ls;
  light->EvalLightState(t, valid, &ls);
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());

  auto sizeNode = lightNode.force_child(L"size");
  if(shape == L"cylinder")
  {
    sizeNode.force_attribute(L"radius").set_value(sizeY);
    sizeNode.force_attribute(L"height").set_value(sizeX);
    sizeNode.force_attribute(L"angle").set_value(360.0f);
  }
  else if (shape == L"sphere")
  {
    sizeNode.force_attribute(L"radius").set_value(sizeX);
  }
  else
  {
    sizeNode.force_attribute(L"radius").set_value(sizeX);
    sizeNode.force_attribute(L"half_length").set_value(sizeY);
    sizeNode.force_attribute(L"half_width").set_value(sizeX);
  }
  lightNode.attribute(L"visible").set_value(isVisible ? 1 : 0);

  if (distribution == L"ies")
    ExtractIESData(light, &lightTransform, lightNode);

  if (distribution == L"spot")
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val").set_value(ls.fallsize);
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val").set_value(ls.hotsize);
  }
}

void hydraRender_mk3::ExtractAndInstanceLights(int& lightsNum, TimeValue t)
{
  ExtractEnvironmentLight(t);
  
  for (auto it = lightNodesPools.begin(); it != lightNodesPools.end(); ++it)
  {
    INode* representative_node = (*it)->GetRepresentativeNode();

    if (representative_node == nullptr)
      continue;

    ObjectState os = representative_node->EvalWorldState(t);
    LightObject* light = static_cast<LightObject*>(os.obj);

    if (!os.obj || os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0) || !light->GetUseLight())
      continue;

    bool isNew = true;
    HRLightRef lightRef;

    auto findLightRef = lightsRefLib.find(os.obj);

    if (findLightRef == lightsRefLib.end())
    {
      lightRef = hrLightCreate(representative_node->GetName());
      lightsRefLib[os.obj] = lightRef;    
    }
    else
    {
      lightRef = findLightRef->second;
      isNew = false;
    }

    if ((*it)->needExport)
    {
      ExtractLightObject(representative_node, t, lightRef, isNew);
      (*it)->needExport = false;
    }

    for (auto j = (*it)->m_pool.begin(); j != (*it)->m_pool.end(); ++j)
    {
      float matrixT[4][4];
      Matrix3 pivot = toMeters((*j)->GetObjectTM(t));
      Matrix3 matrixFinal;

      ObjectState _os = (*j)->EvalWorldState(t);
      LightObject* _light = static_cast<LightObject*>(_os.obj);

      auto renderThisInstance = _light->GetUseLight();// (GetCOREInterface13()->GetRendHidden() || !(*j)->IsNodeHidden());
      if (renderThisInstance)
      {
        matrixFinal = TransformMatrixFromMax(pivot);
        MaxMatrix3ToFloat16V2(matrixFinal, &matrixT[0][0]);

        hrLightInstance(currentScene, lightsRefLib[os.obj], &matrixT[0][0]);
        lightsNum += 1;
      }
    }
  }

  
  /*for (auto it = lightInstancesLib.begin(); it != lightInstancesLib.end(); ++it)
  {
    INode* representative_node = it->second.at(0);

    ObjectState os = representative_node->EvalWorldState(t);

    if (!os.obj || os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
      continue;

    HRLightRef lightRef;
    bool isNew = true;
    auto findLightRef = lightsRefLib.find(os.obj);

    if (findLightRef == lightsRefLib.end())
    {
      lightRef = hrLightCreate(representative_node->GetName());
    }
    else
    {
      lightRef = findLightRef->second;
      isNew = false;
    }

    ExtractLightObject(representative_node, t, lightRef, isNew);
    lightsRefLib[os.obj] = lightRef;

    for (auto j = it->second.begin(); j != it->second.end(); ++j)
    {
      float matrixT[4][4];
      Matrix3 pivot = toMeters((*j)->GetObjectTM(t));
      Matrix3 matrixFinal;

      matrixFinal = TransformMatrixFromMax(pivot);
      MaxMatrix3ToFloat16V2(matrixFinal, &matrixT[0][0]);

      hrLightInstance(currentScene, lightRef, &matrixT[0][0]);
      lightsNum += 1;
    }
  }*/
}

void hydraRender_mk3::ExtractEnvironmentLight(TimeValue t)
{
  Texmap* envMap = GetCOREInterface()->GetEnvironmentMap();
  BOOL    useMap = GetCOREInterface()->GetUseEnvironmentMap();
  Point3 bgColor = GetCOREInterface()->GetBackGround(t, FOREVER);
  Point3 amColor = GetCOREInterface()->GetAmbient(t, FOREVER);
  Point3 tintColor = GetCOREInterface()->GetLightTint(t, FOREVER);
  float  tintLevel = GetCOREInterface()->GetLightLevel(t, FOREVER);


  /*if(IsNewSunSky(envMap))
  { 
     IPhysicalSunSky* const physical_sun_sky = dynamic_cast<IPhysicalSunSky*>(envMap);
     const IPhysicalSunSky::ShadingParameters params = physical_sun_sky->EvaluateShadingParameters(t, INFINITE);
  }*/


  float intensity = 1.0f;
  
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  if (m_firstRender)
  {
    //mode = HR_WRITE_DISCARD;
    m_envRef = hrLightCreate(L"environment");
  }
  else
  {
    //mode = HR_OPEN_EXISTING;
  }

  const bool doNotUpdateEnv = rendParams.debugCheckBoxes[L"BLACK_IMAGE"]; //#TODO: fix this inside HydraAPI (via memcmp or hash)

  hrLightOpen(m_envRef, mode);
  {
    auto lightNode = hrLightParamNode(m_envRef);
    auto intensityNode = lightNode.force_child(L"intensity");
    lightNode.force_attribute(L"type").set_value(L"sky");
    if (envMap != nullptr && useMap && envMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
    {
      envTex = BakeTexture(envMap, std::get<0>(envTex), L"", doNotUpdateEnv);

      if (std::get<0>(envTex).id == -1)
      {
        lightNode.force_attribute(L"distribution").set_value(L"uniform");
        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(bgColor * tintColor).c_str());
      }
      else
      {
        lightNode.force_attribute(L"distribution").set_value(L"map");

        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(tintColor).c_str());
        auto texNode = hrTextureBind(std::get<0>(envTex), intensityNode.force_child(L"color"));
        if (std::get<1>(envTex))
        {
          ExtractSampler(texNode, envTex);
          FixEnvironmentTexOffset(texNode);
        }
      }
    }
    // HydraBack/Envir node
    else if (envMap != nullptr && useMap && envMap->ClassID() == Class_ID(0xe954bddf, 0x4a0184fd)) 
    {
      Texmap* subTexBack = envMap->GetSubTexmap(0);
      Texmap* subTexEnvir = envMap->GetSubTexmap(1);
      Point3 backColor =  FindColor(L"BackColor", envMap);
      Point3 envColor = FindColor(L"EnvirColor", envMap);

      texInfo backTex = BakeTexture(subTexBack, std::get<0>(backTex), L"", doNotUpdateEnv);
      envTex = BakeTexture(subTexEnvir, std::get<0>(envTex), L"", doNotUpdateEnv);

      // Environment

      if (std::get<0>(envTex).id == -1)
      {
        lightNode.force_attribute(L"distribution").set_value(L"uniform");
        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(envColor * tintColor).c_str());
      }
      else
      {
        lightNode.force_attribute(L"distribution").set_value(L"map");

        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(envColor * tintColor).c_str());
        auto texNode = hrTextureBind(std::get<0>(envTex), intensityNode.force_child(L"color"));
        if (std::get<1>(envTex))
        {
          ExtractSampler(texNode, envTex);
          FixEnvironmentTexOffset(texNode);
        }
      }

      // Background

      auto backNode = lightNode.force_child(L"back");
      auto intensityNode = backNode.force_child(L"intensity");

      if (std::get<0>(backTex).id == -1)
      {
        backNode.force_attribute(L"distribution").set_value(L"uniform");
        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(backColor * tintColor).c_str());
      }
      else
      {
        backNode.force_attribute(L"distribution").set_value(L"map");

        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(backColor * tintColor).c_str());
        auto texNode = hrTextureBind(std::get<0>(backTex), intensityNode.force_child(L"color"));
        if (std::get<1>(backTex))
        {
          ExtractSampler(texNode, backTex);
        }
      }
    }
    else
    {
      lightNode.force_attribute(L"distribution").set_value(L"uniform");
      intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(bgColor * tintColor).c_str());
    }

    intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(rendParams.env_mult * tintLevel);
  }
  hrLightClose(m_envRef);

  float matrixT[16] = { 1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1 };

  hrLightInstance(currentScene, m_envRef, matrixT);
}

void hydraRender_mk3::ExtractLightObject(INode* node, TimeValue t, HRLightRef lightRef, bool isNew)
{
  ObjectState os = node->EvalWorldState(t);
  if (!os.obj)
    return;

  LightObject* light = static_cast<LightObject*>(os.obj);
  Texmap* envMap = GetCOREInterface()->GetEnvironmentMap();

  if (light->GetUseLight())
  {
    Interval valid = FOREVER;
    HR_OPEN_MODE mode;
    if (isNew)
      mode = HR_WRITE_DISCARD;
    else
      mode = HR_OPEN_EXISTING;

    hrLightOpen(lightRef, mode);
    {
      auto lightNode = hrLightParamNode(lightRef);

      if (IsPhotometricLight(light)) ExtractPhotometricLightParams(light, node, lightNode, t, valid);
      else if (IsHydraSkyPortal(light)) ExtractHydraSkyPortalParams(light, node, lightNode, t, valid);
      else if (IsMrSkyPortal(light)) ExtractMrSkyPortalParams(light, node, lightNode, t, valid);
      else if (IsMrSun(light)) ExtractSunParams(light, node, lightNode, t, valid);
      else if (IsPhysSky(light)) ExtractPhysSkyParams(light, lightNode, t, valid);
    //  else if (light->IsSubClassOf(MaxSDK::IPhysicalSunSky::GetClassID())) {}
      else ExtractStdLightParams(light, node, lightNode, t, valid);

    }
    hrLightClose(lightRef);
  }
}


bool hydraRender_mk3::IsPhotometricLight(LightObject* a_light)
{
  return a_light->IsSubClassOf(LIGHTSCAPE_LIGHT_CLASS);
}

bool  hydraRender_mk3::IsMrSun(LightObject* a_light)
{
  BaseInterface* bi = a_light->GetInterface(IID_MR_PHYSICAL_SUN_LIGHT);
  IMRPhysicalSunLight* mrSun = dynamic_cast<IMRPhysicalSunLight*>(bi);
  if (mrSun != nullptr)
    return true;
  return false;
}

bool  hydraRender_mk3::IsPhysSky(LightObject* a_light)
{
  BaseInterface* skyInterface = a_light->GetInterface(MRPHYSSKY_LIGHT_INTERFACECLASS_ID);
  IMRPhysicalSkyInterface* physSkyInterface = static_cast<IMRPhysicalSkyInterface*>(skyInterface);
  if (physSkyInterface != nullptr)
    return true;
  return false;
}

bool  hydraRender_mk3::IsNewSunSky(Texmap* a_env)
{
  MaxSDK::IPhysicalSunSky* const physical_sun_sky = dynamic_cast<MaxSDK::IPhysicalSunSky*>(a_env);
  if (physical_sun_sky != nullptr)
    return true;
  return false;
}


bool hydraRender_mk3::IsHydraSkyPortal(LightObject* a_light)
{
  return a_light->IsSubClassOf(Class_ID(0x69729747, 0x8c40aaf7)); // HydraSkyPortal_CLASS_ID
}

bool hydraRender_mk3::IsMrSkyPortal(LightObject* a_light)
{ 
   BaseInterface* bi = a_light->GetInterface(IID_MR_SKY_PORTAL_LIGHT);
   IMrSkyPortalLight* mrSkyPortal = dynamic_cast<IMrSkyPortalLight*>(bi);
   if(mrSkyPortal != nullptr)
     return true;
   return false;
}

