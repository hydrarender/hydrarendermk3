#include "zimage.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace ZImageUtils
{
  void initTables();
};

ImageZ::ImageZ(const char* imageFileName, const char* mutexName, int a_width, int a_height, std::ostream* plog, int layersNum, bool a_attach) : zblocks(nullptr), m_imageFile(NULL), m_mutex(NULL), wasFinalOnce(false), m_pLog(plog)
{
  for (int i = 0; i < AUX_COLORS_MAX; i++)
    colors[i] = nullptr;

  if (m_pLog != nullptr)
    (*m_pLog) << "ImageZ::ImageZ()" << std::endl;

  ZImageUtils::initTables();

  if (a_attach)
    m_mutex = OpenMutexA(MUTEX_ALL_ACCESS, FALSE, mutexName);
  else
    m_mutex = CreateMutexA(NULL, FALSE, mutexName);

  if (m_mutex == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> OpenMutexA/CreateMutexA " << std::endl;
    return;
  }

  if (a_attach)
    m_imageFile = OpenFileMappingA(FILE_MAP_READ | FILE_MAP_WRITE, 0, imageFileName);
  else
  {
    size_t imSize    = (a_width + 16)*(a_height + 16);
    m_bufferByteSize = (uint64_t)sizeof(ImageZInfo) + (uint64_t)imSize*(sizeof(float) * 4 * layersNum) + (uint64_t)imSize*sizeof(ZBlockT2) + (uint64_t)1024;
    DWORD imageSizeL = m_bufferByteSize & 0x00000000FFFFFFFF;
    DWORD imageSizeH = ((uint64_t)(m_bufferByteSize & 0xFFFFFFFF00000000)) >> 32;

    m_imageFile = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, imageSizeH, imageSizeL, imageFileName);
    if (m_imageFile == NULL)
    {
      if (m_pLog != nullptr)
        (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> CreateFileMappingA " << std::endl;
      CloseHandle(m_mutex);
      return;
    }
  }

  char* data = (char*)MapViewOfFile(m_imageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  if (data == nullptr)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::ImageZ() -> MapViewOfFile " << std::endl;
    CloseHandle(m_imageFile);
    CloseHandle(m_mutex);
    return;
  }

  m_bufferData = data;
  m_pInfo = (ImageZInfo*)data;

  if (!a_attach)
  {
    m_pInfo->width  = a_width;
    m_pInfo->height = a_height;
    m_pInfo->layersNum = layersNum;
  }

  InitFromBufferData();

  if (!a_attach)
    Clear();

}

void ImageZ::InitFromBufferData()
{
  int a_width  = 0;
  int a_height = 0;

  char* data = (char*)m_bufferData;
  m_pInfo = (ImageZInfo*)data;  data += sizeof(ImageZInfo);

  a_width  = m_pInfo->width;
  a_height = m_pInfo->height;

  m_zblockCount = (m_pInfo->width*m_pInfo->height) / ZBLOCKSIZE;

  size_t offset1 = (a_width + 16)*(a_height + 16)*sizeof(float) * 4;
  size_t offset3 = m_zblockCount*sizeof(ZBlockT2);

  for (int i = 0; i < AUX_COLORS_MAX; i++)
    colors[i] = nullptr;

  for (int i = 0; i < m_pInfo->layersNum; i++)
  {
    colors[i] = (float*)data;
    data += offset1;
  }

  zblocks = (ZBlockT2*)data;    
  data += offset3;
  
  commands = data;

  size_t imSize = (m_pInfo->width + 16)*(m_pInfo->height + 16);
  m_bufferByteSize = (uint64_t)sizeof(ImageZInfo) + (uint64_t)imSize*(sizeof(float) * 4 * m_pInfo->layersNum) + (uint64_t)imSize*sizeof(ZBlockT2) + (uint64_t)1024;

  memset(lastMsg, 0, 1024);
  lastMsgId  = 0;
  lastMsgRcv = -1;
}


ImageZ::~ImageZ()
{
  UnmapViewOfFile(m_bufferData); m_bufferData = nullptr;
  CloseHandle(m_imageFile);
  CloseHandle(m_mutex);
}

void ImageZ::Clear()
{
  int size = m_pInfo->width*m_pInfo->height;

  for (int i = 0; i < m_pInfo->layersNum; i++)
    memset(colors[i], 0, size*sizeof(float) * 4);

  memset(commands, 0, 1024);

  for (int i = 0; i < m_zblockCount; i++)
    zblocks[i] = ZBlockT2();

  m_pInfo->flags = ZIMAGE_IS_EMPTY;

  wasFinalOnce = false;
}

bool ImageZ::IsEmpty() const
{ 
  return (m_pInfo->flags & ZIMAGE_IS_EMPTY); 
}

namespace ZImageUtils
{
  struct float2
  {
    float2() : x(0.0f), y(0.0f) {}
    float2(float a_x, float a_y) : x(a_x), y(a_y) {}

    float x, y;
  };

  struct float3
  {
    float3() : x(0.0f), y(0.0f), z(0.0f) {}
    float3(float a_x, float a_y, float a_z) : x(a_x), y(a_y), z(a_z) {}

    float x, y, z;
  };

  struct float4
  {
    float x, y, z, w;
  };

  static inline float MonteCarloStdErr(float3 avgColor, float sqrColor, int nSamples)
  {
    float fnSamples  = ((float)(nSamples + 1));
    float nSampleInv = 1.0f / fnSamples;

    float maxColor   = fmaxf(avgColor.x, fmaxf(avgColor.y, avgColor.z));

    float variance   = sqrt(fmax(sqrColor - (maxColor*maxColor), 0.0f));
    float stdError   = variance / sqrt(fnSamples);

    return stdError / (fmaxf(maxColor, 0.00001f));
  }


  static uint16_t MortonTable256Host[] =
  {
    0x0000, 0x0001, 0x0004, 0x0005, 0x0010, 0x0011, 0x0014, 0x0015,
    0x0040, 0x0041, 0x0044, 0x0045, 0x0050, 0x0051, 0x0054, 0x0055,
    0x0100, 0x0101, 0x0104, 0x0105, 0x0110, 0x0111, 0x0114, 0x0115,
    0x0140, 0x0141, 0x0144, 0x0145, 0x0150, 0x0151, 0x0154, 0x0155,
    0x0400, 0x0401, 0x0404, 0x0405, 0x0410, 0x0411, 0x0414, 0x0415,
    0x0440, 0x0441, 0x0444, 0x0445, 0x0450, 0x0451, 0x0454, 0x0455,
    0x0500, 0x0501, 0x0504, 0x0505, 0x0510, 0x0511, 0x0514, 0x0515,
    0x0540, 0x0541, 0x0544, 0x0545, 0x0550, 0x0551, 0x0554, 0x0555,
    0x1000, 0x1001, 0x1004, 0x1005, 0x1010, 0x1011, 0x1014, 0x1015,
    0x1040, 0x1041, 0x1044, 0x1045, 0x1050, 0x1051, 0x1054, 0x1055,
    0x1100, 0x1101, 0x1104, 0x1105, 0x1110, 0x1111, 0x1114, 0x1115,
    0x1140, 0x1141, 0x1144, 0x1145, 0x1150, 0x1151, 0x1154, 0x1155,
    0x1400, 0x1401, 0x1404, 0x1405, 0x1410, 0x1411, 0x1414, 0x1415,
    0x1440, 0x1441, 0x1444, 0x1445, 0x1450, 0x1451, 0x1454, 0x1455,
    0x1500, 0x1501, 0x1504, 0x1505, 0x1510, 0x1511, 0x1514, 0x1515,
    0x1540, 0x1541, 0x1544, 0x1545, 0x1550, 0x1551, 0x1554, 0x1555,
    0x4000, 0x4001, 0x4004, 0x4005, 0x4010, 0x4011, 0x4014, 0x4015,
    0x4040, 0x4041, 0x4044, 0x4045, 0x4050, 0x4051, 0x4054, 0x4055,
    0x4100, 0x4101, 0x4104, 0x4105, 0x4110, 0x4111, 0x4114, 0x4115,
    0x4140, 0x4141, 0x4144, 0x4145, 0x4150, 0x4151, 0x4154, 0x4155,
    0x4400, 0x4401, 0x4404, 0x4405, 0x4410, 0x4411, 0x4414, 0x4415,
    0x4440, 0x4441, 0x4444, 0x4445, 0x4450, 0x4451, 0x4454, 0x4455,
    0x4500, 0x4501, 0x4504, 0x4505, 0x4510, 0x4511, 0x4514, 0x4515,
    0x4540, 0x4541, 0x4544, 0x4545, 0x4550, 0x4551, 0x4554, 0x4555,
    0x5000, 0x5001, 0x5004, 0x5005, 0x5010, 0x5011, 0x5014, 0x5015,
    0x5040, 0x5041, 0x5044, 0x5045, 0x5050, 0x5051, 0x5054, 0x5055,
    0x5100, 0x5101, 0x5104, 0x5105, 0x5110, 0x5111, 0x5114, 0x5115,
    0x5140, 0x5141, 0x5144, 0x5145, 0x5150, 0x5151, 0x5154, 0x5155,
    0x5400, 0x5401, 0x5404, 0x5405, 0x5410, 0x5411, 0x5414, 0x5415,
    0x5440, 0x5441, 0x5444, 0x5445, 0x5450, 0x5451, 0x5454, 0x5455,
    0x5500, 0x5501, 0x5504, 0x5505, 0x5510, 0x5511, 0x5514, 0x5515,
    0x5540, 0x5541, 0x5544, 0x5545, 0x5550, 0x5551, 0x5554, 0x5555
  };

  static inline uint32_t ZIndexHost(uint16_t x, uint16_t y)
  {
    return	MortonTable256Host[y >> 8]   << 17 |
            MortonTable256Host[x >> 8]   << 16 |
            MortonTable256Host[y & 0xFF] << 1  |
            MortonTable256Host[x & 0xFF];
  }

  uint32_t g_ztable2D16[16][16];

  void initTables()
  {
    for (int y = 0; y < 16; y++)
      for (int x = 0; x < 16; x++)
        g_ztable2D16[x][y] = ZIndexHost(x, y);
  }

  /*const int Z_ORDER_BLOCK_SIZE = 16;

  uint32_t IndexZB(int x, int y, int pitch)
  {
    uint32_t zOrderX = x % Z_ORDER_BLOCK_SIZE;
    uint32_t zOrderY = y % Z_ORDER_BLOCK_SIZE;

    uint32_t zIndex = ZIndexHost(zOrderX, zOrderY);

    uint32_t wBlocks = pitch / Z_ORDER_BLOCK_SIZE;
    uint32_t blockX  = x / Z_ORDER_BLOCK_SIZE;
    uint32_t blockY  = y / Z_ORDER_BLOCK_SIZE;

    return (blockX + (blockY)*(wBlocks))*(Z_ORDER_BLOCK_SIZE*Z_ORDER_BLOCK_SIZE) + zIndex;
  }*/


  uint32_t IndexZB(int x, int y, int pitch) 
  { 
    return (((x >> 4) + (y >> 4)*(pitch >> 4)) << 8) + g_ztable2D16[(x & 0x0000000F)][(y & 0x0000000F)]; 
  }

  uint32_t Index2D(uint32_t x, uint32_t y, int pitch) { return y*pitch + x; }


  void ImageZBlockMemToRowPitch(const float4* inData, float4* outData, int w, int h)
  {
    for (int y = 0; y<h; y++)
    {
      for (int x = 0; x<w; x++)
      {
        auto indexSrc = IndexZB(x, y, w);
        auto indexDst = Index2D(x, y, w);
        outData[indexDst] = inData[indexSrc];
      }
    }

  }


  float BlockError(int offset, float* a_color, int nSamples, float a_pathTraceError)
  {
    float error  = 0.0f;

    size_t start = offset;
    size_t end   = offset + ImageZ::ZBLOCKSIZE;

    const float ptErrorInv = 1.0f / fmaxf(a_pathTraceError, 1e-10f);

    for (size_t x = start; x < end; x++)
    {
      float3 color;

      color.x = a_color[x * 4 + 0];
      color.y = a_color[x * 4 + 1];
      color.z = a_color[x * 4 + 2];

      float colorSquare = a_color[x * 4 + 3];
      float pixelError = MonteCarloStdErr(color, colorSquare, nSamples);

      if (pixelError >= a_pathTraceError)
      {
        float fuckedUp = (pixelError*ptErrorInv);  // how many times we are fucked up with error ?
        error += fuckedUp*fuckedUp;
      }
    }

    return error;
  }

  static bool BlockFinished(ZBlockT2 block, int a_minRaysPerPixel, int a_maxRaysPerPixel) // for use on the cpu side ... for current
  { 
    if (block.index2 == 0xFFFFFFFF)
      return true;

    int samplesPerPixel     = block.counter;
    float acceptedBadPixels = 8.0f; // sqrtf((float)(256));
    bool summErrorOk        = (block.diff <= acceptedBadPixels);

    return ((summErrorOk && samplesPerPixel >= a_minRaysPerPixel) || (samplesPerPixel >= a_maxRaysPerPixel)) || (block.index2 == 0xFFFFFFFF);
  }

};


bool haveSameIds(ImageZ* pA, const ImageZ* pB)
{
  const char* msg1 = pA->message();
  const char* msg2 = pB->message();

  std::string sid1 = "0";
  std::string sid2 = "0";

  std::string mid1 = "0";
  std::string mid2 = "0";

  std::string layer1 = "color";
  std::string layer2 = "color";

  std::stringstream inCmd1(msg1);
  std::stringstream inCmd2(msg2);

  char name[64];
  char value[64];

  while (inCmd1.good())
  {
    inCmd1 >> name >> value;

    if (std::string(name) == "-layer")
      layer1 = value;

    if (std::string(name) == "-sid")
      sid1 = value;

    if (std::string(name) == "-mid")
      mid1 = value;
  }

  int finalImageB = 0;

  while (inCmd2.good())
  {
    inCmd2 >> name >> value;

    if (std::string(name) == "-layer")
      layer2 = value;

    if (std::string(name) == "-sid")
      sid2 = value;

    if (std::string(name) == "-mid")
      mid2 = value;

    if (std::string(name) == "-final")
      finalImageB = atoi(value);
  }

  pA->wasFinalOnce = pA->wasFinalOnce || (finalImageB == 1);

  return (sid1 == sid2) && (layer1 == layer2) && (mid1 == mid2);
}

void ImageZ::UnionWith(const ImageZ* a_pOtherImage)
{
  if (!haveSameIds(this, a_pOtherImage)) // if server responce is valid
  {
    const char* msg1 = this->message();
    const char* msg2 = a_pOtherImage->message();

    (*m_pLog) << "[ABImages]: Render responce is REJECTED! " << std::endl;
    (*m_pLog) << "  msg(A) = " << msg1 << std::endl;
    (*m_pLog) << "  msg(B) = " << msg2 << std::endl;

    return;
  }

  // add spectial case for emmpy ImageZ to reply general purpose messages
  //

  // union image. Main color or primary for MLT.
  //
  for (size_t z = 0; z < m_zblockCount; z++)
  {
    auto block2  = a_pOtherImage->zblocks[z];  int z2 = block2.index; // usually z2 == z, but anything is possible in future
    auto block1  = zblocks[z2];

    size_t start = block2.index*ZBLOCKSIZE;
    size_t end   = block2.index*ZBLOCKSIZE + ZBLOCKSIZE;

    float countersAreValid = ((block1.counter >= 0) && (block2.counter > 0));

    if (block1.index2 != 0xFFFFFFFF && countersAreValid) // && block2.index2 != 0xFFFFFFFF
    {
      float w1 = float(block1.counter) / float(block1.counter + block2.counter);
      float w2 = float(block2.counter) / float(block1.counter + block2.counter);

      float* color = this->ColorPtr();
      const float* otherColor = a_pOtherImage->ColorPtr();

      for (size_t x = start; x < end; x++) // can opt with sse, but need to be sure poiners are aligned !!!
      {
        float otherColorX = otherColor[x * 4 + 0];
        float otherColorY = otherColor[x * 4 + 1];
        float otherColorZ = otherColor[x * 4 + 2];
        float otherColorW = otherColor[x * 4 + 3];

        float oldColorX = color[x * 4 + 0];
        float oldColorY = color[x * 4 + 1];
        float oldColorZ = color[x * 4 + 2];
        float oldColorW = color[x * 4 + 3];

        color[x * 4 + 0] = oldColorX * w1 + otherColorX * w2;
        color[x * 4 + 1] = oldColorY * w1 + otherColorY * w2;
        color[x * 4 + 2] = oldColorZ * w1 + otherColorZ * w2;
        color[x * 4 + 3] = oldColorW * w1 + otherColorW * w2;
      }

      zblocks[z2].index   = z2;
      zblocks[z2].counter = block1.counter + block2.counter;
      zblocks[z2].index2  = block1.index2; // | block2.index2;    // resulting block finished if one of them is finished --> old way, does not applied now
    }
  }

  // union secondary for MLT
  //
  if (this->ColorPtr(1) != nullptr)
  {
    for (size_t z = 0; z < m_zblockCount; z++)
    {
      auto block2 = a_pOtherImage->zblocks[z];  int z2 = block2.index; // usually z2 == z, but anything is possible in future
      auto block1 = zblocks[z2];

      size_t start = block2.index*ZBLOCKSIZE;
      size_t end   = block2.index*ZBLOCKSIZE + ZBLOCKSIZE;
      
      float* color = this->ColorPtr(1);
      const float* otherColor = a_pOtherImage->ColorPtr(1);
      
      for (size_t x = start; x < end; x++) // can opt with sse, but need to be sure poiners are aligned !!!
      {
        float otherColorX = otherColor[x * 4 + 0];
        float otherColorY = otherColor[x * 4 + 1];
        float otherColorZ = otherColor[x * 4 + 2];
        float otherColorW = otherColor[x * 4 + 3];
      
        float oldColorX = color[x * 4 + 0];
        float oldColorY = color[x * 4 + 1];
        float oldColorZ = color[x * 4 + 2];
        float oldColorW = color[x * 4 + 3];
      
        color[x * 4 + 0] = oldColorX + otherColorX;
        color[x * 4 + 1] = oldColorY + otherColorY;
        color[x * 4 + 2] = oldColorZ + otherColorZ;
        color[x * 4 + 3] = oldColorW + otherColorW;
      }
      
    }
  }

}

float ImageZ::EstimateProgress(float relError, int minRaysPerPixel, int maxRaysPerPixel)
{
  int BLOCK_NUMBER = (m_pInfo->width*m_pInfo->height) / ZBLOCKSIZE;
  float totalSppmf = float(BLOCK_NUMBER)*float(maxRaysPerPixel);
  int totalSPPDone = 0;

  for (size_t z = 0; z < m_zblockCount; z++)
  {
    auto& block = zblocks[z];
    block.diff  = ZImageUtils::BlockError(block.index*ZBLOCKSIZE, ColorPtr(), block.counter, relError);

    if (ZImageUtils::BlockFinished(block, minRaysPerPixel, maxRaysPerPixel))
    {
      totalSPPDone += maxRaysPerPixel;
      block.index2 = 0xFFFFFFFF; // mark as finished
    }
    else
      totalSPPDone += block.counter;
  }

  return (float(totalSPPDone) / totalSppmf);
}

bool ImageZ::Lock(int a_miliseconds)
{
  const DWORD res = WaitForSingleObject(m_mutex, a_miliseconds);
  
  if (res == WAIT_FAILED)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "[syscall failed]: ImageZ::Lock() -> WaitForSingleObject() " << std::endl;
  }

  if (res == WAIT_TIMEOUT || res == WAIT_FAILED)
    return false;
  else
    return true;
}

void ImageZ::Unlock()
{
  ReleaseMutex(m_mutex);
}

void ImageZ::SaveToFile(const wchar_t* fname)
{
  std::ofstream fout(fname, std::ios::binary);

  if (!fout.is_open())
    return;

  fout.write((const char*)&m_bufferByteSize, sizeof(uint64_t));
  fout.write((const char*)m_bufferData,      m_bufferByteSize);
  fout.flush();
  fout.close();
}


bool ImageZ::LoadFromFile(const wchar_t* fname)
{
  std::ifstream fin(fname, std::ios::binary);

  if (!fin.is_open())
    return false;

  uint64_t buffSize = 0;
  fin.read((char*)&buffSize, sizeof(uint64_t));

  if (buffSize != m_bufferByteSize)
  {
    std::cerr << "ImageZ::LoadFromFile: failed to load due to different size of image in file and buffer" << std::endl;
    return false;
  }

  fin.read((char*)m_bufferData, m_bufferByteSize);
  fin.close();

  InitFromBufferData();

  return true;
}

struct Pixel
{
  unsigned char r, g, b;
};

void WriteBMP(const wchar_t* fname, Pixel* a_pixelData, int width, int height)
{
  BITMAPFILEHEADER bmfh;
  BITMAPINFOHEADER info;

  memset(&bmfh, 0, sizeof(BITMAPFILEHEADER));
  memset(&info, 0, sizeof(BITMAPINFOHEADER));

  int paddedsize = (width*height)*sizeof(Pixel);

  bmfh.bfType      = 0x4d42;       // 0x4d42 = 'BM'
  bmfh.bfReserved1 = 0;
  bmfh.bfReserved2 = 0;
  bmfh.bfSize      = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + paddedsize;
  bmfh.bfOffBits   = 0x36;

  info.biSize          = sizeof(BITMAPINFOHEADER);
  info.biWidth         = width;
  info.biHeight        = height;
  info.biPlanes        = 1;
  info.biBitCount      = 24;
  info.biCompression   = BI_RGB;
  info.biSizeImage     = 0;
  info.biXPelsPerMeter = 0x0ec4;
  info.biYPelsPerMeter = 0x0ec4;
  info.biClrUsed       = 0;
  info.biClrImportant  = 0;

  std::ofstream out(fname, std::ios::out | std::ios::binary);
  out.write((const char*)&bmfh, sizeof(BITMAPFILEHEADER));
  out.write((const char*)&info, sizeof(BITMAPINFOHEADER));
  out.write((const char*)a_pixelData, paddedsize);
  out.flush();
  out.close();
}


void ImageZ::SaveToBMP(const wchar_t* fname, float a_gamma)
{
  int width  = m_pInfo->width;
  int height = m_pInfo->height;

  float gammaInv = 1.0f / a_gamma;

  ZImageUtils::float4* inColor = (ZImageUtils::float4*)ColorPtr();
  Pixel* pxdata = new Pixel[width*height];

  for (int y = 0; y<height; y++)
  {
    for (int x = 0; x<width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, width);
      int indexDst = ZImageUtils::Index2D(x, y, width);
      
      ZImageUtils::float4 colorf = inColor[indexSrc];

      colorf.x = fmin(powf(colorf.x, gammaInv), 1.0f);
      colorf.y = fmin(powf(colorf.y, gammaInv), 1.0f);
      colorf.z = fmin(powf(colorf.z, gammaInv), 1.0f);

      Pixel colorub;
      colorub.r = (unsigned char)(255.0f*colorf.z);
      colorub.g = (unsigned char)(255.0f*colorf.y);
      colorub.b = (unsigned char)(255.0f*colorf.x);

      pxdata[indexDst] = colorub;
    }
  }

  WriteBMP(fname, pxdata, width, height);

  delete[] pxdata;
}

void ImageZ::SaveToImage4f(const wchar_t* a_path)
{
  int width  = m_pInfo->width;
  int height = m_pInfo->height;

  ZImageUtils::float4* inColor = (ZImageUtils::float4*)ColorPtr();
  std::vector<ZImageUtils::float4> pxdata(width*height);

  for (int y = 0; y<height; y++)
  {
    for (int x = 0; x<width; x++)
    {
      int indexSrc = ZImageUtils::IndexZB(x, y, width);
      int indexDst = ZImageUtils::Index2D(x, y, width);

      pxdata[indexDst] = inColor[indexSrc];
    }
  }

  int wh[2] = { width, height };

  std::ofstream fout(a_path, std::ios::binary);
  fout.write((const char*)wh, sizeof(int)*2);
  fout.write((const char*)&pxdata[0], width*height*sizeof(ZImageUtils::float4));
  fout.close();
}


bool ImageZ::SendMsg(const char* a_msg, bool inc_mid)
{
  if (this->Lock(250))
  {
    char temp[64];
    memset(temp, 0, 64);
    memset(message(), 0, 1024);
    
    if (inc_mid)
    {
      sprintf(temp, " -mid %d", lastMsgId);
      strncpy(message(), a_msg, 1024);
      strncat(message(), temp, 1024);
      lastMsgId++;
    }
    else
      strcpy(message(), a_msg);

    this->Unlock();
    return true;
  }
  else
    return false;
}

const char* ImageZ::RecieveMsg()
{
  char name[64];
  char value[64];

  if (this->Lock(0))
  {
    int thisMessageId = -1;

    std::stringstream msgInput(message());

    while (msgInput.good())
    {
      msgInput >> name >> value;
      if (std::string(name) == "-mid")
      {
        thisMessageId = atoi(value);
        break;
      }
    }

    if (lastMsgRcv < thisMessageId)
    {
      memcpy(lastMsg, message(), 1024);
      lastMsgRcv = thisMessageId;
      this->Unlock();
      return lastMsg;
    }
    else
    {
      this->Unlock();
      return nullptr;
    }

  }
  else
    return nullptr;
}




