#include "hydraRender mk3.h"

#include <MeshNormalSpec.h>

#include <unordered_map>
#include <map>

float weightOfEdges(Point3 edgeA, Point3 edgeB)
{
  const float len = ::Length(edgeA^edgeB);

  edgeA = ::Normalize(edgeA);
  edgeB = ::Normalize(edgeB);

  float dp = DotProd(edgeA,edgeB);
  
  if (dp > 1.0f) 
    dp=1.0f; // shouldn't happen, but might
  if (dp < -1.0f) 
    dp=-1.0f; // shouldn't happen, but might

  return len*fabsf(acosf(dp));
}


void hydraRender_mk3::CalcMaxNormalsAsInExample2(Mesh* mesh, GeometryObj* geom, Matrix3 tm)
{
  // geom->vnorms2
  geom->vnorms2.resize(mesh->getNumVerts());
  geom->faceV.resize(mesh->getNumFaces()*3);

  Point3* verts = mesh->verts;

  for (size_t i = 0; i < geom->vnorms2.size(); i++)
  {
    geom->vnorms2[i] = MaxSDK::VertexNormal(Point3(0, 0, 0), 0);
    geom->vnorms2[i].init = false;
  }

  for (int i = 0; i < mesh->getNumFaces(); i++)
  {
    Face* face = mesh->faces + i;

    Point3 v0, v1, v2;
    v0 = tm*verts[face->v[0]];
    v1 = tm*verts[face->v[1]];
    v2 = tm*verts[face->v[2]];

    geom->faceV[i * 3 + 0] = face->v[0];
    geom->faceV[i * 3 + 1] = face->v[1];
    geom->faceV[i * 3 + 2] = face->v[2];

    Point3 edgeA1, edgeB1;
    edgeA1   = v1 - v0;
    edgeB1   = v2 - v0;

    Point3 edgeA2, edgeB2;
    edgeA2 = v0 - v1;
    edgeB2 = v2 - v1;

    Point3 edgeA3, edgeB3;
    edgeA3 = v0 - v2;
    edgeB3 = v1 - v2;

    Point3 fNormal;
    fNormal = ::Normalize(edgeA1^edgeB1);

    const float w1 = fmax(weightOfEdges(edgeA1, edgeB1), 1e-5f);
    const float w2 = fmax(weightOfEdges(edgeA2, edgeB2), 1e-5f);
    const float w3 = fmax(weightOfEdges(edgeA3, edgeB3), 1e-5f);

    geom->vnorms2[face->v[0]].AddNormal(w1*fNormal, face->smGroup);
    geom->vnorms2[face->v[1]].AddNormal(w2*fNormal, face->smGroup);
    geom->vnorms2[face->v[2]].AddNormal(w3*fNormal, face->smGroup);
  }


}

void hydraRender_mk3::ExtractUsedDefinedNormals(Mesh* mesh, GeometryObj* geom, Matrix3 tm)
{
  if(mesh == NULL)
    return;

  int vx = 0;
  int vy = 1;
  int vz = 2;

  geom->normsSpecified.resize(0);
  geom->normsSpecIndices.resize(0);

  MeshNormalSpec* normalSpec = mesh->GetSpecifiedNormals();

  if(normalSpec == NULL)
    return;

  if(normalSpec->GetNumNormals() == 0)
    return;

  /////////////////////////////////////////////////// --> original OpenCollada code
  /*if(normalSpec == NULL)
  {
    mesh->SpecifyNormals();
    normalSpec = mesh->GetSpecifiedNormals();
  }

  if( normalSpec->GetNumNormals() == 0 )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}*/
  ////////////////////////////////////////////////////////////////////////////////////////////////

  bool isSetMESH_NORMAL_NORMALS_BUILT    = normalSpec->GetFlag(MESH_NORMAL_NORMALS_BUILT);
	bool isSetMESH_NORMAL_NORMALS_COMPUTED = normalSpec->GetFlag(MESH_NORMAL_NORMALS_COMPUTED);
	bool isSetMESH_NORMAL_MODIFIER_SUPPORT = normalSpec->GetFlag(MESH_NORMAL_MODIFIER_SUPPORT);

  if( !isSetMESH_NORMAL_NORMALS_BUILT || !isSetMESH_NORMAL_NORMALS_COMPUTED )
    return;

  /////////////////////////////////////////////////// --> original OpenCollada code
  /*
	if( !isSetMESH_NORMAL_NORMALS_BUILT || !isSetMESH_NORMAL_NORMALS_COMPUTED )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}

	if( normalSpec->GetNumNormals() == 0 )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}*/

	int normalCount = normalSpec->GetNumNormals();

  if(normalCount == 0)
	 	return;

  geom->normsSpecified.resize(normalCount*3);
  
  Matrix3 tmRot = tm;

  tmRot.SetTranslate(Point3(0,0,0));

  Point3 normal;
  for( int i = 0; i < normalCount; ++i )
  {
  	normal = tmRot*normalSpec->Normal(i);
    normal = ::Normalize(normal);

    geom->normsSpecified[i*3+0] = normal.x;
    geom->normsSpecified[i*3+1] = normal.y;
    geom->normsSpecified[i*3+2] = normal.z;
  }


  geom->normsSpecIndices.resize(mesh->getNumFaces()*3);

  for(int faceIndex=0; faceIndex < mesh->getNumFaces(); faceIndex++)
  {
    //for(int vertexIndex = 0; vertexIndex < 3; vertexIndex++ )
    //{
    //  int normalIndex = normalSpec->GetNormalIndex(faceIndex, vertexIndex);
    //  geom->normsSpecIndices[faceIndex*3 + vertexIndex] = normalIndex;
    //}

    int normalIndex1 = normalSpec->GetNormalIndex(faceIndex, 0);
    int normalIndex2 = normalSpec->GetNormalIndex(faceIndex, 1);
    int normalIndex3 = normalSpec->GetNormalIndex(faceIndex, 2);

    geom->normsSpecIndices[faceIndex*3 + 0] = normalIndex1;
    geom->normsSpecIndices[faceIndex*3 + 1] = normalIndex2;
    geom->normsSpecIndices[faceIndex*3 + 2] = normalIndex3;
  }

}


