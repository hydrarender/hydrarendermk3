#include "hydraRender mk3.h"

#include <unordered_map>
#include <thread>
#include <codecvt>
#include <mutex>
#include "gamma.h"

struct translateTexmap
{
  Texmap* texmap;
};

std::mutex mtx;

bool g_inMatEditor = false;


void hermiteSplinePart(Point2 start, Point2 end, Point2 tangent1, Point2 tangent2, std::vector<Point2> &f)
{
  int steps = 10;
  for (int t = 0; t < steps; t++)
  {
    float s = (float)t / (float)steps;    // scale s to go from 0 to 1
    float h1 = 2 * pow(s, 3) - 3 * pow(s, 2) + 1;          // calculate basis function 1
    float h2 = -2 * pow(s, 3) + 3 * pow(s, 2);              // calculate basis function 2
    float h3 = pow(s, 3) - 2 * pow(s, 2) + s;         // calculate basis function 3
    float h4 = pow(s, 3) - pow(s, 2);              // calculate basis function 4

    Point2 res;
    res.y = h1*start.y + h2*end.y + h3*tangent1.y + h4*tangent2.y;
    res.x = h1*start.x + h2*end.x + h3*tangent1.x + h4*tangent2.x;
    f.push_back(res);
    /*
    vector p = h1*P1 +                    // multiply and sum all funtions
    h2*P2 +                    // together to build the interpolated
    h3*T1 +                    // point along the curve.
    h4*T2;
    */
  }
}


bool OutputNodeDoesNothing(BitmapTex* pBitMap, TimeValue t)
{
  if (pBitMap == nullptr)
    return true;

  TextureOutput* texout = pBitMap->GetTexout();

  if (texout == nullptr)
    return true;

  float outpAmt = texout->GetOutputLevel(t);

  StdTexoutGen* ptexOutGen = dynamic_cast<StdTexoutGen*>(texout);
  if (ptexOutGen != nullptr)
    outpAmt *= ptexOutGen->GetOutAmt(t);

  AColor colorFromOutput;
  float  monoFromOutput;
  bool colorChanged = false;

  for (float val = 0.0f; val <= 1.0; val += 0.1f)
  {
    colorFromOutput = texout->Filter(AColor(val, val, val));
    monoFromOutput  = texout->Filter(val);

    if((fabs(colorFromOutput.r - val) > 1e-5f) || 
       (fabs(colorFromOutput.g - val) > 1e-5f) || 
       (fabs(colorFromOutput.b - val) > 1e-5f) || 
       (fabs(monoFromOutput    - val) > 1e-5f))
    {
      colorChanged = true;
      break;
    }
  }

  return (fabs(outpAmt - 1.0f) < 1e-5f) && !texout->GetInvert() && !colorChanged; 
}

std::wstring AlteredTexPath(Texmap* tex, bool needHDR, std::wstring mtl_name)
{
  if (tex == nullptr)
    return std::wstring(L"");

  MSTR mapName1 = tex->GetName();
	std::wstring mapName_w = strConverter::c2ws(mapName1.data());
	if (needHDR)
		return L"C:\\[Hydra]\\temp\\" + mapName_w + L"-" + mtl_name + L".hdr";

  // if (tex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
  // {
  //   BitmapTex* pBitMap = (BitmapTex*)tex;
  //   Bitmap* bmap_src   = pBitMap->GetBitmap(0);
  //   if (bmap_src != nullptr && bmap_src->HasAlpha())
  //     return std::wstring(L"C:\\[Hydra]\\temp\\") + mapName_w + L".png";
  // }

  return std::wstring(L"C:\\[Hydra]\\temp\\") + mapName_w + L"-" + mtl_name + L".bmp";
}

Bitmap* CreateBitMapCopyWithAppliedOutputNode(BitmapTex* pBitMap, const hydraStr& mapPath, bool needHDR, TimeValue t)
{
  if (pBitMap == nullptr)
    return nullptr;

  Bitmap* bmap_src = pBitMap->GetBitmap(t);
  if (bmap_src == nullptr)
    return nullptr;

  TextureOutput* texout = pBitMap->GetTexout();
  if (texout == nullptr)
    return nullptr;

  int src_w = bmap_src->Width();
  int src_h = bmap_src->Height();

  std::lock_guard<std::mutex> lck(mtx);
  BitmapInfo bi_out;

	if (needHDR)
		bi_out.SetType(BMM_REALPIX_32);
	else
		bi_out.SetType(BMM_TRUE_64);

  bi_out.SetWidth(src_w);
  bi_out.SetHeight(src_h);
  bi_out.SetPath(mapPath.c_str());

  bi_out.SetCustomGamma(bmap_src->Gamma());
  bi_out.SetGamma(bmap_src->Gamma());

  Bitmap* bmap_out = nullptr;
  bmap_out = TheManager->Create(&bi_out);
  bmap_out->Fill(0, 0, 0, 0);

	//if (needHDR)
  if(true)
  {
		BMM_Color_fl *temp          = new BMM_Color_fl[src_w];
		BMM_Color_fl *temp_filtered = new BMM_Color_fl[src_w];

		for (int i = 0; i < src_h; i++)
		{
			bmap_src->GetPixels(0, i, src_w, temp);

			for (int j = 0; j < src_w; j++)
				temp_filtered[j] = texout->Filter(temp[j]);

			bmap_out->PutPixels(0, i, src_w, temp_filtered);
		}

		delete[] temp; temp = nullptr;
		delete[] temp_filtered; temp_filtered = nullptr;
	}
	else
	{
		BMM_Color_64 *temp = new BMM_Color_64[src_w];
		BMM_Color_64 *temp_filtered = new BMM_Color_64[src_w];

		for (int i = 0; i < src_h; i++)
		{
			bmap_src->GetPixels(0, i, src_w, temp);
			for (int j = 0; j < src_w; j++)
				temp_filtered[j] = texout->Filter(temp[j]);

			bmap_out->PutPixels(0, i, src_w, temp_filtered);
		}

		delete[] temp; temp = nullptr;
		delete[] temp_filtered; temp_filtered = nullptr;
	}

  bmap_out->OpenOutput(&bi_out);
  bmap_out->Write(&bi_out);
  bmap_out->Close(&bi_out);

  return bmap_out;
}

Bitmap* CreateBitMapCopyWithAlphaAsGray(BitmapTex* pBitMap, const hydraStr& mapPath, bool needHDR, TimeValue t)
{
  if (pBitMap == nullptr)
    return nullptr;

  Bitmap* bmap_src = pBitMap->GetBitmap(t);
  if (bmap_src == nullptr)
    return nullptr;

  TextureOutput* texout = pBitMap->GetTexout();
  if (texout == nullptr)
    return nullptr;

  int src_w = bmap_src->Width();
  int src_h = bmap_src->Height();

  std::lock_guard<std::mutex> lck(mtx);
  BitmapInfo bi_out;

  if (needHDR)
    bi_out.SetType(BMM_REALPIX_32);
  else
    bi_out.SetType(BMM_TRUE_64);

  bi_out.SetWidth(src_w);
  bi_out.SetHeight(src_h);
  bi_out.SetPath(mapPath.c_str());

  bi_out.SetCustomGamma(bmap_src->Gamma());
  bi_out.SetGamma(bmap_src->Gamma());

  Bitmap* bmap_out = nullptr;
  bmap_out = TheManager->Create(&bi_out);
  bmap_out->Fill(0, 0, 0, 0);

  //if (needHDR)
  if (true)
  {
    BMM_Color_fl *temp = new BMM_Color_fl[src_w];
    BMM_Color_fl *temp_filtered = new BMM_Color_fl[src_w]; 

    for (int i = 0; i < src_h; i++)
    {
      bmap_src->GetPixels(0, i, src_w, temp);

      for (int j = 0; j < src_w; j++)
      {
        temp[j].r = temp[j].a;
        temp[j].g = temp[j].a;
        temp[j].b = temp[j].a;
        temp_filtered[j] = texout->Filter(temp[j]); // apply output rollup
      }

      bmap_out->PutPixels(0, i, src_w, temp_filtered);
    }

    delete[] temp; temp = nullptr;
    delete[] temp_filtered; temp_filtered = nullptr;
  }

  bmap_out->OpenOutput(&bi_out);
  bmap_out->Write(&bi_out);
  bmap_out->Close(&bi_out);

  return bmap_out;
}

void hydraRender_mk3::CropToFile(BitmapTex* pBitMap, Bitmap *bmap, const hydraStr& mapPath, TimeValue t)
{
  if (bmap == nullptr || pBitMap == nullptr)
    return;

  bool applyTexCrop = FindBool(L"Apply", pBitMap);
  bool place = FindInt(L"CropPlace", pBitMap);

  float clipu = FindFloat(L"Clip U Offset", pBitMap);
  float clipv = FindFloat(L"Clip V Offset", pBitMap);
  float clipw = FindFloat(L"Clip U Width", pBitMap);
  float cliph = FindFloat(L"Clip V Width", pBitMap);

  Bitmap *bmap2;
  BitmapInfo bi;

  int w = bmap->Width();
  int h = bmap->Height();

  int x0, y0, nw, nh;
  nw = int(float(w)*clipw);
  nh = int(float(h)*cliph);
  x0 = int(float(w - 1)*clipu);
  y0 = int(float(h - 1)*clipv);

  if (place)
  {
    {
      std::lock_guard<std::mutex> lck(mtx);
      bi.SetType(BMM_TRUE_64);
      bi.SetWidth(w);
      bi.SetHeight(h);
      bi.SetPath(mapPath.c_str());
      bmap2 = TheManager->Create(&bi);

      if (bmap2 != nullptr)
      {
        bmap2->Fill(0, 0, 0, 0);

        if (nw < 1) nw = 1;
        if (nh < 1) nh = 1;
        PixelBuf row(nw);

        Bitmap* tmpBM = nullptr;
        BitmapInfo bi2;
        bi2.SetName(TEXT("temp"));
        bi2.SetType(BMM_TRUE_64);
        bi2.SetWidth(nw);
        bi2.SetHeight(nh);
        tmpBM = TheManager->Create(&bi2);
        if (tmpBM != nullptr)
        {
          tmpBM->CopyImage(bmap, COPY_IMAGE_RESIZE_HI_QUALITY, 0);
          BMM_Color_64*  p1 = row.Ptr();
          for (int y = 0; y < nh; y++)
          {
            tmpBM->GetLinearPixels(0, y, nw, p1);
            for (int ix = 0; ix < nw; ix++)
              p1[ix].a = 0xffff;

            bmap2->PutPixels(x0, y + y0, nw, p1);
          }
          tmpBM->DeleteThis();
        }

      }
    }

  }
  else
  {
    if (nw < 1) nw = 1;
    if (nh < 1) nh = 1;
    {
      std::lock_guard<std::mutex> lck(mtx);
      bi.SetType(BMM_TRUE_64);
      bi.SetWidth(nw);
      bi.SetHeight(nh);
      bi.SetPath(mapPath.c_str());
      bmap2 = TheManager->Create(&bi);

      PixelBuf row(nw);
      if (bmap2 != nullptr)
      {
        BMM_Color_64*  p1 = row.Ptr();
        for (int y = 0; y < nh; y++) {
          bmap->GetLinearPixels(x0, y + y0, nw, p1);
          for (int ix = 0; ix < nw; ix++)
            p1[ix].a = 0xffff;

          bmap2->PutPixels(0, y, nw, p1);
        }
      }
    }
  }

  bmap2->OpenOutput(&bi);
  bmap2->Write(&bi);
  bmap2->Close(&bi);
  bmap2->DeleteThis();
}

void hydraRender_mk3::CropToFileHDR(BitmapTex* pBitMap, Bitmap *bmap, const hydraStr& mapPath, TimeValue t)
{
	if (bmap == nullptr || pBitMap == nullptr)
		return;

	bool applyTexCrop = FindBool(L"Apply", pBitMap);
	bool place = FindInt(L"CropPlace", pBitMap);

	float clipu = FindFloat(L"Clip U Offset", pBitMap);
	float clipv = FindFloat(L"Clip V Offset", pBitMap);
	float clipw = FindFloat(L"Clip U Width", pBitMap);
	float cliph = FindFloat(L"Clip V Width", pBitMap);

	Bitmap *bmap2;
	BitmapInfo bi;

	int w = bmap->Width();
	int h = bmap->Height();

	int x0, y0, nw, nh;
	nw = int(float(w)*clipw);
	nh = int(float(h)*cliph);
	x0 = int(float(w - 1)*clipu);
	y0 = int(float(h - 1)*clipv);

	if (place)
	{
		{
			std::lock_guard<std::mutex> lck(mtx);
			bi.SetType(BMM_REALPIX_32);
			bi.SetWidth(w);
			bi.SetHeight(h);
			bi.SetPath(mapPath.c_str());
			bmap2 = TheManager->Create(&bi);

			if (bmap2 != nullptr)
			{
				bmap2->Fill(0, 0, 0, 0);

				if (nw < 1) nw = 1;
				if (nh < 1) nh = 1;
				PixelBufFloat row(nw);

				Bitmap* tmpBM = nullptr;
				BitmapInfo bi2;
				bi2.SetName(_T("temp"));
				bi2.SetType(BMM_REALPIX_32);
				bi2.SetWidth(nw);
				bi2.SetHeight(nh);
				tmpBM = TheManager->Create(&bi2);
				if (tmpBM != nullptr)
				{
					tmpBM->CopyImage(bmap, COPY_IMAGE_RESIZE_HI_QUALITY, 0);
					BMM_Color_fl*  p1 = row.Ptr();
					for (int y = 0; y < nh; y++)
					{
						tmpBM->GetLinearPixels(0, y, nw, p1);
						for (int ix = 0; ix < nw; ix++)
							p1[ix].a = 0xffff;

						bmap2->PutPixels(x0, y + y0, nw, p1);
					}
					tmpBM->DeleteThis();
				}

			}
		}

	}
	else
	{
		if (nw < 1) nw = 1;
		if (nh < 1) nh = 1;
		{
			std::lock_guard<std::mutex> lck(mtx);
			bi.SetType(BMM_REALPIX_32);
			bi.SetWidth(nw);
			bi.SetHeight(nh);
			bi.SetPath(mapPath.c_str());
			bmap2 = TheManager->Create(&bi);

			PixelBufFloat row(nw);
			if (bmap2 != nullptr)
			{
				BMM_Color_fl*  p1 = row.Ptr();
				for (int y = 0; y < nh; y++) {
					bmap->GetLinearPixels(x0, y + y0, nw, p1);
					for (int ix = 0; ix < nw; ix++)
						p1[ix].a = 0xffff;

					bmap2->PutPixels(0, y, nw, p1);
				}
			}
		}
	}

	bmap2->OpenOutput(&bi);
	bmap2->Write(&bi);
	bmap2->Close(&bi);
	bmap2->DeleteThis();
}


void SaveTexureInFileWithLowRes(std::wstring mapPath, Bitmap* a_img)
{
  if (a_img == nullptr)
    return;

  int w = a_img->Width();
  int h = a_img->Height();

  do
  {
    w = w / 2;
    h = h / 2;

  } while (w > 1024 || h > 1024);

  BitmapInfo bi;

  if (a_img->IsHighDynamicRange())
    bi.SetType(BMM_REALPIX_32);
  else
    bi.SetType(BMM_TRUE_64);

  bi.SetFlags(a_img->Flags());


  bi.SetName(mapPath.c_str());
  bi.SetPath(mapPath.c_str());
  bi.SetWidth(w);
  bi.SetHeight(h);

  Bitmap *bmap2;
  bmap2 = TheManager->Create(&bi);
  
  if (bmap2 != nullptr)
  {
    bmap2->CopyImage(a_img, COPY_IMAGE_RESIZE_HI_QUALITY, BMM_Color_fl(0, 0, 0, 0), &bi);
    bmap2->OpenOutput(&bi);
    bmap2->Write(&bi);
    bmap2->Close(&bi);
    bmap2->DeleteThis();
  }

}


void hydraRender_mk3::ExtractTextureMatrixFromUVGen(StdUVGen* uvGen, float matrixData[16], int* pTexTilingFlags)
{
  TimeValue t = GetStaticFrame();

  float uOffset;
  float vOffset;
  float uTiling;
  float vTiling;
  float angle;
  float angleUVW[3];
  int texTilingFlags;

  Matrix3 rotU(1), rotV(1), rotW(1);
  Matrix3 scale(1), trans(1);

  Matrix3 moveToCenter(1), moveToCenterInv(1);

  if (uvGen != nullptr && !uvGen->GetUseRealWorldScale())
  {
    moveToCenter.SetTrans(Point3(0.5f, 0.5f, 0.0f));
    moveToCenterInv.SetTrans(Point3(-0.5f, -0.5f, 0.0f));
  }

  if (uvGen != nullptr)
  {
    uOffset = uvGen->GetUOffs(t);
    vOffset = uvGen->GetVOffs(t);
    uTiling = uvGen->GetUScl(t);
    vTiling = uvGen->GetVScl(t);
    angle = uvGen->GetAng(t);

    angleUVW[0] = uvGen->GetUAng(t);
    angleUVW[1] = uvGen->GetVAng(t);
    angleUVW[2] = uvGen->GetWAng(t);

    texTilingFlags = uvGen->GetTextureTiling();
  }
  else
  {
    uOffset = 0;
    vOffset = 0;
    uTiling = 1;
    vTiling = 1;
    angle = 0;

    angleUVW[0] = 0;
    angleUVW[1] = 0;
    angleUVW[2] = 0;


    texTilingFlags = 0;
  }

  rotU.RotateX(angleUVW[0]);
  rotV.RotateY(angleUVW[1]);
  rotW.RotateZ(angleUVW[2]);

  scale.SetScale(Point3(uTiling, vTiling, 1.0f));
  trans.SetTrans(Point3(-uOffset, -vOffset, 0.0f));

  scale = moveToCenterInv*(rotU*rotV*rotW*scale)*moveToCenter;

  Matrix3 texMatrix;
  texMatrix = trans*scale;

  MaxMatrix3ToFloat16V2(texMatrix, matrixData);

  if (pTexTilingFlags != nullptr)
    (*pTexTilingFlags) = texTilingFlags;
}


std::string GetCorrectTexPath(const wchar_t* a_name, IFileResolutionManager* pFRM)
{
  std::string texPath;

  if (pFRM != nullptr)
  {
    MaxSDK::Util::Path path(a_name);

    if (pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kBitmapAsset))
      texPath = wstring_to_utf8(strConverter::c2ws(path.GetCStr()));
    else
      texPath = wstring_to_utf8(a_name);
  }
  else
    texPath = wstring_to_utf8(a_name);

  return texPath;
}


std::wstring GetCorrectTexPath2(const wchar_t* a_name, IFileResolutionManager* pFRM)
{
  std::wstring texPath;

  if (pFRM != nullptr)
  {
    MaxSDK::Util::Path path(a_name);

    if (pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kBitmapAsset))
      texPath = strConverter::c2ws(path.GetCStr());
    else
      texPath = a_name;
  }
  else
    texPath = a_name;

  return texPath;
}


//////////////////////

texInfo hydraRender_mk3::BakeBitmap(Texmap* tex, HRTextureNodeRef currentRef, std::wstring mtl_name, bool dontupdate)
{
  TimeValue t = GetStaticFrame();

  BitmapTex* pBitMap = dynamic_cast<BitmapTex*>(tex); 
  if (pBitMap == nullptr)
    return texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");

  MSTR mapName = pBitMap->GetMapName();
  std::wstring texName = strConverter::c2ws(mapName.data());
  std::wstring displaceTexName = strConverter::c2ws(mapName.data()); // in case of bitmap is in the bump slot
  bool hasDisplacement = true;

  // crop
  //
  bool applyTexCrop = FindBool(L"Apply", pBitMap);
  bool place = FindInt(L"CropPlace", pBitMap);

  IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
  std::wstring tempStr = strConverter::c2ws(pBitMap->GetMapName());
  std::wstring mapPath = GetCorrectTexPath2(tempStr.c_str(), pFRM);

  TextureOutput* texout = pBitMap->GetTexout();
  Bitmap *bmap_out = nullptr;
  Bitmap *bmap_src = pBitMap->GetBitmap(t);

  if (bmap_src != nullptr)
  {
    bool isHDR = bmap_src->IsHighDynamicRange();

    bool copyWasCreated = false;

    if (bmap_src != nullptr && !OutputNodeDoesNothing(pBitMap, t))
    {
      mapPath = AlteredTexPath(tex, isHDR, mtl_name);
      bmap_out = CreateBitMapCopyWithAppliedOutputNode(pBitMap, mapPath, isHDR, t);
      copyWasCreated = true;
    }
    else
    {
      bmap_out = bmap_src;
      copyWasCreated = false;
    }

    int alphaAsGray = FindInt(L"rgbOutput", pBitMap);
    if (alphaAsGray == 1)
    {
      mapPath = AlteredTexPath(tex, isHDR, mtl_name);
      bmap_out = CreateBitMapCopyWithAlphaAsGray(pBitMap, mapPath, isHDR, t);
      copyWasCreated = true;
    }

    if (applyTexCrop)
    {
      mapPath = AlteredTexPath(tex, isHDR, mtl_name);
      if (isHDR)
        CropToFileHDR(pBitMap, bmap_out, mapPath, t);
      else
        CropToFile(pBitMap, bmap_out, mapPath, t);
    }


    if (bmap_src != nullptr && m_inMtlEditorNow) // go faster for big textures in material editor
    {
      if (bmap_src->Width() > 1024 || bmap_src->Height() > 1024)
      {
        mapPath = AlteredTexPath(tex, isHDR, mtl_name);
        SaveTexureInFileWithLowRes(mapPath, bmap_out);
      }
    }    

    if (bmap_out != nullptr && copyWasCreated)
      bmap_out->DeleteThis();


    Bitmap* pBitMap2 = pBitMap->GetBitmap(t);
    float gamma = 2.2f;

    if (pBitMap2 != nullptr)
      gamma = pBitMap2->Gamma();
    else
      gamma = gammaMgr.GetFileInGamma();

    if (isHDR && copyWasCreated) // if HDR texture was transformed with output node, it gamma is always one because it's linear HDR texture
      gamma = 1.0f;

    int alphaSourceInt = FindInt(L"AlphaSource", pBitMap);
    int rgbSourceInt = FindInt(L"rgbOutput", pBitMap);
    int monoOutput = FindInt(L"MonoOutput", pBitMap);

    std::wstring alphaSource = L"alpha";

   // if (outColorType == TEX_OUT_ALPHA)
      alphaSource = (alphaSourceInt == 0) ? L"alpha" : L"rgb";
   /* else if (outColorType == TEX_OUT_MONO)
      alphaSource = (monoOutput == 1) ? L"alpha" : L"rgb";
    else
      alphaSource = (rgbSourceInt == 1) ? L"alpha" : L"rgb";
      */

    if (mapPath.size() > 4)
    {
      std::wstring fileRes = mapPath.substr(mapPath.size() - 4, 4);
      if (fileRes == L".jpg" || fileRes == L".bmp" || fileRes == L".JPG" || fileRes == L".BMP")
        alphaSource = L"rgb";
    }

    HRTextureNodeRef texRef;

    if (currentRef.id == -1)
      texRef = hrTexture2DCreateFromFile(mapPath.c_str());
    else if(!dontupdate)
      texRef = hrTexture2DUpdateFromFile(currentRef, mapPath.c_str());

    return texInfo(texRef, static_cast<StdUVGen*>(tex->GetTheUVGen()), gamma, alphaSource);
  }
  else
  {
    return texInfo(HRTextureNodeRef(), nullptr, 2.2f, L"alpha");
  }
}

texInfo hydraRender_mk3::BakeNormalMap(Texmap* tex, HRTextureNodeRef currentRef)
{
  //int numSubTex = tex->NumSubTexmaps();
  Texmap* subTex = tex->GetSubTexmap(0); //we only support "Normal" slot and ignore "Additional bump"

  return BakeBitmap(subTex, currentRef);
}

texInfo hydraRender_mk3::BakeHydraAOTexmap(Texmap * tex, HRTextureNodeRef currentRef)
{
  return texInfo();
}

void hydraRender_mk3::ExtractNormalBump(Mtl* mtl, bool &isNormalMap, bool &flipRed, bool &flipGreen, bool &swap)
{
  int numSubTex = mtl->NumSubTexmaps();
  Texmap* subTex = nullptr;
  
  bool invertHeight = false;

  if (FindBool(L"DisplacementInvertHeightState", mtl))
    invertHeight = true;
  

  for (int i = 0; i < numSubTex; ++i)
  {
    std::wstring slotName = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());

    if (slotName == L"NormalMap")
    {
      subTex = mtl->GetSubTexmap(i);
      break;
    }
  }

  if (subTex)
  {
    Texmap* subSubTex = subTex->GetSubTexmap(0);
    if (subSubTex != nullptr)
    {
      MSTR slotName = subTex->GetSubTexmapSlotName(0);
      std::wstring slotNameData = strConverter::c2ws(slotName.data());

      if (slotNameData == L"Normal")
      {
        isNormalMap = true;
        flipRed = FindBool(L"flip red", subTex);
        flipGreen = FindBool(L"flip green", subTex);
        swap = FindBool(L"swap red green", subTex);

        if (invertHeight)
        {
          flipRed = !flipRed;
          flipGreen = !flipGreen;
        }
      }
    }
    
  }
  
}


HRTextureNodeRef CreateFalloff()
{
  HRTextureNodeRef texProc = hrTextureCreateAdvanced(L"proc", L"my_falloff");

  hrTextureNodeOpen(texProc, HR_WRITE_DISCARD);
  {
    pugi::xml_node texNode = hrTextureParamNode(texProc);

    pugi::xml_node code_node = texNode.append_child(L"code");
    code_node.append_attribute(L"file") = L"data/code/falloff_example.c";
    code_node.append_attribute(L"main") = L"main";
  }
  hrTextureNodeClose(texProc);

  return texProc;
}

//void hydraRender_mk3::BindingFalloff(Texmap* tex, HRTextureNodeRef currentRef)
//{
//  // proc texture sampler settings
//  //
//  HRTextureNodeRef texBitmap1;  //hrTexture2DCreateFromFile(L"data/textures/texture1.bmp");
//  HRTextureNodeRef texBitmap2;  //hrTexture2DCreateFromFile(L"data/textures/300px-Bump2.jpg");
//
//  texBitmap1.id = -1;
//  texBitmap2.id = -1;
//
//  HRTextureNodeRef texProc = CreateFalloff();
//
//  pugi::xml_node colorNode = .append_child(L"color");
//
//  colorNode.append_attribute(L"val") = L"0.5 0.0 0.0";
//  colorNode.append_attribute(L"tex_apply_mode") = L"replace";
//
//  auto texNode = hrTextureBind(texProc, colorNode);
//
//  auto code_node = texNode.append_child(L"code");
//  code_node.force_attribute(L"file") = L"data/code/falloff_example.c";
//  code_node.force_attribute(L"main") = L"main";
//
//
//  auto p1 = texNode.append_child(L"arg");
//  auto p2 = texNode.append_child(L"arg");
//  auto p3 = texNode.append_child(L"arg");
//  auto p4 = texNode.append_child(L"arg");
//
//  p1.append_attribute(L"id") = 0;
//  p1.append_attribute(L"name") = L"tex1";
//  p1.append_attribute(L"type") = L"sampler2D";
//  p1.append_attribute(L"size") = 1;
//  p1.append_attribute(L"val") = texBitmap1.id;
//
//  p2.append_attribute(L"id") = 1;
//  p2.append_attribute(L"name") = L"tex2";
//  p2.append_attribute(L"type") = L"sampler2D";
//  p2.append_attribute(L"size") = 1;
//  p2.append_attribute(L"val") = texBitmap2.id;
//
//  p3.append_attribute(L"id") = 2;
//  p3.append_attribute(L"name") = L"texOffset";
//  p3.append_attribute(L"type") = L"float2";
//  p3.append_attribute(L"size") = 1;
//  p3.append_attribute(L"val") = L"0 0";
//
//  p4.append_attribute(L"id") = 3;
//  p4.append_attribute(L"name") = L"inColor";
//  p4.append_attribute(L"type") = L"float4";
//  p4.append_attribute(L"size") = 1;
//  p4.append_attribute(L"val") = L"1 1 1 1";
//
//}

Bitmap* getRenderedBitmapFromTexmap(BitmapInfo &bi, Texmap* tex)
{
  Bitmap* bmap = TheManager->Create(&bi);
  
  bmap->Fill(0, 0, 0, 0);

  TimeValue t = 0;
  UVGen* uvGen = tex->GetTheUVGen();
  float uOffset, vOffset, uTiling, vTiling, angle, angleUVW[3];
  StdUVGen* stdUVgen;

  MSTR className;
  tex->GetClassName(className);
  std::wstring wClassName = strConverter::c2ws(className.data());
  int numSubs = tex->NumSubTexmaps();


  //if (wClassName == L"Color Correction" || wClassName == L"Composite" || wClassName == L"Mask" ||
  //    wClassName == L"Mix" || wClassName == L"Output" || wClassName == L"RGB Multiply" || wClassName == L"RGB Tint")
  //{
  //  if (numSubs > 0)
  //  {
  //    Texmap* subTex = nullptr;
  //    subTex = tex->GetSubTexmap(0);

  //    if (subTex)
  //    {
  //      MSTR className2;
  //      subTex->GetClassName(className2);

  //      if (std::wstring(className2) == L"Bitmap")  // check if subtex-bitmap exists
  //      {
  //        UVGen* uvGenSub = nullptr;
  //        uvGenSub = subTex->GetTheUVGen();
  //        uvGen = uvGenSub;
  //      }
  //    }
  //  }
  //}

  if (uvGen) //absolute bullshit
  {
    stdUVgen = static_cast<StdUVGen*>(uvGen);
    uOffset = stdUVgen->GetUOffs(t);
    vOffset = stdUVgen->GetVOffs(t);
    uTiling = stdUVgen->GetUScl(t);
    vTiling = stdUVgen->GetVScl(t);
    angle = stdUVgen->GetAng(t);
    angleUVW[0] = stdUVgen->GetUAng(t);
    angleUVW[1] = stdUVgen->GetVAng(t);
    angleUVW[2] = stdUVgen->GetWAng(t);

    if (!g_inMatEditor)
    {
      stdUVgen->SetUOffs(0.0f, t);
      stdUVgen->SetVOffs(0.0f, t);
      stdUVgen->SetUScl(1.0f, t);
      stdUVgen->SetVScl(1.0f, t);
      stdUVgen->SetAng(0.0f, t);

      stdUVgen->SetUAng(0.0f, t);
      stdUVgen->SetVAng(0.0f, t);
      stdUVgen->SetWAng(0.0f, t);
    }

    tex->RenderBitmap(0, bmap);

   /* bmap->OpenOutput(&bi);
    bmap->Write(&bi);
    bmap->Close(&bi);*/

    if (!g_inMatEditor)
    {
      stdUVgen->SetUOffs(uOffset, t);
      stdUVgen->SetVOffs(vOffset, t);
      stdUVgen->SetUScl(uTiling, t);
      stdUVgen->SetVScl(vTiling, t);
      stdUVgen->SetAng(angle, t);

      stdUVgen->SetUAng(angleUVW[0], t);
      stdUVgen->SetVAng(angleUVW[1], t);
      stdUVgen->SetWAng(angleUVW[2], t);
    }

  }
  else
  {

    if (className != TEXT("Falloff"))
    {
      tex->RenderBitmap(t, bmap);
    }
  }

  return bmap;
}

void computeMaxProcTexHDR(float* a_buffer, int w, int h, void* texVoid)
{
  if (texVoid == nullptr)
    return;

  Texmap* tex = ((translateTexmap*)(texVoid))->texmap; //static_cast<Texmap*>(texVoid);

  BitmapInfo bi;

  bi.SetType(BMM_REALPIX_32);

  bi.SetWidth(w);
  bi.SetHeight(h);

  bi.SetFlags(MAP_HAS_ALPHA);
  bi.SetCustomFlag(0);

  Bitmap* bmap = getRenderedBitmapFromTexmap(bi, tex);


#pragma omp parallel for
  for (int y = 0; y < h; y++)
  {
    std::vector<BMM_Color_fl> line;
    line.resize(w);

    bmap->GetPixels(0, h - y - 1, w, &line[0]);
    for (int x = 0; x < w; x++)
    {
      a_buffer[(y*w + x) * 4 + 0] = line[x].r;
      a_buffer[(y*w + x) * 4 + 1] = line[x].g;
      a_buffer[(y*w + x) * 4 + 2] = line[x].b;
      a_buffer[(y*w + x) * 4 + 3] = line[x].a;
    }
  }
  bmap->DeleteThis();
}

void computeMaxProcTexLDR(unsigned char* a_buffer, int w, int h, void* texVoid)
{
  if (texVoid == nullptr)
    return;

  Texmap* tex = static_cast<Texmap*>(texVoid);//((translateTexmap*)(texVoid))->texmap; //static_cast<Texmap*>(texVoid);
  
  BitmapInfo bi;

  bi.SetType(BMM_TRUE_64);

  bi.SetWidth(w);
  bi.SetHeight(h);

  bi.SetFlags(MAP_HAS_ALPHA);
  bi.SetCustomFlag(0);
  std::wstring mapPath = L"C:\\[Hydra]\\temp\\temp.bmp";
  bi.SetPath(mapPath.c_str());
  Bitmap* bmap = getRenderedBitmapFromTexmap(bi, tex);

#pragma omp parallel for
  for (int y = 0; y < h; y++)
  {
    std::vector<BMM_Color_64> line;
    line.resize(w);

    bmap->GetPixels(0, h - y - 1, w, &line[0]);
    for (int x = 0; x < w; x++)
    {
      a_buffer[(y*w + x) * 4 + 0] = 255 * line[x].r / 65535;
      a_buffer[(y*w + x) * 4 + 1] = 255 * line[x].g / 65535;
      a_buffer[(y*w + x) * 4 + 2] = 255 * line[x].b / 65535;
      a_buffer[(y*w + x) * 4 + 3] = 255 * line[x].a / 65535;
    }
  }
  bmap->DeleteThis();
}

void GetMaxTextureSize(Texmap* tex, float w, float h, float& maxSizeW, float& maxSizeH, int currDepth)
{
  currDepth++;

  const int numSubs = tex->NumSubTexmaps();

  for (int i = 0; i < numSubs; ++i)
  {
    Texmap* subTex = nullptr;
    subTex = tex->GetSubTexmap(i);

    if (subTex)
    {
      MSTR className2;
      subTex->GetClassName(className2);

      if (strConverter::c2ws(className2.data()) == L"Bitmap")
      {
        auto pBitMap = static_cast<BitmapTex*>(subTex);
        auto bm = pBitMap->GetBitmap(TimeValue(0));

        if (bm)
        {
          w = bm->Width();
          h = bm->Height();

          if (w > maxSizeW || h > maxSizeH)
          {
            maxSizeW = w;
            maxSizeH = h;
          }
        }
        else
        {
          maxSizeW = 2048;
          maxSizeH = 2048;
        }
      }
      else
      {
        if (currDepth < 10)
          GetMaxTextureSize(subTex, w, h, maxSizeW, maxSizeH, currDepth);
      }
    }
  }
}

texInfo hydraRender_mk3::BakeOtherTexmap(Texmap* tex, HRTextureNodeRef currentRef)
{
  MSTR className;
  tex->GetClassName(className);
  bool isHDR = false;
  auto t = GetStaticFrame();

  std::wstring wClassName = strConverter::c2ws(className.data());

  const int notSuppNum = 11;
  std::wstring notSupported[] = { L"Cellular", L"Dent", L"Noise", L"Marble", L"Perlin Marble", L"Smoke", L"Speckle", L"Splat", L"Stucco", L"Waves", L"Wood" };
  bool supported = true;

  for (int i = 0; i < notSuppNum; ++i)
  {
    int channelSrc = tex->GetUVWSource();

    if (wClassName == notSupported[i] && (channelSrc != UVWSRC_EXPLICIT))
      supported = false;
  }

  if (supported)
  {
    //float gamma = gammaMgr.GetFileInGamma();
    g_inMatEditor = rendParams.inMtlEditor;

    auto uvgen = static_cast<StdUVGen*>(tex->GetTheUVGen());
    std::wstring alphaSource = L"rgb";
    

    float maxSizeW = 256;
    float maxSizeH = 256;
    int currDepth = 0;
    GetMaxTextureSize(tex, 256, 256, maxSizeW, maxSizeH, currDepth);


    int numSubs = tex->NumSubTexmaps();
    for (int i = 0; i < numSubs; ++i)
    {
      Texmap* subTex = nullptr;
      subTex = tex->GetSubTexmap(i);

      if (subTex)
      {
        MSTR className2;
        subTex->GetClassName(className2);
        if (strConverter::c2ws(className2.data()) == L"Bitmap")
        {
          auto pBitMap = static_cast<BitmapTex*>(subTex);
          isHDR = pBitMap->IsHighDynamicRange();
          auto bm = pBitMap->GetBitmap(t);

          if (bm)
          {
            auto stdUVgen = static_cast<StdUVGen*>(pBitMap->GetTheUVGen());

            int alphaSourceInt = FindInt(L"AlphaSource", pBitMap);
            alphaSource = (alphaSourceInt == 0) ? L"alpha" : L"rgb";

            //if (wClassName == L"Color Correction" || wClassName == L"Composite" || wClassName == L"Mask" ||
            //  wClassName == L"Mix" || wClassName == L"Output" || wClassName == L"RGB Multiply" || wClassName == L"RGB Tint")
            //{
            //  //uvgen = stdUVgen;
            //}
          }
        }
      }
    }


    HRTextureNodeRef texRef;
    if (isHDR)
    {
      translateTexmap texmapStruct;
      texmapStruct.texmap = tex;

      if (currentRef.id == -1)
        texRef = hrTexture2DCreateFromProcHDR(&computeMaxProcTexHDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
      else
        texRef = hrTexture2DUpdateFromProcHDR(currentRef, &computeMaxProcTexHDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
    }
    else
    {
      translateTexmap texmapStruct;
      texmapStruct.texmap = tex;

      if (currentRef.id == -1)
        texRef = hrTexture2DCreateFromProcLDR(&computeMaxProcTexLDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
      else
        texRef = hrTexture2DUpdateFromProcLDR(currentRef, &computeMaxProcTexLDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
    }

    return texInfo(texRef, uvgen, 1.0f, alphaSource); // gamma for this type maps (composite, checker and other) must have = 1.0f
  }
  else
    return texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");
}

texInfo hydraRender_mk3::BakeTexture(Texmap* tex, HRTextureNodeRef currentRef, std::wstring mtl_name, bool dontupdate)
{
  texInfo res(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");

  MSTR className;
  float gamma = 1.0f;
  if (tex == nullptr) return res;

  tex->GetClassName(className);

  std::wstring texName  = strConverter::c2ws(tex->GetName().data());
  std::wstring texClass = strConverter::c2ws(className.data());

  if (tex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00)) //bitmap
  {
    res = BakeBitmap(tex, currentRef, mtl_name, dontupdate);
  }
  else if (texClass == L"Normal Bump") // this is normalmap based bump texture
  {
    res = BakeNormalMap(tex, currentRef);
  }
  else if (texClass == L"HydraAO") // this is ambient occlusion proc. texture
  {
    res = BakeHydraAOTexmap(tex, currentRef);
  }
  //else if (tex->ClassID() == Class_ID(FALLOFF_CLASS_ID, 0x00))
  //{
  //  BindingFalloff(tex, currentRef);
  //}
  else //procedural texture or smth. else
  {
    res = BakeOtherTexmap(tex, currentRef);
  }

  return res;
}

std::unordered_map<std::wstring, texInfo> hydraRender_mk3::ExtractHydraTextures(Mtl* mtl, const std::vector<std::wstring> &mapNames)
{
  std::unordered_map<std::wstring, texInfo> result;

  int numSubTex = mtl->NumSubTexmaps();
  texInfo oneTex;

  for (int i = 0; i < numSubTex; ++i)
  {
    Texmap* subTex = nullptr;

    std::wstring slotName = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());

    for (int j = 0; j < mapNames.size(); ++j)
    {
      if (slotName == mapNames.at(j))
      {
        subTex = mtl->GetSubTexmap(i);
        break;
      }
    }


    if (subTex)
    {
      //std::wstring slotNameStr = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());
      //auto isOpacityMap = (slotName == L"OpacityMap") ? TEX_OUT_COLOR : TEX_OUT_ALPHA;
      auto found = texmapLib.find(subTex);
      HRTextureNodeRef texRef;

      if (found == texmapLib.end())
      {
        texTracker->AddTexmap(subTex, mtl);
        oneTex = BakeTexture(subTex, HRTextureNodeRef(), mtl->GetName().data());
        if (std::get<0>(oneTex).id != -1)
          texmapLib[subTex] = oneTex;
        
        texTracker->texmaps[subTex].first = false;
      }
      else if (texTracker->texmaps[subTex].first)
      {
        //refAndGamma = found->second;
        oneTex = BakeTexture(subTex, std::get<0>(found->second));
        texTracker->texmaps[subTex].first = false;

        if (std::get<0>(oneTex).id != -1)
          texmapLib[subTex] = oneTex;
      }
      else
      {
        oneTex = found->second;
      }

     // auto uvGen = static_cast<StdUVGen*>(subTex->GetTheUVGen());
      result[slotName] = oneTex;
    }
  }
  return result;
}




/////////////////////////////////////////////////////////////////
// Old unused functions left for reference for the time being  //
/////////////////////////////////////////////////////////////////

/*
bool hydraRender_mk3::MaxOtherMapToTextureObj(Texmap* tex, TextureObj* texture)
{
  TimeValue t = GetStaticFrame();

  MSTR mapName = tex->GetName();
  texture->mapName = strConverter::c2ws(mapName.data());

  std::pair<std::set<std::wstring>::iterator, bool> ret = procTexNames.insert(texture->mapName);

  std::wstring mapPath;

  mapPath = L"C:\\[Hydra]\\temp\\" + texture->mapName + L".bmp";

  UVGen* uvGen = tex->GetTheUVGen();
  Matrix3 uvTrans;

  MSTR className;
  tex->GetClassName(className);
  int numSubs = tex->NumSubTexmaps();

  std::wstring wClassName = strConverter::c2ws(className.data());


  // find textures that are functins of world position or object space position. Can't render them.
  //
  const int notSuppNum = 11;
  std::wstring notSupported[] = { L"Cellular", L"Dent", L"Noise", L"Marble", L"Perlin Marble", L"Smoke", L"Speckle", L"Splat", L"Stucco", L"Waves", L"Wood" };
  bool supported = true;
  for (int i = 0; i < notSuppNum; ++i)
  {
    int channelSrc = tex->GetUVWSource();

    if (wClassName == notSupported[i] && (channelSrc != UVWSRC_EXPLICIT))
      supported = false;
  }

  //TraceNodeCustomProperties(texture->mapName, tex, 1);

  if (supported)
  {
    if (uvGen)
      DumpUVGen((StdUVGen*)uvGen, texture);
    else if ((wClassName == L"Color Correction" || wClassName == L"Output" || wClassName == L"RGB Multiply" || wClassName == L"RGB Tint") && numSubs > 0)
    {
      // find DumpUVGen inside one of texmaps
      //
      for (int subTexId = 0; subTexId < tex->NumSubTexmaps(); subTexId++)
      {
        Texmap* subTex = tex->GetSubTexmap(subTexId);
        if (subTex)
        {
          MSTR className2;
          subTex->GetClassName(className2);

          UVGen* uvGenSub = subTex->GetTheUVGen();
          if (uvGenSub)
          {
            DumpUVGen((StdUVGen*)uvGenSub, texture);
            break;
          }
        }
      }

    }
    else
    {
      texture->uOffset = 0;
      texture->vOffset = 0;
      texture->uTiling = 1;
      texture->vTiling = 1;
      texture->angle = 0;

      texture->angleUVW[0] = 0;
      texture->angleUVW[1] = 0;
      texture->angleUVW[2] = 0;
    }

    //procTexs[procTexToCheck] = tex;
    //plugin_log.PrintValue("proc tex", strConverter::ws2s(mapPath));

    if (ret.second)
      procTexsV.push_back(tex);

    //procTexToCheck++;
    //curTex++;
    //TraceNodeCustomProperties(texture->mapName, tex, 1);

    bool isHDR = false;

    if (strConverter::c2ws(className.data()) == L"Color Correction")
    {
      int numSubs = tex->NumSubTexmaps();

      for (int i = 0; i < numSubs; ++i)
      {
        Texmap* subTex = nullptr;
        std::wstring slotName = strConverter::c2ws(tex->GetSubTexmapSlotName(i).data());

        if (slotName == L"Map")
        {
          subTex = tex->GetSubTexmap(i);

          if (subTex)
          {
            MSTR className2;
            subTex->GetClassName(className2);
            if (strConverter::c2ws(className2.data()) == L"Bitmap")
            {
              BitmapTex* pBitMap = (BitmapTex*)subTex;
              isHDR = pBitMap->IsHighDynamicRange();
            }
          }
        }

      }
    }
    if (isHDR)
    {
      mapPath = L"C:\\[Hydra]\\temp\\" + texture->mapName + L".hdr";
      texture->mapName = mapPath;
      texture->displacementMapPath = mapPath;
    }
    else
    {
      texture->mapName = mapPath;
      texture->displacementMapPath = mapPath;
    }
    texture->hasDisplacement = true;

    if (strConverter::c2ws(className.data()) == L"Falloff")
    {
      texture->isFalloff = true;

      int numSubs = tex->NumSubTexmaps();

      for (int i = 0; i < numSubs; ++i)
      {
        Texmap* subTex = nullptr;
        std::wstring slotName = strConverter::c2ws(tex->GetSubTexmapSlotName(i).data());

        if (slotName == L"Map 1" || slotName == L"Map 2")
        {
          subTex = tex->GetSubTexmap(i);


          float amt = 1.0f;
          Color col_1(0, 0, 0);
          Color col_2(0, 0, 0);

          if (slotName == L"Map 1")
          {
            if (!FindBool(L"Map 1 Enable", tex)) continue;
            amt = FindFloat(L"Map Amount 1", tex);
            if (subTex)
            {
              texture->col_1.r = 1;
              texture->col_1.g = 1;
              texture->col_1.b = 1;
            }
            else
            {
              col_1 = FindColor(L"Color 1", tex);
              texture->col_1 = col_1;
            }
          }

          else if (slotName == L"Map 2")
          {
            if (!FindBool(L"Map 2 Enable", tex)) continue;
            amt = FindFloat(L"Map Amount 2", tex);
            if (subTex)
            {
              texture->col_2.r = 1;
              texture->col_2.g = 1;
              texture->col_2.b = 1;
            }
            else
            {
              col_2 = FindColor(L"Color 2", tex);
              texture->col_2 = col_2;
            }
          }

          if (subTex)
          {
            TextureObj tex_temp;
            bool supported = DumpTexture(subTex, tex->ClassID(), amt, &tex_temp);
            if (supported) texture->subTextures[strConverter::ws2s(slotName.data())] = tex_temp;
          }

        }
      }

      // find curve 
      //
      ICurveCtl *curCtrl = nullptr;
      for (auto q = 0; q < tex->NumRefs(); ++q)
      {
        MSTR className;
        if (tex->GetReference(q))
        {
          tex->GetReference(q)->GetClassName(className);
          if (strConverter::c2ws(className.data()) == L"CurveControl")
          {
            curCtrl = (ICurveCtl *)tex->GetReference(q);
            break;
          }
        }
      }

      if (curCtrl)
      {
        ICurve * curve = curCtrl->GetControlCurve(0);

        int num_points = curve->GetNumPts();
        std::wstringstream tmp, tmp2;
        tmp << "          <points> ";
        tmp2 << "          <tangents> ";
        for (auto j = 0; j < num_points; ++j)
        {
          CurvePoint p = curve->GetPoint(t, j);
          tmp << p.p.x << " " << p.p.y << " ";
          tmp2 << p.out.x << " " << p.out.y << " ";
        }
        tmp << "</points>" << std::endl;
        tmp2 << "</tangents>" << std::endl;
        texture->points = tmp.str();
        texture->tangents = tmp2.str();
      }

      // if fresnel ?
      //
      int faloffType = FindInt(L"Falloff Type", tex);
      if (faloffType == 2) // 0 - default Perpendicular/Planar; 2 - Fresnel;
      {
        float fresnelIOR = FindFloat(L"Index of Refraction", tex);
        bool  overrideIOR = FindBool(L"Mtl IOR Override Enable", tex);

        texture->isFaloffFresnel = true;
        texture->isFaloffFresnelOverride = overrideIOR;
        texture->faloffFresnelIOR = fresnelIOR;
      }
      else
        texture->isFaloffFresnel = false;

      // TraceNodeCustomProperties(texture->mapName, tex, 1);
    }
    return true;
  }
  else
    return false;


}


void hydraRender_mk3::MaxNormalMapToTextureObj(Texmap* tex, TextureObj* texture)
{
  bool hasNormalmap = false;
  bool hasDisplacementMap = false;

  TextureObj texNormal;
  TextureObj texDisplacement;

  for (int i = 0; i<tex->NumSubTexmaps(); i++)
  {
    Texmap* subTex = tex->GetSubTexmap(i);

    if (subTex != nullptr)
    {
      MSTR slotName = tex->GetSubTexmapSlotName(i);
      std::wstring slotNameData = strConverter::c2ws(slotName.data());

      TextureObj* pResult = nullptr;

      if (slotNameData == L"Normal")
      {
        pResult = &texNormal;
        pResult->nmFlipRed = FindBool(L"flip red", tex);
        pResult->nmFlipGreen = FindBool(L"flip green", tex);
        pResult->nmSwapRedAndGreen = FindBool(L"swap red green", tex);
        hasNormalmap = true;
      }
      else if (slotNameData == L"Additional Bump")
      {
        pResult = &texDisplacement;
        hasDisplacementMap = true;
        pResult->displacementAmount = FindFloat(L"Bump Multiplier", tex);
      }
      else
      {
        std::string slotName = strConverter::ws2s(slotNameData);
        plugin_log.PrintValue("(error) MaxNormalMapToTextureObj(); Unknown normalBump slot name", slotName.c_str());
      }

      if (subTex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00) && pResult != nullptr)
      {
        MSTR className, texName;

        subTex->GetClassName(className);
        texName = subTex->GetName();

        pResult->texName = strConverter::c2ws(texName);
        pResult->texClass = strConverter::c2ws(className);
        MaxBitMapDataToTextureObj(subTex, pResult, TEX_OUT_COLOR);
      }
    }
  }

  if (hasNormalmap)
    *texture = texNormal;
  else if (hasDisplacementMap)
    *texture = texDisplacement;

  texture->normalMapPath = texNormal.mapName;
  texture->displacementMapPath = texDisplacement.mapName;

  texture->isBump = true;
  texture->hasNormals = hasNormalmap;
  texture->hasDisplacement = hasDisplacementMap;

  texture->displacementAmount = texDisplacement.displacementAmount;
  texture->nmFlipRed = texNormal.nmFlipRed;
  texture->nmFlipGreen = texNormal.nmFlipGreen;
  texture->nmSwapRedAndGreen = texNormal.nmSwapRedAndGreen;

  //TraceNodeCustomProperties(texture->mapName, tex, 1);
}*/