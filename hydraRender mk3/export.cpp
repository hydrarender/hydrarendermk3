#include "hydraRender mk3.h"

#include <MeshNormalSpec.h>
#include <mentalray/imrPhysicalSkyEnvironmentMap.h>

#include <unordered_map>
#include <map>
#include <time.h>
#include <polyobj.h>
#include <NotificationAPI/NotificationAPI_Events.h>

#include "modstack.h"

static BOOL exportSelected;


int	hydraRender_mk3::DoExport(INode* root, INode *vnode, ViewParams* a_viewParams, TimeValue t, bool forMatEditor)
{
  m_inMtlEditorNow = forMatEditor;

  plugin_log.Print("Entering HydraRender::DoExport");

  // clear all states
  geoInstancesLib.clear();

  m_sunUseMrSky = false;

  if (m_firstRender || forMatEditor)
  {
    materialsRefLib.clear();
    texmapLib.clear();    
    envTex = texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");
    ExtractPlaceholderMaterial();
  }

  PreProcess(root, t, forMatEditor);	//get materials from nodes and put them into mtlList; also count all renderable nodes
  ExtractMaterialList(forMatEditor); //export materials in mtlList to hydraAPI 
  plugin_log.Print("after extract material list");

  if (!forMatEditor)
  {
    ExtractAndDumpSceneData(root, t); //instance geometry nodes in hydraAPI scene
    ExtractCameraFromActiveViewport(vnode, a_viewParams, t);
    hrSceneClose(currentScene);
  }
  else
  {
    ExtractForMatEditor(t); //create scene for rendering in material editor
    hrSceneClose(matEditorScene);
  }


  plugin_log.Print("Leaving HydraRender::DoExport API");
  return 1;
}


HRMeshRef createCube(int mat_id)
{
  uint32_t numberVertices = 24;
  uint32_t numberIndices = 36;

  float cubeVertices[] =
  {
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f
  };

  float cubeNormals[] =
  {
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f
  };

  float cubeTexCoords[] =
  {
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
  };

  uint32_t cubeIndices[] =
  {
    0, 2, 1,
    0, 3, 2,
    4, 5, 6,
    4, 6, 7,
    8, 9, 10,
    8, 10, 11,
    12, 15, 14,
    12, 14, 13,
    16, 17, 18,
    16, 18, 19,
    20, 23, 22,
    20, 22, 21
  };

  auto cubeRef = hrMeshCreate(L"cube");

  hrMeshOpen(cubeRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);
  {
    hrMeshVertexAttribPointer4f(cubeRef, L"pos", cubeVertices);
    hrMeshVertexAttribPointer4f(cubeRef, L"norm", cubeNormals);
    hrMeshVertexAttribPointer2f(cubeRef, L"texcoord", cubeTexCoords);


    hrMeshMaterialId(cubeRef, mat_id);
    hrMeshAppendTriangles3(cubeRef, numberIndices, (int*)cubeIndices);
  }
  hrMeshClose(cubeRef);

  return cubeRef;
}


HRMeshRef createSphere(int mat_id)
{
  int i, j;
  int numberSlices = 64;
  float radius = 1.0f;
  int numberParallels = numberSlices;
  int numberVertices = (numberParallels + 1) * (numberSlices + 1);
  int numberIndices = numberParallels * numberSlices * 3;

  float angleStep = (2.0f * 3.14159265358979323846f) / ((float)numberSlices);

  std::vector<float> vPos, vNorm, vTexCoord;
  std::vector<int> triIndices, matIndices;
  vPos.resize(numberVertices * 4);
  vNorm.resize(numberVertices * 4);
  vTexCoord.resize(numberVertices * 2);

  triIndices.resize(numberIndices);
  matIndices.resize(numberIndices / 3);

  for (i = 0; i < numberParallels + 1; i++)
  {
    for (j = 0; j < numberSlices + 1; j++)
    {
      int vertexIndex = (i * (numberSlices + 1) + j) * 4;
      int normalIndex = (i * (numberSlices + 1) + j) * 4;
      int texCoordsIndex = (i * (numberSlices + 1) + j) * 2;

      vPos[vertexIndex + 0] = radius * sinf(angleStep * (float)i) * sinf(angleStep * (float)j);
      vPos[vertexIndex + 1] = radius * cosf(angleStep * (float)i);
      vPos[vertexIndex + 2] = radius * sinf(angleStep * (float)i) * cosf(angleStep * (float)j);
      vPos[vertexIndex + 3] = 1.0f;

      vNorm[normalIndex + 0] = vPos[vertexIndex + 0] / radius;
      vNorm[normalIndex + 1] = vPos[vertexIndex + 1] / radius;
      vNorm[normalIndex + 2] = vPos[vertexIndex + 2] / radius;
      vNorm[normalIndex + 3] = 1.0f;

      vTexCoord[texCoordsIndex + 0] = (float)j / (float)numberSlices;
      vTexCoord[texCoordsIndex + 1] = (1.0f - (float)i) / (float)(numberParallels - 1);
    }
  }

  int* indexBuf = &triIndices[0];

  for (i = 0; i < numberParallels; i++)
  {
    for (j = 0; j < numberSlices; j++)
    {
      *indexBuf++ = i * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + (j + 1);

      *indexBuf++ = i * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + (j + 1);
      *indexBuf++ = i * (numberSlices + 1) + (j + 1);

      int diff = int(indexBuf - &triIndices[0]);
      if (diff >= numberIndices)
        break;
    }

    int diff = int(indexBuf - &triIndices[0]);
    if (diff >= numberIndices)
      break;
  }

  HRMeshRef meshRef = hrMeshCreate(L"sphere");

  hrMeshOpen(meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);
  {
    hrMeshVertexAttribPointer4f(meshRef, L"pos", &vPos[0]);
    hrMeshVertexAttribPointer4f(meshRef, L"norm", &vNorm[0]);
    hrMeshVertexAttribPointer2f(meshRef, L"texcoord", &vTexCoord[0]);

    hrMeshMaterialId(meshRef, mat_id);
    hrMeshAppendTriangles3(meshRef, int(triIndices.size()), &triIndices[0]);
  }
  hrMeshClose(meshRef);

  return meshRef;
}

void hydraRender_mk3::ExtractForMatEditor(TimeValue t)
{
  plugin_log.Print("Exporting & creating material editor scene");

  hrSceneOpen(matEditorScene, HR_WRITE_DISCARD);

  auto meshRef = createSphere(largestMaterialId);

  auto camRef = hrCameraCreate(L"my camera");
  hrCameraOpen(camRef, HR_WRITE_DISCARD);
  {
    auto camNode = hrCameraParamNode(camRef);
    camNode.append_child(L"fov").text().set(L"45");
    camNode.append_child(L"nearClipPlane").text().set(L"0.01");
    camNode.append_child(L"farClipPlane").text().set(L"100.0");
    camNode.append_child(L"up").text().set(L"0 1 0");
    camNode.append_child(L"position").text().set(L"0 0 0");
    camNode.append_child(L"look_at").text().set(L"0 0 -1");
  }
  hrCameraClose(camRef);

  HRLightRef rectLight = hrLightCreate(L"my_area_light");
  hrLightOpen(rectLight, HR_WRITE_DISCARD);
  {
    auto lightNode = hrLightParamNode(rectLight);

    lightNode.attribute(L"type").set_value(L"area");
    lightNode.attribute(L"shape").set_value(L"rect");
    lightNode.attribute(L"distribution").set_value(L"diffuse");

    auto sizeNode = lightNode.append_child(L"size");

    sizeNode.append_attribute(L"half_length").set_value(4.0f);
    sizeNode.append_attribute(L"half_width").set_value(4.0f);

    auto intensityNode = lightNode.append_child(L"intensity");

    intensityNode.append_child(L"color").append_attribute(L"val").set_value(L"1 1 1");
    intensityNode.append_child(L"multiplier").append_attribute(L"val").set_value(L"4.0");
  }
  hrLightClose(rectLight);


  float matrixT[16] = { 1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f, -4.0f,
                        0.0f, 0.0f, 0.0f, 1.0f };

  hrMeshInstance(matEditorScene, meshRef, matrixT);

  float matrixLight[16] = { 1.0f, 0.0f, 0.0f, 0.0f,
                            0.0f, 0.707f, 0.707f, 5.0f,
                            0.0f, -0.707f, 0.707f, 0.0f,
                            0.0f, 0.0f, 0.0f, 1.0f };

  hrLightInstance(matEditorScene, rectLight, matrixLight);
  
}


int hydraRender_mk3::ExtractAndDumpSceneData(INode* root, TimeValue t)
{
  int numChildren = root->NumberOfChildren();

  plugin_log.Print("Exporting & creating scene dump api");


  int geomObjNum = 0;
  int lightsNum = 0;
  unsigned int allVerticesNum = 0;
  unsigned int allIndicesNum = 0;

  clock_t start, finish;
  start = clock();

  int vertWritten = 0;

  hrSceneOpen(currentScene, HR_WRITE_DISCARD);

/*  if(m_firstRender)
    hrSceneOpen(currentScene, HR_WRITE_DISCARD);
  else
    hrSceneOpen(currentScene, HR_OPEN_EXISTING);*/
 // for (int idx = 0; idx<numChildren; idx++)
 //   TraverseNodeTree(root->GetChildNode(idx), geomObjNum, vertWritten, t);

  ExtractAndInstanceMeshes(geomObjNum, vertWritten, t);

  ExtractAndInstanceLights(lightsNum, t);

  {
    plugin_log.PrintValue("Total vertices number: ", vertWritten);
    finish = clock();

    float tt = (float)((finish - start) / CLOCKS_PER_SEC);
    float tt2 = (float)(finish - start);
    plugin_log.PrintValue("geometry export time: ", tt);
    plugin_log.PrintValue("geometry export time (clocks): ", tt2);
  }

  return geomObjNum;

}


void MaxMatrix3ToFloat16(const Matrix3& pivot, float* m)
{
  Point3 row1, row2, row3, row4;

  row1 = pivot.GetRow(0);
  row2 = pivot.GetRow(1);
  row3 = pivot.GetRow(2);
  row4 = pivot.GetRow(3);

  m[0] = row1[0];
  m[1] = row1[1];
  m[2] = row1[2];
  m[3] = 0;

  m[4] = row2[0];
  m[5] = row2[1];
  m[6] = row2[2];
  m[7] = 0;

  m[8]  = row3[0];
  m[9]  = row3[1];
  m[10] = row3[2];
  m[11] = 0;

  m[12] = row4[0];
  m[13] = row4[1];
  m[14] = row4[2];
  m[15] = 1;
}

void MaxMatrix3ToFloat16V2(const Matrix3& pivot, float* m)
{
  Point4 col1, col2, col3;

  col1 = pivot.GetColumn(0);
  col2 = pivot.GetColumn(1);
  col3 = pivot.GetColumn(2);

  m[0] = col1.x;
  m[1] = col1.y;
  m[2] = col1.z;
  m[3] = col1.w;

  m[4] = col2.x;
  m[5] = col2.y;
  m[6] = col2.z;
  m[7] = col2.w;

  m[8]  = col3.x;
  m[9]  = col3.y;
  m[10] = col3.z;
  m[11] = col3.w;

  m[12] = 0;
  m[13] = 0;
  m[14] = 0;
  m[15] = 1;
}

void MaxMatrix3ToFloat16MagicSwap(const Matrix3& pivot, float* m)
{
  Point3 row1, row2, row3, row4;

  row1 = pivot.GetRow(0);
  row2 = pivot.GetRow(1);
  row3 = pivot.GetRow(2);
  row4 = pivot.GetRow(3);

  m[0] = row1[0];
  m[1] = row1[2];
  m[2] = row1[1];
  m[3] = row4[0];

  m[4] = row3[0];
  m[5] = row3[2];
  m[6] = row3[1];
  m[7] = row4[2];

  m[8] = row2[0];
  m[9] = row2[2];
  m[10] = row2[1];
  m[11] = -row4[1];

  m[12] = 0;
  m[13] = 0;
  m[14] = 0;
  m[15] = 1;
}

Matrix3 TransformMatrixFromMax(const Matrix3 &m)
{
  Matrix3 swapAxis;
  swapAxis.SetRow(0, Point3(1, 0, 0));
  swapAxis.SetRow(1, Point3(0, 0, -1));
  swapAxis.SetRow(2, Point3(0, 1, 0));
  swapAxis.SetRow(3, Point3(0, 0, 0));

  Matrix3 swapAxisInv;
  swapAxisInv.SetRow(0, Point3(1, 0, 0));
  swapAxisInv.SetRow(1, Point3(0, 0, 1));
  swapAxisInv.SetRow(2, Point3(0, -1, 0));
  swapAxisInv.SetRow(3, Point3(0, 0, 0));

  Matrix3 matrixFinal;
  matrixFinal = swapAxisInv*m*swapAxis;

  return matrixFinal;
}


Matrix3 Float16ToMaxMatrix3(const float* a_m)
{
  Matrix3 m;
  m.SetRow(0, Point3(a_m[0*4+0], a_m[0*4+1], a_m[0*4+2]));
  m.SetRow(1, Point3(a_m[1*4+0], a_m[1*4+1], a_m[1*4+2]));
  m.SetRow(2, Point3(a_m[2*4+0], a_m[2*4+1], a_m[2*4+2]));
  m.SetRow(3, Point3(a_m[3*4+0], a_m[3*4+1], a_m[3*4+2]));
  return m;
}


float scaleWithMatrix(float a_val, const Matrix3& a_externTransform)
{
  Point3 tmpVec(0.0f, a_val, 0.0f);

  Matrix3 extTransform = a_externTransform;
  extTransform.SetRow(3, Point3(0,0,0));
  
  return (extTransform*tmpVec).Length();
}

float myclamp(float u, float a, float b) { float r = fmax(a, u); return fmin(r, b); }

int RealColor3ToUint32(const float real_color[3])
{
  float  r = myclamp(real_color[0] * 255.0f, 0.0f, 255.0f);
  float  g = myclamp(real_color[1] * 255.0f, 0.0f, 255.0f);
  float  b = myclamp(real_color[2] * 255.0f, 0.0f, 255.0f);
  //float  a = myclamp(real_color[3] * 255.0f, 0.0f, 255.0f);

  unsigned char red = (unsigned char)r;
  unsigned char green = (unsigned char)g;
  unsigned char blue = (unsigned char)b;
  //unsigned char alpha = (unsigned char)a;

  //return red | (green << 8) | (blue << 16) | (alpha << 24);
  return red | (green << 8) | (blue << 16);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


BOOL hydraRender_mk3::TMNegParity(Matrix3 &m)
{
  return (DotProd(CrossProd(m.GetRow(0),m.GetRow(1)),m.GetRow(2))<0.0)?1:0;
}


TriObject* hydraRender_mk3::GetTriObjectFromNode(INode *node, TimeValue t, int &deleteIt)
{
  deleteIt = FALSE;
  Object *obj = node->EvalWorldState(t).obj;

  if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0))) 
  { 
    TriObject *tri = (TriObject *) obj->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID, 0));
    // Note that the TriObject should only be deleted
    // if the pointer to it is not equal to the object
    // pointer that called ConvertToType()
    if (obj != tri) deleteIt = TRUE;
    return tri;
  }
  else {
    return nullptr;
  }
}


static Point3 basic_tva[3] = { 
  Point3(0.0,0.0,0.0),Point3(1.0,0.0,0.0),Point3(1.0,1.0,0.0)
};
static Point3 basic_tvb[3] = { 
  Point3(1.0,1.0,0.0),Point3(0.0,1.0,0.0),Point3(0.0,0.0,0.0)
};
static int nextpt[3] = {1,2,0};
static int prevpt[3] = {2,0,1};

void hydraRender_mk3::make_face_uv(Face *f, Point3 *tv)
{
  int na,nhid,i;
  Point3 *basetv;
  /* make the invisible edge be 2->0 */
  nhid = 2;
  if (!(f->flags&EDGE_A))  nhid=0;
  else if (!(f->flags&EDGE_B)) nhid = 1;
  else if (!(f->flags&EDGE_C)) nhid = 2;
  na = 2-nhid;
  basetv = (f->v[prevpt[nhid]]<f->v[nhid]) ? basic_tva : basic_tvb; 
  for (i=0; i<3; i++) {  
    tv[i] = basetv[na];
    na = nextpt[na];
  }
}

void AddMtlsRecursive(Mtl* material, std::shared_ptr<Hydra::MaterialTracker> tracker)
{
  if (material->ClassID().PartA() == MULTI_CLASS_ID)
  {
    (*tracker).AddMtl(material);
    int subMats = material->NumSubMtls();
    if (subMats != 0)
    {
      for (int j = 0; j < subMats; j++)
      {
        Mtl* subMtl = material->GetSubMtl(j);
        if(subMtl != nullptr)
          AddMtlsRecursive(subMtl, tracker);
      }
    }
    else
    {
      (*tracker).AddMtl(material);
    }
  }
  else
  {
    (*tracker).AddMtl(material);
  }
}

void hydraRender_mk3::PreProcess(INode* node, TimeValue t, bool isInMatEditor)
{

  bool rendThisNode = (GetCOREInterface13()->GetRendHidden() || !node->IsNodeHidden());
  //if (node->Renderable() && rendThisNode)
  //mtlList.AddMtl(node->GetMtl());

  const ObjectState os = node->EvalWorldState(t);

  if (os.obj != nullptr)
  {
    switch (os.obj->SuperClassID())
    {
    
    case SHAPE_CLASS_ID:
      rendThisNode = rendThisNode && static_cast<ShapeObject*>(os.obj)->GetRenderable();
    case GEOMOBJECT_CLASS_ID:
      if (node->Renderable() && rendThisNode)
      {
        Object* const object = node->GetObjOrWSMRef();
        bool objectAlreadyIn = false;

        auto nodeMtl = node->GetMtl();
        if(nodeMtl != nullptr)
          AddMtlsRecursive(nodeMtl, mtlTracker);
        //(*mtlTracker).AddMtl(node->GetMtl());
        for (auto it = geoNodesPools.begin(); it != geoNodesPools.end(); ++it)
        {
          if((*it)->GetReferencedObject() == object && (*it)->IsNodeCompatible(node, t))
          {
            objectAlreadyIn = true;
            (*it)->AddNode(node, t);
            break;
          }
        }

        if(!objectAlreadyIn && !isInMatEditor)
        {
          std::shared_ptr<Hydra::NodePool> newPool = std::make_shared<Hydra::NodePool>(node, MaxSDK::NotificationAPI::NotifierType_Node_Geom, t);
          geoNodesPools.push_back(newPool);
        }
      }
      break;
    case LIGHT_CLASS_ID:
      if (/*rendThisNode && */!isInMatEditor)
      {
        Object* const object = node->GetObjOrWSMRef();
        bool objectAlreadyIn = false;
        LightObject* light_object = static_cast<LightObject*>(os.obj);
        if (light_object != nullptr /*&& !light_object->IsRenderable()*/) //another check just in case
        {
          if (IsMrSun(light_object))
          {
            m_sunUseMrSky = true;
            m_sunSkyNameW = node->GetName();
          }

          for (auto it = lightNodesPools.begin(); it != lightNodesPools.end(); ++it)
          {
            if ((*it)->GetReferencedObject() == object && (*it)->IsNodeCompatible(node, t))
            {
              objectAlreadyIn = true;
              (*it)->AddNode(node, t);
              break;
            }
          }
          if (!objectAlreadyIn)
          {
            std::shared_ptr<Hydra::NodePool> newPool = std::make_shared<Hydra::NodePool>(node, MaxSDK::NotificationAPI::NotifierType_Node_Light, t);
            lightNodesPools.push_back(newPool);
          }
        }
      }
      break;
    }
  }
  

  for (int c = 0; c < node->NumberOfChildren(); c++) 
  {
    PreProcess(node->GetChildNode(c), t, isInMatEditor);
  }

}

std::vector<int32_t> createRemapList(const std::vector<int32_t> &repMatIDs, const std::vector<int32_t> &instanceMatIDs)
{
  std::vector<int32_t> remap_list;

  for (auto i = 0; i < repMatIDs.size(); ++i)
  {
    auto id_to_replace = repMatIDs.at(i);
    int id_replacement;
    
    if ((instanceMatIDs.size() != repMatIDs.size()) && instanceMatIDs.size() == 1) //multi material -> material
      id_replacement = instanceMatIDs.at(0);
    else
      id_replacement = instanceMatIDs.at(i);

    remap_list.push_back(id_to_replace);
    remap_list.push_back(id_replacement);
  }


  return remap_list;
}

void hydraRender_mk3::ExtractAndInstanceMeshes(int& geomObjNum, int& vertWritten, TimeValue t)
{
  vertWritten = 0;
  geomObjNum = 0;

  int exports = 0;

  for (auto it = geoNodesPools.begin(); it != geoNodesPools.end(); ++it)
  {
    INode* representative_node = (*it)->GetRepresentativeNode();

    if (representative_node == nullptr || (*it)->m_pool.size() == 0)
      continue;

    ObjectState os = representative_node->EvalWorldState(t);

    if (!os.obj || os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
      continue;

    if ((*it)->needExport)
    {
      HRMeshRef meshRef; 
      vertWritten += ExtractMeshNoIndexing(representative_node, t, meshRef);

      (*it)->needExport = false;
      if (meshRef.id != -1)
      {
        // vertWritten += ExtractMesh(representative_node, t, meshRef);
        meshRefLib[(*it)->GetReferencedObject()] = meshRef;
        exports++;
      }
    }

    auto repMat = representative_node->GetMtl();
    std::vector<int32_t> repMatIDs;

    if (repMat != nullptr)                     // fix #1 here
    {
      auto classID = repMat->ClassID().PartA();
                
      if (classID == MULTI_CLASS_ID)
      {
        int subMats = repMat->NumSubMtls();
        for (int j = 0; j < subMats; j++)
        {
          Mtl* subMtl = repMat->GetSubMtl(j);
          int mat_id = materialsRefLib[subMtl].id;
          if (mat_id == -1)
            mat_id = 0;

          repMatIDs.push_back(mat_id);  
        }
      }
      else
      {
        int mat_id = materialsRefLib[repMat].id;
        if (mat_id == -1)
          mat_id = 0;

        repMatIDs.push_back(mat_id);
      }
    }
    else
    {
      repMatIDs.push_back(0); // if nullptr; ok ? maybe
    }

    for (auto j = (*it)->m_pool.begin(); j != (*it)->m_pool.end(); ++j)
    {
      float matrixT[4][4];
      Matrix3 pivot = toMeters((*j)->GetObjectTM(t));
      Matrix3 matrixFinal;
      matrixFinal = TransformMatrixFromMax(pivot);
      MaxMatrix3ToFloat16V2(matrixFinal, &matrixT[0][0]);


      std::vector<int32_t> instanceMatIDs;
      auto instMat = (*j)->GetMtl();
      if (instMat != nullptr)             // fix #2 here
      {
        auto classID = instMat->ClassID().PartA();
        if (classID == MULTI_CLASS_ID)
        {
          int subMats = instMat->NumSubMtls();
          for (int j = 0; j < subMats; j++)
          {
            Mtl* subMtl = instMat->GetSubMtl(j);
            int mat_id = materialsRefLib[subMtl].id;
            if (mat_id == -1)
              mat_id = 0;

            if (rendParams.overrideGrayMatOn)
              instanceMatIDs.push_back(grayMat.id);
            else
              instanceMatIDs.push_back(mat_id);
          }
        }
        else
        {
          int mat_id = materialsRefLib[instMat].id;
          if (mat_id == -1)
            mat_id = 0;

          if (rendParams.overrideGrayMatOn)
            instanceMatIDs.push_back(grayMat.id);
          else
            instanceMatIDs.push_back(mat_id);
        }
      }
      else
      {
        if (rendParams.overrideGrayMatOn)
          instanceMatIDs.push_back(grayMat.id);
        else
          instanceMatIDs.push_back(0);
      }


      auto renderThisInstance = (GetCOREInterface13()->GetRendHidden() || !(*j)->IsNodeHidden());
      if (renderThisInstance)
      {
        if (instanceMatIDs == repMatIDs)
        {
          hrMeshInstance(currentScene, meshRefLib[(*it)->GetReferencedObject()], &matrixT[0][0]);
        }
        else
        {
          std::vector<int32_t> remap_list = createRemapList(repMatIDs, instanceMatIDs);
          hrMeshInstance(currentScene, meshRefLib[(*it)->GetReferencedObject()], &matrixT[0][0], &remap_list[0], remap_list.size());
        }

        geomObjNum += 1;
      }
    }
  }

  plugin_log.PrintValue("Exported unique meshes number", exports);
}



int hydraRender_mk3::ExtractMeshNoIndexing(INode* node, TimeValue t, HRMeshRef &meshRef)
{
  Matrix3 tm;
  if (node->GetObjTMAfterWSM(t).IsIdentity())
    tm = node->GetObjTMAfterWSM(t);
  else
    tm = node->GetObjTMBeforeWSM(t);// ->GetObjTMAfterWSM(t);

  ObjectState os = node->EvalWorldState(t);
  if (!os.obj || (os.obj->SuperClassID() != SHAPE_CLASS_ID && os.obj->SuperClassID() != GEOMOBJECT_CLASS_ID))
  {
    plugin_log.Print("Calling extract mesh on invalid object. This shouldn't happen");
    return 0; // Safety net. This shouldn't happen.
  }

  // Order of the vertices. Get 'em counter clockwise if the objects is negatively scaled.
  int vx1 = 0;
  int vx2 = 1;
  int vx3 = 2;

  BOOL negScale = TMNegParity(tm);

  Mesh* mesh = nullptr;
  BOOL delMesh = false;
  BOOL needDel = false;
  TriObject* tri = nullptr;


  std::vector<bool> offInViewport;
  std::vector<bool> offInRender;
  IDerivedObject* derObj = nullptr;
  int numModif = 0;
  
  if (os.obj->SuperClassID() == SHAPE_CLASS_ID)
  {
    ShapeObject* shape = static_cast<ShapeObject*>(os.obj);
    shape->GenerateMesh(t, GENMESH_RENDER, mesh);
    mesh = shape->GetRenderMesh(t, node, theView, delMesh);
  }
  else
  {
    // Check modify stack and apply "off in viewport" and " off in render" options.
    if (node->GetObjectRef()->SuperClassID() == GEN_DERIVOB_CLASS_ID)
    {
      derObj = (IDerivedObject*)node->GetObjectRef();

      numModif = derObj->NumModifiers();      
      offInViewport.resize(numModif);
      offInRender.resize(numModif);

      for (int i = 0; i < numModif; i++)
      {
        if (!derObj->GetModifier(i)->IsEnabledInViews()) // "off" in viewport 
        {
          offInViewport[i] = true;
          derObj->GetModifier(i)->EnableModInViews();
        }
        else if (!derObj->GetModifier(i)->IsEnabledInRender()) // "off" in render 
        {
          offInRender[i] = true;
          derObj->GetModifier(i)->DisableMod();
        }
      }
    }


    tri = GetTriObjectFromNode(node, t, needDel);
    if (tri == nullptr) return false;
    mesh = tri->GetRenderMesh(t, node, theView, delMesh);
  }

  // return modify options
  for (int i = 0; i < numModif; i++)
  {
    if (offInViewport[i])
      derObj->GetModifier(i)->DisableModInViews();
    if (offInRender[i])
    {
      // if modify "off"(DisableMod()), then DisableModInViews() and DisableModInRender() not work. First you need to enable EnableMod() for clear flags.
      derObj->GetModifier(i)->EnableMod();
      derObj->GetModifier(i)->DisableModInRender();
    }
  }

  if (mesh == nullptr)
    return false;

  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> texcoords;
  std::vector<int> indices;
  std::vector<int> materialIDs;

  int nVerts = mesh->numFaces * 3;

  if (nVerts == 0)
  {
    plugin_log.PrintValue("Empty mesh detected:", node->GetName());
    return false;
  }

  positions.reserve(nVerts * 4);
  normals.reserve(nVerts * 4);
  texcoords.reserve(nVerts * 2);
  indices.reserve(nVerts);
  materialIDs.reserve(mesh->numFaces);

  MeshNormalSpec* mesh_normal_spec = mesh->GetSpecifiedNormals();
  if (mesh_normal_spec != nullptr)
  {
    // Are any normals specified?
    if ((mesh_normal_spec->GetNumFaces() <= 0) || (mesh_normal_spec->GetNumNormals() <= 0))
    {
      // Don't use specified normals
      mesh_normal_spec = nullptr;
    }
  }


  Mtl* nodeMtl = node->GetMtl();
  
  std::vector<VertexNormal> calcNormals;

  CalculateNormals(mesh, calcNormals);

  for (int face_index = 0; face_index < mesh->numFaces; ++face_index)
  {
    Face& face = mesh->faces[face_index];

    //Material IDs
    const MtlID face_material_id = face.getMatID();
    if ((*mtlTracker).materials.size() != 0 && nodeMtl != nullptr)
    {
      int nodeSubMtls = nodeMtl->NumSubMtls();
      if (nodeSubMtls == 0 || nodeMtl->ClassID().PartA() == MIXMAT_CLASS_ID)
      {
        int mat_id = materialsRefLib[nodeMtl].id;
      
        if (mat_id == -1)
          mat_id = 0;
        
        materialIDs.push_back(mat_id);
      }
      else
      {
        int subMatId = face_material_id;
        Mtl* faceMat = nodeMtl->GetSubMtl(subMatId);

        if (faceMat == nullptr)
          materialIDs.push_back(0);
        else
        {
          int mat_id = materialsRefLib[faceMat].id;

          if (mat_id == -1)
            mat_id = 0;

          materialIDs.push_back(mat_id);
        }
      }
    }
    else
      materialIDs.push_back(0);


    //Indices
    int index0 = mesh->faces[face_index].v[vx1];
    int index1 = mesh->faces[face_index].v[vx2];
    int index2 = mesh->faces[face_index].v[vx3];

    indices.push_back(face_index * 3 + 0);
    indices.push_back(face_index * 3 + 1);
    indices.push_back(face_index * 3 + 2);

    Matrix3 conv = getConversionMatrix();

    Matrix3 normM = conv;
    normM.Invert();

    //Normals
    Point3 vertex_normal0, vertex_normal1, vertex_normal2;
    if (face.smGroup != 0)
    {
      vertex_normal0 = calcNormals.at(index0).GetNormal(face.smGroup).Normalize() * conv;
      vertex_normal1 = calcNormals.at(index1).GetNormal(face.smGroup).Normalize() * conv;
      vertex_normal2 = calcNormals.at(index2).GetNormal(face.smGroup).Normalize() * conv;
    }
    else
    {
      const Point3& v0 = mesh->verts[face.v[0]];
      const Point3& v1 = mesh->verts[face.v[1]];
      const Point3& v2 = mesh->verts[face.v[2]];
      Point3 faceNormal = ((v1 - v0) ^ (v2 - v1)).Normalize();
      vertex_normal0 = faceNormal.Normalize() * conv;
      vertex_normal1 = faceNormal.Normalize() * conv;
      vertex_normal2 = faceNormal.Normalize() * conv;
    }

    
    normals.push_back(vertex_normal0.x);
    normals.push_back(vertex_normal0.y);
    normals.push_back(vertex_normal0.z);
    normals.push_back(1.0f);

    normals.push_back(vertex_normal1.x);
    normals.push_back(vertex_normal1.y);
    normals.push_back(vertex_normal1.z);
    normals.push_back(1.0f);

    normals.push_back(vertex_normal2.x);
    normals.push_back(vertex_normal2.y);
    normals.push_back(vertex_normal2.z);
    normals.push_back(1.0f);

    

    //Positions
    Point3 position0 = toMeters(mesh->verts[index0]) * conv;
    Point3 position1 = toMeters(mesh->verts[index1]) * conv;
    Point3 position2 = toMeters(mesh->verts[index2]) * conv;

    positions.push_back(position0.x);
    positions.push_back(position0.y);
    positions.push_back(position0.z);
    positions.push_back(1.0f);

    positions.push_back(position1.x);
    positions.push_back(position1.y);
    positions.push_back(position1.z);
    positions.push_back(1.0f);

    positions.push_back(position2.x);
    positions.push_back(position2.y);
    positions.push_back(position2.z);
    positions.push_back(1.0f);


    //Texture coordinates

    const UVVert* const map_verts = mesh->tVerts;
    const TVFace* const map_faces = mesh->tvFace;
    if ((map_verts != nullptr) && (map_faces != nullptr))
    {
      auto uvw0 = map_verts[map_faces[face_index].t[0]];
      auto uvw1 = map_verts[map_faces[face_index].t[1]];
      auto uvw2 = map_verts[map_faces[face_index].t[2]];

      texcoords.push_back(uvw0.x);
      texcoords.push_back(uvw0.y);

      texcoords.push_back(uvw1.x);
      texcoords.push_back(uvw1.y);

      texcoords.push_back(uvw2.x);
      texcoords.push_back(uvw2.y);

    }

  }

  plugin_log.PrintValue("Mesh", std::wstring(node->GetName()).c_str());
  plugin_log.PrintValue("Positions", positions.size());
  plugin_log.PrintValue("Normals", normals.size());
  plugin_log.PrintValue("TexCoords", texcoords.size());
  plugin_log.PrintValue("Indices", indices.size());
  plugin_log.PrintValue("Material IDs", materialIDs.size());


  meshRef = hrMeshCreate(node->GetName());

  hrMeshOpen(meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);
  {
    hrMeshVertexAttribPointer4f(meshRef, L"pos", &positions[0]);
    hrMeshVertexAttribPointer4f(meshRef, L"norm", &normals[0]);
    hrMeshVertexAttribPointer2f(meshRef, L"texcoord", &texcoords[0]);

    //hrMeshMaterialId(meshRef, materialIDs[0]);
    hrMeshPrimitiveAttribPointer1i(meshRef, L"mind", &materialIDs[0]);
    hrMeshAppendTriangles3(meshRef, int(indices.size()), &indices[0]);
  }
  hrMeshClose(meshRef);



  if (delMesh)
  {
    mesh->DeleteThis();
    mesh = nullptr;
  }

  if (tri != nullptr && needDel)
  {
    tri->DeleteThis();
    tri = nullptr;
  }

  return nVerts;
}


/////////////////////////////////////////////////////////////
// Old functions


//bool hydraRender_mk3::TraverseNodeTree(INode* node, int& geomObjNum, int& vertWritten, TimeValue t)
//{
//  if (exportSelected && node->Selected() == FALSE)
//    return TREE_CONTINUE;
//
//
//  // Only export if exporting everything or it's selected
//  if ((!exportSelected || node->Selected()) && node->Renderable())
//  {
//    ObjectState os;
//    os = node->EvalWorldState(t);  // call EvalWorldState() to evaluate the object at end of the pipeline
//
//    if (os.obj)
//    {
//      switch (os.obj->SuperClassID())
//      {
//
//      case GEOMOBJECT_CLASS_ID:
//      case SHAPE_CLASS_ID:
//      {
//        bool rendThisNode = (GetCOREInterface13()->GetRendHidden() || !node->IsNodeHidden());
//        if (rendThisNode && os.obj->IsRenderable() && node->Renderable())
//          ExtractGeomObject(node, t);
//        break;
//      }
//
//      case LIGHT_CLASS_ID:
//      {
//      //  ExtractLightObject(node, t);
//        break;
//      }
//      default:
//        break;
//      }
//    }
//  }
//
//  // traverse node tree recursively 
//  for (int c = 0; c < node->NumberOfChildren(); c++) {
//    if (!TraverseNodeTree(node->GetChildNode(c), geomObjNum, vertWritten, t))
//      return false;
//  }
//
//  return true;
//}


//void hydraRender_mk3::ExtractGeomObject(INode* node, TimeValue t)
//{
//  ObjectState os = node->EvalWorldState(t);
//  if (!os.obj)
//    return;
//
//  if (os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
//    return;
//
//
//  HRMeshRef meshRef = hrMeshCreate(node->GetName());
//  
//  ExtractMesh(node, t, meshRef);
//
//  float matrixT[4][4];
//  Matrix3 pivot = toMeters(node->GetObjectTM(t));
//  Matrix3 test(1);
//  Matrix3 conv = getConversionMatrix();
//
//  //pivot = pivot * conv;
//
//  MaxMatrix3ToFloat16MagicSwap(pivot, &matrixT[0][0]);
//
//  hrMeshInstance(currentScene, meshRef, &matrixT[0][0]);
//}


//int hydraRender_mk3::ExtractMesh(INode* node, TimeValue t, const HRMeshRef &meshRef)
//{
//  Matrix3 tm;
//  if (node->GetObjTMAfterWSM(t).IsIdentity())
//    tm = node->GetObjTMAfterWSM(t);
//  else
//    tm = node->GetObjTMBeforeWSM(t);// ->GetObjTMAfterWSM(t);
//
//  ObjectState os = node->EvalWorldState(t);
//  if (!os.obj || (os.obj->SuperClassID() != SHAPE_CLASS_ID && os.obj->SuperClassID() != GEOMOBJECT_CLASS_ID))
//  {
//    plugin_log.Print("Calling extract mesh on invalid object. This shouldn't happen");
//    return 0; // Safety net. This shouldn't happen.
//  }
//
//  // Order of the vertices. Get 'em counter clockwise if the objects is negatively scaled.
//  int vx1 = 0;
//  int vx2 = 1;
//  int vx3 = 2;
//
//  BOOL negScale = TMNegParity(tm);
//
//  Mesh* mesh = nullptr;
//  BOOL delMesh = false;
//  BOOL needDel = false;
//  TriObject* tri = nullptr;
//
//  if (os.obj->SuperClassID() == SHAPE_CLASS_ID)
//  {
//    ShapeObject* shape = (ShapeObject*)os.obj;
//
//    shape->GenerateMesh(t, GENMESH_RENDER, mesh);
//    mesh = shape->GetRenderMesh(t, node, theView, delMesh);
//  }
//  else
//  {
//    tri = GetTriObjectFromNode(node, t, needDel);
//
//    if (tri == nullptr)
//      return false;
//
//    mesh = tri->GetRenderMesh(t, node, theView, delMesh);
//  }
//
//  if (mesh == nullptr)
//    return false;
//
//  std::vector<float> positions;
//  std::vector<float> normals;
//  std::vector<float> texcoords;
//  std::vector<int> indices;
//  std::vector<int> materialIDs;
//
//  int nVerts = mesh->numVerts;
//
//  positions.resize(nVerts * 4);
//  normals.resize(nVerts * 4);
//  texcoords.resize(nVerts * 2);
//  indices.reserve(nVerts * 3);  
//  materialIDs.reserve(mesh->numFaces);
//
//  MeshNormalSpec* mesh_normal_spec = mesh->GetSpecifiedNormals();
//  if (mesh_normal_spec != nullptr)
//  {
//    // Are any normals specified?
//    if ((mesh_normal_spec->GetNumFaces() <= 0) || (mesh_normal_spec->GetNumNormals() <= 0))
//    {
//      // Don't use specified normals
//      mesh_normal_spec = nullptr;
//    }
//  }
//
//
//  Mtl* nodeMtl = node->GetMtl();
//
//  std::vector<VertexNormal> calcNormals;
//
//  CalculateNormals(mesh, calcNormals);
//
//  for (int face_index = 0; face_index < mesh->numFaces; ++face_index)
//  {
//    Face& face = mesh->faces[face_index];
//
//    //Material IDs
//    const MtlID face_material_id = face.getMatID();
//    if ((*mtlTracker).materials.size() != 0 && nodeMtl != nullptr)
//    {
//      int nodeSubMtls = nodeMtl->NumSubMtls();
//      if (nodeSubMtls == 0 || nodeMtl->ClassID().PartA() == MIXMAT_CLASS_ID)
//      {
//        int mat_id = materialsRefLib[nodeMtl].id;
//        if (mat_id == -1)
//          mat_id = 0;
//
//        materialIDs.push_back(mat_id);
//      }
//      else
//      {
//        int subMatId = face_material_id;
//        Mtl* faceMat = nodeMtl->GetSubMtl(subMatId);
//
//        if (faceMat == nullptr)
//          materialIDs.push_back(0);
//        else
//        {
//          int mat_id = materialsRefLib[nodeMtl].id;
//          if (mat_id == -1)
//            mat_id = 0;
//
//          materialIDs.push_back(mat_id);
//        }
//      }
//    }
//    else
//      materialIDs.push_back(0);
//  
//
//    //Indices
//    int index0 = mesh->faces[face_index].v[vx1];
//    int index1 = mesh->faces[face_index].v[vx2];
//    int index2 = mesh->faces[face_index].v[vx3];
//
//    indices.push_back(index0);
//    indices.push_back(index1);
//    indices.push_back(index2);
//
//    Matrix3 conv = getConversionMatrix();
//
//    Matrix3 normM = conv;
//    normM.Invert();
//
//    //Normals
//    Point3 vertex_normal0 = getVertexNormal((*mesh), mesh_normal_spec, face_index, vx1) * conv;
//    Point3 vertex_normal1 = getVertexNormal((*mesh), mesh_normal_spec, face_index, vx2) * conv;
//    Point3 vertex_normal2 = getVertexNormal((*mesh), mesh_normal_spec, face_index, vx3) * conv;
//    
// 
//    normals.at(index0 * 4 + 0) = vertex_normal0.x;
//    normals.at(index0 * 4 + 1) = vertex_normal0.y;
//    normals.at(index0 * 4 + 2) = vertex_normal0.z;
//    normals.at(index0 * 4 + 3) = 1.0f;
//
//    normals.at(index1 * 4 + 0) = vertex_normal1.x;
//    normals.at(index1 * 4 + 1) = vertex_normal1.y;
//    normals.at(index1 * 4 + 2) = vertex_normal1.z;
//    normals.at(index1 * 4 + 3) = 1.0f;
//
//    normals.at(index2 * 4 + 0) = vertex_normal2.x;
//    normals.at(index2 * 4 + 1) = vertex_normal2.y;
//    normals.at(index2 * 4 + 2) = vertex_normal2.z;
//    normals.at(index2 * 4 + 3) = 1.0f;
//
//
//    //Positions
//    Point3 position0 = toMeters(mesh->verts[index0]) * conv;
//    Point3 position1 = toMeters(mesh->verts[index1]) * conv;
//    Point3 position2 = toMeters(mesh->verts[index2]) * conv;
//
//    positions.at(index0 * 4 + 0) = position0.x;
//    positions.at(index0 * 4 + 1) = position0.y;
//    positions.at(index0 * 4 + 2) = position0.z;
//    positions.at(index0 * 4 + 3) = 1.0f;
//
//    positions.at(index1 * 4 + 0) = position1.x;
//    positions.at(index1 * 4 + 1) = position1.y;
//    positions.at(index1 * 4 + 2) = position1.z;
//    positions.at(index1 * 4 + 3) = 1.0f;
//
//    positions.at(index2 * 4 + 0) = position2.x;
//    positions.at(index2 * 4 + 1) = position2.y;
//    positions.at(index2 * 4 + 2) = position2.z;
//    positions.at(index2 * 4 + 3) = 1.0f;
//
//
//    //Texture coordinates
//
//    const UVVert* const map_verts = mesh->tVerts;
//    const TVFace* const map_faces = mesh->tvFace;
//    if ((map_verts != nullptr) && (map_faces != nullptr))
//    {
//      auto uvw0 = map_verts[map_faces[face_index].t[0]];
//      auto uvw1 = map_verts[map_faces[face_index].t[1]];
//      auto uvw2 = map_verts[map_faces[face_index].t[2]];
//
//      texcoords.at(index0 * 2 + 0) = uvw0.x;
//      texcoords.at(index0 * 2 + 1) = uvw0.y;
//
//      texcoords.at(index1 * 2 + 0) = uvw1.x;
//      texcoords.at(index1 * 2 + 1) = uvw1.y;
//
//      texcoords.at(index2 * 2 + 0) = uvw2.x;
//      texcoords.at(index2 * 2 + 1) = uvw2.y;
//
//    }
//
//  }
//
//  plugin_log.PrintValue("Mesh", std::wstring(node->GetName()).c_str());
//  plugin_log.PrintValue("Positions", positions.size());
//  plugin_log.PrintValue("Normals", normals.size());
//  plugin_log.PrintValue("TexCoords", texcoords.size());
//  plugin_log.PrintValue("Indices", indices.size());
//  plugin_log.PrintValue("Material IDs", materialIDs.size());
//  
//
//
//  hrMeshOpen(meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);
//  {
//    hrMeshVertexAttribPointer4f(meshRef, L"pos", &positions[0]);
//    hrMeshVertexAttribPointer4f(meshRef, L"norm", &normals[0]);
//    hrMeshVertexAttribPointer2f(meshRef, L"texcoord", &texcoords[0]);
//
//    //hrMeshMaterialId(meshRef, materialIDs[0]);
//    hrMeshPrimitiveAttribPointer1i(meshRef, L"mind", &materialIDs[0]);
//    hrMeshAppendTriangles3(meshRef, int(indices.size()), &indices[0]);
//  }
//  hrMeshClose(meshRef);
//
//
//
//  if (delMesh)
//  {
//    mesh->DeleteThis();
//    mesh = nullptr;
//  }
//
//  if (tri != nullptr && needDel)
//  {
//    tri->DeleteThis();
//    tri = nullptr;
//  }
//
//  return nVerts;
//}

/////////////////////////////////////////////////////////////
