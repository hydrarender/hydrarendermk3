#include "hydraRender mk3.h"

#include <MeshNormalSpec.h>


float weightOfEdges(Point3 edgeA, Point3 edgeB)
{
  const float len = ::Length(edgeA^edgeB);

  edgeA = ::Normalize(edgeA);
  edgeB = ::Normalize(edgeB);

  float dp = DotProd(edgeA,edgeB);
  
  if (dp > 1.0f) 
    dp=1.0f; // shouldn't happen, but might
  if (dp < -1.0f) 
    dp=-1.0f; // shouldn't happen, but might

  return len*fabsf(acosf(dp));
}


void hydraRender_mk3::CalculateNormals(Mesh* mesh, std::vector<VertexNormal> &normals)
{
  // geom->vnorms2
  normals.resize(mesh->getNumVerts());

  Point3* verts = mesh->verts;

  for (size_t i = 0; i < normals.size(); i++)
  {
    normals[i] = MaxSDK::VertexNormal(Point3(0, 0, 0), 0);
    normals[i].init = false;
  }

  for (int i = 0; i < mesh->getNumFaces(); i++)
  {
    Face* face = mesh->faces + i;

    Point3 v0, v1, v2;
    v0 = verts[face->v[0]];
    v1 = verts[face->v[1]];
    v2 = verts[face->v[2]];

    Point3 edgeA1, edgeB1;
    edgeA1 = v1 - v0;
    edgeB1 = v2 - v0;

    Point3 edgeA2, edgeB2;
    edgeA2 = v0 - v1;
    edgeB2 = v2 - v1;

    Point3 edgeA3, edgeB3;
    edgeA3 = v0 - v2;
    edgeB3 = v1 - v2;

    Point3 fNormal;
    fNormal = ::Normalize(edgeA1^edgeB1);

    const float w1 = fmax(weightOfEdges(edgeA1, edgeB1), 1e-5f);
    const float w2 = fmax(weightOfEdges(edgeA2, edgeB2), 1e-5f);
    const float w3 = fmax(weightOfEdges(edgeA3, edgeB3), 1e-5f);

    normals[face->v[0]].AddNormal(w1*fNormal, face->smGroup);
    normals[face->v[1]].AddNormal(w2*fNormal, face->smGroup);
    normals[face->v[2]].AddNormal(w3*fNormal, face->smGroup);
  }
}


void hydraRender_mk3::CalcMaxNormalsAsInExample2(Mesh* mesh, GeometryObj* geom, Matrix3 tm)
{
  // geom->vnorms2
  geom->vnorms2.resize(mesh->getNumVerts());
  geom->faceV.resize(mesh->getNumFaces()*3);

  Point3* verts = mesh->verts;

  for (size_t i = 0; i < geom->vnorms2.size(); i++)
  {
    geom->vnorms2[i] = MaxSDK::VertexNormal(Point3(0, 0, 0), 0);
    geom->vnorms2[i].init = false;
  }

  for (int i = 0; i < mesh->getNumFaces(); i++)
  {
    Face* face = mesh->faces + i;

    Point3 v0, v1, v2;
    v0 = tm*verts[face->v[0]];
    v1 = tm*verts[face->v[1]];
    v2 = tm*verts[face->v[2]];

    geom->faceV[i * 3 + 0] = face->v[0];
    geom->faceV[i * 3 + 1] = face->v[1];
    geom->faceV[i * 3 + 2] = face->v[2];

    Point3 edgeA1, edgeB1;
    edgeA1   = v1 - v0;
    edgeB1   = v2 - v0;

    Point3 edgeA2, edgeB2;
    edgeA2 = v0 - v1;
    edgeB2 = v2 - v1;

    Point3 edgeA3, edgeB3;
    edgeA3 = v0 - v2;
    edgeB3 = v1 - v2;

    Point3 fNormal;
    fNormal = ::Normalize(edgeA1^edgeB1);

    const float w1 = fmax(weightOfEdges(edgeA1, edgeB1), 1e-5f);
    const float w2 = fmax(weightOfEdges(edgeA2, edgeB2), 1e-5f);
    const float w3 = fmax(weightOfEdges(edgeA3, edgeB3), 1e-5f);

    geom->vnorms2[face->v[0]].AddNormal(w1*fNormal, face->smGroup);
    geom->vnorms2[face->v[1]].AddNormal(w2*fNormal, face->smGroup);
    geom->vnorms2[face->v[2]].AddNormal(w3*fNormal, face->smGroup);
  }
}

void hydraRender_mk3::ExtractUserDefinedNormals(Mesh* mesh, GeometryObj* geom, Matrix3 tm)
{
  if(mesh == NULL)
    return;

  int vx = 0;
  int vy = 1;
  int vz = 2;

  geom->normsSpecified.resize(0);
  geom->normsSpecIndices.resize(0);

  MeshNormalSpec* normalSpec = mesh->GetSpecifiedNormals();

  if(normalSpec == NULL)
    return;

  if(normalSpec->GetNumNormals() == 0)
    return;

  /////////////////////////////////////////////////// --> original OpenCollada code
  /*if(normalSpec == NULL)
  {
    mesh->SpecifyNormals();
    normalSpec = mesh->GetSpecifiedNormals();
  }

  if( normalSpec->GetNumNormals() == 0 )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}*/
  ////////////////////////////////////////////////////////////////////////////////////////////////

  bool isSetMESH_NORMAL_NORMALS_BUILT    = normalSpec->GetFlag(MESH_NORMAL_NORMALS_BUILT);
	bool isSetMESH_NORMAL_NORMALS_COMPUTED = normalSpec->GetFlag(MESH_NORMAL_NORMALS_COMPUTED);
	bool isSetMESH_NORMAL_MODIFIER_SUPPORT = normalSpec->GetFlag(MESH_NORMAL_MODIFIER_SUPPORT);

  if( !isSetMESH_NORMAL_NORMALS_BUILT || !isSetMESH_NORMAL_NORMALS_COMPUTED )
    return;

  /////////////////////////////////////////////////// --> original OpenCollada code
  /*
	if( !isSetMESH_NORMAL_NORMALS_BUILT || !isSetMESH_NORMAL_NORMALS_COMPUTED )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}

	if( normalSpec->GetNumNormals() == 0 )
	{
		normalSpec->SetParent( mesh );
		normalSpec->CheckNormals();
	}*/

	int normalCount = normalSpec->GetNumNormals();

  if(normalCount == 0)
	 	return;

  geom->normsSpecified.resize(normalCount*3);
  
  Matrix3 tmRot = tm;

  tmRot.SetTranslate(Point3(0,0,0));

  Point3 normal;
  for( int i = 0; i < normalCount; ++i )
  {
  	normal = tmRot*normalSpec->Normal(i);
    normal = ::Normalize(normal);

    geom->normsSpecified[i*3+0] = normal.x;
    geom->normsSpecified[i*3+1] = normal.y;
    geom->normsSpecified[i*3+2] = normal.z;
  }


  geom->normsSpecIndices.resize(mesh->getNumFaces()*3);

  for(int faceIndex=0; faceIndex < mesh->getNumFaces(); faceIndex++)
  {
    //for(int vertexIndex = 0; vertexIndex < 3; vertexIndex++ )
    //{
    //  int normalIndex = normalSpec->GetNormalIndex(faceIndex, vertexIndex);
    //  geom->normsSpecIndices[faceIndex*3 + vertexIndex] = normalIndex;
    //}

    int normalIndex1 = normalSpec->GetNormalIndex(faceIndex, 0);
    int normalIndex2 = normalSpec->GetNormalIndex(faceIndex, 1);
    int normalIndex3 = normalSpec->GetNormalIndex(faceIndex, 2);

    geom->normsSpecIndices[faceIndex*3 + 0] = normalIndex1;
    geom->normsSpecIndices[faceIndex*3 + 1] = normalIndex2;
    geom->normsSpecIndices[faceIndex*3 + 2] = normalIndex3;
  }

}


const Point3 hydraRender_mk3::getVertexNormal(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index)
{
  // Get the vertex normal as specified by the mesh
  const Point3 vertex_normal = getVertexNormalUnchecked(mesh, mesh_normal_spec, face_index, face_vertex_index);

  // Check that the normal is valid
  const float normal_length = vertex_normal.FLength();
  if (fabsf(normal_length - 1.0f) < 1.0e-4f)   // already correctly normalized?
  {
    return vertex_normal;
  }
  else if (normal_length > 1.0e-3f)
  {
    // Not properly normalized: normalize it
    return vertex_normal.Normalize();
  }
  else
  {
    // Normal is degenerate: use geometric normal instead
    const Face& face = mesh.faces[face_index];
    const Point3& v0 = mesh.verts[face.v[0]];
    const Point3& v1 = mesh.verts[face.v[1]];
    const Point3& v2 = mesh.verts[face.v[2]];
    return Point3((v1 - v0) ^ (v2 - v1)).Normalize();
  }
}

const Point3 hydraRender_mk3::getVertexNormalUnchecked(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index)
{
  if (mesh_normal_spec == nullptr)
  {
    // No specified normals interface: fetch normals in the good old way
    Face& face = mesh.faces[face_index];
    const int vertex_index = face.v[face_vertex_index];
    RVertex& rVert = mesh.getRVert(vertex_index);

    if (rVert.rFlags & SPECIFIED_NORMAL)
    {
      // A single normal, specified
      return rVert.rn.getNormal();
    }
    else
    {
      const int num_normals = (rVert.rFlags & NORCT_MASK);
      for (int normal_index = 0; normal_index < num_normals; ++normal_index)
      {
        RNormal& rNormal = (num_normals == 1) ? rVert.rn : rVert.ern[normal_index];
        if (face.getSmGroup() & rNormal.getSmGroup())
        {
          // This normal matches the face's smoothing group: use it
          return rNormal.getNormal();
        }
      }

      // Matching smoothing group not found: use the face normal
      return mesh.getFaceNormal(face_index);
    }
  }
  else
  {
    // Use the specified normals interface

    const int specNormalIndex = mesh_normal_spec->GetNormalIndex(face_index, face_vertex_index);
    const int numNormals = mesh_normal_spec->GetNumNormals();
    DbgAssert(numNormals > 0 && specNormalIndex >= 0 && specNormalIndex < numNormals);
    if ((specNormalIndex >= 0) && (specNormalIndex < numNormals))
    {
      return mesh_normal_spec->Normal(specNormalIndex);
    }
    else
    {
      DbgAssert(false);
      return mesh.getFaceNormal(face_index);
    }
  }
}


Point3 hydraRender_mk3::ComputeSmGroupNormal(const std::vector<int>& faceIndeces, int faceNum, const std::vector<int> &face_smoothgroups, float *face_normals)
{
  Point3 v(0, 0, 0);
  int counter = 0;

  Point3 faceNorm;
  faceNorm.x = face_normals[faceNum * 3 + 0];
  faceNorm.y = face_normals[faceNum * 3 + 1];
  faceNorm.z = face_normals[faceNum * 3 + 2];

  v = faceNorm;

  for (int i = 0; i < faceIndeces.size(); i++)
  {
    Point3 norm;
    norm.x = face_normals[faceIndeces[i] * 3 + 0];
    norm.y = face_normals[faceIndeces[i] * 3 + 1];
    norm.z = face_normals[faceIndeces[i] * 3 + 2];

    if (face_smoothgroups[faceNum] == face_smoothgroups[faceIndeces[i]] && v%norm > 0.173648f) // if angle is less than 80 degrees 
    {
      v.x += norm.x;
      v.y += norm.y;
      v.z += norm.z;
      counter++;
    }

  }

  v = v*(1.0f / float(counter));
  return ::Normalize(v);
}