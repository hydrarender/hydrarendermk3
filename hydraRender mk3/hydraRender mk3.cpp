//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Appwizard generated plugin
// AUTHOR: ALICE
//***************************************************************************/


#include "hydraRender mk3.h"
#include "3dsmaxport.h"
#include "gamma.h"
#include "HR_HDRImage.h"

//#include "path.h"
//#include "AssetType.h"
#include "IFileResolutionManager.h"
#include <ctime>
#include <mutex>
#include <future>
#include <chrono> 
#include <IPathConfigMgr.h>

// #include <cmdmode.h>
#include "HydraRenderDriverAPI.h"
#include "qmc_sobol_niederreiter.h"




IHRRenderDriver* CreateDriverRTE(const wchar_t* a_cfg) { return nullptr; }

extern HINSTANCE hInstance;
std::mutex matMutex;

std::string g_layerName;
int g_sessionId;

HydraRenderParamDlg* hydraRender_mk3::m_pLastParamDialog = nullptr;
int hydraRender_mk3::m_instanceCounters                  = 0;
HRRenderRef hydraRender_mk3::m_rendRef                   = HRRenderRef();

HydraRenderParams hydraRender_mk3::m_lastRendParams;
HydraLogger hydraRender_mk3::plugin_log;



hydraRender_mk3::hydraRender_mk3()
{
  rendParams.renderer = this;
  m_pScanlineRender = nullptr;

  plugin_log.OpenLogFile("C:/[Hydra]/logs/plugin_log.txt");
  plugin_log.OpenLogFileM("C:/[Hydra]/logs/material_log.txt");

	plugin_log.Print("!");
  m_paramTrace.open("C:/[Hydra]/pluginFiles/paramTrace.txt");

	convertMatScriptPath = "c:/[hydra]/pluginFiles/ConvertMat.mcr";
  convertLightsScriptPath = "c:/[hydra]/pluginFiles/ConvertLights.ms";

  if(m_instanceCounters == 0)
    hrInit(L"-sort_indices 1"); //TODO: turn sorting off for small speedup

  mtlTracker = std::make_shared<Hydra::MaterialTracker>();
  texTracker = std::make_unique<Hydra::TextureTracker>(mtlTracker);

  envTex = texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");

  geoInstancesLib.clear();
  geoNodesPools.clear();
  lightInstancesLib.clear();
  lightNodesPools.clear();
  texmapLib.clear();
  meshRefLib.clear();
  lightsRefLib.clear();
  materialsRefLib.clear();


	last_render = nullptr;

  if(hydra_out_render.id == -1)
    hydra_out_render = hrFBICreate(L"out", 1, 1, 16);
  
  if (hydra_raw_render.id == -1)
    hydra_raw_render = hrFBICreate(L"framebuffer", 1, 1, 16);

  nKeyFrameStep = 5;
  nMeshFrameStep = 5;
	nStaticFrame = GetCOREInterface()->GetTime();

  mtlEditorWasOpen = FALSE;

	pScene  = nullptr;
	pVnode  = nullptr;
  nodesToBake.reserve(100);

  if (m_instanceCounters == 0)
  {
    ClearTempFolder();

    hrSceneLibraryOpen(L"C:/[Hydra]/pluginFiles/scenelib", HR_WRITE_DISCARD);
    m_rendRef     = hrRenderCreate(L"HydraModern");
    m_firstRender = true;

#ifdef SILENT_DEVICE_ID
		hydraRender_mk3::m_lastRendParams.device_id.push_back(3); // your device ID
		hydraRender_mk3::m_lastRendParams.engine_type = 1;        // 1 - OpenCL; 0 - CUDA
		hydraRender_mk3::m_lastRendParams.liteMode    = false;
#else
		if (!readAndCheckDeviceSettingsFile(hydraRender_mk3::m_lastRendParams.engine_type, hydraRender_mk3::m_lastRendParams.device_id))
			UserSelectDeviceDialog(nullptr);
		else
			rendParams.rememberDevice = true;
#endif 

  }

  // Create gray material for override all materials from GUI render setup
  grayMat = hrMaterialCreate(L"grayOverrideMat");
  hrMaterialOpen(grayMat, HR_WRITE_DISCARD);
  {
    auto matNode = hrMaterialParamNode(grayMat);
    auto diff = matNode.append_child(L"diffuse");

    diff.append_attribute(L"brdf_type").set_value(L"lambert");
    diff.append_child(L"color").append_attribute(L"val") = L"0.5 0.5 0.5";
  }
  hrMaterialClose(grayMat);


  m_instanceCounters++;
}

extern bool g_materialProcessStart;

hydraRender_mk3::~hydraRender_mk3()
{
  m_instanceCounters--;

  if ((m_instanceCounters) == 0)
  {
    if (m_pScanlineRender != nullptr)
     m_pScanlineRender->DeleteThis();

    hrDestroy();
  }
}

#ifdef MAX2017
bool hydraRender_mk3::IsStopSupported() const
{
	return false;
}

void hydraRender_mk3::StopRendering()
{
	// Do nothing
}

Renderer::PauseSupport hydraRender_mk3::IsPauseSupported() const
{
	return PauseSupport::None;
}

void hydraRender_mk3::PauseRendering()
{
	// Do nothing
}

void hydraRender_mk3::ResumeRendering()
{
	// Do nothing
}

bool hydraRender_mk3::HasRequirement(const Requirement requirement)
{
  switch (requirement)
  {
  case kRequirement_Wants32bitFPOutput:
    // Need floating-point buffer
    return true;
  /*case kRequirement_SupportsConcurrentRendering:
     concurrent MEdit/ActiveShade renders
    return false;*/
  default:
    // Don't care about other requirements
    return false;
  }
}

IInteractiveRender* hydraRender_mk3::GetIInteractiveRender()
{
	return nullptr;
}

void hydraRender_mk3::GetVendorInformation(MSTR& info) const
{
	info = ClassName();
}

void hydraRender_mk3::GetPlatformInformation(MSTR& info) const
{
	// nothing
}

bool hydraRender_mk3::CompatibleWithAnyRenderElement()  const
{
	return false;
}

bool hydraRender_mk3::CompatibleWithRenderElement(IRenderElement& pIRenderElement) const
{
	return false;
}
#endif


int hydraRender_mk3::Open(INode *scene, INode *vnode, ViewParams* viewPar, RendParams& rpar, HWND hwnd, DefaultLight* defaultLights, int numDefLights, RendProgressCallback* prog)
{
  plugin_log.Print("Entering HydraRender::Open");

  //if (rpar.GetRenderElementMgr() != 0)
    //plugin_log.PrintValue("GetRenderElementMgr()::NumRenderElements()", rpar.GetRenderElementMgr()->NumRenderElements() );

  //if (GetCOREInterface()->GetCurRenderElementMgr() != 0)
    //plugin_log.PrintValue("GetCurRenderElementMgr()::NumRenderElements()", GetCOREInterface()->GetCurRenderElementMgr()->NumRenderElements());


  // Important!! This has to be done here in MAX Release 2!
  // Also enable it again in Renderer::Close()
  GetCOREInterface()->DisableSceneRedraw();

  if (!rpar.inMtlEdit)
    BroadcastNotification(NOTIFY_PRE_RENDER, static_cast<void*>(&rpar));	// 

  // Get options from RenderParams
  // These are the options that are common to all renderers
  rendParams.bVideoColorCheck = rpar.colorCheck;
  rendParams.bForce2Sided     = rpar.force2Side;
  rendParams.bRenderHidden    = rpar.rendHidden;
  rendParams.bSuperBlack      = rpar.superBlack;
  rendParams.bRenderFields    = rpar.fieldRender;
  rendParams.bNetRender       = rpar.isNetRender;
  rendParams.rendType         = rpar.rendType;
	
	rendParams.inMtlEditor = rpar.inMtlEdit;

  // Default lights
  rendParams.nNumDefLights  = numDefLights;
  rendParams.pDefaultLights = defaultLights;

  // Support Render effects
  rendParams.effect = rpar.effect;

  rendParams.devWidth  = rpar.width;
  rendParams.devHeight = rpar.height;

  rendParams.nMinx     = 0;
  rendParams.nMiny     = 0;
  rendParams.nMaxx     = rendParams.devWidth;
  rendParams.nMaxy     = rendParams.devHeight;

	pScene = scene;
	pVnode = vnode;

	if (viewPar)
		view = *viewPar;

  nodesToBake.clear();
  FindNodesToBake(pScene, &nodesToBake); // for further usage (if baking mode is enabled)
  //InspectBakeProps(nodesToBake);

	//Animation
	rendParams.rendTimeType = GetCOREInterface()->GetRendTimeType();
	rendParams.rendStart    = GetCOREInterface()->GetRendStart();
	rendParams.rendEnd      = GetCOREInterface()->GetRendEnd();
	rendParams.nthFrame     = GetCOREInterface()->GetRendNThFrame();
	rendParams.rendSaveFile = GetCOREInterface()->GetRendSaveFile();

	/*
	IRenderElementMgr *rendElem = rpar.GetRenderElementMgr();
	int num_elems = rendElem->NumRenderElements();

	for (int i = 0; i < num_elems; ++i)
	{
		IRenderElement *elem = rendElem->GetRenderElement(i);
		plugin_log.PrintValue("Render Element:", elem->GetName());
	}
	*/
  
	theView.pRendParams = &rendParams;
  

	BitmapInfo bi;

	if (rendParams.rendSaveFile)
		bi = GetCOREInterface()->GetRendFileBI();

	plugin_log.PrintValue("Rendered Image path: ", bi.Filename());
  if (!rpar.inMtlEdit)
  {
    mtlEditorWasOpen = GetCOREInterface7()->IsMtlDlgShowing();
		GetCOREInterface7()->CloseMtlDlg();
		GetCOREInterface7()->CloseEnvEffectsDialog();
  }
  else
  {
    if(m_pScanlineRender == nullptr)
      m_pScanlineRender = GetCOREInterface()->CreateDefaultScanlineRenderer();
    
    if(m_pScanlineRender != nullptr)
      m_pScanlineRender->Open(scene, vnode, viewPar, rpar, hwnd, defaultLights, numDefLights, prog);
  }

  plugin_log.Print("Leaving HydraRender::Open");

  return 1; 	
}


void hydraRender_mk3::Close(HWND hwnd, RendProgressCallback* prog)
{
  plugin_log.Print("Entering HydraRender::Close");

  plugin_log.flush();
  if(m_pScanlineRender != nullptr && rendParams.inMtlEditor)
    m_pScanlineRender->Close(hwnd, prog);
  
  if (last_render != nullptr && rendParams.postProcOn)  // For cancel render.
    DoPostProc(last_render);

  if (!rendParams.inMtlEditor)
	  BroadcastNotification(NOTIFY_POST_RENDER); // PASHA BUG No "Map" function for undefined


  GetCOREInterface()->EnableSceneRedraw();


  plugin_log.Print("Leaving HydraRender::Close");
  plugin_log.Print("----------------------||\n");
}

void hydraRender_mk3::SaveRenderElementsToDisk(Bitmap* tobm)
{
  const int w = tobm->Width();
  const int h = tobm->Height();

  std::vector<float>          line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
  std::vector<HRGBufferPixel> lineGBuff(w);

  if (rendParams.renElemAlphaOn)
  {
    for (int y = 0; y < h; ++y)
    {
      hrRenderGetFrameBufferLineHDR4f(m_rendRef, 0, w, y, &line[0]);
      hrRenderGetGBufferLine(m_rendRef, y, &lineGBuff[0], 0, w);

      for (int x = 0; x < w; x++)
        line[x * 4 + 3] = lineGBuff[x].rgba[3];
      
      tobm->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)&line[0]);
    }
  }
  else
  {
    for (int y = 0; y < h; ++y)
    {
      hrRenderGetFrameBufferLineHDR4f(m_rendRef, 0, w, y, &line[0]);      

      for (int x = 0; x < w; x++)
        line[x * 4 + 3] = 1.0f;

      tobm->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)&line[0]);
    }
  }

  if (rendParams.rendSaveFile)
  {
    BitmapInfo bi;
    bi = GetCOREInterface()->GetRendFileBI();

    const wchar_t* a_pathFile = bi.Name(); // full path name with ext.

    if (rendParams.renElemColorOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_diffcolor.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"diffcolor", nullptr, 0); // passName2.c_str()
    }
    if (rendParams.renElemCoordOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_texcoord.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"texcoord", nullptr, 0);
    }
    if (rendParams.renElemCoverOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_coverage.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"coverage", nullptr, 0);
    }
    if (rendParams.renElemDepthOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_depth.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"depth", nullptr, 0);
    }

    int palette[256];   
    float colorPoint[3];
    const int paletteSize = 256;
    unsigned int table[QRNG_DIMENSIONS][QRNG_RESOLUTION];

    initQuasirandomGenerator(table);

    for (int i = 1; i < paletteSize; i++)
    {
      colorPoint[0] = rndQmcSobolN(i, 0, &table[0][0]);
      colorPoint[1] = rndQmcSobolN(i, 1, &table[0][0]);
      colorPoint[2] = rndQmcSobolN(i, 2, &table[0][0]);

      palette[i] = RealColor3ToUint32(colorPoint);
    }

    if (rendParams.renElemInstIdOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_instid.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"instid", palette, paletteSize);
    }
    if (rendParams.renElemMatIdOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_matid.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"matid", palette, paletteSize);
    }
    if (rendParams.renElemNormalsOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_normals.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"normals", nullptr, 0);
    }
    if (rendParams.renElemObjIdOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_objid.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"objid", palette, paletteSize);
    }
    if (rendParams.renElemShadowOn)
    {
      std::wstring fullPathSaveResult = a_pathFile + std::wstring(L"_shadow.png");
      hrRenderSaveGBufferLayerLDR(m_rendRef, fullPathSaveResult.c_str(), L"shadow", nullptr, 0);
    }

    plugin_log.Print("Leaving HydraRender::SaveRenderElementsToDisk");
  }
}


size_t blocksST(size_t elems, int threadsPerBlock)
{
  if (elems % threadsPerBlock == 0 && elems >= threadsPerBlock)
    return elems / threadsPerBlock;
  else
    return (elems / threadsPerBlock) + 1;
}

size_t roundBlocks(size_t elems, int threadsPerBlock)
{
  if (elems < threadsPerBlock)
    return static_cast<size_t>(threadsPerBlock);
  else
    return blocksST(elems, threadsPerBlock) * threadsPerBlock;
}


RendProgressCallback* g_pCallBack = nullptr;

void FilterProgressBar(const char* message, float a_progress)
{
  if (g_pCallBack == nullptr)
    return;
  else
  {
  #ifdef MAX2012
    g_pCallBack->SetTitle(message);
  #else
    std::wstring myStr = strConverter::ToWideString(message);
    g_pCallBack->SetTitle(myStr.c_str());
  #endif
    g_pCallBack->Progress(int(a_progress*100.0f), 100);
  }
}


int hydraRender_mk3::Render(TimeValue t, Bitmap* tobm, FrameRendParams &frp, HWND hwnd, RendProgressCallback* prog, ViewParams* viewPar)
{
  if (prog) // strange case of 3ds max when it call Render multiple times and immediately abort it.
  {
    auto res = prog->Progress(0, 100);
    if (res == RENDPROG_ABORT)
      return 1;
  }


  if (rendParams.inMtlEditor)
  {
    /*  hrSceneLibraryOpen(L"C:/[Hydra]/pluginFiles/mateditorlib", HR_WRITE_DISCARD);
    matEditorScene = hrSceneCreate(L"3ds max material editor");*/

    if (m_pScanlineRender != nullptr)
      return m_pScanlineRender->Render(t, tobm, frp, hwnd, prog, viewPar);
  }
  else
  {
    if (m_firstRender && !rendParams.inMtlEditor)
    {
      currentScene = hrSceneCreate(L"my scene");
    }

    g_pCallBack = prog;

    plugin_log.isMtlRender = rendParams.inMtlEditor;
    rendParams.estimateLDR = rendParams.debugCheckBoxes[L"ESTIMATE_IN_LDR"];

    plugin_log.Print("Entering HydraRender::Render");


    if (tobm == nullptr)
    {
      plugin_log.Print("tobm == NULL, No output bitmap, not much we can do");
      return 0;
    }

    if (rendParams.device_id.empty())
    {
      rendParams.device_id = hydraRender_mk3::m_lastRendParams.device_id;
      rendParams.engine_type = hydraRender_mk3::m_lastRendParams.engine_type;
      rendParams.liteMode = hydraRender_mk3::m_lastRendParams.liteMode;

      if (rendParams.device_id.empty())
      {
        UserSelectDeviceDialog(hwnd);
        rendParams.device_id = hydraRender_mk3::m_lastRendParams.device_id;
        rendParams.engine_type = hydraRender_mk3::m_lastRendParams.engine_type;
        rendParams.liteMode = hydraRender_mk3::m_lastRendParams.liteMode;
      }

      if (rendParams.device_id.empty())
      {
        MessageBoxA(nullptr, "Please select at least one rendering device before render.", "Error", MB_OK);
        return 0;
      }
    }

    // Pre-eval notification needs to be sent before scene nodes are evaluated, and called again whenever the time changes
    {
      TimeValue eval_time = t;
      BroadcastNotification(NOTIFY_RENDER_PREEVAL, &eval_time);
    }

    SetRenderParamsImageSizeFromBitMap(&rendParams, tobm);

    rendParams.progCallback = prog;
    rendParams.time = t;
    rendParams.pFrp = &frp;

    if (viewPar)
      view = *viewPar;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    tobm->Storage()->bi.SetType(BMM_FLOAT_RGBA_32);


    std::wstring oldFileOutput = L"", hydraFileOutput = L"";
    if (!rendParams.inMtlEditor)
    {
      BitmapInfo biR = GetCOREInterface13()->GetRendFileBI();
      oldFileOutput = strConverter::c2ws(biR.GetPathEx().GetCStr());
      hydraFileOutput = alterSavedName(oldFileOutput, L"_hdr");
    }

    BitmapInfo bi_out;
    bi_out.SetType(BMM_FLOAT_RGBA_32);
    bi_out.SetFlags(MAP_HAS_ALPHA);
    bi_out.SetWidth(tobm->Width());
    bi_out.SetHeight(tobm->Height());

    tobm->Manager()->Create(&bi_out);

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    Bitmap* bmap_out = tobm;

    DoExport(pScene, pVnode, viewPar, t, rendParams.inMtlEditor);

    if (this->rendParams.debugCheckBoxes[L"BLACK_IMAGE"])
    {
      m_rendRef = SetupRender(frp);
      hrFlush(currentScene); // export scene, dont render it actually
      m_firstRender = false;
      return 1;
    }

    int renderWidth = rendParams.nMaxx - rendParams.nMinx;
    int renderHeight = rendParams.nMaxy - rendParams.nMiny;

    if (!VerifyThings(renderWidth, renderHeight))
    {
      plugin_log.Print("hydraRender_mk3::Render: VerifyThings failed");
      return 0;
    }

    if (!rendParams.inMtlEditor && prog)
    {
      prog->SetTitle(_M("Waiting for hydra.exe to start rendering ..."));
    }

    if (rendParams.inMtlEditor)
    {
      m_rendRefMatEditor = SetupRenderForMatEditor(frp);
      hrFlush(matEditorScene, m_rendRefMatEditor);

      //renderWidth = MTL_WINDOW_SIZE;
      //renderHeight = MTL_WINDOW_SIZE;

      FrameBufferUpdateLoop(bmap_out, renderWidth, renderHeight, prog, true);
    }
    else
    {
      BroadcastNotification(NOTIFY_PRE_RENDERFRAME, (void*)(RenderGlobalContext*)&rendParams);

      m_rendRef = SetupRender(frp);

      hrFlush(currentScene, m_rendRef);
      m_firstRender = false;

      if (rendParams.postProcOn)
      {
        hrFBIResize(hydra_out_render, renderWidth, renderHeight);
        hrFBIResize(hydra_raw_render, renderWidth, renderHeight);
      }

      FrameBufferUpdateLoop(bmap_out, renderWidth, renderHeight, prog, false);

      last_render = bmap_out;

      if (mtlEditorWasOpen)
      {
        GetCOREInterface7()->OpenMtlDlg();
        mtlEditorWasOpen = FALSE;
      }

      g_pCallBack = nullptr;
      hrRenderCommand(m_rendRef, L"exitnow");

      // please don't clear device id list! it is needed further when material editor is opened. 
      // rendParams.device_id.clear(); 

      if (rendParams.postProcOn)
        DoPostProc(last_render);
      else
        CopyImageFromHRRenderToBitmap(bmap_out, renderWidth, renderHeight);

      BroadcastNotification(NOTIFY_POST_RENDERFRAME, (void*)(RenderGlobalContext*)&rendParams);
    }  
  }

  plugin_log.Print("Leaving HydraRender::Render");


  SaveRenderElementsToDisk(tobm);
  
  // {
  //   //int currFrame   = GetCOREInterface()->getBkgFrameNum(t);
  //   // int currFrame = GetCOREInterface()->GetRendNThFrame();
  //   // MSTR timeStrMax;
  //   // TimeToString(t, timeStrMax);
  //   
  //   // BitmapInfo bi;
  //   // bi = GetCOREInterface()->GetRendFileBI();
  //   // bi = GetCOREInterface()->GetRendDeviceBI();
  //   
  //   plugin_log.PrintValue("<=> t ", t);
  // }

  m_firstRender = false;
  return 1;
}

void hydraRender_mk3::CopyRawImageToBitmap(Bitmap* bmap_out, const float *image_src, int src_w, int src_h)
{
  ULONG ctype;
  RealPixel* pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype));

  if (pGbufRealPix == nullptr)
  {
    bmap_out->CreateChannels(BMM_CHAN_REALPIX);
    pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype));
  }

  const int w = bmap_out->Width();
  const int h = bmap_out->Height();
  auto black = BMM_Color_fl(0.0f, 0.0f, 0.0f, 0.0f);

  bmap_out->Fill(black);

  if (w != src_w || h != src_h)
    bmap_out->CropImage(w, h, black);

  std::vector<BMM_Color_fl> line(w);

  for (int y = 0; y < h; y++)
  {
    for (int x = 0; x < w; x++)
    {
      int indexSrc = y*w + x;
      const float r = image_src[indexSrc * 4 + 0];
      const float g = image_src[indexSrc * 4 + 1];
      const float b = image_src[indexSrc * 4 + 2];
      const float a = image_src[indexSrc * 4 + 3];

      if (pGbufRealPix != nullptr)
      {
        int gbufIndex = (h - y - 1)*w + x;

        RealPixel rpix;
        rpix = MakeRealPixel(r, g, b);
        pGbufRealPix[gbufIndex] = rpix;
      }

      line[x] = BMM_Color_fl(r, g, b, a);
    }

    bmap_out->PutPixels(0, h - y - 1, w, &line[0]);
  }
}

void hydraRender_mk3::CopyImageFromHRRenderToBitmap(Bitmap* bmap_out, int src_w, int src_h)
{
  ULONG ctype;
  RealPixel* pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype));

  if (pGbufRealPix == nullptr)
  {
    bmap_out->CreateChannels(BMM_CHAN_REALPIX);
    pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_CHAN_REALPIX, ctype));
  }

  const int w = bmap_out->Width();
  const int h = bmap_out->Height();

  if (w != src_w || h != src_h)
  {
    auto black = BMM_Color_fl(0.0f, 0.0f, 0.0f, 0.0f);
    bmap_out->Fill(black);
    bmap_out->CropImage(w, h, black);
  }

  std::vector<float>          line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
  std::vector<HRGBufferPixel> lineGBuff(w);

  for (int y = 0; y < h; ++y)
  {
    hrRenderGetFrameBufferLineHDR4f(m_rendRef, 0, w, y, &line[0]);
    bmap_out->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)&line[0]);
  }

}


bool hydraRender_mk3::FrameBufferUpdateLoop(Bitmap* bmap_out, int renderWidth, int renderHeight, RendProgressCallback* prog, bool forMatEditor)
{
  //std::vector<float> image(renderWidth * renderHeight * 4, 0.0f);
  //bmap_out->Fill(BMM_Color_64(0, 0, 0, 0));
  
  auto timeStart = std::chrono::system_clock::now();

  plugin_log.Print("hydraRender_mk3::Render: Before image update loop");
  bool hadAnyUpdates = false;
  while (true)
  {
    
    Sleep(500);
    HRRenderUpdateInfo info;

    if(forMatEditor)
      info = hrRenderHaveUpdate(m_rendRefMatEditor);
    else
      info = hrRenderHaveUpdate(m_rendRef);

    if (info.haveUpdateFB)
    {
      hadAnyUpdates = true;
      /*if (forMatEditor)
        hrRenderGetFrameBufferHDR4f(m_rendRefMatEditor, renderWidth, renderHeight, &image[0]);
      else
        hrRenderGetFrameBufferHDR4f(m_rendRef, renderWidth, renderHeight, &image[0]);
        */
      if (bmap_out == nullptr)
        return false;

      if (rendParams.postProcOn)
        DoPostProc(bmap_out);
      else
        CopyImageFromHRRenderToBitmap(bmap_out, renderWidth, renderHeight);
    }

    if (info.finalUpdate || (forMatEditor && hadAnyUpdates && info.progress < 0.00001))
    {
      plugin_log.Print("render finished");
      //hrRenderSaveFrameBufferHDR(m_rendRef, L"C:\\[Hydra]\\temp\\output.exr");
      break;
    }

    bmap_out->RefreshWindow();
    if (bmap_out->GetWindow())
      UpdateWindow(bmap_out->GetWindow());

    if (prog)
    {
      auto res = prog->Progress(int(info.progress), 100);

      if (res != RENDPROG_ABORT)
      {
        std::wstringstream strOut;
        float roundedValue = ceilf(info.progress * 10.0f) / 10;
        strOut << L"Rendering: " << roundedValue << "\% ";
        auto progstr = strOut.str();
        prog->SetTitle(progstr.c_str());
      }
      else
        break;
    }

    if (rendParams.timeLimitOn)
    {
      auto timeCurr = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed_seconds = timeCurr - timeStart;

      if (elapsed_seconds.count() >= rendParams.timeLimit*60)
        break;
    }

  }
  plugin_log.Print("hydraRender_mk3::Render: After image update loop");

  return true;
}


Point2 HydraView::ViewToScreen(Point3 p)
{
	return pRendParams->MapToScreen(p);
}


std::string HydraInstallPath()
{
	return std::string("C:\\[Hydra]\\");
}

std::wstring HydraInstallPathW()
{
	return std::wstring(L"C:\\[Hydra]\\");
}


