#include "3dsmaxsdk_preinclude.h"
#include "resource.h"
#include "hydraRender mk3.h"

#include "3dsmaxport.h"
#include "maxscript/maxscript.h"

extern HINSTANCE hInstance;
extern bool g_materialProcessStart;

static INT_PTR CALLBACK HydraRenderParamDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK PathTracingDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK SPPMCDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK SPPMDDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK PostProcessingDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK OverrideDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);
static INT_PTR CALLBACK DebugDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK ToolsDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK RendElemDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

#include <icolorman.h>

bool IsDarkTheme()
{
  bool darkScheme = true;
  IColorManager* pColorManager = GetColorManager();

  if (pColorManager != nullptr)
  {
    auto theme = pColorManager->GetAppFrameColorTheme();
    if (theme == IColorManager::kLightTheme)
      darkScheme = false;
  }

  return darkScheme;
}




HydraRenderParamDlg::HydraRenderParamDlg(hydraRender_mk3 *r, IRendParams *i, BOOL prog)
{
  rend       = r;
  ir         = i;
  this->prog = prog;

	for (int i = 0; i < rollupsCount; i++)
		rollupIndices[i] = -1;

	hMain = nullptr;
	hPathTracing = nullptr;
/*	hSPPMC = nullptr;
	hSPPMD = nullptr;*/
	hPostProc = nullptr;
	hOverr = nullptr;
	hDebug = nullptr;
	hMLT = nullptr;
	hTools = nullptr;
  hRendElem = nullptr;

	
  if(!prog)
	{
		rollupIndices[MAIN] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_HYDRADIALOG1), HydraRenderParamDlgProc, _T("Rendering parameters"), (LPARAM)this);
		hMain = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[MAIN]);

		rollupIndices[PATH] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_PATHTRACING), PathTracingDlgProc, _M("Path tracing"), (LPARAM)this, APPENDROLL_CLOSED);
		hPathTracing = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[PATH]);

		rollupIndices[MLT] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_MLT), MLTDlgProc, _M("MLT"), (LPARAM)this, APPENDROLL_CLOSED);
		hMLT = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[MLT]);

    rollupIndices[OVERRIDE] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_OVERRIDE), OverrideDlgProc, _M("Override"), (LPARAM)this, APPENDROLL_CLOSED);
    hOverr = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[OVERRIDE]);

    rollupIndices[POSTPROC] = ir->GetTabIRollup(kTabClassID2)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_POSTPROC), PostProcessingDlgProc, _M("Post processing"), (LPARAM)this);
    hPostProc = ir->GetTabIRollup(kTabClassID2)->GetPanelDlg(rollupIndices[POSTPROC]);

    rollupIndices[TOOLS] = ir->GetTabIRollup(kTabClassID3)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_TOOLS), ToolsDlgProc, _M("Tools"), (LPARAM)this);
    hTools = ir->GetTabIRollup(kTabClassID3)->GetPanelDlg(rollupIndices[TOOLS]);
/*
		rollupIndices[SPPMC] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_SPPM_CAUSTIC), SPPMCDlgProc, _M("SPPM (Caustics)"), (LPARAM)this, APPENDROLL_CLOSED);
		hSPPMC = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[SPPMC]);

		rollupIndices[SPPMD] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_SPPM_DIFF), SPPMDDlgProc, _M("SPPM (Diffuse)"), (LPARAM)this, APPENDROLL_CLOSED);
		hSPPMD = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[SPPMD]);
		*/

		rollupIndices[DEBUG] = ir->GetTabIRollup(kTabClassID3)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_DEBUG), DebugDlgProc, _M("Debug"), (LPARAM)this);
		hDebug = ir->GetTabIRollup(kTabClassID3)->GetPanelDlg(rollupIndices[DEBUG]);

    rollupIndices[RENDELEM] = ir->GetTabIRollup(kTabClassID4)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_RENDER_ELEMENTS), RendElemDlgProc, _M("Render elements"), (LPARAM)this);
    hRendElem = ir->GetTabIRollup(kTabClassID4)->GetPanelDlg(rollupIndices[RENDELEM]);

	}
}

HydraRenderParamDlg::~HydraRenderParamDlg()
{
  if (hMain)        ir->DeleteTabRollupPage(kTabClassID, hMain);
  if (hPathTracing) ir->DeleteTabRollupPage(kTabClassID, hPathTracing);
  if (hMLT)         ir->DeleteTabRollupPage(kTabClassID, hMLT);
  if (hOverr)         ir->DeleteTabRollupPage(kTabClassID, hOverr);
  if (hPostProc)    ir->DeleteTabRollupPage(kTabClassID2, hPostProc);
  if (hTools)       ir->DeleteTabRollupPage(kTabClassID3, hTools);
  if (hDebug)       ir->DeleteTabRollupPage(kTabClassID3, hDebug);
  if (hRendElem)    ir->DeleteTabRollupPage(kTabClassID4, hRendElem);

  /*if(hSPPMC) ir->DeleteTabRollupPage(kTabClassID, hSPPMC);
  if(hSPPMD) ir->DeleteTabRollupPage(kTabClassID, hSPPMD);*/

	for (int i = 0; i < rollupsCount; i++)
	{
		if (rollupIndices[i] != -1)
		{
			ir->GetTabIRollup(kTabClassID)->DeleteRollup(rollupIndices[i], 1);
			rollupIndices[i] = -1;
		}
	}
	hMain = nullptr;
	hPathTracing = nullptr;
	/*hSPPMC = nullptr;
	hSPPMD = nullptr;*/
	hPostProc = nullptr;
	hOverr = nullptr;
	hDebug = nullptr;
	hMLT = nullptr;
	hTools = nullptr;
  hRendElem = nullptr;
}


void HydraRenderParamDlg::InitProgDialog(HWND hWnd)
{
	hMain = nullptr;
	hPathTracing = nullptr;
	/*hSPPMC = nullptr;
	hSPPMD = nullptr;*/
	hPostProc = nullptr;
	hOverr = nullptr;
	hDebug = nullptr;
	hMLT = nullptr;
	hTools = nullptr;
  hRendElem = nullptr;

}

std::vector<HydraRenderDevice> InitDeviceListInternal(HRRenderRef a_renderRef)
{
  std::vector<HydraRenderDevice> res;

  auto pList = hrRenderGetDeviceList(a_renderRef);
  
  while (pList != nullptr)
  {
    HydraRenderDevice dev;
    dev.id         = pList->id;
    dev.name       = pList->name;
    dev.driverName = pList->driver;
    dev.isCPU      = pList->isCPU;
    res.push_back(dev);
    pList = pList->next;
  }

  return res;
}

std::vector<HydraRenderDevice> InitDeviceList(HWND devices_list, int mode, bool init, HRRenderRef a_renderRef)
{
  std::vector<HydraRenderDevice> result = InitDeviceListInternal(a_renderRef); // InitDeviceList(mode);

#ifdef SILENT_DEVICE_ID
  return result;
#endif

	if (devices_list == nullptr)
		return result;

	if (!init)
		ListView_DeleteAllItems(devices_list);

  LVITEM lvI;
  lvI.mask = LVIF_TEXT | LVIF_STATE;
  lvI.stateMask = 0;
  lvI.iSubItem = 0;
  lvI.state = 0;
  lvI.cColumns = 3;

  for (auto p = result.begin(); p != result.end(); ++p)
  {
    auto str        = p->name;
    auto dev_name   = p->name;
    auto dev_id     = p->id;
    auto dev_driver = p->driverName;

#ifdef MAX2012

    std::stringstream devIdStrOut;
    std::string tmp = strConverter::ws2s(str);
    lvI.pszText     = (LPSTR)tmp.c_str();

#else
    std::wstringstream devIdStrOut;
    lvI.pszText = (LPWSTR)str.c_str();
#endif

    lvI.iItem   = dev_id;
    ListView_InsertItem(devices_list, &lvI);

    devIdStrOut << dev_id;
    auto dev_id_str = devIdStrOut.str();

#ifdef MAX2012

    std::string devNameS   = strConverter::ws2s(dev_name);
    std::string devDriverS = strConverter::ws2s(dev_driver);

    ListView_SetItemText(devices_list, lvI.iItem, 0, (LPSTR)dev_id_str.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 1, (LPSTR)devNameS.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 2, (LPSTR)devDriverS.c_str());

#else
    ListView_SetItemText(devices_list, lvI.iItem, 0, (LPWSTR)dev_id_str.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 1, (LPWSTR)dev_name.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 2, (LPWSTR)dev_driver.c_str());
#endif

    ListView_SetCheckState(devices_list, 0, true);
  }

  return result;
}

void HydraRenderParamDlg::LoadPreset(int preset_id)
{
	float tmp = 0.0f;
	if (false)
	{
		switch (preset_id+1)
		{
		case PRESET_VHIGH:
			break;
		case PRESET_HIGH:
			break;
		case PRESET_MEDIUM:
			break;
		case PRESET_LOW:
			break;
		default:
			break;
		}
	}
	else
	{
		rend->plugin_log.Print("Failed to load presets.xml");
	}
}

void HydraRenderParamDlg::SavePreset()
{
	if (false)
	{
	}
	else
	{
		rend->plugin_log.Print("Failed to load presets.xml");
	}
}

void HydraRenderParamDlg::LoadPresetToGUI()
{
	float f_tmp = 0.0f;
	int i_tmp   = 0;


	minrays_edit->SetText(i_tmp);
	minrays_spin->SetValue(i_tmp, FALSE);

	maxrays_edit->SetText(i_tmp);
	maxrays_spin->SetValue(i_tmp, FALSE);

  relative_error_edit->SetText(f_tmp);
  relative_error_spin->SetValue(f_tmp, FALSE);
}

void HydraRenderParamDlg::InitMainHydraDialog(HWND hWnd)
{
  hydraRender_mk3::m_pLastParamDialog = this;
  
	//PRIMARY BOUNCE
	SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
 // SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));

  //SECONDARY BOUNCE
	SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
  //SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));
  //SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("MLT")));
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("Light Tracing")));
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("IBPT")));

	//TERTIARY BOUNCE
	/*SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
  SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));
  SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("MLT")));*/

	SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_SETCURSEL,   rend->rendParams.primary, 0);
	SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_SETCURSEL, rend->rendParams.secondary, 0);
	//SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_SETCURSEL,  rend->rendParams.tertiary, 0);

	//CAUSTICS
  CheckDlgButton(hWnd, IDC_CAUSTICSON, rend->rendParams.enableCaustics);
	/*SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("None")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("MLT")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));*/

	//SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_SELECTSTRING, -1, (LPARAM)(L"None"));
	//SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_SETCURSEL, rend->rendParams.causticsMode, 0);

  CheckDlgButton(hWnd, IDC_ENABLE_TEXTURE_RESIZE, rend->rendParams.enableTextureResize);

  CheckDlgButton(hWnd, IDC_PRODUCTION_MODE, rend->rendParams.productionMode);

  renderQuality_slider = GetISlider(GetDlgItem(hWnd, IDC_RENDERQUALITY_SLIDER));
  renderQuality_slider->SetLimits(1.0f, 100.0f, FALSE);
  renderQuality_slider->SetResetValue(50.0f);
  renderQuality_slider->SetValue(rend->rendParams.renderQuality, FALSE);
  renderQuality_slider->SetNumSegs(4);
	

	//DEVICE ID
	/*for (int i = 0; i <= 10; ++i)
	{
		SendDlgItemMessage(hWnd, IDC_DEVICE, CB_ADDSTRING, 0, (LPARAM)std::to_wstring(i).c_str());
	}
	SendDlgItemMessage(hWnd, IDC_DEVICE, CB_SETCURSEL, rend->rendParams.device_id, 0);*/


	lensradius_edit = GetICustEdit(GetDlgItem(hWnd, IDC_LENS));
	lensradius_edit->SetText(rend->rendParams.lensradius);

	lensradius_spin = GetISpinner(GetDlgItem(hWnd, IDC_LENS_SPIN));
  lensradius_spin->SetLimits(0.0f, 1000.0f, FALSE);
  lensradius_spin->SetResetValue(1.0f); 
  lensradius_spin->SetValue(rend->rendParams.lensradius, FALSE); 
	lensradius_spin->SetScale(0.1f);      // value change step
  lensradius_spin->LinkToEdit(GetDlgItem(hWnd, IDC_LENS), EDITTYPE_FLOAT);


	timelimit_edit = GetICustEdit(GetDlgItem(hWnd, IDC_TIMELIMIT_EDIT));
  timelimit_edit->SetText(rend->rendParams.timeLimit);

  timelimit_spin = GetISpinner(GetDlgItem(hWnd, IDC_TIMELIMIT_SPIN));
  timelimit_spin->SetLimits(0, 60000, FALSE);
  timelimit_spin->SetResetValue(0); 
  timelimit_spin->SetValue(rend->rendParams.timeLimit, FALSE); 
	timelimit_spin->SetScale(1);      // value change step
  timelimit_spin->LinkToEdit(GetDlgItem(hWnd, IDC_TIMELIMIT_EDIT), EDITTYPE_INT);


	CheckDlgButton(hWnd, IDC_DOF_ON, rend->rendParams.enableDOF);
	CheckDlgButton(hWnd, IDC_TIMELIMIT, rend->rendParams.timeLimit);


  maxrays_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXRAY));
  maxrays_edit->SetText(rend->rendParams.maxrays);

  maxrays_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXRAY_SPIN));
  maxrays_spin->SetLimits(64, 1000000, FALSE);
  maxrays_spin->SetResetValue(4156); // 4156 = 50% quality slider.
  maxrays_spin->SetValue(rend->rendParams.maxrays, FALSE);
  maxrays_spin->SetScale(64); //value change step
  maxrays_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXRAY), EDITTYPE_INT);


  raybounce_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RAYBOUNCE));
  raybounce_edit->SetText(rend->rendParams.raybounce);

  raybounce_spin = GetISpinner(GetDlgItem(hWnd, IDC_RAYBOUNCE_SPIN));
  raybounce_spin->SetLimits(0, 255, FALSE);
  raybounce_spin->SetResetValue(8);
  raybounce_spin->SetValue(rend->rendParams.raybounce, FALSE);
  raybounce_spin->SetScale(1); //value change step
  raybounce_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RAYBOUNCE), EDITTYPE_INT);


  diffbounce_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFBOUNCE));
  diffbounce_edit->SetText(rend->rendParams.diffbounce);

  diffbounce_spin = GetISpinner(GetDlgItem(hWnd, IDC_DIFFBOUNCE_SPIN));
  diffbounce_spin->SetLimits(0, 16, FALSE);
  diffbounce_spin->SetResetValue(4);
  diffbounce_spin->SetValue(rend->rendParams.diffbounce, FALSE);
  diffbounce_spin->SetScale(1); //value change step
  diffbounce_spin->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFBOUNCE), EDITTYPE_INT);

	//CheckDlgButton(hWnd, IDC_ALL_SECONDARY, 0); 
	

#ifdef MAX2012
	SetWindowText(GetDlgItem( hWnd, IDC_VERSION), rend->GetHydraVersion());	
#else
	SetWindowText(GetDlgItem(hWnd, IDC_VERSION), rend->GetHydraVersionW());
#endif


	HWND devices_list = GetDlgItem(hWnd, IDC_DEVICES_LIST);

	ListView_SetExtendedListViewStyleEx(devices_list, 0, LVS_EX_CHECKBOXES);
	//ListView_SetView(devices_list, LV_VIEW_DETAILS);
	ListView_SetBkColor(devices_list, CLR_NONE);
	ListView_SetTextBkColor(devices_list, CLR_NONE);

  if (IsDarkTheme())
	  ListView_SetTextColor(devices_list, RGB(228, 228, 228));
  else
    ListView_SetTextColor(devices_list, RGB(20, 20, 20));

	LVCOLUMN lvc;
	int iCol;
	int nCol = 3;
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	hydraStr cols[] = { _M("ID"), _M("Device Name"), _M("Driver/CC") };

	for (iCol = 0; iCol < nCol; iCol++)
	{
		lvc.iSubItem = iCol;

#ifdef MAX2012
		lvc.pszText = (LPSTR)cols[iCol].c_str();
#else
		lvc.pszText = (LPWSTR)cols[iCol].c_str();
#endif
		lvc.fmt = LVCFMT_LEFT;

		if (iCol < 1) lvc.cx = 40;
		else lvc.cx = 150;

		ListView_InsertColumn(devices_list, iCol, &lvc);
	}

	
  bool res = false;
  std::unordered_map<int, bool> deviceIDs;
	int mode = 1;

	res = rend->readAndCheckDeviceSettingsFile(mode, deviceIDs);

	if (res && !deviceIDs.empty())
	{
		rend->rendParams.rememberDevice = true;	
		rend->m_devList = InitDeviceList(devices_list, mode, true, rend->m_rendRef);
		ListView_SetCheckState(devices_list, 0, false);
		for (auto it = deviceIDs.begin(); it != deviceIDs.end(); ++it)
			ListView_SetCheckState(devices_list, it->first, it->second);

		rend->rendParams.engine_type = 1;
	}
	else if (!hydraRender_mk3::m_lastRendParams.device_id.empty())
	{
		rend->m_devList = InitDeviceList(devices_list, hydraRender_mk3::m_lastRendParams.engine_type, true, rend->m_rendRef);
		ListView_SetCheckState(devices_list, 0, false);
		for (auto it = hydraRender_mk3::m_lastRendParams.device_id.begin(); it != hydraRender_mk3::m_lastRendParams.device_id.end(); ++it)
			ListView_SetCheckState(devices_list, it->first, it->second);

		rend->rendParams.engine_type = hydraRender_mk3::m_lastRendParams.engine_type;
		rend->rendParams.rememberDevice = false;
	}
	else //should not happen
	{
		rend->m_devList = InitDeviceList(devices_list, rend->rendParams.engine_type, true, rend->m_rendRef);
		rend->rendParams.rememberDevice = false;
	}

	CheckDlgButton(hWnd, IDC_REMEMBER_MDLG, rend->rendParams.rememberDevice);

	UpdateWindow(hWnd);
}



void HydraRenderParamDlg::InitPathTracingDialog(HWND hWnd)
{
	//PRESETS
	//SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(L"Custom"));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Ultra")));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("High")));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Medium")));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Low")));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("******")));
	SendDlgItemMessage(hWnd, IDC_PRESETS, CB_SETCURSEL, rend->rendParams.preset, 4);


	minrays_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MINRAY));
	minrays_edit->SetText(rend->rendParams.minrays);
	
	minrays_spin = GetISpinner(GetDlgItem(hWnd, IDC_MINRAY_SPIN));
  minrays_spin->SetLimits(1, 65536, FALSE);
  minrays_spin->SetResetValue(16); 
  minrays_spin->SetValue(rend->rendParams.minrays, FALSE); 
	minrays_spin->SetScale(16); //value change step
  minrays_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MINRAY), EDITTYPE_INT);

  
	relative_error_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RELATIVE_ERR));
	relative_error_edit->SetText(rend->rendParams.relative_error);

	relative_error_spin = GetISpinner(GetDlgItem(hWnd, IDC_RELATIVE_ERR_SPIN));
  relative_error_spin->SetLimits(0.0f, 100.0f, FALSE);
  relative_error_spin->SetResetValue(2.0f);                              // Right click reset value
  relative_error_spin->SetValue(rend->rendParams.relative_error, FALSE); // FALSE = don't send notify yet.
	relative_error_spin->SetScale(0.1f);                                   // value change step
  relative_error_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RELATIVE_ERR), EDITTYPE_FLOAT);

	bsdf_clamp_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BSDF_CLAMP_EDIT));
	bsdf_clamp_edit->SetText(rend->rendParams.bsdf_clamp);

	bsdf_clamp_spin = GetISpinner(GetDlgItem(hWnd, IDC_BSDF_CLAMP_SPIN));
	bsdf_clamp_spin->SetLimits(1.0f, 1000000.0f, FALSE);
	bsdf_clamp_spin->SetResetValue(100.0f);                              // Right click reset value
	bsdf_clamp_spin->SetValue(rend->rendParams.bsdf_clamp, FALSE); // FALSE = don't send notify yet.
	bsdf_clamp_spin->SetScale(10.0f);                                   // value change step
	bsdf_clamp_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BSDF_CLAMP_EDIT), EDITTYPE_FLOAT);

	env_clamp_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ENV_CLAMP));
	env_clamp_edit->SetText(rend->rendParams.env_clamp);

	env_clamp_spin = GetISpinner(GetDlgItem(hWnd, IDC_ENV_CLAMP_SPIN));
	env_clamp_spin->SetLimits(1.0f, 1000000.00f, FALSE);
	env_clamp_spin->SetResetValue(5.0f);   // Right click reset value
	env_clamp_spin->SetValue(rend->rendParams.env_clamp, FALSE); // FALSE = don't send notify yet.
	env_clamp_spin->SetScale(1.0f);       // value change step
	env_clamp_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ENV_CLAMP), EDITTYPE_FLOAT);


	SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("None")));
  SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("Per warp")));
  SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("All")));

  //SendDlgItemMessage(hWnd, IDC_SEED, CB_SELECTSTRING, -1, (LPARAM)(L"None"));
	SendDlgItemMessage(hWnd, IDC_SEED, CB_SETCURSEL, rend->rendParams.seed, 0);

	CheckDlgButton(hWnd, IDC_CAUSTICS_ON,   rend->rendParams.causticRays); 
	CheckDlgButton(hWnd, IDC_RR_ON,         rend->rendParams.useRR);
	CheckDlgButton(hWnd, IDC_GUIDED,        rend->rendParams.guided);
	CheckDlgButton(hWnd, IDC_BSDF_CLAMP_ON, rend->rendParams.bsdf_clamp_on);

}

void HydraRenderParamDlg::InitMLTDialog(HWND hWnd)
{
	mlt_plarge_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_LSTEP_EDIT));
	mlt_plarge_edit->SetText(rend->rendParams.mlt_plarge);

	mlt_plarge_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_LSTEP_SLIDER));
	mlt_plarge_slider->SetLimits(10, 50, FALSE);
	mlt_plarge_slider->SetResetValue(50); 
	mlt_plarge_slider->SetValue((int)(100.0f*rend->rendParams.mlt_plarge), FALSE); 
	mlt_plarge_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_LSTEP_EDIT), EDITTYPE_INT);
	mlt_plarge_slider->SetNumSegs(5);
  mlt_plarge_slider->SetTooltip(true, TEXT("Large step sample image uniformly. Make it larger to sample dark regions more often. Make it smaller to sample caustics more often."));

	mlt_iters_mult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_ITERMULT_EDIT));
	mlt_iters_mult_edit->SetText(rend->rendParams.mlt_iters_mult);

	mlt_iters_mult_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_ITERMULT_SLIDER));
	mlt_iters_mult_slider->SetLimits(1, 4, FALSE);
	mlt_iters_mult_slider->SetResetValue(2); 
	mlt_iters_mult_slider->SetValue(rend->rendParams.mlt_iters_mult, FALSE); //1/(rend->rendParams.mlt_iters_mult + 1)
	mlt_iters_mult_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_ITERMULT_EDIT), EDITTYPE_INT);
	mlt_iters_mult_slider->SetNumSegs(3);
  mlt_iters_mult_slider->SetTooltip(true, TEXT("This is balance between direct and indirect light. Make it larger if you have strong indirect light. Make it smaller if you have complex direct light. Value 1 means 1/2 for direct and 1/2 for indirect. Value 2 means 2/3 for direct light and 1/3 for indirect. Value 16 means 1/17 for direct light 16/17 for indirect."));


	mlt_burn_iters_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_EDIT));
	mlt_burn_iters_edit->SetText(rend->rendParams.mlt_burn_iters/64);

	mlt_burn_iters_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_SLIDER));
	mlt_burn_iters_slider->SetLimits(1, 16, FALSE);
	mlt_burn_iters_slider->SetResetValue(2);   
	mlt_burn_iters_slider->SetValue(rend->rendParams.mlt_burn_iters/64, FALSE); 
	mlt_burn_iters_slider->SetNumSegs(3);
	mlt_burn_iters_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_EDIT), EDITTYPE_INT);
  mlt_burn_iters_slider->SetTooltip(true, TEXT("Normally you should not change this preset. Make it greater if your lighting is very complex. Make it smaller if your burn-in period is too long."));

	CheckDlgButton(hWnd, IDC_MLT_MEDIAN_ON, rend->rendParams.mlt_enable_median_filter);

	mlt_median_filter_threshold_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_MED_THRES_EDIT));
	mlt_median_filter_threshold_edit->SetText(rend->rendParams.mlt_median_filter_threshold);

	mlt_median_filter_threshold_spin = GetISpinner(GetDlgItem(hWnd, IDC_MLT_MED_THRES_SPIN));
	mlt_median_filter_threshold_spin->SetLimits(0.1f, 2.0f, FALSE);
	mlt_median_filter_threshold_spin->SetResetValue(0.1f);   
	mlt_median_filter_threshold_spin->SetValue(rend->rendParams.mlt_median_filter_threshold, FALSE); 
	mlt_median_filter_threshold_spin->SetScale(0.05f);       
	mlt_median_filter_threshold_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_MED_THRES_EDIT), EDITTYPE_FLOAT);


}

void HydraRenderParamDlg::InitSPPMCDialog(HWND hWnd)
{
	maxphotons_c_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_C));
	maxphotons_c_edit->SetText(rend->rendParams.maxphotons_c);

	maxphotons_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXPHOTONS_C_SPIN));
  maxphotons_c_spin->SetLimits(1, 2000, FALSE);
  maxphotons_c_spin->SetResetValue(1000);   // Right click reset value
  maxphotons_c_spin->SetValue(rend->rendParams.maxphotons_c, FALSE); // FALSE = don't send notify yet.
	maxphotons_c_spin->SetScale(50);       // value change step
  maxphotons_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_C), EDITTYPE_INT);


	initial_radius_c_edit = GetICustEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_C));
	initial_radius_c_edit->SetText(rend->rendParams.initial_radius_c);
  initial_radius_c_edit->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

	initial_radius_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_INIT_RADIUS_C_SPIN));
  initial_radius_c_spin->SetLimits(0.5, 32.0f, FALSE);
  initial_radius_c_spin->SetResetValue(4);   // Right click reset value
  initial_radius_c_spin->SetValue(rend->rendParams.initial_radius_c, FALSE); // FALSE = don't send notify yet.
	initial_radius_c_spin->SetScale(0.5f);       // value change step
  initial_radius_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_C), EDITTYPE_FLOAT);

  initial_radius_c_spin->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

	caustic_power_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CAUSTICPOW));
	caustic_power_edit->SetText(rend->rendParams.caustic_power);

	caustic_power_spin = GetISpinner(GetDlgItem(hWnd, IDC_CAUSTICPOW_SPIN));
  caustic_power_spin->SetLimits(0, 10, FALSE);
  caustic_power_spin->SetResetValue(1);   // Right click reset value
  caustic_power_spin->SetValue(rend->rendParams.caustic_power, FALSE); // FALSE = don't send notify yet.
	caustic_power_spin->SetScale(0.5);       // value change step
  caustic_power_spin->LinkToEdit(GetDlgItem(hWnd, IDC_CAUSTICPOW), EDITTYPE_FLOAT);

	retrace_c_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RETRACE_C));
	retrace_c_edit->SetText(rend->rendParams.retrace_c);
  retrace_c_edit->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

	retrace_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_RETRACE_C_SPIN));
  retrace_c_spin->SetLimits(1, 16, FALSE);
  retrace_c_spin->SetResetValue(rend->rendParams.retrace_c);   // Right click reset value
  retrace_c_spin->SetValue(rend->rendParams.retrace_c, FALSE); // FALSE = don't send notify yet.
	retrace_c_spin->SetScale(1);                 // value change step
  retrace_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RETRACE_C), EDITTYPE_INT);
  retrace_c_spin->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

	alpha_c_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_C));
	alpha_c_edit->SetText(rend->rendParams.alpha_c);

	alpha_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_ALPHA_SPIN_C));
  alpha_c_spin->SetLimits(0.66f, 1.0f, FALSE);
  alpha_c_spin->SetResetValue(rend->rendParams.alpha_c);   // Right click reset value
  alpha_c_spin->SetValue(rend->rendParams.alpha_c, FALSE); // FALSE = don't send notify yet.
	alpha_c_spin->SetScale(0.05f);                 // value change step
  alpha_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_C), EDITTYPE_FLOAT);


	CheckDlgButton(hWnd, IDC_VISIBILITY_C_ON, rend->rendParams.visibility_c);
}

void HydraRenderParamDlg::InitSPPMDDialog(HWND hWnd)
{
	maxphotons_d_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_D));
	maxphotons_d_edit->SetText(rend->rendParams.maxphotons_d);

	maxphotons_d_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXPHOTONS_D_SPIN));
  maxphotons_d_spin->SetLimits(1, 2000, FALSE);
  maxphotons_d_spin->SetResetValue(1000);   // Right click reset value
  maxphotons_d_spin->SetValue(rend->rendParams.maxphotons_d, FALSE); // FALSE = don't send notify yet.
	maxphotons_d_spin->SetScale(50);       // value change step
  maxphotons_d_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_D), EDITTYPE_INT);


	initial_radius_d_edit = GetICustEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_D));
	initial_radius_d_edit->SetText(rend->rendParams.initial_radius_d);
  initial_radius_d_edit->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

	initial_radius_d_spin = GetISpinner(GetDlgItem(hWnd, IDC_INIT_RADIUS_D_SPIN));
  initial_radius_d_spin->SetLimits(1, 64, FALSE);
  initial_radius_d_spin->SetResetValue(10);   // Right click reset value
  initial_radius_d_spin->SetValue(rend->rendParams.initial_radius_d, FALSE); // FALSE = don't send notify yet.
	initial_radius_d_spin->SetScale(0.5f);     // value change step
  initial_radius_d_spin->LinkToEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_D), EDITTYPE_FLOAT);

  initial_radius_d_spin->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));


	retrace_d_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RETRACE_D));
	retrace_d_edit->SetText(rend->rendParams.retrace_d);
  retrace_d_edit->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

	retrace_d_spin = GetISpinner(GetDlgItem(hWnd, IDC_RETRACE_D_SPIN));
  retrace_d_spin->SetLimits(1, 16, FALSE);
  retrace_d_spin->SetResetValue(rend->rendParams.retrace_d);   // Right click reset value
  retrace_d_spin->SetValue(rend->rendParams.retrace_d, FALSE); // FALSE = don't send notify yet.
	retrace_d_spin->SetScale(1);                 // value change step
  retrace_d_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RETRACE_D), EDITTYPE_INT);
  retrace_d_spin->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

	alpha_d_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_D));
	alpha_d_edit->SetText(rend->rendParams.alpha_d);

	alpha_d_spin = GetISpinner(GetDlgItem(hWnd, IDC_ALPHA_SPIN_D));
  alpha_d_spin->SetLimits(0.66f, 1.0f, FALSE);
  alpha_d_spin->SetResetValue(rend->rendParams.alpha_d);   // Right click reset value
  alpha_d_spin->SetValue(rend->rendParams.alpha_d, FALSE); // FALSE = don't send notify yet.
	alpha_d_spin->SetScale(0.05f);                 // value change step
  alpha_d_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_D), EDITTYPE_FLOAT);

	CheckDlgButton(hWnd, IDC_VISIBILITY_D_ON, rend->rendParams.visibility_d);
	CheckDlgButton(hWnd, IDC_IRRMAP, rend->rendParams.irr_map);
}

void HydraRenderParamDlg::InitPostProcDialog(HWND hWnd)
{
  CheckDlgButton(hWnd, IDC_DENOISER, rend->rendParams.use_denoiser);

  noise_lvl_edit = GetICustEdit(GetDlgItem(hWnd, IDC_NOISELVL_EDIT));
  noise_lvl_edit->SetText(rend->rendParams.noiseLvl);

  noise_lvl_slider = GetISlider(GetDlgItem(hWnd, IDC_NOISELVL_SLIDER));
  noise_lvl_slider->SetLimits(0.0f, 1.0f, FALSE);
  noise_lvl_slider->SetResetValue(0.5f);
  noise_lvl_slider->SetValue(rend->rendParams.noiseLvl, FALSE);
  noise_lvl_slider->LinkToEdit(GetDlgItem(hWnd, IDC_NOISELVL_EDIT), EDITTYPE_FLOAT);
  noise_lvl_slider->SetNumSegs(4);

  denoise_btn = GetICustButton(GetDlgItem(hWnd, IDC_DO_DENOISE));
  denoise_btn->SetType(CBT_PUSH);

  CheckDlgButton(hWnd, IDC_POSTPROC_ON, rend->rendParams.postProcOn);

  resetPostProc = GetICustButton(GetDlgItem(hWnd, IDC_RESET_POSTPROC));
  resetPostProc->SetType(CBT_PUSH);

  exposure_edit = GetICustEdit(GetDlgItem(hWnd, IDC_EXPOSURE_EDIT));
  exposure_edit->SetText(rend->rendParams.exposure);

  exposure_slider = GetISlider(GetDlgItem(hWnd, IDC_EXPOSURE_SLIDER));
  exposure_slider->SetLimits(0.0f, 10.0f, FALSE);
  exposure_slider->SetResetValue(1.0f);
  exposure_slider->SetValue(rend->rendParams.exposure, FALSE); 
  exposure_slider->LinkToEdit(GetDlgItem(hWnd, IDC_EXPOSURE_EDIT), EDITTYPE_FLOAT);
  exposure_slider->SetNumSegs(4);

  compress_edit = GetICustEdit(GetDlgItem(hWnd, IDC_COMPRESS_EDIT));
  compress_edit->SetText(rend->rendParams.compress);

  compress_slider = GetISlider(GetDlgItem(hWnd, IDC_COMPRESS_SLIDER));
  compress_slider->SetLimits(0.0f, 1.0f, FALSE);
  compress_slider->SetResetValue(0.0f);
  compress_slider->SetValue(rend->rendParams.compress, FALSE);
  compress_slider->LinkToEdit(GetDlgItem(hWnd, IDC_COMPRESS_EDIT), EDITTYPE_FLOAT);
  compress_slider->SetNumSegs(4);

  contrast_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CONTRAST_EDIT));
  contrast_edit->SetText(rend->rendParams.contrast);
  
  contrast_slider = GetISlider(GetDlgItem(hWnd, IDC_CONTRAST_SLIDER));
  contrast_slider->SetLimits(1.0f, 2.0f, FALSE);
  contrast_slider->SetResetValue(1.0f);
  contrast_slider->SetValue(rend->rendParams.contrast, FALSE);
  contrast_slider->LinkToEdit(GetDlgItem(hWnd, IDC_CONTRAST_EDIT), EDITTYPE_FLOAT);
  contrast_slider->SetNumSegs(4);

  saturation_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SATURATION_EDIT));
  saturation_edit->SetText(rend->rendParams.saturation);
  
  saturation_slider = GetISlider(GetDlgItem(hWnd, IDC_SATURATION_SLIDER));
  saturation_slider->SetLimits(0.0f, 2.0f, FALSE);
  saturation_slider->SetResetValue(1.0f);
  saturation_slider->SetValue(rend->rendParams.saturation, FALSE);
  saturation_slider->LinkToEdit(GetDlgItem(hWnd, IDC_SATURATION_EDIT), EDITTYPE_FLOAT);
  saturation_slider->SetNumSegs(4);

  whiteBalance_edit = GetICustEdit(GetDlgItem(hWnd, IDC_WHITE_BALANCE_EDIT));
  whiteBalance_edit->SetText(rend->rendParams.whiteBalance);
  
  whiteBalance_slider = GetISlider(GetDlgItem(hWnd, IDC_WHITE_BALANCE_SLIDER));
  whiteBalance_slider->SetLimits(0.0f, 1.0f, FALSE);
  whiteBalance_slider->SetResetValue(0.0f);
  whiteBalance_slider->SetValue(rend->rendParams.whiteBalance, FALSE);
  whiteBalance_slider->LinkToEdit(GetDlgItem(hWnd, IDC_WHITE_BALANCE_EDIT), EDITTYPE_FLOAT);
  whiteBalance_slider->SetNumSegs(4);

  whitePoint_selector = GetIColorSwatch(GetDlgItem(hWnd, IDC_WHITEPOINT_COLOR), RGB(0, 0, 0), _M("Select white point. Black color - auto white balance"));
  whitePoint_selector->SetColor(rend->rendParams.whitePointColor);

  uniformContrast_edit = GetICustEdit(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_EDIT));
  uniformContrast_edit->SetText(rend->rendParams.uniformContrast);
  
  uniformContrast_slider = GetISlider(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_SLIDER));
  uniformContrast_slider->SetLimits(0.0f, 1.0f, FALSE);
  uniformContrast_slider->SetResetValue(0.0f);
  uniformContrast_slider->SetValue(rend->rendParams.uniformContrast, FALSE);
  uniformContrast_slider->LinkToEdit(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_EDIT), EDITTYPE_FLOAT);
  uniformContrast_slider->SetNumSegs(4);

  normalize_edit = GetICustEdit(GetDlgItem(hWnd, IDC_NORMALIZE_EDIT));
  normalize_edit->SetText(rend->rendParams.normalize);
  
  normalize_slider = GetISlider(GetDlgItem(hWnd, IDC_NORMALIZE_SLIDER));
  normalize_slider->SetLimits(0.0f, 1.0f, FALSE);
  normalize_slider->SetResetValue(0.0f);
  normalize_slider->SetValue(rend->rendParams.normalize, FALSE);
  normalize_slider->LinkToEdit(GetDlgItem(hWnd, IDC_NORMALIZE_EDIT), EDITTYPE_FLOAT);
  normalize_slider->SetNumSegs(4);

  chromAberr_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CHROM_ABERR_EDIT));
  chromAberr_edit->SetText(rend->rendParams.chromAberr);
  
  chromAberr_slider = GetISlider(GetDlgItem(hWnd, IDC_CHROM_ABERR_SLIDER));
  chromAberr_slider->SetLimits(0.0f, 1.0f, FALSE);
  chromAberr_slider->SetResetValue(0.0f);
  chromAberr_slider->SetValue(rend->rendParams.chromAberr, FALSE);
  chromAberr_slider->LinkToEdit(GetDlgItem(hWnd, IDC_CHROM_ABERR_EDIT), EDITTYPE_FLOAT);
  chromAberr_slider->SetNumSegs(4);

  vignette_edit = GetICustEdit(GetDlgItem(hWnd, IDC_VIGNETTE_EDIT));
  vignette_edit->SetText(rend->rendParams.vignette);
  
  vignette_slider = GetISlider(GetDlgItem(hWnd, IDC_VIGNETTE_SLIDER));
  vignette_slider->SetLimits(0.0f, 1.0f, FALSE);
  vignette_slider->SetResetValue(0.0f);
  vignette_slider->SetValue(rend->rendParams.vignette, FALSE);
  vignette_slider->LinkToEdit(GetDlgItem(hWnd, IDC_VIGNETTE_EDIT), EDITTYPE_FLOAT);
  vignette_slider->SetNumSegs(4);
  
  sharpness_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SHARPNESS_EDIT));
  sharpness_edit->SetText(rend->rendParams.sharpness);

  sharpness_slider = GetISlider(GetDlgItem(hWnd, IDC_SHARPNESS_SLIDER));
  sharpness_slider->SetLimits(0.0f, 1.0f, FALSE);
  sharpness_slider->SetResetValue(0.0f);
  sharpness_slider->SetValue(rend->rendParams.sharpness, FALSE);
  sharpness_slider->LinkToEdit(GetDlgItem(hWnd, IDC_SHARPNESS_EDIT), EDITTYPE_FLOAT);
  sharpness_slider->SetNumSegs(4);

  sizeStar_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_EDIT));
  sizeStar_edit->SetText(rend->rendParams.sizeStar);

  sizeStar_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_SLIDER));
  sizeStar_slider->SetLimits(0.0f, 100.0f, FALSE);
  sizeStar_slider->SetResetValue(0.0f);
  sizeStar_slider->SetValue(rend->rendParams.sizeStar, FALSE);
  sizeStar_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_EDIT), EDITTYPE_FLOAT);
  sizeStar_slider->SetNumSegs(4);

  numRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_EDIT));
  numRay_edit->SetText(rend->rendParams.numRay);
  
  numRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_SLIDER));
  numRay_slider->SetLimits(2, 16, FALSE);
  numRay_slider->SetResetValue(8);
  numRay_slider->SetValue(rend->rendParams.numRay, FALSE);
  numRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_EDIT), EDITTYPE_INT);
  numRay_slider->SetNumSegs(4);

  rotateRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_EDIT));
  rotateRay_edit->SetText(rend->rendParams.rotateRay);
  
  rotateRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_SLIDER));
  rotateRay_slider->SetLimits(0, 360, FALSE);
  rotateRay_slider->SetResetValue(10);
  rotateRay_slider->SetValue(rend->rendParams.rotateRay, FALSE);
  rotateRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_EDIT), EDITTYPE_INT);
  rotateRay_slider->SetNumSegs(4);

  randomAngle_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_EDIT));
  randomAngle_edit->SetText(rend->rendParams.randomAngle);
  
  randomAngle_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_SLIDER));
  randomAngle_slider->SetLimits(0.0f, 1.0f, FALSE);
  randomAngle_slider->SetResetValue(0.0f);
  randomAngle_slider->SetValue(rend->rendParams.randomAngle, FALSE);
  randomAngle_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_EDIT), EDITTYPE_FLOAT);
  randomAngle_slider->SetNumSegs(4);

  sprayRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_EDIT));
  sprayRay_edit->SetText(rend->rendParams.sprayRay);
  
  sprayRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_SLIDER));
  sprayRay_slider->SetLimits(0.0f, 1.0f, FALSE);
  sprayRay_slider->SetResetValue(0.0f);
  sprayRay_slider->SetValue(rend->rendParams.sprayRay, FALSE);
  sprayRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_EDIT), EDITTYPE_FLOAT);
  sprayRay_slider->SetNumSegs(4);


	bloom_radius_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_EDIT));
	bloom_radius_edit->SetText(rend->rendParams.bloom_radius);

	bloom_radius_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_SPIN));
	bloom_radius_spin->SetLimits(0.00f, 100.00f, FALSE);
	bloom_radius_spin->SetResetValue(7.0f);   // Right click reset value
	bloom_radius_spin->SetValue(rend->rendParams.bloom_radius, FALSE); // FALSE = don't send notify yet.
	bloom_radius_spin->SetScale(1.0f);       // value change step
	bloom_radius_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_EDIT), EDITTYPE_FLOAT);
  
	CheckDlgButton(hWnd, IDC_BLOOM_ON, rend->rendParams.bloom);

	bloom_str_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLOOM_STR_EDIT));
	bloom_str_edit->SetText(rend->rendParams.bloom_radius);

	bloom_str_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLOOM_STR_SPIN));
	bloom_str_spin->SetLimits(0.00f, 5.00f, FALSE);
	bloom_str_spin->SetResetValue(0.5f);   // Right click reset value
	bloom_str_spin->SetValue(rend->rendParams.bloom_str, FALSE); // FALSE = don't send notify yet.
	bloom_str_spin->SetScale(0.1f);       // value change step
	bloom_str_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BLOOM_STR_EDIT), EDITTYPE_FLOAT);
  
	CheckDlgButton(hWnd, IDC_MLAA, rend->rendParams.mlaa);
}

void HydraRenderParamDlg::InitToolsDialog(HWND hWnd)
{
	run_script_btn = GetICustButton(GetDlgItem(hWnd, IDC_CONVERT_MAT));
  run_script_btn = GetICustButton(GetDlgItem(hWnd, IDC_CONVERT_LIGHTS));

	run_script_btn->SetType(CBT_PUSH);
}

void HydraRenderParamDlg::InitRendElemDialog(HWND hWnd)
{
  CheckDlgButton(hWnd, IDC_REND_ELEM_ALPHA, rend->rendParams.renElemAlphaOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COLOR, rend->rendParams.renElemColorOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COORD, rend->rendParams.renElemCoordOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COVERAGE, rend->rendParams.renElemCoverOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_DEPTH, rend->rendParams.renElemDepthOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_INSTID, rend->rendParams.renElemInstIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_MATID, rend->rendParams.renElemMatIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_NORMALS, rend->rendParams.renElemNormalsOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_OBJID, rend->rendParams.renElemObjIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_SHADOW, rend->rendParams.renElemShadowOn);

}

void HydraRenderParamDlg::InitOverrideDialog(HWND hWnd)
{
	env_mult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ENV_MULT));
	env_mult_edit->SetText(rend->rendParams.env_mult);

	env_mult_spin = GetISpinner(GetDlgItem(hWnd, IDC_ENV_MULT_SPIN));
  env_mult_spin->SetLimits(0.0f, 100.00f, FALSE);
  env_mult_spin->SetResetValue(1.0f);   // Right click reset value
  env_mult_spin->SetValue(rend->rendParams.env_mult, FALSE); // FALSE = don't send notify yet.
	env_mult_spin->SetScale(0.1f);       // value change step
  env_mult_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ENV_MULT), EDITTYPE_FLOAT);

  CheckDlgButton(hWnd, IDC_GRAYMAT_ON, rend->rendParams.overrideGrayMatOn);

}


void HydraRenderParamDlg::InitDebugDialog(HWND hWnd)
{
 
  CheckDlgButton(hWnd, IDC_LOG, rend->rendParams.enableLog);
  CheckDlgButton(hWnd, IDC_SEPSWAP, rend->rendParams.useSeparateSwap);

	HWND debug_list = GetDlgItem(hWnd, IDC_DEBUG_LIST);

	ListView_SetExtendedListViewStyleEx(debug_list, 0, LVS_EX_CHECKBOXES);
	//ListView_SetView(devices_list, LV_VIEW_DETAILS);
	ListView_SetBkColor(debug_list, CLR_NONE);
	ListView_SetTextBkColor(debug_list, CLR_NONE);

  if (IsDarkTheme())
	  ListView_SetTextColor(debug_list, RGB(228, 228, 228));
  else
    ListView_SetTextColor(debug_list, RGB(20, 20, 20));

	LVCOLUMN lvc;
	int iCol;
	int nCol = 1;
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	hydraStr cols[] = { _M("Name")};

	for (iCol = 0; iCol < nCol; iCol++)
	{
		lvc.iSubItem = iCol;

#ifdef MAX2012
		lvc.pszText = (LPSTR)cols[iCol].c_str();
#else
		lvc.pszText = (LPWSTR)cols[iCol].c_str();
#endif
		lvc.fmt = LVCFMT_LEFT;

		lvc.cx = 250;

		ListView_InsertColumn(debug_list, iCol, &lvc);
	}

	LVITEM lvI;
	lvI.mask = LVIF_TEXT | LVIF_STATE;
	lvI.stateMask = 0;
	lvI.iSubItem = 0;
	lvI.state = 0;
	lvI.cColumns = 1;

	std::wstring str;

	int i = 0;

	for (auto it = rend->rendParams.debugCheckBoxes.begin(); it != rend->rendParams.debugCheckBoxes.end(); it++) 
	{
		
		str = it->first;

#ifdef MAX2012
    std::string tmp = strConverter::ws2s(str);
    lvI.pszText = (LPSTR)tmp.c_str();
#else
		lvI.pszText = (LPWSTR)str.c_str();
#endif

		lvI.iItem = i;
		ListView_InsertItem(debug_list, &lvI);

#ifdef MAX2012
    std::string tmp2 = strConverter::ws2s(str);
    ListView_SetItemText(debug_list, lvI.iItem, 0, (LPSTR)tmp2.c_str());
#else
		ListView_SetItemText(debug_list, lvI.iItem, 0, (LPWSTR)str.c_str());
#endif

		ListView_SetCheckState(debug_list, 0, it->second); 
		
		i++;
	}

	UpdateWindow(hWnd);

}


void HydraRenderParamDlg::RemovePage(HWND &hPanel)
{
	if(hPanel != NULL)
	{
		ir->DeleteTabRollupPage(kTabClassID, hPanel);
		hPanel = NULL;
	}
}

void HydraRenderParamDlg::AddPage(HWND &hPanel, int IDD)
{
	if (hPanel == nullptr)
	{
		switch(IDD)
		{
		case IDD_HYDRADIALOG1:
			hMain = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_HYDRADIALOG1), HydraRenderParamDlgProc, _M("Hydra Renderer"), (LPARAM)this);
			break;
		case IDD_PATHTRACING:
			hPathTracing = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_PATHTRACING), PathTracingDlgProc, _M("Path tracing"), (LPARAM)this);
			break;
		case IDD_POSTPROC:
			hPostProc = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_POSTPROC), PostProcessingDlgProc, _M("Post Processing"), (LPARAM)this);
			/*rollupIndices[POSTPROC] = ir->GetTabIRollup(kTabClassID)->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_POSTPROC), PostProcessingDlgProc, L"Tone mapping", (LPARAM)this, APPENDROLL_CLOSED);
			hPostProc = ir->GetTabIRollup(kTabClassID)->GetPanelDlg(rollupIndices[POSTPROC]);
			ShowWindow(hPostProc,SW_SHOWNOACTIVATE);*/
			break;
		/*case IDD_SPPM_CAUSTIC:
			hSPPMC = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_SPPM_CAUSTIC), SPPMCDlgProc, _M("SPPM (Caustics)"), (LPARAM)this);
			break;
		case IDD_SPPM_DIFF:
			hSPPMD = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_SPPM_DIFF), SPPMDDlgProc, _M("SPPM (Diffuse)"), (LPARAM)this);
			break;*/
		case IDD_OVERRIDE:
			hOverr = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_OVERRIDE), OverrideDlgProc, _M("Override"), (LPARAM)this);
			break;
		case IDD_DEBUG:
			hDebug = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_DEBUG), DebugDlgProc, _M("Debug"), (LPARAM)this);
			break;
		case IDD_MLT:
			hMLT = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_MLT), MLTDlgProc, _M("MLT"), (LPARAM)this);
			break;
		case IDD_TOOLS:
			hTools = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_TOOLS), ToolsDlgProc, _M("Tools"), (LPARAM)this);
			break;
    case IDD_RENDER_ELEMENTS:
      hRendElem = ir->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_RENDER_ELEMENTS), RendElemDlgProc, _M("Render elements"), (LPARAM)this);
      break;

		default:
			break;
		}
	}

}


void HydraRenderParamDlg::ReadGuiToRendParams(HydraRenderParams* pRendParams)
{
  if (pRendParams == nullptr)
    return;

  //MessageBoxA(NULL, "HydraRenderParamDlg::ReadGuiToRendParams", "", 0);

  pRendParams->renderQuality = renderQuality_slider->GetFVal();

  pRendParams->primary      = SendDlgItemMessage(hMain, IDC_PRIMARY, CB_GETCURSEL, 0, 0);
  pRendParams->secondary    = SendDlgItemMessage(hMain, IDC_SECONDARY, CB_GETCURSEL, 0, 0);

  pRendParams->manualMode   = 1;
  pRendParams->enableCaustics = IsDlgButtonChecked(hMain, IDC_CAUSTICSON);
  pRendParams->enableTextureResize = IsDlgButtonChecked(hMain, IDC_ENABLE_TEXTURE_RESIZE);
  pRendParams->productionMode = IsDlgButtonChecked(hMain, IDC_PRODUCTION_MODE);

	pRendParams->device_id.clear();
	HWND devices_list = GetDlgItem(hMain, IDC_DEVICES_LIST);
	int numItems = ListView_GetItemCount(devices_list);

	for (auto i = 0; i < numItems; ++i)
	{
    hydraChar bufText[4];
    ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));
		if (ListView_GetCheckState(devices_list, i))
		{
			pRendParams->device_id[std::stoi(bufText)] = true;
		}
    else
    {
      pRendParams->device_id[std::stoi(bufText)] = false;
    }
	}
  //pRendParams->device_id = SendDlgItemMessage(hMain, IDC_DEVICE, CB_GETCURSEL, 0, 0);

  pRendParams->timeLimitOn = IsDlgButtonChecked(hMain, IDC_TIMELIMIT);
  if (pRendParams->timeLimitOn)
    pRendParams->timeLimit = timelimit_spin->GetIVal();
  else
    pRendParams->timeLimit = 0;

  pRendParams->lensradius = lensradius_spin->GetFVal();
  pRendParams->enableDOF  = IsDlgButtonChecked(hMain, IDC_DOF_ON);
 	//pRendParams->saveComposeLayers = IsDlgButtonChecked(hMain, IDC_LAYERS_ON);

  pRendParams->rememberDevice    = IsDlgButtonChecked(hMain, IDC_REMEMBER_MDLG);

  /*
	if (pRendParams->rememberDevice)
		createDeviceSettingsFile(pRendParams->engine_type, pRendParams->device_id, hydraRender_mk3::m_rendRef);
	else
		createDeviceSettingsFile(-1, rend->rendParams.device_id, hydraRender_mk3::m_rendRef);
  */

  //path tracing
  pRendParams->seed = SendDlgItemMessage(hPathTracing, IDC_SEED, CB_GETCURSEL, 0, 0);
  pRendParams->minrays = minrays_spin->GetIVal();
  pRendParams->maxrays = maxrays_spin->GetIVal();
  pRendParams->raybounce  = raybounce_spin->GetIVal();
  pRendParams->diffbounce = diffbounce_spin->GetIVal();
  pRendParams->relative_error = relative_error_spin->GetFVal();
	pRendParams->bsdf_clamp = bsdf_clamp_spin->GetFVal();
  pRendParams->useRR = IsDlgButtonChecked(hPathTracing, IDC_RR_ON);
  pRendParams->causticRays = IsDlgButtonChecked(hPathTracing, IDC_CAUSTICS_ON);
  pRendParams->guided = IsDlgButtonChecked(hPathTracing, IDC_GUIDED);
	pRendParams->bsdf_clamp_on = IsDlgButtonChecked(hPathTracing, IDC_BSDF_CLAMP_ON);
	pRendParams->env_clamp = env_clamp_spin->GetFVal();
	pRendParams->preset = SendDlgItemMessage(hPathTracing, IDC_PRESETS, CB_GETCURSEL, 0, 0);
  
  // estimateLDR;
  // errorLDR;


  //SPPM Caustics
  /*pRendParams->maxphotons_c = maxphotons_c_spin->GetIVal();
  pRendParams->caustic_power = caustic_power_spin->GetFVal();
  pRendParams->retrace_c = retrace_c_spin->GetIVal();
  pRendParams->initial_radius_c = initial_radius_c_spin->GetFVal();
  pRendParams->visibility_c = IsDlgButtonChecked(hSPPMC, IDC_VISIBILITY_C_ON);
  pRendParams->alpha_c = alpha_c_spin->GetFVal();


  //SPPM Diffuse
  pRendParams->maxphotons_d = maxphotons_d_spin->GetIVal();
  pRendParams->retrace_d = retrace_d_spin->GetIVal();
  pRendParams->initial_radius_d = initial_radius_d_spin->GetFVal();
  pRendParams->visibility_d = IsDlgButtonChecked(hSPPMD, IDC_VISIBILITY_D_ON);
  pRendParams->irr_map = IsDlgButtonChecked(hSPPMD, IDC_IRRMAP);
  pRendParams->alpha_d = alpha_d_spin->GetFVal();*/


  //Post process
  pRendParams->use_denoiser = IsDlgButtonChecked(hMain, IDC_DENOISER);
  pRendParams->noiseLvl = noise_lvl_slider->GetFVal();

	pRendParams->postProcOn      = IsDlgButtonChecked(hPostProc, IDC_POSTPROC_ON);
  pRendParams->exposure        = exposure_slider->GetFVal();
  pRendParams->compress        = compress_slider->GetFVal();
  pRendParams->contrast        = contrast_slider->GetFVal();
  pRendParams->saturation      = saturation_slider->GetFVal();
  pRendParams->whiteBalance    = whiteBalance_slider->GetFVal();
  pRendParams->whitePointColor = whitePoint_selector->GetAColor();
  pRendParams->uniformContrast = uniformContrast_slider->GetFVal();
  pRendParams->normalize       = normalize_slider->GetFVal();
  pRendParams->chromAberr      = chromAberr_slider->GetFVal();
  pRendParams->vignette        = vignette_slider->GetFVal();
  pRendParams->sharpness       = sharpness_slider->GetFVal();
  pRendParams->sizeStar        = sizeStar_slider->GetFVal();
  pRendParams->numRay          = numRay_slider->GetFVal();
  pRendParams->rotateRay       = rotateRay_slider->GetFVal();
  pRendParams->randomAngle     = randomAngle_slider->GetFVal();
  pRendParams->sprayRay        = sprayRay_slider->GetFVal();

  pRendParams->bloom           = IsDlgButtonChecked(hPostProc, IDC_BLOOM_ON);
	pRendParams->bloom_radius    = bloom_radius_spin->GetFVal();
	pRendParams->bloom_str       = bloom_str_spin->GetFVal();
	//pRendParams->hydraFrameBuf   = IsDlgButtonChecked(hPostProc, IDC_FRAMEBUFFER);
	pRendParams->mlaa            = IsDlgButtonChecked(hPostProc, IDC_MLAA);



  //Override
  pRendParams->env_mult = env_mult_spin->GetFVal();
  pRendParams->overrideGrayMatOn = IsDlgButtonChecked(hOverr, IDC_GRAYMAT_ON);


	//MLT
  pRendParams->mlt_plarge     = 0.5f; // (float)(mlt_plarge_slider->GetIVal()) / 100.0f;
	pRendParams->mlt_iters_mult = mlt_iters_mult_slider->GetIVal();
	pRendParams->mlt_burn_iters = mlt_burn_iters_slider->GetIVal()*64;
	pRendParams->mlt_enable_median_filter = IsDlgButtonChecked(hMLT, IDC_MLT_MEDIAN_ON);
	pRendParams->mlt_median_filter_threshold = mlt_median_filter_threshold_spin->GetFVal();


	//Debug
	HWND debug_list = GetDlgItem(hDebug, IDC_DEBUG_LIST);
	numItems = ListView_GetItemCount(debug_list);
	for (auto i = 0; i < numItems; ++i)
	{
		hydraChar bufText[64];
		ListView_GetItemText(debug_list, i, 0, &bufText[0], sizeof(bufText));
			
		std::wstring debugParamName = strConverter::c2ws(bufText);
		pRendParams->debugCheckBoxes[debugParamName] = ListView_GetCheckState(debug_list, i);
	}
  
  pRendParams->liteMode = pRendParams->debugCheckBoxes[L"LITE_CORE"];
  pRendParams->useSeparateSwap = IsDlgButtonChecked(hDebug, IDC_SEPSWAP);
  pRendParams->enableLog = IsDlgButtonChecked(hDebug, IDC_LOG);
  //hydraRender_mk3::m_lastRendParams = (*pRendParams);


  // Render elements
  pRendParams->renElemAlphaOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_ALPHA);
  pRendParams->renElemColorOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_COLOR);
  pRendParams->renElemCoordOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_COORD);
  pRendParams->renElemCoverOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_COVERAGE);
  pRendParams->renElemDepthOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_DEPTH);
  pRendParams->renElemInstIdOn  = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_INSTID);
  pRendParams->renElemMatIdOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_MATID);
  pRendParams->renElemNormalsOn = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_NORMALS);
  pRendParams->renElemObjIdOn   = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_OBJID);
  pRendParams->renElemShadowOn  = IsDlgButtonChecked(hRendElem, IDC_REND_ELEM_SHADOW);

}

void HydraRenderParamDlg::AcceptParams()
{
  //MessageBoxA(NULL, "HydraRenderParamDlg::AcceptParams", "", 0);

  if (rend == nullptr)
    return;

  ReadGuiToRendParams(&rend->rendParams);
}

void HydraRenderParamDlg::RejectParams()
{
  //MessageBoxA(NULL, "HydraRenderParamDlg::RejectParams", "", 0);
}


void HydraRenderParamDlg::SetPTParamsFromSlider()
{
  const float qualitySliderVal = renderQuality_slider->GetFVal() / 100.0f;

  const float min_maxRaysPP = 64.0f;
  const float max_maxRaysPP = 65536.0f;

  const float maxRaysPP     = (max_maxRaysPP - min_maxRaysPP)  * (qualitySliderVal*qualitySliderVal*qualitySliderVal*qualitySliderVal) + min_maxRaysPP;

  maxrays_spin->SetValue(maxRaysPP, TRUE);
  maxrays_edit->SetText(int(maxRaysPP));
}

void HydraRenderParamDlg::SetSliderFromPTParams()
{
  const float currRaysPP = maxrays_spin->GetIVal();

  const float min_maxRaysPP = 64.0f;
  const float max_maxRaysPP = 65536.0f;

  const float qualitySliderVal = pow((currRaysPP - min_maxRaysPP) / (max_maxRaysPP - min_maxRaysPP), 0.25f);

  renderQuality_slider->SetValue(qualitySliderVal * 100.0f, TRUE);
}

static INT_PTR CALLBACK HydraRenderParamDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
            dlg->InitMainHydraDialog(hWnd);
        }
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
          ReleaseISlider(dlg->renderQuality_slider);

          ReleaseICustEdit(dlg->maxrays_edit);
          ReleaseICustEdit(dlg->raybounce_edit);
          ReleaseICustEdit(dlg->diffbounce_edit);
					ReleaseICustEdit(dlg->timelimit_edit);
					ReleaseICustEdit(dlg->lensradius_edit);

          ReleaseISpinner(dlg->maxrays_spin);
          ReleaseISpinner(dlg->raybounce_spin);
          ReleaseISpinner(dlg->diffbounce_spin);
					ReleaseISpinner(dlg->timelimit_spin);
					ReleaseISpinner(dlg->lensradius_spin);
				}
        break;      

      case WM_COMMAND:
				switch (HIWORD(wParam)) 
				{ 
					case LBN_SELCHANGE: 
					{
						if(LOWORD( wParam ) == IDC_PRIMARY)
						{
						}
						else if(LOWORD( wParam ) == IDC_SECONDARY)
							int pr = SendDlgItemMessage(dlg->hMain, IDC_SECONDARY, CB_GETCURSEL, 0, 0);
					}
					break;
					case BN_CLICKED:  
						if(LOWORD( wParam ) == IDC_TIMELIMIT)
						{
							LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
							if (chkState == BST_CHECKED)
							{
								dlg->timelimit_edit->Enable();
								dlg->timelimit_spin->Enable();
							}
							else
							{
								dlg->timelimit_edit->Disable();
								dlg->timelimit_spin->Disable();
							}
						}
						else if (LOWORD(wParam) == IDC_MAT_DEVICE_BTN)
						{
              std::wstring path1 = HydraInstallPathW() + L"pluginFiles/material/hydra_profile_generated.xml";
              std::wstring path2 = HydraInstallPathW() + L"pluginFiles/material/hydra_profile_generated_copy.xml";

              CopyFileW(path2.c_str(), path1.c_str(), FALSE); // restore hydra_profile_generated

							dlg->ReadGuiToRendParams(&dlg->rend->rendParams);

              g_materialProcessStart = false;
						}
            else if (LOWORD(wParam) == IDC_REMEMBER_MDLG)
						{
							LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);

              if (chkState == BST_UNCHECKED)
              {
                createDeviceSettingsFile(-1, dlg->rend->rendParams.device_id, hydraRender_mk3::m_rendRef);
              }
							else
							{
								int mode = SendDlgItemMessage(hWnd, IDC_ENGINE, CB_GETCURSEL, 0, 0);
								HWND devices_list = GetDlgItem(hWnd, IDC_DEVICES_LIST);
								int numItems = ListView_GetItemCount(devices_list);
                std::unordered_map<int, bool> deviceIDs;

								for (auto i = 0; i < numItems; ++i)
								{
                  hydraChar bufText[4];
                  ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));
									if (ListView_GetCheckState(devices_list, i))
                    deviceIDs[std::stoi(bufText)] = true;
                  else
                    deviceIDs[std::stoi(bufText)] = false;
								}

								createDeviceSettingsFile(mode, deviceIDs, hydraRender_mk3::m_rendRef);
							}
						}
						break;
				}
        break;
      case CC_SLIDER_CHANGE:
        if (LOWORD(wParam) == IDC_RENDERQUALITY_SLIDER)
          dlg->SetPTParamsFromSlider();
        break;
      case CC_SPINNER_CHANGE:
        if (LOWORD(wParam) == IDC_MAXRAY_SPIN)
          dlg->SetSliderFromPTParams();
        break;
      case WM_NOTIFY:
        switch (((LPNMHDR)lParam)->code)
        {
        case LVN_ITEMCHANGED:
          if (((LPNMHDR)lParam)->idFrom == IDC_DEVICES_LIST)
          {
            HWND checkBoxRememberDevice = GetDlgItem(hWnd, IDC_REMEMBER_MDLG);
            SendMessage(checkBoxRememberDevice, BM_SETCHECK, BST_UNCHECKED, 0);
          }
          break;
        }
        break;
      case WM_LBUTTONDOWN:
      case WM_MOUSEMOVE:
      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd,msg,wParam,lParam);
        break;
      default:
        return FALSE;
  }  
  return TRUE;
}

static INT_PTR CALLBACK PathTracingDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
            dlg->InitPathTracingDialog(hWnd);
        }
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
					ReleaseICustEdit(dlg->minrays_edit);				
					ReleaseICustEdit(dlg->relative_error_edit);
					ReleaseICustEdit(dlg->bsdf_clamp_edit);
					ReleaseICustEdit(dlg->env_clamp_edit);

					ReleaseISpinner(dlg->minrays_spin);
					ReleaseISpinner(dlg->relative_error_spin);
					ReleaseISpinner(dlg->bsdf_clamp_spin);
					ReleaseISpinner(dlg->env_clamp_spin);
				}
        break;      

      case WM_COMMAND:
				switch (HIWORD(wParam))
				{
					case BN_CLICKED:
					{
						if (LOWORD(wParam) == IDC_BSDF_CLAMP_ON)
						{
							LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
							if (chkState == BST_UNCHECKED)
							{
								dlg->rend->rendParams.bsdf_clamp_on = 0;
								dlg->rend->rendParams.bsdf_clamp = 1000000.0f;
								dlg->bsdf_clamp_edit->SetText(1000000.0f);
								dlg->bsdf_clamp_edit->Disable();
								dlg->bsdf_clamp_spin->Disable();
							}
							else
							{
								dlg->rend->rendParams.bsdf_clamp_on = 1;
								dlg->rend->rendParams.bsdf_clamp = 100.0f;
								dlg->bsdf_clamp_edit->SetText(100.0f);
								dlg->bsdf_clamp_edit->Enable();
								dlg->bsdf_clamp_spin->Enable();
							}
						}
					}
					case LBN_SELCHANGE:
					{
						if (LOWORD(wParam) == IDC_PRESETS)
						{
							int pr = SendDlgItemMessage(dlg->hPathTracing, IDC_PRESETS, CB_GETCURSEL, 0, 0);
							dlg->LoadPreset(pr);
						}
						break;
					}
				}

      case WM_LBUTTONDOWN:
      case WM_MOUSEMOVE:
      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd,msg,wParam,lParam);
        break;
      default:
        return FALSE;
  }  
  return TRUE;
}

static INT_PTR CALLBACK MLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
	switch (msg)
	{
	case WM_INITDIALOG:

		dlg = (HydraRenderParamDlg*)lParam;
		DLSetWindowLongPtr(hWnd, lParam);
		if (dlg)
		{
			if (dlg->prog)
				dlg->InitProgDialog(hWnd);
			else
				dlg->InitMLTDialog(hWnd);
		}
		break;

	case WM_DESTROY:
		if (!dlg->prog)
		{
			ReleaseICustEdit(dlg->mlt_median_filter_threshold_edit);
			ReleaseICustEdit(dlg->mlt_burn_iters_edit);
			ReleaseICustEdit(dlg->mlt_plarge_edit);
			ReleaseICustEdit(dlg->mlt_iters_mult_edit);

			ReleaseISpinner(dlg->mlt_median_filter_threshold_spin);

			ReleaseISlider(dlg->mlt_burn_iters_slider);
			ReleaseISlider(dlg->mlt_plarge_slider);
			ReleaseISlider(dlg->mlt_iters_mult_slider);
		}
		break;

	case WM_COMMAND:
		break;

	case WM_LBUTTONDOWN:
	case WM_MOUSEMOVE:
	case WM_LBUTTONUP:
		dlg->ir->RollupMouseMessage(hWnd, msg, wParam, lParam);
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

static INT_PTR CALLBACK SPPMCDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
            dlg->InitSPPMCDialog(hWnd);
        }
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
					ReleaseICustEdit(dlg->maxphotons_c_edit);
					ReleaseICustEdit(dlg->initial_radius_c_edit);
					ReleaseICustEdit(dlg->caustic_power_edit);
					ReleaseICustEdit(dlg->retrace_c_edit);
					ReleaseICustEdit(dlg->alpha_c_edit);

					ReleaseISpinner(dlg->maxphotons_c_spin);
					ReleaseISpinner(dlg->initial_radius_c_spin);
					ReleaseISpinner(dlg->caustic_power_spin);
					ReleaseISpinner(dlg->retrace_c_spin);
					ReleaseISpinner(dlg->alpha_c_spin);
				}
        break;      

      case WM_COMMAND:
        // We don't care about the UI controls.
        // We take the value in AcceptParams() instead.
        break;

      case WM_LBUTTONDOWN:
      case WM_MOUSEMOVE:
      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd,msg,wParam,lParam);
        break;
      default:
        return FALSE;
  }  
  return TRUE;
}

static INT_PTR CALLBACK SPPMDDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
            dlg->InitSPPMDDialog(hWnd);
        }
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
					ReleaseICustEdit(dlg->maxphotons_d_edit);
					ReleaseICustEdit(dlg->initial_radius_d_edit);
					ReleaseICustEdit(dlg->retrace_d_edit);
					ReleaseICustEdit(dlg->alpha_d_edit);

					ReleaseISpinner(dlg->maxphotons_d_spin);
					ReleaseISpinner(dlg->initial_radius_d_spin);
					ReleaseISpinner(dlg->retrace_d_spin);
					ReleaseISpinner(dlg->alpha_d_spin);
				}
        break;      

      case WM_COMMAND:
        // We don't care about the UI controls.
        // We take the value in AcceptParams() instead.
        break;

      case WM_LBUTTONDOWN:
      case WM_MOUSEMOVE:
      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd,msg,wParam,lParam);
        break;
      default:
        return FALSE;
  }  
  return TRUE;
}



static INT_PTR CALLBACK PostProcessingDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
            dlg->InitPostProcDialog(hWnd);
        }
				//dlg->ir->GetTabIRollup(kTabClassID)->Hide(dlg->rollupIndices[dlg->POSTPROC]);
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
          ReleaseICustEdit(dlg->exposure_edit);
          ReleaseICustEdit(dlg->compress_edit);
          ReleaseICustEdit(dlg->contrast_edit);
          ReleaseICustEdit(dlg->saturation_edit);
          ReleaseICustEdit(dlg->whiteBalance_edit);
          ReleaseIColorSwatch(dlg->whitePoint_selector);
          ReleaseICustEdit(dlg->uniformContrast_edit);
          ReleaseICustEdit(dlg->normalize_edit);
          ReleaseICustEdit(dlg->chromAberr_edit);
          ReleaseICustEdit(dlg->vignette_edit);
          ReleaseICustEdit(dlg->sharpness_edit);
          ReleaseICustEdit(dlg->sizeStar_edit);
          ReleaseICustEdit(dlg->numRay_edit);
          ReleaseICustEdit(dlg->rotateRay_edit);
          ReleaseICustEdit(dlg->randomAngle_edit);
          ReleaseICustEdit(dlg->sprayRay_edit);

					ReleaseICustEdit(dlg->bloom_radius_edit);
					ReleaseICustEdit(dlg->bloom_str_edit);


          ReleaseISlider(dlg->exposure_slider);
          ReleaseISlider(dlg->compress_slider);
          ReleaseISlider(dlg->contrast_slider);
          ReleaseISlider(dlg->saturation_slider);
          ReleaseISlider(dlg->whiteBalance_slider);
          ReleaseISlider(dlg->uniformContrast_slider);
          ReleaseISlider(dlg->normalize_slider);
          ReleaseISlider(dlg->chromAberr_slider);
          ReleaseISlider(dlg->vignette_slider);
          ReleaseISlider(dlg->sharpness_slider);
          ReleaseISlider(dlg->sizeStar_slider);
          ReleaseISlider(dlg->numRay_slider);
          ReleaseISlider(dlg->rotateRay_slider);
          ReleaseISlider(dlg->randomAngle_slider);
          ReleaseISlider(dlg->sprayRay_slider);

					ReleaseISpinner(dlg->bloom_radius_spin);
					ReleaseISpinner(dlg->bloom_str_spin);

					ReleaseICustButton(dlg->denoise_btn);
          ReleaseICustButton(dlg->resetPostProc);

          ReleaseICustEdit(dlg->noise_lvl_edit);
          ReleaseISlider(dlg->noise_lvl_slider);

				}
        break;      

      case WM_COMMAND:
        switch (HIWORD(wParam)) 
				{ 
					case BN_CLICKED: 
           /* switch (LOWORD(wParam))
            {
            case IDC_FAST_EXR:
            case IDC_BLABLABA:
              //........
            }*/
            // ��������� denoise � ����������� �� ������.
            if (LOWORD(wParam) == IDC_DO_DENOISE)
            {
              if (dlg != nullptr && dlg->rend != nullptr && dlg->rend->last_render != nullptr)
              {
                int renderWidth = dlg->rend->rendParams.nMaxx - dlg->rend->rendParams.nMinx;
                int renderHeight = dlg->rend->rendParams.nMaxy - dlg->rend->rendParams.nMiny;
            
                SetFocus(dlg->rend->last_render->GetWindow());
                SetActiveWindow(dlg->rend->last_render->GetWindow());
            
                SetFocus(hWnd);
                SetActiveWindow(hWnd);

                hrFBIResize(dlg->rend->hydra_out_render, renderWidth, renderHeight);
                hrFBIResize(dlg->rend->hydra_raw_render, renderWidth, renderHeight);

                dlg->rend->DoDenoise(dlg->rend->last_render, renderHeight, renderWidth);
                dlg->rend->DoPostProc(dlg->rend->last_render);
                dlg->rend->displayLastRender();
              } 
            }
						else if (LOWORD(wParam) == IDC_POSTPROC_ON)
						{              
							LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);

							if (chkState == BST_CHECKED)
                dlg->rend->UpdatePostProc(hWnd, dlg);
							else
                dlg->rend->DisplaySource(hWnd, dlg);
						}
            else if (LOWORD(wParam) == IDC_RESET_POSTPROC)
            {
              dlg->rend->ResetPostProc(dlg);
              dlg->rend->UpdatePostProc(hWnd, dlg);
            }
          break;

					/*default:
						dlg->AcceptParams();*/
				}
        break;

      case CC_SLIDER_BUTTONUP:        
        if (dlg->rend->rendParams.postProcOn)
          dlg->rend->UpdatePostProc(hWnd, dlg);
        break;

      case CC_COLOR_SEL:
        if (dlg->rend->rendParams.postProcOn)
          dlg->rend->DisplaySource(hWnd, dlg);
        break;

      case CC_COLOR_CLOSE:
        if (dlg->rend->rendParams.postProcOn)
          dlg->rend->UpdatePostProc(hWnd, dlg);
        break;

      case WM_CUSTEDIT_ENTER:
        dlg->rend->UpdatePostProc(hWnd, dlg);
        break;

      case WM_LBUTTONDOWN:
        break;

      case WM_MOUSEMOVE:
        break;

      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd, msg, wParam, lParam);
        break;

      default:
        return FALSE;
  }  
  return TRUE;
}

static INT_PTR CALLBACK OverrideDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg) 
  {
      case WM_INITDIALOG:
        
        dlg = (HydraRenderParamDlg*)lParam;
        DLSetWindowLongPtr(hWnd, lParam);
        if (dlg) 
        {
          if (dlg->prog)
            dlg->InitProgDialog(hWnd);
          else
						dlg->InitOverrideDialog(hWnd);
        }
        break;

      case WM_DESTROY:
				if (!dlg->prog)
				{
					ReleaseICustEdit(dlg->env_mult_edit);
					ReleaseISpinner(dlg->env_mult_spin);
				}
        break;      

      case WM_COMMAND:
        // We don't care about the UI controls.
        // We take the value in AcceptParams() instead.
        break;

      case WM_LBUTTONDOWN:
      case WM_MOUSEMOVE:
      case WM_LBUTTONUP:
        dlg->ir->RollupMouseMessage(hWnd,msg,wParam,lParam);
        break;
      default:
        return FALSE;
  }  
  return TRUE;
}

static INT_PTR CALLBACK ToolsDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
	switch (msg)
	{
	case WM_INITDIALOG:

		dlg = (HydraRenderParamDlg*)lParam;
		DLSetWindowLongPtr(hWnd, lParam);
		if (dlg)
		{
			if (dlg->prog)
				dlg->InitProgDialog(hWnd);
			else
				dlg->InitToolsDialog(hWnd);
		}
		break;

	case WM_DESTROY:
		ReleaseICustButton(dlg->run_script_btn);
		break;

	case WM_COMMAND:
		switch (HIWORD(wParam))
		{
			case BN_CLICKED:
				if (LOWORD(wParam) == IDC_CONVERT_MAT)
				{
					//filein_script_ex(const MCHAR* filename, MCHAR* error_message);
					std::ifstream infile(dlg->rend->convertMatScriptPath);					
					std::stringstream buffer;
					buffer << infile.rdbuf();

					std::string convertMatScript = buffer.str();
#ifdef MAX2012
          ExecuteMAXScriptScript((char*)convertMatScript.c_str(), FALSE);
#else
					ExecuteMAXScriptScript(strConverter::s2ws(convertMatScript).c_str(), FALSE);
#endif
				}
        else if (LOWORD(wParam) == IDC_CONVERT_LIGHTS)
        {
          //filein_script_ex(const MCHAR* filename, MCHAR* error_message);
          std::ifstream infile(dlg->rend->convertLightsScriptPath);
          std::stringstream buffer;
          buffer << infile.rdbuf();

          std::string convertLightsScript = buffer.str();
#ifdef MAX2012
          ExecuteMAXScriptScript((char*)convertMatScript.c_str(), FALSE);
#else
          ExecuteMAXScriptScript(strConverter::s2ws(convertLightsScript).c_str(), FALSE);
#endif
        }
		}
		break;

	case WM_LBUTTONDOWN:
	case WM_MOUSEMOVE:
	case WM_LBUTTONUP:
		dlg->ir->RollupMouseMessage(hWnd, msg, wParam, lParam);
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

static INT_PTR CALLBACK RendElemDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
  case WM_INITDIALOG:
    dlg = (HydraRenderParamDlg*)lParam;
    DLSetWindowLongPtr(hWnd, lParam);
    if (dlg)
    {
      if (dlg->prog)
        dlg->InitProgDialog(hWnd);
      else
        dlg->InitRendElemDialog(hWnd);
    }
    break;

  case WM_DESTROY:
    break;

  case WM_COMMAND:
    switch (HIWORD(wParam))
    {
    case BN_CLICKED:
      if (LOWORD(wParam) == IDC_GENERATE_RENDER_ELEMENTS)
      {
        // Press button for generate pass.
      }
      break;
    }
    break;

  case WM_LBUTTONDOWN:
    break;
  case WM_MOUSEMOVE:
    break;
  case WM_LBUTTONUP:
    dlg->ir->RollupMouseMessage(hWnd, msg, wParam, lParam);
    break;

  default:
    return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK DebugDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
	switch (msg)
	{
	case WM_INITDIALOG:

		dlg = (HydraRenderParamDlg*)lParam;
		DLSetWindowLongPtr(hWnd, lParam);
		if (dlg)
		{
			if (dlg->prog)
				dlg->InitProgDialog(hWnd);
			else
				dlg->InitDebugDialog(hWnd);
		}
		break;

	case WM_DESTROY:
		break;

	case WM_COMMAND:
		break;

	case WM_LBUTTONDOWN:
	case WM_MOUSEMOVE:
	case WM_LBUTTONUP:
		dlg->ir->RollupMouseMessage(hWnd, msg, wParam, lParam);
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

RendParamDlg *hydraRender_mk3::CreateParamDialog(IRendParams *ir,BOOL prog)
{
  if (!prog)
  {
    hydraRender_mk3::m_pLastParamDialog = new HydraRenderParamDlg(this, ir, prog);
    return hydraRender_mk3::m_pLastParamDialog;
  }
  else
  {
    return nullptr;
  }
}



static const int kRendRollupWidth = 222;
//static const Class_ID blurRaytrace ( 0x4fa95e9b, 0x9a26e66 );


BaseInterface* hydraRender_mk3::GetInterface ( Interface_ID id )
{
   if ( id == TAB_DIALOG_OBJECT_INTERFACE_ID ) {
      ITabDialogObject* r = this;
      return r;
   }
	 /*else if (id == IREND_ELEM_MGR_INTERFACE)
	 {
		 return this;
	 }*/
   else {
      return Renderer::GetInterface ( id );
   }
}

// ITabDialogObject
// Add the pages you want to the dialog. Use tab as the plugin
// associated with the pages. This will allows the manager
// to remove and add the correct pages to the dialog.
void hydraRender_mk3::AddTabToDialog ( ITabbedDialog* dialog, ITabDialogPluginTab* tab )
{
   //dialog->AddRollout ( GetString( IDS_CLASS_NAME ), NULL,
   //   kTabClassID, tab, -1, kRendRollupWidth, 0,
   //   0, ITabbedDialog::kSystemPage );

   dialog->AddRollout(L"Engine", NULL,
     kTabClassID, tab, -1, kRendRollupWidth, 0,
     0, ITabbedDialog::kSystemPage);

   dialog->AddRollout(L"Post process", NULL,
     kTabClassID2, tab, -1, kRendRollupWidth, 0,
     0, ITabbedDialog::kSystemPage);

   dialog->AddRollout(L"Tools", NULL,
     kTabClassID3, tab, -1, kRendRollupWidth, 0,
     0, ITabbedDialog::kSystemPage);

   dialog->AddRollout(L"Render elements", NULL,
     kTabClassID4, tab, -1, kRendRollupWidth, 0,
     0, ITabbedDialog::kSystemPage);

}

int hydraRender_mk3::AcceptTab ( ITabDialogPluginTab* tab )
{
  Class_ID id = tab->GetClassID();
	
	/*switch (tab->GetSuperClassID())
	{
	case RENDER_ELEMENT_CLASS_ID:
		return TAB_DIALOG_ADD_TAB;
	}*/
	
	if (id == hydraRender_mk3_CLASS_ID)
		return TAB_DIALOG_ADD_TAB;
	else
		return 0;
		
	/*
	
	switch ( tab->GetSuperClassID ( ) ) {
   case RADIOSITY_CLASS_ID:
      return 0;         // Don't show the advanced lighting tab
   }
	 */
   // Accept all other tabs
   //return TAB_DIALOG_ADD_TAB;
}