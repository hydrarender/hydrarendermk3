#ifndef __IMPORTSTRUCTS_H__
#define __IMPORTSTRUCTS_H__

#include <vector>
#include <string>
#include <map>


#ifdef MAX2012
typedef std::string hydraStr;
typedef char hydraChar;
#else
typedef std::wstring hydraStr;
typedef wchar_t hydraChar;
#endif

#include <VertexNormal.h>

using MaxSDK::VertexNormal;




struct GeometryObj
{
  GeometryObj()
  {
    clear();
  }

  float m[16];
  int n_verts;
  int n_faces;
  hydraStr mesh_id;
  
  std::vector<int> material_id;
  
  std::vector<float> positions;
  std::vector<int> pos_indeces;

  std::vector<int> face_smoothgroups;

  std::vector<VertexNormal> vnorms2;
  std::vector<int>          faceV;

  std::vector<float> normsSpecified; // like in Open Collada plugin
  std::vector<int>   normsSpecIndices;


  std::vector<float> tex_coords;
  std::vector<int>   tex_coords_indices;

  bool hasNegativeScale;
  
  float bbox[6];

	template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & m;
    ar & n_verts;
		ar & n_faces;
		ar & mesh_id;
		ar & material_id;
		ar & positions;
		ar & pos_indeces;
    ar & face_smoothgroups;
		ar & tex_coords;
    ar & tex_coords_indices;
    ar & bbox;
  }

  void clear()
  {
    n_verts = 0;
    n_faces = 0;

    material_id.resize(0);
    positions.resize(0);
    pos_indeces.resize(0);
    face_smoothgroups.resize(0);
    tex_coords.resize(0);
    tex_coords_indices.resize(0);
    
    faceV            = std::vector<int>();
    normsSpecified   = std::vector<float>();
    normsSpecIndices = std::vector<int>();

    vnorms2          = std::vector<VertexNormal>();

    hasNegativeScale = false;
  }

  void reserveMemory(int n)
  {
    n_verts = 0;
    n_faces = 0;

    pos_indeces.reserve(3*n);
    positions.reserve(n);
    vnorms2.reserve(n);
    material_id.reserve(n/10);
    tex_coords_indices.reserve(3*n);
    tex_coords.reserve(n);
  }


};


struct CameraObj
{
  CameraObj()
  {
    fov = 45;
    nearClipPlane = 0.001f;
    farClipPlane  = 10000.0f;
    zoom = 1.0f;
    tdist = 1.0f;

    dofEnabled = false;
    isOrtho = false;

    tiltX      = 0.0f;
    tiltY      = 0.0f;
    tiltShiftX = 0.0f;
    tiltShiftY = 0.0f;

    upVector[0] = 0.0f;
    upVector[1] = 1.0f;
    upVector[2] = 0.0f;
  }

  float fov;
  float farClipPlane;
  float nearClipPlane;
  float direction[3];
  float upVector[3];
  float zoom;
  float tdist;

  float tiltX;
  float tiltY;
  float tiltShiftX;
  float tiltShiftY;

  float dofFocalDist;
  float dofStrength;
  bool  dofEnabled;

  bool  isOrtho;
  bool  isTargeted;

  float posMatrix[16];
  float targetMatrix[16];
  float worldViewMatrix[16];

	bool isAnimated;
	int frames;
	std::vector <std::string> perFrame;

  std::wstring camName;

};


struct LightObj
{
  enum LDISTRIBTYPES{ LD_UNIFORM = 0, LD_DIFFUSE = 1, LD_SPOT = 2, LD_IES = 3};

  LightObj()
  {
    for(int i=0;i<3;i++)
    {
      position[i]  = 0;
      direction[i] = 0;
      color[i]     = 0;
      colorSecondary[i] = 0;
    }

    intensity = 0;
    lightName        = _M("default_name");
    skyPortalEnvMode = L"environment"; 
    skyPortalEnvTex  = L""; 

    for(int k=0;k<4;k++)
    {
      for(int j=0;j<4;j++)
      {
        float val = (k == j) ? 1.0f : 0.0f; 
        m[j*4+k] = val;
        targetMatrix[j*4+k] = val;
      }
    }

    isTargeted   = false;
    on           = true;
    isVisiable   = true;
    causticLight = false;

    directLightStart = 0.0f;
    envTexAmt = 1.0f;
    kc = 1.0f; kl = 0.0f; kq = 0.0f;

    lightDitributionType = LD_UNIFORM;
    useSeparateDiffuseLight = false;

    shadowSoftness = 0.0f;
    skyTurbidity = 1.0f;
    photonMaxRadius = 1e6f;
  }

	hydraStr lightName;
	std::wstring lightType;
	std::wstring shadowType;
	std::wstring spotShape;
	std::wstring iesXmlData;

	std::wstring envTexturePath;
	std::string envTextureSampler;

	std::wstring skyPortalEnvMode;
	std::wstring skyPortalEnvTex;
	std::string skyPortalEnvTexSampler;
	std::wstring shadowTexStr;
	std::string  shadowTexSamplerStr;

  ///////////////////////////////////// for skylight only
  // this will influence only on diffuse surfaces
  //
	std::wstring envTexturePathSecondary;
	std::string envTextureSamplerSecondary;
  float        colorSecondary[3];  
  bool         useSeparateDiffuseLight;
  ///////////////////////////////////// for skylight only

  int lightDitributionType;

  bool useGlobal;
  bool absMapBias;
  bool overshoot;
  bool on;
  bool computeShadow;
  bool isTargeted;
  bool isVisiable;
  bool causticLight;
  bool mrDisablePhotonsD;
  bool mrDisablePhotonsC;

  float position[3];
  float direction[3];
  float color[3];
  float shadowSoftness;

  float intensity;
  float aspect;
  float hotsize;
  float fallsize;
  float attenStart;
  float attenEnd;
  float TDist;
  float kc,kl,kq;
  float directLightStart;
  float envTexAmt;
  float skyTurbidity;

  float mapBias;
  float mapRange;
  float mapSize;
  float rayBias;
  float photonMaxRadius;

  float sizeX;
  float sizeY;

  float m[16];
  float targetMatrix[16];
  float iesMatrixRot[16];
};

struct TextureObj
{
	std::wstring texName;
	std::wstring texClass;
	std::wstring mapName;
	std::wstring texFilter;
	std::wstring mapType;

	std::wstring normalMapPath;
	std::wstring displacementMapPath;

	std::wstring points;
	std::wstring tangents;
	std::wstring alphaSource;
  float displacementAmount;

	bool texInvert;
	bool crop;

  bool useRealWorldScale;
  bool isBump;
  bool hasNormals;
  bool hasDisplacement;
  bool nmFlipRed;
  bool nmFlipGreen;
  bool nmSwapRedAndGreen;
	bool needHDR;

	bool  isFalloff;
  bool  isFaloffFresnel;
  bool  isFaloffFresnelOverride;
  float faloffFresnelIOR;


	std::map<std::string, TextureObj> subTextures;

	Color col_1;
	Color col_2;


	float uOffset;
	float vOffset;
	float uTiling;
	float vTiling;
	
  float cporuoffs;
  float cporvoffs;
  float cropuscale;
  float cropvscale;
  float customGamma;
  
  float angle;
  float angleUVW[3];
	float blur;
	float blurOffset;
	float noiseAmt;
	float noiseSize;
	int noiseLevel;
  int texTilingFlags;
	float noisePhase;
  float texAmount;


  TextureObj()
  {
    alphaSource = L"rgb";
    texInvert   = false;
    isBump      = false;
    hasNormals  = false;
    hasDisplacement   = false;
    nmFlipRed         = false;
    nmFlipGreen       = false;
    nmSwapRedAndGreen = false;
    useRealWorldScale = false;
		crop              = false;
		needHDR           = false;

		isFalloff         = false;
    isFaloffFresnel   = false;
    isFaloffFresnelOverride = false;
    faloffFresnelIOR        = 1.6f;

    uOffset    = 0.0f;
    vOffset    = 0.0f;
    uTiling    = 1.0f;
    vTiling    = 1.0f;

    cporuoffs  = 0.0f;
    cporvoffs  = 0.0f;
    cropuscale = 1.0f;
    cropvscale = 1.0f;

    angle      = 0;
    blur       = 0;
    blurOffset = 0;
    noiseAmt   = 0;
    noiseSize  = 0;
    noiseLevel = 0;
    noisePhase = 0;
    displacementAmount = 0.0f;
    customGamma = 2.2f;

  }

};

struct MaterialObj
{
	bool sub;
  bool isHydraNative;
	bool isVrayMtl;
	bool isStringMat;
	bool isBlend;

	std::wstring materialBodyXML;
	std::wstring auxMaterialBodyXML;
	std::wstring opacityBodyXML = L"";

	int id;
	std::wstring name;
	float ambient_color [3];
	float diffuse_color [3];
	float specular_color[3];
  float filter_color  [3];
  float emission_color[3];
	float reflect_color[3];
	float transparency_color[3];
	float fog_color[3];
	float exit_color[3];
	float translucency_color[3];


	float shininess;
	float shine_strength;
	float transparency;
	//float wire_size;
	std::wstring shading;
  float opacity;
  float IOR;
	float opacity_falloff;
	float self_illumination;
	bool twosided;
  bool affect_shadows_on;
	/*bool wire;
	bool wire_units; //true - units, false - pixels*/
	bool falloff; //true - out, false - in
	bool facemap;
	bool soften;
	bool no_ic_records;
	std::wstring transparency_type;
	int num_subMat;

	bool lock_specular, hilight_gloss_tex, refr_gloss_tex, reflect_gloss_tex;
  
  bool displacement_on, displacement_invert_height_on, diffuse_mult_on, specular_mult_on, emission_mult_on, transparency_mult_on, reflect_mult_on, transparency_thin_on;
	bool specular_fresnel_on, reflect_fresnel_on, emission_gi, shadow_matte, translucency_mult_on;
	int specular_gloss_or_cos, reflect_gloss_or_cos, transparency_gloss_or_cos;
  float specular_ior, reflect_ior, specular_roughness, reflect_roughness, reflect_cospower, transparency_cospower, specular_cospower, fog_multiplier, displacement_height, diffuse_mult, specular_mult, emission_mult, transparency_mult, reflect_mult;
	float specular_glossiness, reflect_glossiness, transparency_glossiness, bump_amount, bump_radius, bump_sigma, translucency_mult;
  int specular_brdf, reflect_brdf, reflect_extrusion; 
	
  float diffuse_roughness;

	std::vector<TextureObj>   textures;
	std::vector<std::wstring> textureSlotNames;
  std::vector<float>        textureAmount;

	std::map<std::wstring, bool> falloffSlots;

  MaterialObj()
  {
    for(int i = 0; i < 3; i++)
      ambient_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      diffuse_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      specular_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      filter_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      reflect_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      transparency_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      fog_color[i] = 0;
  
    for(int i = 0; i < 3; i++)
      exit_color[i] = 0;

		for (int i = 0; i < 3; i++)
			translucency_color[i] = 0;
  
    isHydraNative = false;
	 	isVrayMtl = false;
		isStringMat = false;
    displacement_on = false;
		isBlend = false;
    displacement_invert_height_on = false;
    diffuse_mult_on = false;
    specular_mult_on = false;
    emission_mult_on = false;
    transparency_mult_on = false;
    reflect_mult_on = false;
    transparency_thin_on = false;
	 	no_ic_records = false;
	 	lock_specular = true;
	 	hilight_gloss_tex = false;
	 	refr_gloss_tex = false;
	 	reflect_gloss_tex = false;
		translucency_mult_on = false;
  
	 	specular_ior = 0;
	   reflect_ior = 0;
    reflect_roughness = 0;
	 	specular_roughness = 0;
    reflect_cospower = 0;
    transparency_cospower = 0;
	 	specular_cospower = 0;
    fog_multiplier = 0;
    displacement_height = 0;
    diffuse_mult = 0;
    specular_mult = 0;
    emission_mult = 0;
    transparency_mult = 0;
    reflect_mult = 0;
		translucency_mult = 0;
    diffuse_roughness = 0;
  
    specular_brdf = 0;
    reflect_brdf = 0;
  
    shininess = 0;
    shine_strength = 0;
    transparency = 0;
    shading = L"";
    opacity = 0;
    IOR = 0;
    opacity_falloff = 0;
    self_illumination = 0;
    twosided = false;
    falloff = false; 
    facemap = false;
    soften = false;
    sub = false;
    transparency_type = L"";
  
	 	specular_gloss_or_cos = 1;
	 	reflect_gloss_or_cos = 1;
	 	transparency_gloss_or_cos = 1;
	 	specular_glossiness = 1;
	 	reflect_glossiness = 1;
	 	transparency_glossiness = 1;
	 	specular_fresnel_on = false;
	 	reflect_fresnel_on = false;
	 	bump_amount = 0.0f;
  
	 	shadow_matte = false;
	 	emission_gi = true;
	 	bump_radius = 1.0f;
	 	bump_sigma = 1.5f;
  
    id = 0;
    name = L"hydra_material_null";
    num_subMat = 0;

		materialBodyXML = L"";
		auxMaterialBodyXML = L"";
		falloffSlots.clear();
  }
  
	inline int FindTextureSlot(const std::wstring* a_slotNames, int a_size) const
  {
    for(int i=0;i<textureSlotNames.size();i++)
    {
      for(int j=0;j<a_size;j++)
      {
				const std::wstring& a_slotName = a_slotNames[j];
        if(a_slotName == textureSlotNames[i])
          return i;
      }
    }
  
    return -1;
  }

};

struct TransferContents
{
  enum {SCENE, TEXTURE_FILES, VRSCENE_FILE, CONFIG, RENDER_SETTINGS, HEADER};
  int contents;

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & contents;
  }

};

struct Header
{
  enum {COLLADA_PROFILE, TEXTURE_FILE};
  int contents;
  std::wstring file_name;

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & contents;
    ar & file_name;
  }

};

struct Included
{
  bool geometry;
  bool lights;
  bool materials;
  bool tree;
  bool spheres;

  std::string sceneDumpName;
  std::string colladaProfile;

  std::wstring render_type;

  int geomObjNum;
  int imgObjNum;

  std::wstring AccType;
  std::wstring ConstrMode;

  void IncludeAdd()
  {
    geometry  = true;
    spheres   = true;
    lights    = false;
    materials = false;
    tree      = false;

    sceneDumpName  = "c:/[hydra]/pluginFiles/test.dump";
    colladaProfile = "c:/[hydra]/pluginFiles/hydra_profile_generated.xml";
  }

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & geometry;
    ar & lights;
    ar & materials;
    ar & tree;
    ar & spheres;

    ar & sceneDumpName;

    ar & geomObjNum;
    ar & imgObjNum;  

    ar & AccType;
    ar & ConstrMode;
  }
};

inline static int find_interval(float x)
{  
  if (fabs(x - 1.0f) < 1e-5f)
    return 10;
  else
    return (int)(x*10);
}

inline static float GetCosPowerFromMaxShiness(float glosiness)
{
  float cMin = 1.0f;
  float cMax = 1000000.0f;

	float x = glosiness;

	double coeff[10][4] = {
			{ 8.88178419700125e-14, -1.77635683940025e-14, 5, 1 }, //0-0.1
			{ 357.142857142857, -35.7142857142857, 5, 1.5 }, //0.1-0.2
			{ -2142.85714285714, 428.571428571429, 8.57142857142857, 2 }, //0.2-0.3
			{ 428.571428571431, -42.8571428571432, 30, 5 }, //0.3-0.4
			{ 2095.23809523810, -152.380952380952, 34.2857142857143, 8 }, //0.4-0.5
			{ -4761.90476190476, 1809.52380952381, 66.6666666666667, 12 },//0.5-0.6
			{ 9914.71215351811, 1151.38592750533, 285.714285714286, 32 }, //0.6-0.7
			{ 45037.7068059246, 9161.90096119855, 813.432835820895, 82 }, //0.7-0.8
			{ 167903.678757035, 183240.189801913, 3996.94423223835, 300 }, //0.8-0.9
			{ -20281790.7444668, 6301358.14889336, 45682.0925553320, 2700 } //0.9-1.0
	};

	int k = find_interval(x);

	if (k == 10 || x >= 0.99f) 
    return cMax;
	else 
    return coeff[k][3] + coeff[k][2] * (x - k*0.1) + coeff[k][1] * pow((x - k*0.1), 2) + coeff[k][0] * pow((x - k*0.1), 3);
}


#endif