#pragma once
/*
 * Based on NodeInstancingPool class from 2017 MaxSDK::RenderingAPI::TranslationHelpers
 */


// max sdk
#include <Noncopyable.h>
#include <RenderingAPI/Translator/Helpers/INodeInstancingPool.h>
#include <NotificationAPI/NotificationAPI_Subscription.h>

#include "object.h"
#include <unordered_set>
#include <unordered_map>
#include <set>


using namespace MaxSDK::NotificationAPI;
namespace Hydra
{
  class NodePool :
    public MaxSDK::Util::Noncopyable,
    private INotificationCallback
  {
  public:

    // mutable std::vector<INotifier*> m_notifiers;

    NodePool(INode* initial_node, const NotifierType a_notification_type, TimeValue t);
    ~NodePool();

    void NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/) override;

    INode* GetRepresentativeNode() const;
    Object* GetReferencedObject() const;
    bool IsNodeCompatible(INode* node, TimeValue t) const;
    void AddNode(INode* a_node, TimeValue t);
    void RemoveNode(INode* a_node);

    bool needExport;
    std::set<INode*> m_pool;

  private:
    Object* m_ref_obj;
    const NotifierType m_node_notification_type;
    std::unique_ptr<IImmediateNotificationClient> m_notification_client;
    INode* representative_node;
    bool isGeoPool;
  };


  class MaterialTracker :
    public MaxSDK::Util::Noncopyable,
    private INotificationCallback
  {
  public:

    MaterialTracker();
    ~MaterialTracker();

    void NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/) override;

    std::unordered_map<Mtl*, std::pair<bool, std::unordered_set <std::wstring> > > materials;
    
    void AddMtl(Mtl* mtl);
    void RemoveMtl(Mtl* mtl);
    void Clear();
  private:
    std::unique_ptr<IImmediateNotificationClient> m_notification_client;
  };


  class TextureTracker :
    public MaxSDK::Util::Noncopyable,
    private INotificationCallback
  {
  public:

    TextureTracker(std::shared_ptr<MaterialTracker> a_mtlTracker);
    ~TextureTracker();

    void NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/) override;

    std::unordered_map<Texmap*, std::pair<bool, Mtl*>> texmaps; // map from texmap to pair (changed_and_needs_export, parent Mtl)

    void AddTexmap(Texmap* tex, Mtl* parentMtl);
    void RemoveTexmap(Texmap* tex);
    void Clear();
  private:
    std::unique_ptr<IImmediateNotificationClient> m_notification_client;
    std::shared_ptr<MaterialTracker> mtlTracker;
  };
}