#ifndef __HYDRARENDER_H__
#define __HYDRARENDER_H__

//**************************************************************************/
// Copyright (c) 1998-2007 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.
//**************************************************************************/
// DESCRIPTION: Includes for Plugins
// AUTHOR: ALICE
//***************************************************************************/

//maxsdk
#include "3dsmaxsdk_preinclude.h"
#include "resource.h"
#include "icurvctl.h"
#include <lslights.h>
#include <iparamb2.h>
#include <ITabDialog.h>
#include "IFileResolutionManager.h"
#include "INodeBakeProperties.h"

//std
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>  
#include <thread>
#include <memory>

//hydra
#include "ImportStructs.h"
#include "HydraLogger.h"
#include "HydraApi.h"
#include "HydraXMLHelpers.h"

#include "change_trackers.h"
#include <HydraLegacyUtils.h>
#include <HydraPostProcessAPI.h>

#define hydraRender_mk3_CLASS_ID	Class_ID(0x7da90dd1, 0x758b0286)
extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define BOOST_ALL_NO_LIB
#define MAX_THREADS 4
#define MAX_DEVICES 32

#define RENDER_METHOD_PT   0
#define RENDER_METHOD_LT 1
#define RENDER_METHOD_IBPT  2
#define RENDER_METHOD_MMLT  3


#define PRESET_CUSTOM 0
#define PRESET_VHIGH  1
#define PRESET_HIGH   2
#define PRESET_MEDIUM 3
#define PRESET_LOW    4

#define DEVICE_FILE_PATH L"C:\\[Hydra]\\pluginFiles\\device.txt"



//#define MAX2014

using texInfo = std::tuple <HRTextureNodeRef, StdUVGen*, float, std::wstring>;

namespace strConverter
{
  std::wstring s2ws(const std::string& s);
  std::string ws2s(const std::wstring& s);
  std::wstring s2ws(const std::wstring& s);
  std::string ws2s(const std::string& s);
  std::wstring c2ws(const char* s);
  std::wstring c2ws(const wchar_t* s);
  std::wstring ToWideString(const std::string& rhs);
  std::string ToNarrowString(const std::wstring& rhs);
  std::wstring ToWideString(const std::wstring& rhs);
  std::string ToNarrowString(const std::string& rhs);

  std::wstring colorToWideString(const Color color);
}

void MaxMatrix3ToFloat16(const Matrix3& pivot, float* m);
void MaxMatrix3ToFloat16V2(const Matrix3& pivot, float* m);
Matrix3 Float16ToMaxMatrix3(const float* a_m);
float scaleWithMatrix(float a_val, const Matrix3& a_externTransform);
void MaxMatrix3ToFloat16MagicSwap(const Matrix3& pivot, float* m);
Matrix3 TransformMatrixFromMax(const Matrix3 &m);

float myclamp(float u, float a, float b);
int RealColor3ToUint32(const float real_color[3]);


const int MTL_WINDOW_SIZE = 512;


class HydraRenderParams : public RenderGlobalContext {
public:
	RendType	rendType;				
	int			nMinx;
	int			nMiny;
	int			nMaxx;
	int			nMaxy;
	int			nNumDefLights;			// The default lights passed into the renderer
	Point2		scrDUV;
	DefaultLight*	pDefaultLights;
	FrameRendParams*	pFrp;			// Frame specific members
	RendProgressCallback*	progCallback;	// Render progress callback



	BOOL inMtlEditor;

	// Standard options
	// These options are configurable for all plugin renderers
	BOOL		bVideoColorCheck;
	BOOL		bForce2Sided;
	BOOL		bRenderHidden;
	BOOL		bSuperBlack;
	BOOL		bRenderFields;
	BOOL		bNetRender;

	// Render effects
	Effect*		effect;

	int rendTimeType;
	TimeValue rendStart, rendEnd;
	int nthFrame;
	bool rendSaveFile;

	//Rendering parameters
  float renderQuality;
	long timeLimit;
	int primary, secondary, tertiary, manualMode, engine_type, preset;
	std::unordered_map<int, bool> device_id; //device id -> on/off


	//int devices_num;
  bool useHydraGUI, enableDOF, noRandomLightSel, enableLog, debugOn, allSecondary, allTertiary, timeLimitOn;
	bool primaryMLFilter, secondaryMLFilter, writeToDisk, liteMode, useSeparateSwap, hydraFrameBuf, specNoiseFilterOn;
	bool rememberDevice, use_denoiser, enableCaustics, enableTextureResize, productionMode;
	float focalplane, lensradius, specNoiseFilter, noiseLvl;


	//path tracing
  int icBounce;
	int seed;
	int minrays, maxrays, raybounce, diffbounce, useRR, causticRays;
	float relative_error;
	bool  guided;
	float env_clamp;
	float bsdf_clamp;
	bool  bsdf_clamp_on;
  bool  estimateLDR;

	//SPPM Caustics
	int maxphotons_c, retrace_c;
	float initial_radius_c, caustic_power, alpha_c;
	bool visibility_c;

	//SPPM Diffuse
	int maxphotons_d, retrace_d;
	float initial_radius_d, alpha_d;
	bool visibility_d, irr_map;

	//Irradiance cache
	int ic_eval;
	int maxpass, fixed_rays;
	float ws_err, ss_err4, ss_err2, ss_err1, ic_relative_error;
		
	//Multi-Layer
	bool filterPrimary;
	float r_sigma, s_sigma;
	int spp;
	int layerNum;
  //bool saveComposeLayers;

	//Post processing
	int filter_id;
  float gamma = 2.2f;
	float bloom_radius, bloom_str;
  int numThreads;
  Color whitePointColor;
  float exposure, compress, contrast, saturation, whiteBalance, uniformContrast, normalize,
    chromAberr, vignette, sharpness, sizeStar, randomAngle, sprayRay;
  int numRay, rotateRay;
	bool bloom, mlaa, postProcOn;

	//Override
	float env_mult;
  bool overrideGrayMatOn;

  //MLT
  float mlt_plarge;
  int   mlt_iters_mult;
  int   mlt_burn_iters;
	bool mlt_enable_median_filter;
	float mlt_median_filter_threshold;

	//Debug
	std::map<std::wstring, bool> debugCheckBoxes;

  // Render elements
  bool renElemAlphaOn, renElemColorOn, renElemCoordOn, renElemCoverOn, renElemDepthOn, 
    renElemInstIdOn, renElemMatIdOn, renElemNormalsOn, renElemObjIdOn, renElemShadowOn;


	HydraRenderParams();
	void		SetDefaults();
	void		ComputeViewParams(const ViewParams&vp);
	Point3		RayDirection(float sx, float sy);

	int				NumRenderInstances() override;
  RenderInstance*	GetRenderInstance(int i) override;
};


class HydraView : public View
{
public:
	HydraRenderParams*	pRendParams;
	Point2		ViewToScreen(Point3 p) override;
};


enum TEX_OUTPUT_TYPE{ TEX_OUT_COLOR = 0, TEX_OUT_MONO = 1, TEX_OUT_ALPHA = 2 };

class HydraRenderParamDlg;


class hydraRender_mk3 : public Renderer, public ITabDialogObject {
	public:
		
    static HydraRenderParamDlg*  m_pLastParamDialog;
    static int m_instanceCounters;
    static HRRenderRef m_rendRef;
    static HydraLogger plugin_log;


    bool m_inMtlEditorNow;

	  HydraRenderParams	rendParams; 
    static HydraRenderParams m_lastRendParams;
    static int               m_matRenderTimes;
		HydraView		theView;
		ViewParams	view;

    inline int	GetMeshFrameStep() { return nMeshFrameStep; }
    inline int	GetKeyFrameStep() { return nKeyFrameStep; }
    inline TimeValue GetStaticFrame() { return nStaticFrame; }
    inline void SetMeshFrameStep(int val) { nMeshFrameStep = val; }
    inline void SetKeyFrameStep(int val) { nKeyFrameStep = val; }
    inline void SetStaticFrame(TimeValue val) { nStaticFrame = val; }


		IOResult Load(ILoad *iload); 
		IOResult Save(ISave *isave); 

		//From Animatable
		Class_ID ClassID() override {return hydraRender_mk3_CLASS_ID;}
		SClass_ID SuperClassID() override { return RENDERER_CLASS_ID; }
		void GetClassName(TSTR& s) override {s = GetString(IDS_CLASS_NAME);}

		RefTargetHandle Clone( RemapDir &remap ) override;

		#ifdef MAX2014
    virtual RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message) override;
		#else
		virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;
		#endif

		int NumSubs() override { return 1; }
		TSTR SubAnimName(int i) override { return GetString(IDS_PARAMS); }

	//	int NumRefs() { return 1; }


    int Open(INode *scene, INode *vnode, ViewParams *viewPar, RendParams &rp, HWND hwnd, DefaultLight *defaultLights=nullptr, int numDefLights=0, RendProgressCallback *prog=nullptr) override;
    int Render(TimeValue t, Bitmap *tobm, FrameRendParams &frp, HWND hwnd, RendProgressCallback *prog=nullptr, ViewParams *viewPar=nullptr) override;
    void Close(HWND hwnd, RendProgressCallback *prog=nullptr) override;
    bool ApplyRenderEffects (TimeValue t, Bitmap *pBitmap, bool updateDisplay=true) override { return false; }
    RendParamDlg* CreateParamDialog(IRendParams *ir, BOOL prog=FALSE) override;
    void SaveRenderElementsToDisk(Bitmap* tobm);

#ifdef MAX2017
		bool IsStopSupported() const override;
		void StopRendering() override;
		Renderer::PauseSupport IsPauseSupported() const override;
		void PauseRendering() override;
		void ResumeRendering() override;
		bool HasRequirement(Requirement requirement) override;
		IInteractiveRender* GetIInteractiveRender() override;
		void GetVendorInformation(MSTR& info) const override;
		void GetPlatformInformation(MSTR& info) const override;
		bool CompatibleWithAnyRenderElement() const override;
		bool CompatibleWithRenderElement(IRenderElement& pIRenderElement) const override;
#endif

    bool SupportsTexureBaking() override { return false; } // ### not yet 
		using Renderer::GetInterface;
		BaseInterface* GetInterface ( Interface_ID id ) override;

		virtual void AddTabToDialog ( ITabbedDialog* dialog, ITabDialogPluginTab* tab ) override;
		virtual int AcceptTab ( ITabDialogPluginTab* tab ) override;
    
    virtual void ResetParams() override;

    int			CheckAbort(int done, int total);
	  void		SetProgTitle(const TCHAR *title);

		void DeleteThis() override { delete this; }
		
		hydraRender_mk3();
		~hydraRender_mk3();

///////utility variables////////////////////////////
    IScanRenderer* m_pScanlineRender;
		std::string convertMatScriptPath;
    std::string convertLightsScriptPath; 
    std::wofstream m_paramTrace;
    int		nMeshFrameStep;
    int		nKeyFrameStep;
    TimeValue	nStaticFrame;
    BOOL    mtlEditorWasOpen;
		INode *pScene;
		INode *pVnode;
    std::vector<HydraRenderDevice>  m_devList;
///////////////////////////////////////////////////


		static LPCWSTR GetHydraVersionW() { return L"Hydra Render v2.3a"; }
		static LPCSTR  GetHydraVersion()  { return "Hydra Render v2.3a"; }
    std::wstring hydraInstallDir = L"C:\\[Hydra]\\";

    HRSceneInstRef currentScene, matEditorScene;
    std::unordered_map<Object*, std::vector<INode*>> geoInstancesLib;
    std::vector<std::shared_ptr<Hydra::NodePool>> geoNodesPools;
    std::unordered_map<Object*, std::vector<INode*>> lightInstancesLib;
    std::vector<std::shared_ptr<Hydra::NodePool>> lightNodesPools;
    bool m_firstRender = true;

    std::unordered_map<Texmap*, texInfo> texmapLib; //map from max texture to hydraAPI reference, uvgen and gamma
    std::unordered_map<Object*, HRMeshRef> meshRefLib;
    std::unordered_map<Object*, HRLightRef> lightsRefLib;
    std::unordered_map<Mtl*, HRMaterialRef> materialsRefLib;
    std::shared_ptr<Hydra::MaterialTracker> mtlTracker;
    std::unique_ptr<Hydra::TextureTracker> texTracker;

    int largestMaterialId; //for rendering in material editor
    HRMaterialRef placeholderMatRef; //for use in blend material
    HRLightRef  m_envRef;
    HRCameraRef m_camRef;
    HRRenderRef m_rendRefMatEditor;
    
    /**
    \brief copies image from renderDriver to 3ds max default framebuffer
    */
    bool FrameBufferUpdateLoop(Bitmap* bmap_out, int renderWidth, int renderHeight, RendProgressCallback* prog, bool forMatEditor);
    void CopyRawImageToBitmap (Bitmap* bmap_out, const float *image_src, int src_w, int src_h);
    /**
    \brief entry point to scene export
    */
    int  DoExport(INode* root, INode *vnode, ViewParams* a_viewParams, TimeValue t, bool forMatEditor = false);

    /**
    \brief walks the scene node graph and fill geometry and light instance pools, also find all materials used in the scene and add
    them in mtlTracker
    */
    void PreProcess(INode* node, TimeValue t, bool isInMatEditor);

    /**
    \brief creates scene and instance meshes and lights
    */
    int  ExtractAndDumpSceneData(INode* root, TimeValue t);

    /**
    \brief creates scene for material editor
    */
    void ExtractForMatEditor(TimeValue t);

    /**
    \brief exports geometry for new/changed meshes and create instances
    */
    void ExtractAndInstanceMeshes(int& geomObjNum, int& vertWritten, TimeValue t);

    /**
    \brief exports geometry data for a single geometry/shape object
    * supports vertex indexing
    */
    //int  ExtractMesh(INode* node, TimeValue t, const HRMeshRef &meshRef);

    /**
    \brief exports geometry data for a single geometry/shape object
    * does not support vertex indexing
    */
    int  ExtractMeshNoIndexing(INode* node, TimeValue t, HRMeshRef &meshRef);

/////////LIGHTS //////////////////////
    /**
    \brief exports parameters for new/changed lights and create instances
    */
    void ExtractAndInstanceLights(int& lightsNum, TimeValue t);

    /**
    \brief exports parameters of a single light
    */
    void ExtractLightObject(INode* node, TimeValue t, HRLightRef lightRef, bool isNew);

    /**
    \brief exports environment parameters
    */
    void ExtractEnvironmentLight(TimeValue t);

    texInfo envTex; 
    
    //need this to reference sun from sky
    bool  m_sunUseMrSky;
    std::string m_sunSkyName;
    std::wstring m_sunSkyNameW;

    //Exporting different light types
    void ExtractPhotometricLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractPhysSkyParams(LightObject *light, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractHydraSkyPortalParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractMrSkyPortalParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractSunParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractStdLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid);
    void ExtractIESData(LightObject *light, Matrix3 *lightTransform, pugi::xml_node lightNode);
    bool IsPhotometricLight(LightObject* a_node);
    bool IsHydraSkyPortal(LightObject* a_node);
    bool IsMrSkyPortal(LightObject* a_node);
    bool IsMrSun(LightObject* a_node);
    bool IsPhysSky(LightObject* a_node);
    bool IsNewSunSky(Texmap* a_env);

/////////MATERIALS //////////////////////   
    /**
    \brief Gray material for override all materials from GUI render setup
    */
    HRMaterialRef grayMat;

    /**
    \brief extracts all new/changed materials
    */
    void ExtractMaterialList(bool forMatEditor = false);

    /**
    \brief checks material class and call appropriate export function
    */
    int  ExtractMaterial(Mtl* mtl, std::unordered_set<std::wstring> changedParams);

    /**
    \brief extracts submaterials of a given material
    */
    void ExtractMaterialRecursive(Mtl *mtl, bool forMatEditor, std::unordered_set<std::wstring> changedParams);

    /**
    \brief determines open mode for material
    */
    HRMaterialRef GetHRMatRefFromMtl(Mtl* mtl, HR_OPEN_MODE &mode);

    /**
    \brief exports placeholder material for use in blend
    */
    void ExtractPlaceholderMaterial();

    /**
    \brief exports 3ds max standard blend material
    */
    void ExtractBlendMaterial(Mtl* mtl, std::vector<HRMaterialRef> &subMatRefs, bool forMatEditor = false);

    /**
    \brief exports 3ds max standard material
    */
    void ExtractStdMaterial(Mtl* mtl);
    void ExtractStdMatDiffuse(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
    void ExtractStdMatReflect(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
    void ExtractStdMatTransparency(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
    void ExtractStdMatOpacity(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
    void ExtractStdMatEmission(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
    void ExtractStdMatDisplacement(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);

    /**
    \brief exports hydra render material
    */
    void ExtractHydraMaterial(Mtl* mtl, std::unordered_set<std::wstring> changedParams);
    void ExtractHydraDiffuse(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraReflect(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraTransparency(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraOpacity(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraEmission(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraTranslucency(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo>  texturesSlotToRef);
    void ExtractHydraDisplacement(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo>  texturesSlotToRef);

    void ExtractHydraMtlCatcher(Mtl* mtl, std::unordered_set<std::wstring> changedParams);
    void ExtractHydraMtlCatcherCameraMap(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraMtlCatcherOpacity(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraMtlCatcherAO(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
    void ExtractHydraMtlCatcherReflect(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);

    void ExtractHydraMtlLight(Mtl* mtl, std::unordered_set<std::wstring> changedParams);

    void ExtractHydraMtlLayers(Mtl* mtl, std::unordered_set<std::wstring> changedParams);


    /**
    \brief exports any other (i.e. unsupported) material as purple diffuse
    */
    void UnsupportedMaterial();

    /**
    \brief gets names of hydra material texture slots that changed and need to be exported
    */
    bool HydraMtlGetChangedTexturesSlotNames(const std::unordered_set<std::wstring> &changedParams, std::vector<std::wstring> &slotNames) const;

    /**
    \brief gets names of all hydra material texture slots
    */
    void HydraMtlGetAllTexturesSlotNames(std::vector<std::wstring> &slotNames) const;

    /**
    \brief exports texture matrix, addressing mode and gamma
    */
    void ExtractSampler(pugi::xml_node &texNode, const texInfo &info);

    /**
    \brief add all textures for given material to texmapLib and get uvGen pointers to export samplers later
    \param mtl - material;
    Returns a map: [slot name] -> tuple(texture reference in hydra API, StdUVGen pointer for max texmap object, gamma)
    */
    std::unordered_map<std::wstring, texInfo> ExtractHydraTextures(Mtl* mtl, const std::vector<std::wstring> &mapNames);

    /**
    \brief export Texmap object, calls BakeBitmap, BakeNormalMap or BakeOtherTexmap
    Returns tuple(texture reference in hydra API, StdUVGen pointer for max texmap object, gamma)
    */
    texInfo BakeTexture(Texmap* tex, HRTextureNodeRef currentRef = HRTextureNodeRef(), std::wstring mtl_name = std::wstring(L""), bool dontupdate = false);

    /**
    \brief bake bitmap (apply output, crop, etc.)
    */
    texInfo BakeBitmap(Texmap* tex, HRTextureNodeRef currentRef, std::wstring mtl_name = std::wstring(L""), bool dontupdate = false);

    /**
    \brief export normalmap parameters
    */
    void ExtractNormalBump(Mtl* mtl, bool &isNormalMap, bool &flipRed, bool &flipGreen, bool &swap);

    texInfo BakeNormalMap(Texmap* tex, HRTextureNodeRef currentRef); //unused

    /**
    \brief export AO proc. textures
    */
    texInfo BakeHydraAOTexmap(Texmap* tex, HRTextureNodeRef currentRef);

    /**
    \brief export Falloff proc. textures
    */
    void BindingFalloff(Texmap* tex, HRTextureNodeRef currentRef);

    /**
    \brief export procedural textures
    */
    texInfo BakeOtherTexmap(Texmap* tex, HRTextureNodeRef currentRef);


/////////CAMERA //////////////////////   
    
    /**
    \brief export active viewport/camera settings 
    */
    HRCameraRef ExtractCameraFromActiveViewport(INode *vnode, ViewParams* a_viewParams, TimeValue t);
    

/////////HYDRA RENDER SETINGS ////////
   
    /**
    \brief export render settings and create render driver
    */
    HRRenderRef SetupRender(FrameRendParams &frp);

    /**
    \brief export render settings and create render driver for material editor
    */
    HRRenderRef SetupRenderForMatEditor(FrameRendParams &frp);

  
///////helper functions for geometry data export
    BOOL TMNegParity(Matrix3 &m);
    TriObject* GetTriObjectFromNode(INode *node, TimeValue t, int &deleteIt);
    void	make_face_uv(Face *f, Point3 *tv);

    void CalculateNormals(Mesh* mesh, std::vector<VertexNormal> &normals);
    const Point3 getVertexNormal(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index);
    const Point3 getVertexNormalUnchecked(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index);
		Point3 ComputeSmGroupNormal(const std::vector<int>& faceIndeces, int faceNum, const std::vector<int> &face_smoothgroups, float *face_normals);
    void CalcMaxNormalsAsInExample2(Mesh* mesh, GeometryObj* geom, Matrix3 tm);
    void ExtractUserDefinedNormals(Mesh* mesh, GeometryObj* geom, Matrix3 tm);

///////helper functions for bitmaps and texmaps
    void CropToFile(BitmapTex* pBitMap, Bitmap *bmap, const hydraStr& mapPath, TimeValue t);
		void CropToFileHDR(BitmapTex* pBitMap, Bitmap *bmap, const hydraStr& mapPath, TimeValue t);
    void ExtractTextureMatrixFromUVGen(StdUVGen* uvGen, float matrixData[16], int* pTexTilingFlags);
    void SetRenderParamsImageSizeFromBitMap(HydraRenderParams* pParams, Bitmap* tobm);
    void CopyImageFromHRRenderToBitmap(Bitmap* bmap_out, int w, int h);

   ///////for post process////////////////////////////
    Bitmap* last_render;

    HRFBIRef hydra_raw_render;
    HRFBIRef hydra_out_render;

    void copyBitmap(Bitmap* from, Bitmap* to);
    void displayLastRender(bool processed = true);

    void DoDenoise(Bitmap* a_maxFrameBuff, int w, int h);
    void DoPostProc(Bitmap* bmap);
    void UpdatePostProc(HWND hWnd, HydraRenderParamDlg *dlg);
    void ResetPostProc(HydraRenderParamDlg *dlg);
    void DisplaySource(HWND hWnd, HydraRenderParamDlg *dlg);

///////exploring IParamBlock2/////////////////////
    IParamBlock2* FindBlockOfParameter(const std::wstring& a_paramName, ReferenceTarget* a_light);
    IParamBlock2* FindBlockOfParameter(const std::wstring& a_paramName, IParamBlock2* a_paramRec);

    bool  FindBool(const std::wstring& a_paramName, ReferenceTarget* a_light);
    float FindFloat(const std::wstring& a_paramName, ReferenceTarget* a_light);
    Color FindColor(const std::wstring& a_paramName, ReferenceTarget* a_light);
    int FindInt(const std::wstring& a_paramName, ReferenceTarget* a_light);
    std::wstring FindString(const std::wstring& a_paramName, ReferenceTarget* a_light);
    Texmap* FindTex(const std::wstring& a_paramName, ReferenceTarget* a_light);
    AColor FindAColor(const std::wstring& a_paramName, ReferenceTarget* a_light);

    void TraceNodeCustomProperties(const std::wstring& a_objectName, ReferenceTarget* a_node, int deep = 0);
    void TraceIParamBlock2(const std::wstring& a_objectName, IParamBlock2* a_block, int deep = 0);
    void TraceIParamBlock(const std::wstring& a_objectName, IParamBlock* a_block, int deep = 0);

///////old, unused and/or unsupported//////

    /**
    \brief first version of geometry export, without support for instancing
    */
    //bool TraverseNodeTree(INode* node, int& geomObjNum, int& vertWritten, TimeValue t);
    //void ExtractGeomObject(INode* node, TimeValue t);
    
    //not complete develop
    std::vector<INode*>                 nodesToBake;
    std::multimap<INode*, std::string>  nodesToBakeUnwrapPath;
    void FindNodesToBake(INode *a_pNode, std::vector<INode*>* a_pBakeList);
    void InspectBakeProps(const std::vector<INode*>& a_nodes);
    void exportAndUnwrapBakeNodes(const std::vector<INode*>& a_nodes, TimeValue t);


/////various utility functions

    std::wstring getSceneName();
    void UserSelectDeviceDialog(HWND hwnd);
		bool readAndCheckDeviceSettingsFile(int &mode, std::unordered_map<int, bool> &deviceIDs);

    bool VerifyThings(int renderWidth, int renderHeight);
    void ClearTempFolder();

    inline float GetGlobalLightScale() const 
    { 
      if(GetUnitDisplayType() == UNITDISP_GENERIC)
        return 0.05f;
      else 
        return 0.0007f;
    }

    inline float toMeters(float a_val)
    {
      return GetMasterScale(UNITS_METERS)*a_val;
    }

    inline Point3 toMeters(Point3 p)
    {
      p.x = toMeters(p.x);
      p.y = toMeters(p.y);
      p.z = toMeters(p.z);
      return p;
    }

    inline Matrix3 toMeters(Matrix3 m)
    {
      Point3 translate = m.GetTrans();
      m.SetTrans(toMeters(translate));
      return m;
    }

    inline Matrix3 getConversionMatrix()
    {
      Matrix3 m;
      m.SetRow(0, Point3(1,0,0));
      m.SetRow(1, Point3(0,0,-1));
      m.SetRow(2, Point3(0,1,0));
      m.SetRow(3, Point3(0,0,0));
      return m;
    }

    inline Matrix3 getConversionMatrix2()
    {
      Matrix3 m;
      m.SetRow(0, Point3(1, 0, 0));
      m.SetRow(1, Point3(0, 0, -1));
      m.SetRow(2, Point3(0, 1, 0));
      m.SetRow(3, Point3(0, 0, 0));
      return m;
    }

		inline int FindTextureSlot(const std::wstring* a_slotNames, const std::vector<std::wstring> textureSlotNames, int a_size) const
		{
			for (int i = 0; i<textureSlotNames.size(); i++)
			{
				for (int j = 0; j<a_size; j++)
				{
					const std::wstring& a_slotName = a_slotNames[j];
					if (a_slotName == textureSlotNames[i])
						return i;
				}
			}

			return -1;
		}
};


class hydraRender_mk3ClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() override	{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) override { return new hydraRender_mk3(); }
	virtual const TCHAR *	ClassName() override { return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() override { return RENDERER_CLASS_ID; }
	virtual Class_ID ClassID() override { return hydraRender_mk3_CLASS_ID; }
	virtual const TCHAR* Category() override { return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() override { return _T("hydraRender_mk3"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() override { return hInstance; }					// returns owning module handle
	

};

static hydraRender_mk3ClassDesc hydraRender_mk3Desc;


static const Class_ID kTabClassID(0x71f46739, 0x266d64bc);
static const Class_ID kTabClassID2(0x7d367fe5, 0x6a8575ca);
static const Class_ID kTabClassID3(0x603a7f38, 0x3f215d2e);
static const Class_ID kTabClassID4(0x184c20a8, 0x283b3a7f);


class HydraRenderParamDlg : public RendParamDlg 
{
   public:
      hydraRender_mk3 *rend;
      IRendParams *ir;
			HWND hMain, hPathTracing, /*hSPPMC, hSPPMD,*/ hPostProc, hOverr, hDebug, hMLT, hTools, hRendElem;// , hNetwork;
			
      BOOL prog;

			static const int rollupsCount = 8;
			int rollupIndices[rollupsCount];
			enum rollUps {/*SPPMC, SPPMD, */PATH, POSTPROC, MAIN, OVERRIDE, DEBUG, MLT, TOOLS, RENDELEM};

			//********UI controls***********
			//main tab
			ICustEdit *lensradius_edit;
			ICustEdit *timelimit_edit;
			ICustEdit *spec_noise_filter_edit;
      ICustEdit *maxrays_edit;
      ICustEdit *raybounce_edit;
      ICustEdit *diffbounce_edit;

      ISpinnerControl *raybounce_spin;
      ISpinnerControl *diffbounce_spin;


			ISpinnerControl *lensradius_spin;
			ISpinnerControl *timelimit_spin;
			ISpinnerControl *spec_noise_filter_spin;
      ISpinnerControl *maxrays_spin;

      ISliderControl *renderQuality_slider;


			//path tracing tab
			ICustEdit *minrays_edit; //deprecated
			ICustEdit *relative_error_edit; //deprecated
			ICustEdit *env_clamp_edit;
			ICustEdit *bsdf_clamp_edit;

			ISpinnerControl *minrays_spin; //deprecated


			ISpinnerControl *relative_error_spin; //deprecated
			ISpinnerControl *env_clamp_spin;
			ISpinnerControl *bsdf_clamp_spin;

			//SPPM Caustics tab
			ICustEdit *maxphotons_c_edit;
			ICustEdit *initial_radius_c_edit;
			ICustEdit *caustic_power_edit;
			ICustEdit *retrace_c_edit;
			ICustEdit *alpha_c_edit;

			ISpinnerControl *maxphotons_c_spin;
			ISpinnerControl *initial_radius_c_spin;
			ISpinnerControl *caustic_power_spin;
			ISpinnerControl *retrace_c_spin;
			ISpinnerControl *alpha_c_spin;

			//SPPM Diffuse tab
			ICustEdit *maxphotons_d_edit;
			ICustEdit *initial_radius_d_edit;
			ICustEdit *retrace_d_edit;
			ICustEdit *alpha_d_edit;

			ISpinnerControl *maxphotons_d_spin;
			ISpinnerControl *initial_radius_d_spin;
			ISpinnerControl *retrace_d_spin;
			ISpinnerControl *alpha_d_spin;
		
			
			//Post processing tab
			ICustButton *denoise_btn;

      ICustEdit *exposure_edit;
      ICustEdit *compress_edit;
      ICustEdit *contrast_edit;
      ICustEdit *saturation_edit;
      ICustEdit *whiteBalance_edit;
      IColorSwatch *whitePoint_selector;
      ICustEdit *uniformContrast_edit;
      ICustEdit *normalize_edit;
      ICustEdit *chromAberr_edit;
      ICustEdit *vignette_edit;
      ICustEdit *sharpness_edit;
      ICustEdit *sizeStar_edit;
      ICustEdit *numRay_edit;
      ICustEdit *rotateRay_edit;
      ICustEdit *randomAngle_edit;
      ICustEdit *sprayRay_edit;

      ICustEdit *noise_lvl_edit;
			ICustEdit *bloom_radius_edit;
			ICustEdit *bloom_str_edit;
			
      ISliderControl *exposure_slider;
      ISliderControl *compress_slider;
      ISliderControl *contrast_slider;
      ISliderControl *saturation_slider;
      ISliderControl *whiteBalance_slider;
      ISliderControl *uniformContrast_slider;
      ISliderControl *normalize_slider;
      ISliderControl *chromAberr_slider;
      ISliderControl *vignette_slider;
      ISliderControl *sharpness_slider;       
      ISliderControl *sizeStar_slider;
      ISliderControl *numRay_slider;
      ISliderControl *rotateRay_slider;
      ISliderControl *randomAngle_slider;
      ISliderControl *sprayRay_slider;

      ISliderControl *noise_lvl_slider;
			ISpinnerControl *bloom_radius_spin;
			ISpinnerControl *bloom_str_spin;
			
      ICustButton *resetPostProc;

			//Environment
			ICustEdit *env_mult_edit;
			ISpinnerControl *env_mult_spin;

			//MLT
			ICustEdit *mlt_median_filter_threshold_edit;
			ICustEdit *mlt_burn_iters_edit;
			ICustEdit *mlt_plarge_edit;
			ICustEdit *mlt_iters_mult_edit;

			ISpinnerControl *mlt_median_filter_threshold_spin;

			ISliderControl *mlt_burn_iters_slider;
			ISliderControl *mlt_plarge_slider;
			ISliderControl *mlt_iters_mult_slider;

			//Tools
			ICustButton *run_script_btn;


			//********************
		
      HydraRenderParamDlg(hydraRender_mk3 *r,IRendParams *i,BOOL prog);
      ~HydraRenderParamDlg();

      void AcceptParams() override;
      void RejectParams() override;
      void DeleteThis() override { delete this; }

      void ReadGuiToRendParams(HydraRenderParams* pRendParams);

      void SetPTParamsFromSlider();
      void SetSliderFromPTParams();

			void LoadPreset(int preset_id);
			void SavePreset();
			void LoadPresetToGUI();

      void InitMainHydraDialog(HWND hWnd);
			void InitPathTracingDialog(HWND hWnd);
			void InitSPPMCDialog(HWND hWnd);
			void InitSPPMDDialog(HWND hWnd);
			void InitPostProcDialog(HWND hWnd);
			void InitOverrideDialog(HWND hWnd);
			void InitDebugDialog(HWND hWnd);
			void InitMLTDialog(HWND hWnd);
			void InitToolsDialog(HWND hWnd);
      void InitRendElemDialog(HWND hWnd);

      void InitProgDialog(HWND hWnd);

			void RemovePage(HWND &hPanel);
			void AddPage(HWND &hPanel, int IDD);
};




std::string HydraInstallPath();
std::wstring HydraInstallPathW();

std::string GetMethodPresets(int methodId);
std::string GetCorrectTexPath(const wchar_t* a_name, IFileResolutionManager* pFRM);


std::string MethodName(int a_name);
float angleOfEdges(Point3 edgeA, Point3 edgeB);
bool isFileExist(const char *fileName);

float ReadFloatParam(const char* message, const char* paramName);

std::string  GetCorrectTexPath(const wchar_t* a_name, IFileResolutionManager* pFRM);
std::wstring GetCorrectTexPath2(const wchar_t* a_name, IFileResolutionManager* pFRM);

std::string wstring_to_utf8(const std::wstring& str);
std::wstring utf8_to_wstring(const std::string& str);

size_t roundBlocks(size_t elems, int threadsPerBlock);

std::wstring alterSavedName(const std::wstring& a_name, const std::wstring& a_alter);

std::vector<HydraRenderDevice> InitDeviceList(HWND devices_list, int mode, bool init, HRRenderRef a_renderRef);
std::vector<HydraRenderDevice> InitDeviceListInternal(HRRenderRef a_renderRef);

void createDeviceSettingsFile(HRRenderRef a_ref);
void createDeviceSettingsFile(int engineType, std::unordered_map<int, bool> deviceIDs, HRRenderRef a_ref);

INT_PTR CALLBACK SelectDeviceProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);

HRTextureNodeRef CreateFalloff();


#endif // __HYDRARENDER_H__
