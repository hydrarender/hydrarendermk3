#include "hydraRender mk3.h"
#include "3dsmaxport.h"
#include "gamma.h"

// #include "RenderDriverHydraLegacyStuff.h"

//#include "path.h"
//#include "AssetType.h"
#include "IFileResolutionManager.h"
#include <ctime>
#include <mutex>
#include <future>
#include <chrono> 

#include <time.h>

#include <cwctype>
#include <clocale>

extern HINSTANCE hInstance;
extern std::mutex matMutex;

extern std::string g_layerName;
extern int g_sessionId;

namespace strConverter
{
  std::wstring s2ws(const std::string& s)
  {
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
  }

  std::wstring s2ws(const std::wstring& s)
  {
    return s;
  }

  std::wstring c2ws(const char* s)
  {
    std::string str(s);
    return strConverter::s2ws(str);
  }

  std::wstring c2ws(const wchar_t* s)
  {
    std::wstring str(s);
    return str;
  }


  std::string ws2s(const std::wstring& s)
  {
    int len;
    int slength = (int)s.length() + 1;
    len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
    char* buf = new char[len];
    WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, buf, len, 0, 0);
    std::string r(buf);
    delete[] buf;
    return r;
  }

  std::string ws2s(const std::string& s)
  {
    return s;
  }

  std::wstring ToWideString(const std::string& rhs)
  {
    return strConverter::s2ws(rhs);
    //std::wstring res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::wstring ToWideString(const std::wstring& rhs)
  {
    return rhs;
    //std::wstring res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::string ToNarrowString(const std::wstring& rhs)
  {
    return strConverter::ws2s(rhs);
    //std::string res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::string ToNarrowString(const std::string& rhs)
  {
    return rhs;
    //std::string res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::wstring colorToWideString(const Color color)
  {
    std::wstringstream tmpSS;

    tmpSS << color.r << " " << color.g << " " << color.b;
    
    return tmpSS.str();
  }
}



float ReadFloatParam(const char* message, const char* paramName)
{
  std::istringstream iss(message);

  float res = 0.0f;

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == paramName)
    {
      res = atof(val.c_str());
      break;
    }
    
  } while (iss);

  return res;
}


#ifdef MAX2014
RefResult hydraRender_mk3::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
  return REF_DONTCARE;
}
#else
RefResult hydraRender_mk3::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  return REF_DONTCARE;
}
#endif

RefTargetHandle hydraRender_mk3::Clone(RemapDir &remap)
{
  hydraRender_mk3 *mnew = new hydraRender_mk3();

  //main
  mnew->rendParams.renderQuality = rendParams.renderQuality;
  mnew->rendParams.primary   = rendParams.primary;
  mnew->rendParams.secondary = rendParams.secondary;
  mnew->rendParams.tertiary  = rendParams.tertiary;
  mnew->rendParams.enableCaustics = rendParams.enableCaustics;
  mnew->rendParams.enableTextureResize = rendParams.enableTextureResize;
  mnew->rendParams.enableTextureResize = rendParams.productionMode;
  mnew->rendParams.timeLimitOn = rendParams.timeLimitOn;
  mnew->rendParams.timeLimit = rendParams.timeLimit;
  mnew->rendParams.useHydraGUI = rendParams.useHydraGUI;
  mnew->rendParams.enableDOF = rendParams.enableDOF;
  mnew->rendParams.noRandomLightSel = rendParams.noRandomLightSel;
  mnew->rendParams.enableLog = rendParams.enableLog;
  mnew->rendParams.focalplane = rendParams.focalplane;
  mnew->rendParams.lensradius = rendParams.lensradius;
  mnew->rendParams.debugOn = rendParams.debugOn;
  mnew->rendParams.allSecondary = rendParams.allSecondary;
  mnew->rendParams.allTertiary = rendParams.allTertiary;
  mnew->rendParams.primaryMLFilter = rendParams.primaryMLFilter;
  mnew->rendParams.secondaryMLFilter = rendParams.secondaryMLFilter;
  mnew->rendParams.writeToDisk = rendParams.writeToDisk;
  mnew->rendParams.engine_type = rendParams.engine_type;
  //mnew->rendParams.device_id = rendParams.device_id;
  mnew->rendParams.liteMode = rendParams.liteMode;
  mnew->rendParams.useSeparateSwap = rendParams.useSeparateSwap;
  mnew->rendParams.specNoiseFilterOn = rendParams.specNoiseFilterOn;
  mnew->rendParams.specNoiseFilter = rendParams.specNoiseFilter;
	mnew->rendParams.preset = rendParams.preset;
	mnew->rendParams.rememberDevice = rendParams.rememberDevice;

  //path tracing
  mnew->rendParams.seed           = rendParams.seed;
  mnew->rendParams.causticRays    = rendParams.causticRays;
  mnew->rendParams.minrays        = rendParams.minrays;
  mnew->rendParams.maxrays        = rendParams.maxrays;
  mnew->rendParams.raybounce      = rendParams.raybounce;
  mnew->rendParams.diffbounce     = rendParams.diffbounce;
  mnew->rendParams.relative_error = rendParams.relative_error;
  mnew->rendParams.guided         = rendParams.guided;
  mnew->rendParams.bsdf_clamp_on  = rendParams.bsdf_clamp_on;
  mnew->rendParams.bsdf_clamp     = rendParams.bsdf_clamp;
  mnew->rendParams.env_clamp      = rendParams.env_clamp;
  //mnew->rendParams.estimateLDR    = rendParams.estimateLDR;
	mnew->rendParams.use_denoiser = rendParams.use_denoiser;
	mnew->rendParams.noiseLvl = rendParams.noiseLvl;


  //SPPM Caustics
  mnew->rendParams.maxphotons_c = rendParams.maxphotons_c;
  mnew->rendParams.caustic_power = rendParams.caustic_power;
  mnew->rendParams.retrace_c = rendParams.retrace_c;
  mnew->rendParams.initial_radius_c = rendParams.initial_radius_c;
  mnew->rendParams.visibility_c = rendParams.visibility_c;
  mnew->rendParams.alpha_c = rendParams.alpha_c;

  //SPPM Diffuse
  mnew->rendParams.maxphotons_d = rendParams.maxphotons_d;
  mnew->rendParams.retrace_d = rendParams.retrace_d;
  mnew->rendParams.initial_radius_d = rendParams.initial_radius_d;
  mnew->rendParams.visibility_d = rendParams.visibility_d;
  mnew->rendParams.alpha_d = rendParams.alpha_d;
  mnew->rendParams.irr_map = rendParams.irr_map;

  //Irradiance cache
  mnew->rendParams.ic_eval = rendParams.ic_eval;
  mnew->rendParams.maxpass = rendParams.maxpass;
  mnew->rendParams.fixed_rays = rendParams.fixed_rays;
  mnew->rendParams.ws_err = rendParams.ws_err;
  mnew->rendParams.ss_err4 = rendParams.ss_err4;
  mnew->rendParams.ss_err2 = rendParams.ss_err2;
  mnew->rendParams.ss_err1 = rendParams.ss_err1;
  mnew->rendParams.ic_relative_error = rendParams.ic_relative_error;

  //Multi-Layer
  mnew->rendParams.filterPrimary     = rendParams.filterPrimary;
  mnew->rendParams.r_sigma           = rendParams.r_sigma;
  mnew->rendParams.s_sigma           = rendParams.s_sigma;
  mnew->rendParams.layerNum          = rendParams.layerNum;
	mnew->rendParams.spp               = rendParams.spp;
  //mnew->rendParams.saveComposeLayers = rendParams.saveComposeLayers;

  //Post process
  mnew->rendParams.postProcOn        = rendParams.postProcOn;
  mnew->rendParams.numThreads        = rendParams.numThreads;
  mnew->rendParams.exposure          = rendParams.exposure;
  mnew->rendParams.compress          = rendParams.compress;
  mnew->rendParams.contrast          = rendParams.contrast;
  mnew->rendParams.saturation        = rendParams.saturation;
  mnew->rendParams.whiteBalance      = rendParams.whiteBalance;
  mnew->rendParams.whitePointColor   = rendParams.whitePointColor;
  mnew->rendParams.uniformContrast   = rendParams.uniformContrast;
  mnew->rendParams.normalize         = rendParams.normalize;
  mnew->rendParams.chromAberr        = rendParams.chromAberr;
  mnew->rendParams.vignette          = rendParams.vignette;
  mnew->rendParams.sharpness         = rendParams.sharpness;
  mnew->rendParams.sizeStar          = rendParams.sizeStar;
  mnew->rendParams.numRay            = rendParams.numRay;
  mnew->rendParams.rotateRay         = rendParams.rotateRay;
  mnew->rendParams.randomAngle       = rendParams.randomAngle;
  mnew->rendParams.sprayRay          = rendParams.sprayRay;


  mnew->rendParams.bloom             = rendParams.bloom;
  mnew->rendParams.icBounce          = rendParams.icBounce;
  //mnew->rendParams.hydraFrameBuf   = rendParams.hydraFrameBuf;
	mnew->rendParams.filter_id         = rendParams.filter_id;
	mnew->rendParams.mlaa              = rendParams.mlaa;
	mnew->rendParams.bloom_radius      = rendParams.bloom_radius;
	mnew->rendParams.bloom_str         = rendParams.bloom_str;
	
  //Override
  mnew->rendParams.env_mult = rendParams.env_mult;
  mnew->rendParams.overrideGrayMatOn = rendParams.overrideGrayMatOn;

	//MLT
	mnew->rendParams.mlt_plarge = rendParams.mlt_plarge;
	mnew->rendParams.mlt_iters_mult = rendParams.mlt_iters_mult;
	mnew->rendParams.mlt_burn_iters = rendParams.mlt_burn_iters;
	mnew->rendParams.mlt_enable_median_filter = rendParams.mlt_enable_median_filter;
	mnew->rendParams.mlt_median_filter_threshold = rendParams.mlt_median_filter_threshold;
	
  // Render elements
  mnew->rendParams.renElemAlphaOn        = rendParams.renElemAlphaOn;
  mnew->rendParams.renElemColorOn        = rendParams.renElemColorOn;
  mnew->rendParams.renElemCoordOn        = rendParams.renElemCoordOn;
  mnew->rendParams.renElemCoverOn        = rendParams.renElemCoverOn;
  mnew->rendParams.renElemDepthOn        = rendParams.renElemDepthOn;
  mnew->rendParams.renElemInstIdOn       = rendParams.renElemInstIdOn;
  mnew->rendParams.renElemMatIdOn        = rendParams.renElemMatIdOn;
  mnew->rendParams.renElemNormalsOn      = rendParams.renElemNormalsOn;
  mnew->rendParams.renElemObjIdOn        = rendParams.renElemObjIdOn;
  mnew->rendParams.renElemShadowOn       = rendParams.renElemShadowOn;


  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}

void hydraRender_mk3::ResetParams() 
{
  DebugPrint(_T("**** Resetting scene\n"));
  
  //This function is called when user does File -> New All (at least in 2018 max)
  //So we need to reset everything and create new Scene library and new scene
  //
  hrSceneLibraryOpen(L"C:/[Hydra]/pluginFiles/scenelib", HR_WRITE_DISCARD);
  m_rendRef     = hrRenderCreate(L"HydraModern");
  m_firstRender = true;

#ifdef SILENT_DEVICE_ID
  hydraRender_mk3::m_lastRendParams.device_id.push_back(3); // your device ID
  hydraRender_mk3::m_lastRendParams.engine_type = 1;        // 1 - OpenCL; 0 - CUDA
  hydraRender_mk3::m_lastRendParams.liteMode = false;
#else
  if (!readAndCheckDeviceSettingsFile(hydraRender_mk3::m_lastRendParams.engine_type, hydraRender_mk3::m_lastRendParams.device_id))
    UserSelectDeviceDialog(nullptr);
  else
    rendParams.rememberDevice = true;
#endif 



  geoInstancesLib.clear();
  geoNodesPools.clear();
  lightInstancesLib.clear();
  lightNodesPools.clear();
  texmapLib.clear();
  meshRefLib.clear();
  lightsRefLib.clear();
  materialsRefLib.clear();

  rendParams.SetDefaults();

  envTex = texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");

  mtlTracker->Clear();
  texTracker->Clear();
}


//Chunk ID's for loading and saving render data

#define MAIN_CHUNK		0x120
#define PATHTRACING_CHUNK		0x130
#define IRRCACHE_CHUNK		0x140
#define MULTILAYER_CHUNK		0x150
#define SPPM_C_CHUNK		0x160
#define SPPM_D_CHUNK		0x170
#define POSTPROC_CHUNK		0x180
#define OVERRIDE_CHUNK		0x190
#define MLT_CHUNK		0x200
#define DENOISE_CHUNK		0x210
#define REND_ELEM_CHUNK		0x220

IOResult hydraRender_mk3::Save(ISave *isave)
{
  ULONG nb;

  isave->BeginChunk(MAIN_CHUNK);
  isave->Write(&rendParams.renderQuality, sizeof(float), &nb);
  isave->Write(&rendParams.primary, sizeof(int), &nb);
  isave->Write(&rendParams.secondary, sizeof(int), &nb);
  isave->Write(&rendParams.tertiary, sizeof(int), &nb);
  isave->Write(&rendParams.enableCaustics, sizeof(bool), &nb);
  isave->Write(&rendParams.enableTextureResize, sizeof(bool), &nb);
  isave->Write(&rendParams.productionMode, sizeof(bool), &nb);
  isave->Write(&rendParams.allSecondary, sizeof(bool), &nb);
  isave->Write(&rendParams.allTertiary, sizeof(bool), &nb);
  isave->Write(&rendParams.timeLimitOn, sizeof(bool), &nb);
  isave->Write(&rendParams.useHydraGUI, sizeof(bool), &nb);
  isave->Write(&rendParams.noRandomLightSel, sizeof(bool), &nb);
  isave->Write(&rendParams.debugOn, sizeof(bool), &nb);
  isave->Write(&rendParams.enableLog, sizeof(bool), &nb);
  isave->Write(&rendParams.enableDOF, sizeof(bool), &nb);
  isave->Write(&rendParams.primaryMLFilter, sizeof(bool), &nb);
  isave->Write(&rendParams.secondaryMLFilter, sizeof(bool), &nb);
  isave->Write(&rendParams.writeToDisk, sizeof(bool), &nb);
  isave->Write(&rendParams.timeLimit, sizeof(long), &nb);
  isave->Write(&rendParams.focalplane, sizeof(float), &nb);
  isave->Write(&rendParams.lensradius, sizeof(float), &nb);
  isave->Write(&rendParams.engine_type, sizeof(int), &nb);
  /*isave->Write(&rendParams.devices_num, sizeof(int), &nb);
  isave->Write(rendParams.device_id.data(), rendParams.devices_num*sizeof(int), &nb);*/
  isave->Write(&rendParams.liteMode, sizeof(bool), &nb);
  isave->Write(&rendParams.useSeparateSwap, sizeof(bool), &nb);
  isave->Write(&rendParams.specNoiseFilterOn, sizeof(bool), &nb);
  isave->Write(&rendParams.specNoiseFilter, sizeof(float), &nb);
	isave->Write(&rendParams.preset, sizeof(int), &nb);
	//isave->Write(&rendParams.rememberDevice, sizeof(bool), &nb);

  isave->EndChunk();

  isave->BeginChunk(PATHTRACING_CHUNK);
  isave->Write(&rendParams.icBounce, sizeof(int), &nb);
  isave->Write(&rendParams.seed, sizeof(int), &nb);
  isave->Write(&rendParams.minrays, sizeof(int), &nb);
  isave->Write(&rendParams.maxrays, sizeof(int), &nb);
  isave->Write(&rendParams.raybounce, sizeof(int), &nb);
  isave->Write(&rendParams.diffbounce, sizeof(int), &nb);
  isave->Write(&rendParams.useRR, sizeof(int), &nb);
  isave->Write(&rendParams.causticRays, sizeof(int), &nb);
  isave->Write(&rendParams.relative_error, sizeof(float), &nb);
  isave->Write(&rendParams.guided, sizeof(bool), &nb);
  isave->Write(&rendParams.bsdf_clamp_on, sizeof(bool), &nb);
  isave->Write(&rendParams.bsdf_clamp, sizeof(float), &nb);
  isave->Write(&rendParams.env_clamp, sizeof(float), &nb);
  //isave->Write(&rendParams.estimateLDR, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(IRRCACHE_CHUNK);
  isave->Write(&rendParams.ic_eval, sizeof(int), &nb);
  isave->Write(&rendParams.maxpass, sizeof(int), &nb);
  isave->Write(&rendParams.fixed_rays, sizeof(int), &nb);
  isave->Write(&rendParams.ws_err, sizeof(float), &nb);
  isave->Write(&rendParams.ss_err4, sizeof(float), &nb);
  isave->Write(&rendParams.ss_err2, sizeof(float), &nb);
  isave->Write(&rendParams.ss_err1, sizeof(float), &nb);
  isave->Write(&rendParams.ic_relative_error, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(MULTILAYER_CHUNK);
  isave->Write(&rendParams.layerNum, sizeof(int), &nb);
  isave->Write(&rendParams.r_sigma, sizeof(float), &nb);
  isave->Write(&rendParams.s_sigma, sizeof(float), &nb);
  isave->Write(&rendParams.spp, sizeof(int), &nb);
  isave->Write(&rendParams.filterPrimary, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(SPPM_C_CHUNK);
  isave->Write(&rendParams.maxphotons_c, sizeof(int), &nb);
  isave->Write(&rendParams.retrace_c, sizeof(int), &nb);
  isave->Write(&rendParams.initial_radius_c, sizeof(float), &nb);
  isave->Write(&rendParams.caustic_power, sizeof(float), &nb);
  isave->Write(&rendParams.visibility_c, sizeof(bool), &nb);
  isave->Write(&rendParams.alpha_c, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(SPPM_D_CHUNK);
  isave->Write(&rendParams.maxphotons_d, sizeof(int), &nb);
  isave->Write(&rendParams.retrace_d, sizeof(int), &nb);
  isave->Write(&rendParams.initial_radius_d, sizeof(float), &nb);
  isave->Write(&rendParams.visibility_d, sizeof(bool), &nb);
  isave->Write(&rendParams.alpha_d, sizeof(float), &nb);
  isave->Write(&rendParams.irr_map, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(POSTPROC_CHUNK);
  isave->Write(&rendParams.postProcOn, sizeof(float), &nb);
  isave->Write(&rendParams.exposure, sizeof(float), &nb);
  isave->Write(&rendParams.compress, sizeof(float), &nb);
  isave->Write(&rendParams.contrast, sizeof(float), &nb);
  isave->Write(&rendParams.saturation, sizeof(float), &nb);
  isave->Write(&rendParams.whiteBalance, sizeof(float), &nb);
  isave->Write(&rendParams.whitePointColor, sizeof(Color), &nb);
  isave->Write(&rendParams.uniformContrast, sizeof(float), &nb);
  isave->Write(&rendParams.normalize, sizeof(float), &nb);
  isave->Write(&rendParams.chromAberr, sizeof(float), &nb);
  isave->Write(&rendParams.vignette, sizeof(float), &nb);
  isave->Write(&rendParams.sharpness, sizeof(float), &nb);
  isave->Write(&rendParams.sizeStar, sizeof(float), &nb);
  isave->Write(&rendParams.numRay, sizeof(int), &nb);
  isave->Write(&rendParams.rotateRay, sizeof(int), &nb);
  isave->Write(&rendParams.randomAngle, sizeof(float), &nb);
  isave->Write(&rendParams.sprayRay, sizeof(float), &nb);

	isave->Write(&rendParams.filter_id, sizeof(int), &nb);
  isave->Write(&rendParams.bloom, sizeof(bool), &nb);
  //isave->Write(&rendParams.hydraFrameBuf, sizeof(bool), &nb);
	isave->Write(&rendParams.mlaa, sizeof(bool), &nb);
	isave->Write(&rendParams.bloom_radius, sizeof(float), &nb);
	isave->Write(&rendParams.bloom_str, sizeof(float), &nb);
  isave->EndChunk();
	
  isave->BeginChunk(OVERRIDE_CHUNK);
  isave->Write(&rendParams.env_mult, sizeof(float), &nb);
  isave->Write(&rendParams.overrideGrayMatOn, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(MLT_CHUNK);
	isave->Write(&rendParams.mlt_plarge, sizeof(float), &nb);
	isave->Write(&rendParams.mlt_iters_mult, sizeof(int), &nb);
	isave->Write(&rendParams.mlt_burn_iters, sizeof(int), &nb);
	isave->Write(&rendParams.mlt_enable_median_filter, sizeof(bool), &nb);
	isave->Write(&rendParams.mlt_median_filter_threshold, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(DENOISE_CHUNK);
	isave->Write(&rendParams.use_denoiser, sizeof(bool), &nb);
	isave->Write(&rendParams.noiseLvl, sizeof(float), &nb);
	isave->EndChunk();
  
  isave->BeginChunk(REND_ELEM_CHUNK);
  isave->Write(&rendParams.renElemAlphaOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemColorOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemCoordOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemCoverOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemDepthOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemInstIdOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemMatIdOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemNormalsOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemObjIdOn, sizeof(bool), &nb);
  isave->Write(&rendParams.renElemShadowOn, sizeof(bool), &nb);

  isave->EndChunk(); 


  return IO_OK;
}

IOResult hydraRender_mk3::Load(ILoad *iload)
{
  ULONG nb;
  int id;
  IOResult res;
  while (IO_OK == (res = iload->OpenChunk())) {
    switch (id = iload->CurChunkID())  {
    case MAIN_CHUNK:
      res = iload->Read(&rendParams.renderQuality, sizeof(float), &nb);
      res = iload->Read(&rendParams.primary, sizeof(int), &nb);
      res = iload->Read(&rendParams.secondary, sizeof(int), &nb);
      res = iload->Read(&rendParams.tertiary, sizeof(int), &nb);
      res = iload->Read(&rendParams.enableCaustics, sizeof(bool), &nb);
      res = iload->Read(&rendParams.enableTextureResize, sizeof(bool), &nb);
      res = iload->Read(&rendParams.productionMode, sizeof(bool), &nb);
      res = iload->Read(&rendParams.allSecondary, sizeof(bool), &nb);
      res = iload->Read(&rendParams.allTertiary, sizeof(bool), &nb);
      res = iload->Read(&rendParams.timeLimitOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.useHydraGUI, sizeof(bool), &nb);
      res = iload->Read(&rendParams.noRandomLightSel, sizeof(bool), &nb);
      res = iload->Read(&rendParams.debugOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.enableLog, sizeof(bool), &nb);
      res = iload->Read(&rendParams.enableDOF, sizeof(bool), &nb);
      res = iload->Read(&rendParams.primaryMLFilter, sizeof(bool), &nb);
      res = iload->Read(&rendParams.secondaryMLFilter, sizeof(bool), &nb);
      res = iload->Read(&rendParams.writeToDisk, sizeof(bool), &nb);
      res = iload->Read(&rendParams.timeLimit, sizeof(long), &nb);
      res = iload->Read(&rendParams.focalplane, sizeof(float), &nb);
      res = iload->Read(&rendParams.lensradius, sizeof(float), &nb);
      res = iload->Read(&rendParams.engine_type, sizeof(int), &nb);
      /*res = iload->Read(&rendParams.devices_num, sizeof(int), &nb);

      int *temp_dev;
      temp_dev = new int[rendParams.devices_num];
      res = iload->Read(temp_dev, rendParams.devices_num*sizeof(int), &nb);
      rendParams.device_id.resize(rendParams.device_id.size() + rendParams.devices_num);
      memcpy(&rendParams.device_id[rendParams.device_id.size() - rendParams.devices_num], &temp_dev[0], rendParams.devices_num * sizeof(int));
      delete temp_dev;
      */
      res = iload->Read(&rendParams.liteMode, sizeof(bool), &nb);
      res = iload->Read(&rendParams.useSeparateSwap, sizeof(bool), &nb);
      res = iload->Read(&rendParams.specNoiseFilterOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.specNoiseFilter, sizeof(float), &nb);
			res = iload->Read(&rendParams.preset, sizeof(int), &nb);

			//res = iload->Read(&rendParams.rememberDevice, sizeof(bool), &nb);
      break;

    case PATHTRACING_CHUNK:
      res = iload->Read(&rendParams.icBounce, sizeof(int), &nb);
      res = iload->Read(&rendParams.seed, sizeof(int), &nb);
      res = iload->Read(&rendParams.minrays, sizeof(int), &nb);
      res = iload->Read(&rendParams.maxrays, sizeof(int), &nb);
      res = iload->Read(&rendParams.raybounce, sizeof(int), &nb);
      res = iload->Read(&rendParams.diffbounce, sizeof(int), &nb);
      res = iload->Read(&rendParams.useRR, sizeof(int), &nb);
      res = iload->Read(&rendParams.causticRays, sizeof(int), &nb);
      res = iload->Read(&rendParams.relative_error, sizeof(float), &nb);
      res = iload->Read(&rendParams.guided, sizeof(bool), &nb);
      res = iload->Read(&rendParams.bsdf_clamp_on, sizeof(bool), &nb);
      res = iload->Read(&rendParams.bsdf_clamp, sizeof(float), &nb);
      res = iload->Read(&rendParams.env_clamp, sizeof(float), &nb);
      //res = iload->Read(&rendParams.estimateLDR, sizeof(bool), &nb);
      break;

    case IRRCACHE_CHUNK:
      res = iload->Read(&rendParams.ic_eval, sizeof(int), &nb);
      res = iload->Read(&rendParams.maxpass, sizeof(int), &nb);
      res = iload->Read(&rendParams.fixed_rays, sizeof(int), &nb);
      res = iload->Read(&rendParams.ws_err, sizeof(float), &nb);
      res = iload->Read(&rendParams.ss_err4, sizeof(float), &nb);
      res = iload->Read(&rendParams.ss_err2, sizeof(float), &nb);
      res = iload->Read(&rendParams.ss_err1, sizeof(float), &nb);
      res = iload->Read(&rendParams.ic_relative_error, sizeof(float), &nb);
      break;
    case MULTILAYER_CHUNK:
      res = iload->Read(&rendParams.layerNum, sizeof(int), &nb);
      res = iload->Read(&rendParams.r_sigma, sizeof(float), &nb);
      res = iload->Read(&rendParams.s_sigma, sizeof(float), &nb);
      res = iload->Read(&rendParams.spp,     sizeof(int), &nb);
      res = iload->Read(&rendParams.filterPrimary,     sizeof(bool), &nb);
      break;
    case SPPM_C_CHUNK:
      res = iload->Read(&rendParams.maxphotons_c, sizeof(int), &nb);
      res = iload->Read(&rendParams.retrace_c, sizeof(int), &nb);
      res = iload->Read(&rendParams.initial_radius_c, sizeof(float), &nb);
      res = iload->Read(&rendParams.caustic_power, sizeof(float), &nb);
      res = iload->Read(&rendParams.visibility_c, sizeof(bool), &nb);
      res = iload->Read(&rendParams.alpha_c, sizeof(float), &nb);
      break;
    case SPPM_D_CHUNK:
      res = iload->Read(&rendParams.maxphotons_d, sizeof(int), &nb);
      res = iload->Read(&rendParams.retrace_d, sizeof(int), &nb);
      res = iload->Read(&rendParams.initial_radius_d, sizeof(float), &nb);
      res = iload->Read(&rendParams.visibility_d, sizeof(bool), &nb);
      res = iload->Read(&rendParams.alpha_d, sizeof(float), &nb);
      res = iload->Read(&rendParams.irr_map, sizeof(bool), &nb);
      break;
    case OVERRIDE_CHUNK:
      res = iload->Read(&rendParams.env_mult, sizeof(float), &nb);
      res = iload->Read(&rendParams.overrideGrayMatOn, sizeof(bool), &nb);
      break;
		case MLT_CHUNK:
			res = iload->Read(&rendParams.mlt_plarge, sizeof(float), &nb);
			res = iload->Read(&rendParams.mlt_iters_mult, sizeof(int), &nb);
			res = iload->Read(&rendParams.mlt_burn_iters, sizeof(int), &nb);
			res = iload->Read(&rendParams.mlt_enable_median_filter, sizeof(bool), &nb);
			res = iload->Read(&rendParams.mlt_median_filter_threshold, sizeof(float), &nb);
			break;
    case DENOISE_CHUNK:
      res = iload->Read(&rendParams.use_denoiser, sizeof(bool), &nb);
      res = iload->Read(&rendParams.noiseLvl, sizeof(float), &nb);
      break;
    case POSTPROC_CHUNK:
      res = iload->Read(&rendParams.postProcOn, sizeof(float), &nb);
      res = iload->Read(&rendParams.exposure, sizeof(float), &nb);
      res = iload->Read(&rendParams.compress, sizeof(float), &nb);
      res = iload->Read(&rendParams.contrast, sizeof(float), &nb);
      res = iload->Read(&rendParams.saturation, sizeof(float), &nb);
      res = iload->Read(&rendParams.whiteBalance, sizeof(float), &nb);
      res = iload->Read(&rendParams.whitePointColor, sizeof(Color), &nb);
      res = iload->Read(&rendParams.uniformContrast, sizeof(float), &nb);
      res = iload->Read(&rendParams.normalize, sizeof(float), &nb);
      res = iload->Read(&rendParams.chromAberr, sizeof(float), &nb);
      res = iload->Read(&rendParams.vignette, sizeof(float), &nb);
      res = iload->Read(&rendParams.sharpness, sizeof(float), &nb);
      res = iload->Read(&rendParams.sizeStar, sizeof(float), &nb);
      res = iload->Read(&rendParams.numRay, sizeof(int), &nb);
      res = iload->Read(&rendParams.rotateRay, sizeof(int), &nb);
      res = iload->Read(&rendParams.randomAngle, sizeof(float), &nb);
      res = iload->Read(&rendParams.sprayRay, sizeof(float), &nb);

      res = iload->Read(&rendParams.filter_id, sizeof(int), &nb);
      res = iload->Read(&rendParams.bloom, sizeof(bool), &nb);
      //res = iload->Read(&rendParams.hydraFrameBuf, sizeof(bool), &nb);
      res = iload->Read(&rendParams.mlaa, sizeof(bool), &nb);
      res = iload->Read(&rendParams.bloom_radius, sizeof(float), &nb);
      res = iload->Read(&rendParams.bloom_str, sizeof(float), &nb);
      break;
    case REND_ELEM_CHUNK:
      res = iload->Read(&rendParams.renElemAlphaOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemColorOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemCoordOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemCoverOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemDepthOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemInstIdOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemMatIdOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemNormalsOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemObjIdOn, sizeof(bool), &nb);
      res = iload->Read(&rendParams.renElemShadowOn, sizeof(bool), &nb);
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  auto savedDevId   = hydraRender_mk3::m_lastRendParams.device_id;
  auto saveEnType   = hydraRender_mk3::m_lastRendParams.engine_type;
  auto saveLiteMode = hydraRender_mk3::m_lastRendParams.liteMode;

  hydraRender_mk3::m_lastRendParams = rendParams;

  hydraRender_mk3::m_lastRendParams.device_id   = savedDevId;  
  hydraRender_mk3::m_lastRendParams.engine_type = saveEnType;
  hydraRender_mk3::m_lastRendParams.liteMode    = saveLiteMode;

  return IO_OK;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


std::string MethodName(int a_name)
{
  if (a_name == RENDER_METHOD_PT)
    return "pathtracing";
  else if (a_name == RENDER_METHOD_MMLT)
    return "mlt";
  else if (a_name == RENDER_METHOD_LT)
    return "lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return "IBPT";
  else
    return "none";
}

std::wstring MethodNameW(int a_name)
{
  if (a_name == RENDER_METHOD_PT)
    return L"pathtracing";
  else if (a_name == RENDER_METHOD_MMLT)
    return L"mmlt";
  else if (a_name == RENDER_METHOD_LT)
    return L"lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return L"IBPT";
  else
    return L"none";
}

std::wstring MethodNamePrimaryW(int a_name)
{
  if (a_name == RENDER_METHOD_PT || a_name == RENDER_METHOD_MMLT)
    return L"pathtracing";
  else if (a_name == RENDER_METHOD_LT)
    return L"lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return L"IBPT";
  else
    return L"none";
}

std::vector<std::string> mlGetRenderLayersStrs(const HydraRenderParams* pRenderParams);

void hydraRender_mk3::SetRenderParamsImageSizeFromBitMap(HydraRenderParams* pParams, Bitmap* tobm)
{
  pParams->devWidth     = tobm->Width();
  pParams->devHeight    = tobm->Height();
  pParams->devAspect    = tobm->Aspect();

  pParams->nMinx = 0;
  pParams->nMiny = 0;
  pParams->nMaxx = pParams->devWidth;
  pParams->nMaxy = pParams->devHeight;
}

HRRenderRef hydraRender_mk3::SetupRender(FrameRendParams &frp)
{
  //set devices
  for(auto it = rendParams.device_id.begin(); it != rendParams.device_id.end(); ++it)
    hrRenderEnableDevice(m_rendRef, it->first, it->second);

  auto renderWidth  = rendParams.nMaxx - rendParams.nMinx;
  auto renderHeight = rendParams.nMaxy - rendParams.nMiny;

  hrRenderOpen(m_rendRef, HR_OPEN_EXISTING);
  {
    auto node = hrRenderParamNode(m_rendRef);

    node.force_child(L"width").text() = renderWidth;
    node.force_child(L"height").text() = renderHeight;

    //methods setup
    node.force_child(L"method_primary").text() = MethodNamePrimaryW(this->rendParams.secondary).c_str();
    node.force_child(L"method_secondary").text() = MethodNameW(this->rendParams.secondary).c_str();
    node.force_child(L"method_tertiary").text() = MethodNameW(this->rendParams.secondary).c_str();
    if (this->rendParams.enableCaustics)
      node.force_child(L"method_caustic").text() = MethodNameW(this->rendParams.secondary).c_str();
    else
      node.force_child(L"method_caustic").text() = L"none"
      ;
    node.force_child(L"shadows").text() = L"1";

    // path tracing
    node.force_child(L"trace_depth").text() = this->rendParams.raybounce;
    node.force_child(L"diff_trace_depth").text() = this->rendParams.diffbounce;

    node.force_child(L"pt_error").text() = this->rendParams.relative_error;
    node.force_child(L"minRaysPerPixel").text() = this->rendParams.minrays;
    node.force_child(L"maxRaysPerPixel").text() = this->rendParams.maxrays;

    // MLT
    node.force_child(L"mlt_plarge").text() = this->rendParams.mlt_plarge;
    node.force_child(L"mlt_iters_mult").text() = this->rendParams.mlt_iters_mult;
    node.force_child(L"mlt_burn_iters").text() = this->rendParams.mlt_burn_iters;

    //clamping
    auto envClamp = this->rendParams.env_clamp;
    auto bsdfClamp = (rendParams.bsdf_clamp_on) ? this->rendParams.bsdf_clamp : 1.0e6f;
    if (this->rendParams.secondary == RENDER_METHOD_MMLT || this->rendParams.enableCaustics)
    {
      envClamp = 1.0e6f;
      bsdfClamp = 1.0e6f;
    }

    node.force_child(L"envclamp").text() = envClamp;
    node.force_child(L"bsdfclamp").text() = bsdfClamp;
    node.force_child(L"separate_swap").text() = int(this->rendParams.useSeparateSwap);

    //region render && blowUp
    if (&frp != nullptr)
    {
      if (rendParams.rendType == RENDTYPE_REGION || RENDTYPE_REGION_SEL == RENDTYPE_REGION)
      {
        node.force_child(L"regxmin").text() = frp.regxmin;
        node.force_child(L"regxmax").text() = frp.regxmax;
        node.force_child(L"regymin").text() = frp.regymin;
        node.force_child(L"regymax").text() = frp.regymax;
      }
      if (rendParams.rendType == RENDTYPE_BLOWUP || rendParams.rendType == RENDTYPE_BLOWUP_SEL)
      {
        auto centerX = float(renderWidth)*0.5f;
        auto centerY = float(renderHeight)*0.5f;

        auto offsetX = (frp.blowupCenter.x - centerX) / float(renderWidth);
        auto offsetY = (frp.blowupCenter.y - centerY) / float(renderHeight);

        auto scaleX = 1.0f / frp.blowupFactor.x;
        auto scaleY = 1.0f / frp.blowupFactor.y;

        node.force_child(L"blowupOffsetX").text() = offsetX;
        node.force_child(L"blowupOffsetY").text() = offsetY;
        node.force_child(L"blowupScaleX").text() = scaleX;
        node.force_child(L"blowupScaleY").text() = scaleY;
      }
    }

    if(rendParams.debugCheckBoxes[L"FORCE_GPU_FRAMEBUFFER"])
      node.force_child(L"forceGPUFrameBuffer").text() = 1;
    else
      node.force_child(L"forceGPUFrameBuffer").text() = 0;

    if (rendParams.renElemAlphaOn || rendParams.renElemColorOn   || rendParams.renElemCoordOn  ||
        rendParams.renElemCoverOn || rendParams.renElemDepthOn   || rendParams.renElemInstIdOn ||
        rendParams.renElemMatIdOn || rendParams.renElemNormalsOn || rendParams.renElemObjIdOn  ||
        rendParams.renElemShadowOn)
    {
      node.force_child(L"evalgbuffer").text() = 1; // Enable gbuffer
    }
    else
      node.force_child(L"evalgbuffer").text() = 0; // Disable gbuffer


    if (rendParams.debugCheckBoxes[L"DEBUG"])
      node.force_child(L"dont_run").text() = 1;
    else
      node.force_child(L"dont_run").text() = 0;

    if (rendParams.enableTextureResize)
      node.force_child(L"scenePrepass").text() = 1;
    else
      node.force_child(L"scenePrepass").text() = 0;

    if (rendParams.productionMode)
      node.force_child(L"offline_pt").text() = 1;
    else
      node.force_child(L"offline_pt").text() = 0;

    node.force_child(L"resources_path").text() = L"C:/[Hydra]/bin2/shaders";
    

    //gamma
    if (gammaMgr.IsEnabled())
      this->rendParams.gamma = gammaMgr.GetDisplayGamma();

    node.force_child(L"tmGamma").text() = this->rendParams.gamma;
    //node.force_child(L"texInputGamma").text() = gammaMgr.GetFileInGamma(); // don't use this anymore!

    // DOF
    if(this->rendParams.enableDOF)
    {
      hrCameraOpen(m_camRef, HR_OPEN_EXISTING);
      auto camNode = hrCameraParamNode(m_camRef);
      camNode.force_child(L"enable_dof").text()      = int(this->rendParams.enableDOF);
      camNode.force_child(L"dof_lens_radius").text() = this->rendParams.lensradius*0.01f;
      hrCameraClose(m_camRef);
      //node.force_child(L"dofFocalPlaneDist").text() = this->rendParams.focalplane;
    }
    else
    {
      hrCameraOpen(m_camRef, HR_OPEN_EXISTING);
      auto camNode = hrCameraParamNode(m_camRef);
      camNode.force_child(L"enable_dof").text() = 0;
      hrCameraClose(m_camRef);
    }

    //other
    if (this->rendParams.enableLog)
    {
      node.force_child(L"outputRedirect").text() = int(this->rendParams.enableLog);
      hrRenderLogDir(m_rendRef, L"C:/[Hydra]/logs/", true);
    }
    else
    {
      node.force_child(L"outputRedirect").text() = int(this->rendParams.enableLog);
      hrRenderLogDir(m_rendRef, L"", false);
    }
  }
  hrRenderClose(m_rendRef);

  return m_rendRef;
}

HRRenderRef hydraRender_mk3::SetupRenderForMatEditor(FrameRendParams &frp)
{
  HR_OPEN_MODE mode;
  //if (m_rendRefMatEditor.id == -1)
  //{
    mode = HR_WRITE_DISCARD;
    m_rendRefMatEditor = hrRenderCreate(L"HydraModern");
  //}
  //else
  //{
  //  mode = HR_OPEN_EXISTING;
  //}

  //set devices
  for (auto i = 0; i < rendParams.device_id.size(); ++i)
    hrRenderEnableDevice(m_rendRefMatEditor, rendParams.device_id.at(i), true);

  auto renderWidth = rendParams.nMaxx - rendParams.nMinx; //MTL_WINDOW_SIZE
  auto renderHeight = rendParams.nMaxy - rendParams.nMiny;

  hrRenderOpen(m_rendRefMatEditor, mode);
  {
    auto node = hrRenderParamNode(m_rendRefMatEditor);

    node.force_child(L"width").text() = renderWidth;
    node.force_child(L"height").text() = renderHeight;

    //methods setup
    node.force_child(L"method_primary").text() = L"pathtracing";
    node.force_child(L"method_secondary").text() = L"pathtracing";
    node.force_child(L"method_tertiary").text() = L"pathtracing";
    node.force_child(L"shadows").text() = L"1";

    // path tracing
    node.force_child(L"trace_depth").text() = 8;
    node.force_child(L"diff_trace_depth").text() = 2;

    node.force_child(L"pt_error").text() = 2.0f;
    node.force_child(L"minRaysPerPixel").text() = 256;
    node.force_child(L"maxRaysPerPixel").text() = 512;

    node.force_child(L"envclamp").text() = 1.0e6f;
    node.force_child(L"bsdfclamp").text() = 1.0e6f;
    node.force_child(L"separate_swap").text() = 0;

    //gamma
    if (gammaMgr.IsEnabled())
      this->rendParams.gamma = gammaMgr.GetDisplayGamma();

    node.force_child(L"tmGamma").text() = this->rendParams.gamma;
    node.force_child(L"texInputGamma").text() = gammaMgr.GetFileInGamma();

    //other
    node.force_child(L"outputRedirect").text() = this->rendParams.enableLog;
  }
  hrRenderClose(m_rendRefMatEditor);

  return m_rendRefMatEditor;
}


bool hydraRender_mk3::VerifyThings(int renderWidth, int renderHeight)
{

  if (!isFileExist("C:\\[Hydra]\\bin2\\hydra.exe"))
  {
    MessageBoxA(NULL, "Can't find 'C:\\[Hydra]\\bin2\\hydra.exe', perhaps forgot copy [Hydra] folder to 'C:\\' ?", "Critical error", MB_OK);

    if (mtlEditorWasOpen)
    {
      GetCOREInterface7()->OpenMtlDlg();
      mtlEditorWasOpen = FALSE;
    }

    return false;
  }
  /*
  if (!rendParams.inMtlEditor && (incl.geomObjNum == 0))
  {
    plugin_log.Print("Empty scene.");
    return false;
  }*/

  return true;
}


void hydraRender_mk3::ClearTempFolder()
{
#ifdef MAX2012

	std::string tempFolder = HydraInstallPath() + "temp\\";
  std::string tempName   = tempFolder + "*";
  WIN32_FIND_DATAA fd;
  HANDLE hFind = ::FindFirstFile(tempName.c_str(), &fd);
  if (hFind != INVALID_HANDLE_VALUE)
  {
    do
    {
      std::string tempName2 = tempFolder + fd.cFileName;
      DeleteFile(tempName2.c_str());

    } while (::FindNextFile(hFind, &fd));

    ::FindClose(hFind);
  }
	
#else
  //C:\[Hydra]\pluginFiles\scenelib\data
  std::wstring tempFolder = HydraInstallPathW() + L"temp\\";
  std::wstring sceneLibFolder = HydraInstallPathW() + L"pluginFiles\\scenelib\\data\\";

  std::vector<std::wstring> folders = { tempFolder, sceneLibFolder };
  for (auto folder : folders)
  {
    std::wstring tempName = folder + L"*";
    WIN32_FIND_DATAW fd;
    HANDLE hFind = ::FindFirstFile(tempName.c_str(), &fd);
    if (hFind != INVALID_HANDLE_VALUE)
    {
      do
      {
        std::wstring tempName2 = folder + fd.cFileName;
        DeleteFileW(tempName2.c_str());

      } while (::FindNextFile(hFind, &fd));

      ::FindClose(hFind);
    }
  }
#endif

}

void hydraRender_mk3::copyBitmap(Bitmap* from, Bitmap* to)
{
  int renderWidth  = rendParams.nMaxx - rendParams.nMinx;
  int renderHeight = rendParams.nMaxy - rendParams.nMiny;

  std::vector<BMM_Color_fl> line(renderWidth);

  for (int y = rendParams.nMiny; y < rendParams.nMaxy; y++)
  {
    from->GetPixels(0, y, renderWidth, &line[0]);
    to->PutPixels(0, y, renderWidth, &line[0]);
  }
}

void hydraRender_mk3::displayLastRender(bool processed)
{
  if (!processed)
  {
    int renderWidth = rendParams.nMaxx - rendParams.nMinx;
    int renderHeight = rendParams.nMaxy - rendParams.nMiny;
    // last_render->CopyImage(raw_render, COPY_IMAGE_CROP, 0);
    CopyImageFromHRRenderToBitmap(last_render, renderWidth, renderHeight);
  }

  if (last_render == nullptr)
    return;

  last_render->RefreshWindow(0);
  if (last_render->GetWindow())
  {
    UpdateWindow(last_render->GetWindow());
    plugin_log.Print("display last render");
  }
  

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

HWND g_hProgBar = NULL;

void DenoiseProgressBar(const char* message, float a_progress)
{
  if (g_hProgBar == NULL)
    return;
	//SendMessage(g_hProgBar, PBM_STEPIT, 0, 0);
  //SendMessageA(g_hProgBar, PBM_SETSTEP, (WPARAM)(a_progress*100.0f), 0);
	SendMessage(g_hProgBar, PBM_SETPOS, (WPARAM)(a_progress*100.0f), 0);
}

void InitDenoiseProgressBar()
{
  HWND hwndParent = GetDesktopWindow();

	g_hProgBar = CreateWindowExA(WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE, PROGRESS_CLASSA, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, 512, 512, 500, 50, hwndParent, (HMENU)0, hInstance, NULL);
  BringWindowToTop(g_hProgBar);

	SendMessageA(g_hProgBar, PBM_SETRANGE, 0, MAKELPARAM(0, 100));
	SendMessage(g_hProgBar, PBM_SETPOS, 0, 0);
	SendMessage(g_hProgBar, PBM_SETSTEP, (WPARAM)1, 0);
}

void DestroyDenoiseProgressBar()
{
  DestroyWindow(g_hProgBar); 
  g_hProgBar = NULL;
}

void hydraRender_mk3::DoDenoise(Bitmap* a_maxFrameBuff, int w, int h)
{
  if (a_maxFrameBuff == nullptr)
    return;


  InitDenoiseProgressBar();

  if (rendParams.postProcOn)
  {
    a_maxFrameBuff->RefreshWindow();
  }
	
  DestroyDenoiseProgressBar();
}

void hydraRender_mk3::DoPostProc(Bitmap* bmap)
{  
  if (hydra_raw_render.id == -1)
    return;

  int w = rendParams.nMaxx - rendParams.nMinx;
  int h = rendParams.nMaxy - rendParams.nMiny;
  int bpp = 16;

  hrRenderCopyFrameBufferToFBI(m_rendRef, L"color", hydra_raw_render);

  pugi::xml_document docSettings;
  pugi::xml_node settings = docSettings.append_child(L"settings");

  settings.append_attribute(L"numThreads")      = rendParams.numThreads;
  settings.append_attribute(L"exposure")        = rendParams.exposure;
  settings.append_attribute(L"compress")        = rendParams.compress;
  settings.append_attribute(L"contrast")        = rendParams.contrast;
  settings.append_attribute(L"saturation")      = rendParams.saturation;
  settings.append_attribute(L"whiteBalance")    = rendParams.whiteBalance;
  settings.append_attribute(L"whitePointColor"); 
  float colorArr[3] = { rendParams.whitePointColor.r, rendParams.whitePointColor.g, rendParams.whitePointColor.b };
  HydraXMLHelpers::WriteFloat3(settings.attribute(L"whitePointColor"), colorArr);
  settings.append_attribute(L"uniformContrast") = rendParams.uniformContrast;
  settings.append_attribute(L"normalize")       = rendParams.normalize;
  settings.append_attribute(L"chromAberr")      = rendParams.chromAberr;
  settings.append_attribute(L"vignette")        = rendParams.vignette;
  settings.append_attribute(L"sharpness")       = rendParams.sharpness;
                                                                     
  settings.append_attribute(L"diffStars_sizeStar")    = rendParams.sizeStar;
  settings.append_attribute(L"diffStars_numRay")      = rendParams.numRay;
  settings.append_attribute(L"diffStars_rotateRay")   = rendParams.rotateRay;
  settings.append_attribute(L"diffStars_randomAngle") = rendParams.randomAngle;
  settings.append_attribute(L"diffStars_sprayRay")    = rendParams.sprayRay;

  hrFilterApply(L"post_process_hydra1", settings, HRRenderRef(),
                  L"in_color", hydra_raw_render,
                  L"out_color", hydra_out_render);

  //hrFBISaveToFile(image2, L"tests_images/test_306/z_out.png");


  auto image = (float*)hrFBIGetData(hydra_out_render, &w, &h, &bpp);

  CopyRawImageToBitmap(bmap, image, w, h);

  bmap->RefreshWindow();
}

void hydraRender_mk3::UpdatePostProc(HWND hWnd, HydraRenderParamDlg *dlg)
{
  if (dlg != nullptr && dlg->rend != nullptr && dlg->rend->last_render != nullptr)
  {
    int renderWidth = dlg->rend->rendParams.nMaxx - dlg->rend->rendParams.nMinx;
    int renderHeight = dlg->rend->rendParams.nMaxy - dlg->rend->rendParams.nMiny;

    SetFocus(dlg->rend->last_render->GetWindow());
    SetActiveWindow(dlg->rend->last_render->GetWindow());

    SetFocus(hWnd);
    SetActiveWindow(hWnd);

    dlg->ReadGuiToRendParams(&dlg->rend->rendParams);
    dlg->rend->rendParams.numThreads = 0; // all threads

    hrFBIResize(hydra_out_render, renderWidth, renderHeight);
    hrFBIResize(hydra_raw_render, renderWidth, renderHeight);

    dlg->rend->DoPostProc(dlg->rend->last_render);
    dlg->rend->displayLastRender();
  }
}

void hydraRender_mk3::ResetPostProc(HydraRenderParamDlg * dlg)
{
  dlg->exposure_slider->SetValue(1, FALSE);
  dlg->compress_slider->SetValue(0, FALSE);
  dlg->contrast_slider->SetValue(0, FALSE);
  dlg->saturation_slider->SetValue(1, FALSE);
  dlg->whiteBalance_slider->SetValue(0, FALSE);
  dlg->whitePoint_selector->SetColor({ 0, 0, 0 });
  dlg->uniformContrast_slider->SetValue(0, FALSE);
  dlg->normalize_slider->SetValue(0, FALSE);
  dlg->chromAberr_slider->SetValue(0, FALSE);
  dlg->vignette_slider->SetValue(0, FALSE);
  dlg->sharpness_slider->SetValue(0, FALSE);
  dlg->sizeStar_slider->SetValue(0, FALSE);
  dlg->numRay_slider->SetValue(8, FALSE);
  dlg->rotateRay_slider->SetValue(10, FALSE);
  dlg->randomAngle_slider->SetValue(0, FALSE);
  dlg->sprayRay_slider->SetValue(0, FALSE);

}
void hydraRender_mk3::DisplaySource(HWND hWnd, HydraRenderParamDlg * dlg)
{
  if (dlg != nullptr && dlg->rend != nullptr && dlg->rend->last_render != nullptr)
  {
    int renderWidth = dlg->rend->rendParams.nMaxx - dlg->rend->rendParams.nMinx;
    int renderHeight = dlg->rend->rendParams.nMaxy - dlg->rend->rendParams.nMiny;

    SetFocus(dlg->rend->last_render->GetWindow());
    SetActiveWindow(dlg->rend->last_render->GetWindow());

    SetFocus(hWnd);
    SetActiveWindow(hWnd);

    dlg->rend->displayLastRender(false);
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void hydraRender_mk3::SetProgTitle(const TCHAR *title)
{
  if (rendParams.progCallback)
    rendParams.progCallback->SetTitle(title);
}

std::wstring& replace(std::wstring& s, const std::wstring& from, const std::wstring& to)
{
  if (!from.empty())
    for (size_t pos = 0; (pos = s.find(from, pos)) != std::string::npos; pos += to.size())
      s.replace(pos, from.size(), to);
  return s;
}

const std::string current_time_and_date()
{
  time_t now = time(0);
  struct tm tstruct;
  char buf[80];
  tstruct = *localtime(&now);
  strftime(buf, sizeof(buf), "__%Y_%m_%d_%H_%M_%S", &tstruct);
  return buf;
}

std::wstring hydraRender_mk3::getSceneName()
{
  std::wstring sceneName = strConverter::c2ws(GetCOREInterface()->GetCurFileName().data());
  std::wstring sceneName2 = replace(sceneName, L".max", L"");

  return sceneName2 + strConverter::ToWideString(current_time_and_date());
}

std::wstring alterSavedName(const std::wstring& a_name, const std::wstring& a_alter)
{
  if (a_name == L"")
    return std::wstring(L"bad_name.hdr");

  std::wstring fname = a_name.substr(0, a_name.size() - 4);
  std::wstring fres  = a_name.substr(a_name.size() - 4, 4);

  return fname + a_alter + L".hdr"; //fres;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void hydraRender_mk3::FindNodesToBake(INode *a_pNode, std::vector<INode*>* a_pBakeList)
{
  INodeBakeProperties* pRes = static_cast<INodeBakeProperties*>(a_pNode->GetInterface(NODE_BAKE_PROPERTIES_INTERFACE));
  if (pRes != nullptr)
  {
    if (pRes->GetNBakeElements() != 0)
      a_pBakeList->push_back(a_pNode);
  }

  int numChildren = a_pNode->NumberOfChildren();

  for (int idx = 0; idx < numChildren; idx++)
    FindNodesToBake(a_pNode->GetChildNode(idx), a_pBakeList);
}

template<typename T>
std::string ToString(const T& a_rhs)
{
  std::stringstream outStr;
  outStr << a_rhs;
  return outStr.str();
}


void hydraRender_mk3::InspectBakeProps(const std::vector<INode*>& a_nodes)
{
  for (size_t i = 0; i < a_nodes.size(); i++)
  {
    INodeBakeProperties* pBakeProps = static_cast<INodeBakeProperties*>(a_nodes[i]->GetInterface(NODE_BAKE_PROPERTIES_INTERFACE));

    if (pBakeProps != nullptr)
    {

      plugin_log.PrintValue("GetNBakeElements()", pBakeProps->GetNBakeElements());

      for (int j = 0; j < pBakeProps->GetNBakeElements(); j++)
      {
        MaxBakeElement* pBakeElem = pBakeProps->GetBakeElement(j);

        plugin_log.Print(std::string("---> GetBakeElement(") + ToString(j) + std::string(") begin"));

        plugin_log.Print(std::string("GetFileType() = ") + strConverter::ws2s(hydraStr(pBakeElem->GetFileType())));
        plugin_log.Print(std::string("GetFileName() = ") + strConverter::ws2s(hydraStr(pBakeElem->GetFileName())));

        int x = 0, y = 0;
        pBakeElem->GetOutputSz(x, y);
        plugin_log.PrintValue("GetOutputSz(x)", x);
        plugin_log.PrintValue("GetOutputSz(y)", x);

        auto pBitmap = pBakeElem->GetRenderBitmap();
        if (pBitmap == nullptr)
          plugin_log.Print("GetRenderBitmap() is NULL");
        else
        {
          plugin_log.Print("GetRenderBitmap() is OK");
          plugin_log.PrintValue("Bitmap->Width()  = ", pBitmap->Width());
          plugin_log.PrintValue("Bitmap->Height() = ", pBitmap->Height());
        }

        plugin_log.Print("custom params begin");

        for (int k = 0; k < pBakeElem->GetNParams(); k++)
          plugin_log.Print(std::string("bakeParam(") + ToString(k) + std::string("); name = ") + strConverter::ws2s(hydraStr(pBakeElem->GetParamName(k))));

        plugin_log.Print("custom params end");

        plugin_log.Print(std::string("---> GetBakeElement(") + ToString(j) + std::string(") begin"));
      }
    }

  }
}

void hydraRender_mk3::exportAndUnwrapBakeNodes(const std::vector<INode*>& a_nodes, TimeValue t)
{
 /* nodesToBakeUnwrapPath.clear();

	for (size_t i = 0; i < a_nodes.size(); i++)
	{
		INodeBakeProperties* pBakeProps = static_cast<INodeBakeProperties*>(a_nodes[i]->GetInterface(NODE_BAKE_PROPERTIES_INTERFACE));

		if (pBakeProps != nullptr)
		{
			int tmp;
			PreProcess(a_nodes[i], tmp, t);
			ExtractMaterialList(&materials, false);

			std::string vsgfFileName = ExtractSingleGeomNode(a_nodes[i], t);
			hydraStr nodeName = a_nodes[i]->GetName();

			for (int j = 0; j < pBakeProps->GetNBakeElements(); j++)
			{
				int x = 0, y = 0;
				MaxBakeElement* pBakeElem = pBakeProps->GetBakeElement(j);
				pBakeElem->GetOutputSz(x, y);

				STARTUPINFOA bakerStartupInfo;
				PROCESS_INFORMATION bakerProcessInfo;

				ZeroMemory(&bakerStartupInfo, sizeof(STARTUPINFOA));
				ZeroMemory(&bakerProcessInfo, sizeof(PROCESS_INFORMATION));

				bakerStartupInfo.cb = sizeof(STARTUPINFO);
				bakerStartupInfo.dwFlags = STARTF_USESHOWWINDOW;
				bakerStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;
				DWORD dwCreationFlags = CREATE_NO_WINDOW;

				std::stringstream ss;
				ss << "C:\\[Hydra]\\bin\\hydraBakerGL.exe" << " " << vsgfFileName << " C:\\[Hydra]\\temp\\" << strConverter::ws2s(nodeName) << " " << x << " " << y;
				plugin_log.PrintValue("baker command line", ss.str());

				CreateProcessA("C:\\[Hydra]\\bin\\hydraBakerGL.exe", (LPSTR)ss.str().c_str(), NULL, NULL, FALSE, dwCreationFlags, NULL, NULL, &bakerStartupInfo, &bakerProcessInfo);

        WaitForSingleObject(bakerProcessInfo.hProcess, 2000);

        std::string unwrapFileName = vsgfFileName.substr(0, vsgfFileName.size() - 4) + "image4f";
        nodesToBakeUnwrapPath.insert(make_pair(a_nodes[i], unwrapFileName));
			}
			
		}
	}*/
}

/*
void hydraRender_mk3::ExportBakeParamsXML(std::ofstream& xmlFile, const std::vector<INode*>& a_nodes, TimeValue t)
{
  xmlFile << std::endl;
  xmlFile << "<library_bake>\n";

  int counter = 0;
  for (size_t i = 0; i < a_nodes.size(); i++)
  {
    INodeBakeProperties* pBakeProps = static_cast<INodeBakeProperties*>(a_nodes[i]->GetInterface(NODE_BAKE_PROPERTIES_INTERFACE));

    if (pBakeProps != nullptr)
    {
      for (int j = 0; j < pBakeProps->GetNBakeElements(); j++)
      {
        MaxBakeElement* pBakeElem = pBakeProps->GetBakeElement(j);
      
        hydraStr bakeFileName = pBakeElem->GetFileName();
        hydraStr bakeFilePath = pBakeElem->GetFileType();

        std::string posNormPath = "posNorm.image4f";
        std::string lightMapType = "unknown";
        float AO_Spread = 1.0f;

        auto keyRange = nodesToBakeUnwrapPath.equal_range(a_nodes[i]);
        if (keyRange.first != nodesToBakeUnwrapPath.end())
          posNormPath = keyRange.first->second; // get file name

        if (bakeFileName.find(TEXT("Ambient Occlusion")) != hydraStr::npos || bakeFileName.find(TEXT("AmbientOcclusion")) != hydraStr::npos)
        {
          lightMapType = "ao";

          /*for (int k = 0; k < pBakeElem->GetNParams(); k++)
          {
            hydraStr name = pBakeElem->GetParamName(k);
          
            if (name == TEXT("Spread"))
              ; // how to gte float from  pBakeElem->GetParamValue(k) ??
          }

        }
        else if (bakeFileName.find(TEXT("CompleteMap")) != hydraStr::npos)
        {
          lightMapType = "all_caustics_yes";
        }
        else if (bakeFileName.find(TEXT("LightingMap")) != hydraStr::npos || bakeFileName.find(TEXT("LightMap")) != hydraStr::npos)
        {
          // extract parameters by name
          //
          BOOL directLightEnabled   = 0;
          BOOL indirectLightEnabled = 0;

          for (int k = 0; k < pBakeElem->GetNParams(); k++)
          {
            hydraStr name = pBakeElem->GetParamName(k);

            if (name == TEXT("Direct Light On"))
              directLightEnabled = pBakeElem->GetParamValue(k);
            
            if (name == TEXT("Indirect Light On"))
              indirectLightEnabled = pBakeElem->GetParamValue(k);
          }

          if (directLightEnabled && !indirectLightEnabled)
            lightMapType = "direct_light";
          else if (!directLightEnabled && indirectLightEnabled)
            lightMapType = "indirect_light";
          else if (directLightEnabled && indirectLightEnabled)
            lightMapType = "all_caustics_no";
        }

#ifdef MAX2012

        std::string filePathUTF8 = bakeFilePath;

        if (filePathUTF8 == "")
          filePathUTF8 = HydraInstallPath() + std::string("rendered_images\\") + bakeFileName;
#else

        std::string filePathUTF8 = wstring_to_utf8(bakeFilePath);

        if (filePathUTF8 == "")
          filePathUTF8 = HydraInstallPath() + std::string("rendered_images\\") + wstring_to_utf8(bakeFileName);

#endif
  
        xmlFile << std::endl;
        xmlFile << "  <bake type = \"" << lightMapType.c_str() << "\" id = \"" << counter << "\" >" << std::endl;
        xmlFile << "    <unwraped_posnorm>  " << posNormPath.c_str()  << " </unwraped_posnorm>" << std::endl;
        xmlFile << "    <out_image>         " << filePathUTF8.c_str() << " </out_image>" << std::endl;
        xmlFile << "  </bake>" << std::endl;

        counter++;
      }
    }
  }

  xmlFile << std::endl;
  xmlFile << "</library_bake>\n";

}
*/

ClassDesc2* GethydraRender_mk3Desc()
{
  return &hydraRender_mk3Desc;
}

std::unordered_map<int, bool> firstTimeDeviceID;
int firstTimeEngineType;
bool rememberDevDlg = false;
bool IsDarkTheme();

void createDeviceSettingsFile(HRRenderRef a_ref)
{
	std::string str = strConverter::ws2s(DEVICE_FILE_PATH);
	if (isFileExist(str.c_str()))
		DeleteFileW(DEVICE_FILE_PATH);

	std::vector<HydraRenderDevice> devList = InitDeviceListInternal(a_ref);

	std::wofstream out;
	out.open(DEVICE_FILE_PATH);
	out << firstTimeEngineType << "\n";
  for (auto it = firstTimeDeviceID.begin(); it != firstTimeDeviceID.end(); ++it)
	{
		out << it->first << "\n";
    out << it->second << "\n";
		for (int j = 0; j < devList.size(); ++j)
		{
			if (devList.at(j).id == it->first)
			{
				out << devList.at(j).name << "\n";
				break;
			}
		}
		
	}
	
	out.flush();
	out.close();
}

void createDeviceSettingsFile(int engineType, std::unordered_map<int, bool> deviceIDs, HRRenderRef a_ref)
{
	std::string str = strConverter::ws2s(DEVICE_FILE_PATH);
	if (isFileExist(str.c_str()))
		DeleteFileW(DEVICE_FILE_PATH);

	if (engineType == -1)
		return;

	std::vector<HydraRenderDevice> devList = InitDeviceListInternal(a_ref);

	std::wofstream out;
	out.open(DEVICE_FILE_PATH);
	out << engineType << "\n";
  for (auto it = deviceIDs.begin(); it != deviceIDs.end(); it++)
  {
		out << it->first << "\n";
    out << it->second << "\n";
		for (int j = 0; j < devList.size(); j++)
		{
			if (devList.at(j).id == it->first)
			{
				out << devList.at(j).name << "\n";
				break;
			}
		}

	}

	out.flush();
	out.close();
}

bool hydraRender_mk3::readAndCheckDeviceSettingsFile(int &mode, std::unordered_map<int, bool> &deviceIDs)
{
	std::wifstream in;

	in.open(DEVICE_FILE_PATH);

	if (!in)
		return false;

	std::wstring tmp;
	std::getline(in, tmp);

  if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
    return false;
	mode = std::stoi(tmp);

	std::vector<HydraRenderDevice> devList = InitDeviceListInternal(hydraRender_mk3::m_rendRef);

	while (!in.eof())
	{
		int id;
    int onOff;
		std::wstring name;
		tmp = L"";

		std::getline(in, tmp);
		if (tmp == L"")
			break;

    if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
      return false;
		id = std::stoi(tmp);

    tmp = L"";
    std::getline(in, tmp);
    if (tmp == L"")
      tmp = L"1";

    if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
      return false;
    onOff = std::stoi(tmp);

		std::getline(in, name);
	
		bool found = false;
		for (int i = 0; i < devList.size(); ++i)
		{
			if (devList.at(i).id == id && devList.at(i).name == name)
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			MessageBoxA(NULL, "One or more previously selected rendering devices could not be found or its name has changed.", "Warning", 0);
			return false;
		}

		deviceIDs[id] = onOff;
	}
	in.close();

	if (deviceIDs.empty())
		return false;

	rendParams.rememberDevice = true;

	return true;
}

INT_PTR CALLBACK SelectDeviceProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

  HWND hwndOwner;
  RECT rc, rcDlg, rcOwner;

  switch (message)
  {
  case WM_INITDIALOG:
  {
    GetCOREInterface()->RegisterDlgWnd(hwndDlg);
    InitCommonControls();

    HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);

    ListView_SetExtendedListViewStyleEx(devices_list, 0, LVS_EX_CHECKBOXES);
    //ListView_SetView(devices_list, LV_VIEW_DETAILS);
    ListView_SetBkColor(devices_list, CLR_NONE);
    ListView_SetTextBkColor(devices_list, CLR_NONE);

    if (IsDarkTheme())
      ListView_SetTextColor(devices_list, RGB(228, 228, 228));
    else
      ListView_SetTextColor(devices_list, RGB(20, 20, 20));

    LVCOLUMN lvc;
    int iCol;
    int nCol = 3;
    lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
    hydraStr cols[] = { _M("ID"), _M("Device Name"), _M("Driver/CC") };

    for (iCol = 0; iCol < nCol; iCol++)
    {
      lvc.iSubItem = iCol;

#ifdef MAX2012
      lvc.pszText = (LPSTR)cols[iCol].c_str();
#else
      lvc.pszText = (LPWSTR)cols[iCol].c_str();
#endif
      lvc.fmt = LVCFMT_LEFT;

      if (iCol < 1) lvc.cx = 40;
      else lvc.cx = 150;

      ListView_InsertColumn(devices_list, iCol, &lvc);
    }

    //int mode = SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
    SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_ADDSTRING, 0, (LPARAM)(_M("CUDA")));
    SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_ADDSTRING, 0, (LPARAM)(_M("OpenCL")));
    SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_SETCURSEL, 1, 1);
    InitDeviceList(devices_list, 1, false, hydraRender_mk3::m_rendRef);

    if ((hwndOwner = GetParent(hwndDlg)) == NULL)
    {
      hwndOwner = GetDesktopWindow();
    }

    GetWindowRect(hwndOwner, &rcOwner);
    GetWindowRect(hwndDlg, &rcDlg);
    CopyRect(&rc, &rcOwner);

    // Offset the owner and dialog box rectangles so that right and bottom 
    // values represent the width and height, and then offset the owner again 
    // to discard space taken up by the dialog box. 

    OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
    OffsetRect(&rc, -rc.left, -rc.top);
    OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

    // The new position is the sum of half the remaining space and the owner's 
    // original position. 

    SetWindowPos(hwndDlg,
      HWND_TOP,
      rcOwner.left + (rc.right / 2),
      rcOwner.top + (rc.bottom / 2),
      0, 0,          // Ignores size arguments. 
      SWP_NOSIZE);

    return TRUE;
  }
  case WM_COMMAND:
  {
    switch (LOWORD(wParam))
    {
    case IDOK:
    {
      HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);
      int numItems = ListView_GetItemCount(devices_list);

      for (auto i = 0; i < numItems; ++i)
      {
        hydraChar bufText[4];
        ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));

        if (ListView_GetCheckState(devices_list, i))
        {
          firstTimeDeviceID[std::stoi(bufText)] = 1;
        }
        else
        {
          firstTimeDeviceID[std::stoi(bufText)] = 0;
        }
      }

			int mode = SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
			firstTimeEngineType = mode;

			if (IsDlgButtonChecked(hwndDlg, IDC_REMEMBER))
			{
				createDeviceSettingsFile(hydraRender_mk3::m_rendRef);
				rememberDevDlg = true;
			}


      EndDialog(hwndDlg, IDOK);
      break;
    }
    case IDC_ENGINE:
    {
      if (HIWORD(wParam) == LBN_SELCHANGE)
      {
        HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);
        int mode = SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
        firstTimeEngineType = mode;
        InitDeviceList(devices_list, mode, false, hydraRender_mk3::m_rendRef);
      }
      break;
    }
    }
    break;
  }
  default:
    return FALSE;
  }
  return TRUE;
}


void hydraRender_mk3::UserSelectDeviceDialog(HWND hwnd)
{
  int ret = DialogBox(hInstance, MAKEINTRESOURCE(IDD_DEVICE_SETUP), hwnd, SelectDeviceProc);

	if (ret == IDOK)
	{
		plugin_log.Print("First time device selection dialog OK");
		rendParams.rememberDevice = rememberDevDlg;
	}
  else if (ret == -1)
    plugin_log.Print("First time device selection dialog failed");


  if (firstTimeDeviceID.empty())
  {
    firstTimeDeviceID[-1] = true;
    firstTimeEngineType = 1;
  }
  else
  {
    m_devList = InitDeviceListInternal(m_rendRef);

    int devId = firstTimeDeviceID[0];
    if (isTargetDevIdAHydraCPU(devId, m_devList))
      firstTimeDeviceID[-1] = true;
  }

  hydraRender_mk3::m_lastRendParams.device_id   = firstTimeDeviceID;
  hydraRender_mk3::m_lastRendParams.engine_type = firstTimeEngineType;
  hydraRender_mk3::m_lastRendParams.liteMode    = false;

  plugin_log.Print("Devices: ");
  for (auto id : hydraRender_mk3::m_lastRendParams.device_id)
    if(id.second) plugin_log.Print(id.first);

}
