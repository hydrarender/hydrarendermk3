#include "hydraRender mk3.h"

#ifdef MAX2016
  #include <Scene/IPhysicalCamera.h>
  using MaxSDK::IPhysicalCamera;
#endif

inline float hordToSectorScale(float fov, float aspect)
{
  float angle        = fov;
  float sectorLength = angle*PI/180.0f;
  float hordLength   = 2.0f*sin(DEG_TO_RAD*angle*0.5f);
  float hordToSector = hordLength/sectorLength;
  float magicScale   = aspect/hordToSector;
  return magicScale*fov;
}


inline float tanScale(float fov, float aspect)
{
  //float x = tan(fov*DEG_TO_RAD);
  //return atan(x*aspect)*RAD_TO_DEG;

  return 2.0f*atan(tan(fov*DEG_TO_RAD / 2.0f)*aspect)*RAD_TO_DEG;
}

HRCameraRef hydraRender_mk3::ExtractCameraFromActiveViewport(INode *vnode, ViewParams* a_viewParams, TimeValue t)
{

  Point3 camUpVector;
  camUpVector = Point3(0.0f, 1.0f, 0.0f); //Oh Gods, purge 3ds max from existence

  Matrix3 posMatrix(1);
  Matrix3 targetMatrix(1);
  Point3 camDirPoint;
  camDirPoint = Point3(0.0f, 0.0f, 0.0f);
  std::wstring camName = L"myCamera";

  float fov = 30.0f;
  float nearClipPlane = 0.01f;
  float farClipPlane = 100.0f;
  //float tdist = 0.0f;
  float zoom = 1.0f;
  bool isTargeted = false;

  Point2 tilt(0.0f, 0.0f);
  Point2 shift(0.0f, 0.0f);

  Matrix3 mConv;
  mConv = getConversionMatrix();

  bool isCamera = false;

  if (vnode != nullptr)
  {
    ObjectState os = vnode->EvalWorldState(t);

    if (os.obj != nullptr && os.obj->SuperClassID() == CAMERA_CLASS_ID)
    {   
      Interval ivalid;
      ivalid.SetInfinite();
      CameraObject* const camera = static_cast<CameraObject*>(os.obj);

      //INode* targetNode = (camera->ClassID().PartA() == LOOKAT_CAM_CLASS_ID) ? vnode->GetTarget() : NULL;
      INode* targetNode = vnode->GetTarget();
      camName = strConverter::c2ws(vnode->GetName());

      Matrix3 debug = vnode->GetNodeTM(t);

      if (targetNode != nullptr)
      {
        targetMatrix = toMeters(targetNode->GetObjectTM(t));
        isTargeted = true;
      }
      else
      {
        camDirPoint = - vnode->GetNodeTM(t).GetRow(2);
      }

      camUpVector = ::Normalize(vnode->GetNodeTM(t).GetRow(1)*mConv);

      if (::Length(camUpVector) < 1e-5f)
        camUpVector = Point3(0.0f, 1.0f, 0.0f);


      posMatrix = toMeters(vnode->GetObjectTM(t));
      fov = camera->GetFOV(t, ivalid)*RAD_TO_DEG; 
      // tdist = toMeters(camera->GetTDist(t, ivalid));

      // export PhysicalCamera parameters if such camera is used
      IPhysicalCamera* const physicalCamera = dynamic_cast<IPhysicalCamera*>(os.obj);
      if (physicalCamera != nullptr)
      {
        tilt = physicalCamera->GetTiltCorrection(t, ivalid);
        shift = physicalCamera->GetFilmPlaneOffset(t, ivalid);
      }
      
      
      isCamera = true;
    }
  }
  else
  {

    ViewExp& viewExp = GetCOREInterface()->GetActiveViewExp();

    camName = L"maxViewPort2";
    fov = viewExp.GetFOV()*RAD_TO_DEG;
    Matrix3 aTM, coordSysTM;
    viewExp.GetAffineTM(aTM);
    coordSysTM = Inverse(aTM);

    camUpVector = ::Normalize(coordSysTM.GetRow(1)*mConv);

    if (::Length(camUpVector) < 1e-5f)
      camUpVector = Point3(0.0f, 1.0f, 0.0f);

    Point3 viewDir, viewPos;
    viewDir = ::Normalize(coordSysTM.GetRow(2));
    viewPos = toMeters(coordSysTM.GetRow(3));
    camDirPoint = -viewDir;
    isTargeted = false;

    posMatrix.SetRow(3, viewPos);
  }

  int renderWidth = rendParams.nMaxx - rendParams.nMinx;
  int renderHeight = rendParams.nMaxy - rendParams.nMiny;

  if (renderWidth != renderHeight) // it seems that when this is happend 3ds max assume spherical screen, not planar
  {
    float aspect = float(renderHeight) / float(renderWidth);
    //fov = hordToSectorScale(cam->fov, aspect); 
    fov = tanScale(fov, aspect);
  }

  HR_OPEN_MODE mode;
  if (m_firstRender)
  {
    mode = HR_WRITE_DISCARD;
    m_camRef = hrCameraCreate(camName.c_str());
  }
  else
  {
    mode = HR_OPEN_EXISTING;
  }

  hrCameraOpen(m_camRef, mode);
  {
    auto camNode = hrCameraParamNode(m_camRef);

    camNode.force_child(L"fov").text().set(fov);
    camNode.force_child(L"nearClipPlane").text().set(nearClipPlane);
    camNode.force_child(L"farClipPlane").text().set(farClipPlane);

    std::wstringstream ss;

    ss << camUpVector.x << " " << camUpVector.y << " " << camUpVector.z;
    camNode.force_child(L"up").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    Point3 maxPos, maxPosT;
    maxPos = Point3(0, 0, 0) * posMatrix;
    maxPosT = Point3(0, 0, 0) * targetMatrix;

    Point3 pos, target, dir;

    pos = maxPos * mConv;
    target = maxPosT * mConv;
    dir = (camDirPoint.Normalize())*mConv;

    if (!isTargeted)
      target = pos + 100.0f*dir;

    ss << pos.x << " " << pos.y << " " << pos.z;
    camNode.force_child(L"position").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    ss << target.x << " " << target.y << " " << target.z;
    camNode.force_child(L"look_at").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    camNode.force_child(L"tiltRotX").text().set(tilt.x);
    camNode.force_child(L"tiltRotY").text().set(tilt.y);
    camNode.force_child(L"tiltShiftX").text().set(shift.x);
    camNode.force_child(L"tiltShiftY").text().set(shift.y);
  }
  hrCameraClose(m_camRef);

//  TraceNodeCustomProperties(L"mycamera", vnode, 1);

  return m_camRef;
}

