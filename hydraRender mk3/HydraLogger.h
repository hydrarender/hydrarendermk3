#ifndef __HYDRALOG__H
#define __HYDRALOG__H

#include <time.h>
#include <fstream>
#include <string>



class HydraLogger
{
  public:

    HydraLogger() : isMtlRender(false) {}
    ~HydraLogger(){ outp.close(); outm.close(); }
 
    void OpenLogFile(std::string file)
    {
      if (outp.is_open())
        return;

      outp.open(file.c_str()); // std::ios::app

      time_t rawtime;
      struct tm * timeinfo;

      time (&rawtime);
      timeinfo = localtime (&rawtime);

      outp << asctime (timeinfo) << "||----------------------" << std::endl;
    }

    void OpenLogFileM(std::string file)
    {
      if (outm.is_open())
        return;

      outm.open(file.c_str()); // std::ios::app

      time_t rawtime;
      struct tm * timeinfo;

      time(&rawtime);
      timeinfo = localtime(&rawtime);

      outm << asctime(timeinfo) << "||----------------------" << std::endl;
    }
    
    bool isMtlRender;

    template<class T> void Print (const T& x) 
    {
      if (isMtlRender)
        outm << x << std::endl;
      else
        outp << x << std::endl; 
    }

    template<class T> void PrintValue (std::string varName, const T& x) 
    { 
      if (isMtlRender)
        outm << varName << " : " << x << std::endl;
      else
        outp << varName << " : " << x << std::endl; 
    }

    std::ofstream& Stream() 
    {
      if (isMtlRender)
        return outm;
      else
        return outp; 
    }

		void CloseLog()
		{
			outp.close();
      outm.close();
		}

    void flush() { outp.flush(); outm.flush(); }

protected:

  std::ofstream outp;
  std::ofstream outm;

};

#endif