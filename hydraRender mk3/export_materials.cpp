#include "hydraRender mk3.h"

#include <unordered_map>
#include <string>
#include <sstream>

#include <sys/stat.h>
#include <time.h>
#include <sstream>
#include <thread>
#include <codecvt>
#include <mutex>
#include <icurvctl.h>

#include "gamma.h"


std::string escape(const std::string& data)
{
  std::string buffer;
  buffer.reserve(data.size() + 10);

  for (size_t pos = 0; pos != data.size(); ++pos) 
  {
    switch (data[pos]) {
    case '&':  buffer.append("&amp;");       break;
    case '\"': buffer.append("&quot;");      break;
    case '\'': buffer.append("&apos;");      break;
    case '<':  buffer.append("&lt;");        break;
    case '>':  buffer.append("&gt;");        break;
    default:   buffer.append(&data[pos], 1); break;
    }
  }
 
  return buffer;
}

// convert UTF - 8 string to wstring
std::wstring utf8_to_wstring(const std::string& str)
{
  std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
  return myconv.from_bytes(str);
}

// convert wstring to UTF-8 string
std::string wstring_to_utf8(const std::wstring& str)
{
  std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;

  return escape(myconv.to_bytes(str));
}



inline bool file_exists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}


void evalComponentColorFaloff(const float mat_color[3], const Color falloff_1, const Color falloff_2, float mult, bool multOn, 
    Color *f1, Color *f2)
{
  if (multOn)
  {
    f1->r = mat_color[0] * mult * falloff_1.r;
    f1->g = mat_color[1] * mult * falloff_1.g;
    f1->b = mat_color[2] * mult * falloff_1.b;

    f2->r = mat_color[0] * mult * falloff_2.r;
    f2->g = mat_color[1] * mult * falloff_2.g;
    f2->b = mat_color[2] * mult * falloff_2.b;
  }
  else
  {
    f1->r = mult * falloff_1.r;
    f1->g = mult * falloff_1.g;
    f1->b = mult * falloff_1.b;

    f2->r = mult * falloff_2.r;
    f2->g = mult * falloff_2.g;
    f2->b = mult * falloff_2.b;
  }

}

Color evalComponentColorAPI(Color color, float mult, bool multOn, bool hasTexture)
{
  Color result(0.0f, 0.0f, 0.0f);
  if (hasTexture)
  {
    if (multOn)
    {
      result.r = color.r * mult;
      result.g = color.g * mult;
      result.b = color.b * mult;
    }
    else
    {
      result.r = mult;
      result.g = mult;
      result.b = mult;
    }
  }
  else
  {
    result.r = color.r * mult;
    result.g = color.g * mult;
    result.b = color.b * mult;
  }
  return result;
}


bool hydraRender_mk3::HydraMtlGetChangedTexturesSlotNames(const std::unordered_set<std::wstring> &changedParams, std::vector<std::wstring> &slotNames) const
{
  if (changedParams.empty())
    return false;

  if ((changedParams.find(L"mtl_diffuse_map") != changedParams.end()))
    slotNames.push_back(L"DiffuseMap");

  if ((changedParams.find(L"mtl_reflect_map") != changedParams.end()))
    slotNames.push_back(L"ReflectivityMap");

  if ((changedParams.find(L"mtl_emission_map") != changedParams.end()))
    slotNames.push_back(L"EmissionMap");

  if ((changedParams.find(L"mtl_transparency_map") != changedParams.end()))
    slotNames.push_back(L"TransparencyMap");

  if ((changedParams.find(L"mtl_normal_map") != changedParams.end()))
    slotNames.push_back(L"NormalMap");

  if ((changedParams.find(L"mtl_refl_gl_map") != changedParams.end()))
    slotNames.push_back(L"ReflectGlossinessMap");

  if ((changedParams.find(L"mtl_transp_gl_map") != changedParams.end()))
    slotNames.push_back(L"TranspGlossinessMap");

  if ((changedParams.find(L"mtl_opacity_map") != changedParams.end()))
    slotNames.push_back(L"OpacityMap");

  if ((changedParams.find(L"mtl_translucency_map") != changedParams.end()))
    slotNames.push_back(L"TranslucencyMap");

  return true;

}

void hydraRender_mk3::HydraMtlGetAllTexturesSlotNames(std::vector<std::wstring> &slotNames) const
{
  std::vector<std::wstring> names = { L"DiffuseMap", L"SpecularMap", L"ReflectivityMap", L"EmissionMap",
    L"TransparencyMap", L"NormalMap", L"SpecularGlossinessMap",
    L"ReflectGlossinessMap", L"TranspGlossinessMap", L"OpacityMap",
    L"TranslucencyMap" };
  slotNames = names;
}


void hydraRender_mk3::ExtractMaterialRecursive(Mtl *mtl, bool forMatEditor, std::unordered_set<std::wstring> changedParams)
{
  hydraStr mtlName = mtl->GetName();
  int subMats = mtl->NumSubMtls();

  if (subMats == 0)
  {
    ExtractMaterial(mtl, changedParams);

    plugin_log.PrintValue("ExtractMaterial : ", strConverter::ws2s(mtlName));
  }
  else
  {
    auto classID = mtl->ClassID().PartA();
    if (classID == MULTI_CLASS_ID)
    {
      for (int j = 0; j < subMats; j++)
      {
        Mtl* subMtl = mtl->GetSubMtl(j);
        if (subMtl)
        {
          mtlTracker->AddMtl(subMtl);
        }
      }
    }
    else
    {
      std::vector <HRMaterialRef> subMatRefs;
      for (int j = 0; j < subMats; j++)
      {
        Mtl* subMtl = mtl->GetSubMtl(j);
        if (subMtl)
        {
          ExtractMaterialRecursive(subMtl, forMatEditor, changedParams);
          subMatRefs.insert(subMatRefs.begin(), materialsRefLib[subMtl]);
        }
        else
          subMatRefs.insert(subMatRefs.begin(), HRMaterialRef());
      }

      if (mtl->ClassID().PartA() == MIXMAT_CLASS_ID) //standard Blend material
      {
        ExtractBlendMaterial(mtl, subMatRefs, forMatEditor);
      }
    }
  }

}


void hydraRender_mk3::ExtractMaterialList(bool forMatEditor)
{
  for (auto mat : (*mtlTracker).materials)
  {
    if (mat.second.first == true) //material changed and needs export
    {
      ExtractMaterialRecursive(mat.first, forMatEditor, mat.second.second);
      (*mtlTracker).materials[mat.first].second.clear(); //clear "changed parameters" map after export

      auto classID = mat.first->ClassID().PartA();
      if (classID != MIXMAT_CLASS_ID /*&& classID != MULTI_CLASS_ID*/) //have to export blend every time since it does not track some parameters
        (*mtlTracker).materials[mat.first].first = false;
    }
  }
/*
  if ((*mtlTracker).materials.size() == 0)
  {
    UnsupportedMaterial();
  }*/
}


void hydraRender_mk3::UnsupportedMaterial()
{
  HRMaterialRef mat = hrMaterialCreate(L"null_plugin_export_material");
/*  HR_OPEN_MODE mode;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  */

  hrMaterialOpen(mat, HR_WRITE_DISCARD);
  {
    pugi::xml_node matNode = hrMaterialParamNode(mat);

    pugi::xml_node diff = matNode.force_child(L"diffuse");

    diff.force_attribute(L"brdf_type").set_value(L"lambert");
    diff.force_child(L"color").text().set(L"0.078 0.0 0.156");

  }
  hrMaterialClose(mat);
}

void hydraRender_mk3::ExtractPlaceholderMaterial()
{
  placeholderMatRef = hrMaterialCreate(L"hydra_placeholder_material");

  hrMaterialOpen(placeholderMatRef, HR_WRITE_DISCARD);
  {
    pugi::xml_node matNode = hrMaterialParamNode(placeholderMatRef);

    pugi::xml_node diff = matNode.force_child(L"diffuse");

    diff.force_attribute(L"brdf_type").set_value(L"lambert");
    diff.force_child(L"color").text().set(L"0.078 0.0 0.156");
  }
  hrMaterialClose(placeholderMatRef);
}

void hydraRender_mk3::ExtractSampler(pugi::xml_node &texNode, const texInfo &info)
{

  TimeValue t = GetStaticFrame();

  float samplerMatrix[16];
  int texTilingFlags = 0;

  auto texRef   = std::get<0>(info);
  auto uvgen    = std::get<1>(info);
  auto gamma    = std::get<2>(info);
  auto alphaSrc = std::get<3>(info);

  if (uvgen != nullptr)
  {
    ExtractTextureMatrixFromUVGen(uvgen, samplerMatrix, &texTilingFlags);

    texNode.force_attribute(L"matrix");

    if (texTilingFlags & U_WRAP)
      texNode.force_attribute(L"addressing_mode_u").set_value(L"wrap");
    else if (texTilingFlags & U_MIRROR)
      texNode.force_attribute(L"addressing_mode_u").set_value(L"mirror");
    else
      texNode.force_attribute(L"addressing_mode_u").set_value(L"clamp");

    if (texTilingFlags & V_WRAP)
      texNode.force_attribute(L"addressing_mode_v").set_value(L"wrap");
    else if (texTilingFlags & V_MIRROR)
      texNode.force_attribute(L"addressing_mode_v").set_value(L"mirror");
    else
      texNode.force_attribute(L"addressing_mode_v").set_value(L"clamp");

    HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
  }
  else //write some default sampler attributes if there's no uvGen
  {
    texNode.force_attribute(L"matrix");
    float samplerMatrix[16] = { 1, 0, 0, 0,
                                0, 1, 0, 0,
                                0, 0, 1, 0,
                                0, 0, 0, 1 };
    texNode.force_attribute(L"addressing_mode_u").set_value(L"clamp");
    texNode.force_attribute(L"addressing_mode_v").set_value(L"clamp");

    HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", samplerMatrix);
  }

  texNode.force_attribute(L"input_gamma").set_value(gamma);
  texNode.force_attribute(L"input_alpha").set_value(alphaSrc.c_str());
}


/////////////////////////////
void hydraRender_mk3::ExtractHydraDiffuse(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::wstring diffuseSlotName = L"DiffuseMap";
  bool hasColorTexture = false;

  Color color     = FindColor(L"DiffuseColor", mtl);
  float mult      = FindFloat(L"DiffuseColorMultiplier", mtl);
  bool  mult_on   = FindBool(L"DiffuseColorMultiplier", mtl);
  float roughness = FindFloat(L"DiffuseRoughness", mtl);

  auto diff = mat.force_child(L"diffuse");

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, mult_on, hasColorTexture);

  if (roughness == 0.0f)
    diff.force_attribute(L"brdf_type").set_value(L"lambert");
  else
    diff.force_attribute(L"brdf_type").set_value(L"orennayar");

  auto colorNode = diff.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[diffuseSlotName]);

    auto texNode = hrTextureBind(texRef, colorNode);
    ExtractSampler(texNode, texturesSlotToRef[diffuseSlotName]);
  }
  else if(colorNode.child(L"texture") != nullptr)
  {
    colorNode.remove_child(L"texture");
  }
  
  diff.force_child(L"roughness").force_attribute(L"val").set_value(roughness);

  if(mult_on)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");

}

void hydraRender_mk3::ExtractHydraReflect(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::wstring reflSlotName = L"ReflectivityMap";
  std::wstring reflectGlossName = L"ReflectGlossinessMap";
  bool hasColorTexture = false;

  int reflect_brdf = FindInt(L"ReflectivityBRDF", mtl);;
  std::string refl_brdf_type = "";

  Color color         = FindColor(L"ReflectivityColor", mtl);
  bool  mult_on       = FindBool(L"ReflectColorMultiplier", mtl);
  float mult          = FindFloat(L"ReflectColorMultiplier", mtl);
  float glossiness    = FindFloat(L"ReflectGlossiness", mtl);
  float ior           = FindFloat(L"ReflectivityIOR", mtl);
  bool  fresnel       = FindBool(L"ReflectFresnelOn", mtl);


  auto refl = mat.force_child(L"reflectivity");

  switch (reflect_brdf)
  {
  case 0:
    refl.force_attribute(L"brdf_type").set_value(L"phong");
    break;
  case 1:
    refl.force_attribute(L"brdf_type").set_value(L"torranse_sparrow");
    break;
  case 2:
    refl.force_attribute(L"brdf_type").set_value(L"cook-torrance");
    break;
  case 3:
    refl.force_attribute(L"brdf_type").set_value(L"fresnel_dielectric");
    break;
  case 4:
    refl.force_attribute(L"brdf_type").set_value(L"fresnel_conductor");
    break;
  }

  std::string extrusion = "";
  switch (FindInt(L"ReflectExtrusion", mtl))
  {
  case 0:
    refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"maxcolor");
    break;
  case 1:
    refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"luminance");
    break;
  }

  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, mult_on, hasColorTexture);

  refl.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (mult_on)
    refl.child(L"color").force_attribute(L"tex_apply_mode").set_value(L"multiply");


  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflSlotName]);
    
    auto texNode = hrTextureBind(texRef, refl.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[reflSlotName]);
  }
  else if (refl.child(L"color").child(L"texture") != nullptr)
  {
    refl.child(L"color").remove_child(L"texture");
  }

  refl.force_child(L"glossiness").force_attribute(L"val").set_value(glossiness);

  if (texturesSlotToRef.find(reflectGlossName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflectGlossName]);
    
    auto texNode = hrTextureBind(texRef, refl.child(L"glossiness"));
    ExtractSampler(texNode, texturesSlotToRef[reflectGlossName]);
  }
  else if (refl.child(L"glossiness").child(L"texture") != nullptr)
  {
    refl.child(L"glossiness").remove_child(L"texture");
  }

  refl.force_child(L"fresnel").force_attribute(L"val").set_value(int(fresnel));
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(ior);
}

void hydraRender_mk3::ExtractHydraTransparency(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::wstring transpSlotName = L"TransparencyMap";
  std::wstring opacitySlotName = L"OpacityMap";
  std::wstring transpGlossSlotName = L"TranspGlossinessMap";
  bool hasColorTexture = false;


  Color color          = FindColor(L"TransparencyColor", mtl);
  Color fog_color      = FindColor(L"FogColor", mtl);
  Color exit_color     = FindColor(L"ExitColor", mtl);
  float mult           = FindFloat(L"TransparencyColorMultiplier", mtl);
  bool  mult_on        = FindBool(L"TransparencyColorMultiplier", mtl);
  float glossiness     = FindFloat(L"TranspGlossiness", mtl);
  int   thin_surface   = int(FindBool(L"ThinTransparency", mtl));
  float ior            = FindFloat(L"IOR", mtl);
  float fog_mult       = FindFloat(L"FogMultiplier", mtl);
  bool  shadow_matte   = FindBool(L"ShadowMatte", mtl);
  

  auto transp = mat.force_child(L"transparency");

  if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, mult_on, hasColorTexture);
 
  transp.force_attribute(L"brdf_type").set_value(L"phong");

  auto colorNode = transp.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if(mult_on)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");

  if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[transpSlotName]);
    
    auto texNode = hrTextureBind(texRef, transp.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[transpSlotName]);
  }
  else if (colorNode.child(L"texture") != nullptr)
  {
    colorNode.remove_child(L"texture");
  }
  
  transp.force_child(L"glossiness").force_attribute(L"val").set_value(glossiness);

  if(texturesSlotToRef.find(transpGlossSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[transpGlossSlotName]);
    
    auto texNode = hrTextureBind(texRef, transp.child(L"glossiness"));
    ExtractSampler(texNode, texturesSlotToRef[transpGlossSlotName]);
  }
  else if (transp.child(L"glossiness").child(L"texture") != nullptr)
  {
    transp.child(L"glossiness").remove_child(L"texture");
  }

  transp.force_child(L"thin_walled").force_attribute(L"val").set_value(thin_surface);
  transp.force_child(L"fog_color").force_attribute(L"val").set_value(strConverter::colorToWideString(fog_color).c_str());
  transp.force_child(L"fog_multiplier").force_attribute(L"val").set_value(fog_mult);
  transp.force_child(L"ior").force_attribute(L"val").set_value(ior);

}

void hydraRender_mk3::ExtractHydraOpacity(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::wstring opacitySlotName = L"OpacityMap";
  bool no_shadows = !FindBool(L"AffectShadows", mtl);
  bool smooth     =  FindBool(L"OpacitySmooth", mtl);

  auto opacity = mat.force_child(L"opacity");

  opacity.force_child(L"skip_shadow").force_attribute(L"val").set_value(int(no_shadows));
  opacity.force_attribute(L"smooth").set_value(int(smooth));

  if (texturesSlotToRef.find(opacitySlotName) != texturesSlotToRef.end())
  {
    auto texRef  = std::get<0>(texturesSlotToRef[opacitySlotName]);
    auto texNode = hrTextureBind(texRef, opacity);

    ExtractSampler(texNode, texturesSlotToRef[opacitySlotName]);
  }
  else if (mat.child(L"opacity") != nullptr && mat.child(L"opacity").child(L"texture") != nullptr)
  {
    mat.child(L"opacity").remove_child(L"texture");
  }
}

void hydraRender_mk3::ExtractHydraEmission(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  auto emission = mat.force_child(L"emission");

  Color color     = FindColor(L"EmissionColor", mtl);
  float mult      = FindFloat(L"EmissionColorMultiplier", mtl);
  bool  mult_on   = FindBool(L"EmissionColorMultiplier", mtl);
  bool  cast_gi   = FindBool(L"EmissionGI", mtl);
  std::wstring emissionSlotName = L"EmissionMap";
  bool hasColorTexture = false;

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }


  emission.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(color).c_str());

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[emissionSlotName]);
    
    auto texNode = hrTextureBind(texRef, emission.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[emissionSlotName]);
  }
  else if (emission.child(L"color").child(L"texture") != nullptr)
  {
    emission.child(L"color").remove_child(L"texture");
  }

  emission.force_child(L"cast_gi").force_attribute(L"val").set_value(int(cast_gi));
  emission.force_child(L"multiplier").force_attribute(L"val").set_value(mult);
}

void hydraRender_mk3::ExtractHydraTranslucency(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo>  texturesSlotToRef)
{
  auto translucency = mat.force_child(L"translucency");

  Color color   = FindColor(L"TranslucencyColor", mtl);
  float mult    = FindFloat(L"TranslucencyColorMultiplier", mtl);
  bool  mult_on = FindBool(L"TranslucencyColorMultOn", mtl);
  std::wstring translucencySlotName = L"TranslucencyMap";
  bool hasColorTexture = false;

  if (texturesSlotToRef.find(translucencySlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, mult_on, hasColorTexture);

  translucency.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(translucencySlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[translucencySlotName]);
    
    auto texNode = hrTextureBind(texRef, translucency.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[translucencySlotName]);
  }
  else if (translucency.child(L"color").child(L"texture") != nullptr)
  {
    translucency.child(L"color").remove_child(L"texture");
  }

  translucency.force_child(L"multiplier").force_attribute(L"val").set_value(mult);
}

void hydraRender_mk3::ExtractHydraDisplacement(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo>  texturesSlotToRef)
{
  std::wstring displSlotName = L"NormalMap";
  if (texturesSlotToRef.find(displSlotName) != texturesSlotToRef.end())
  {
    bool isNormalMap = false;
    bool flipRed = false;
    bool flipGreen = false;
    bool swap = false;

    ExtractNormalBump(mtl, isNormalMap, flipRed, flipGreen, swap);

    auto displace = mat.force_child(L"displacement");

    auto texRef = std::get<0>(texturesSlotToRef[displSlotName]);
    
    if(isNormalMap)
    {
      auto heightNode = displace.force_child(L"normal_map");
      displace.force_attribute(L"type").set_value(L"normal_bump");
      auto invert = heightNode.force_child(L"invert");
      invert.force_attribute(L"x").set_value(int(flipRed));
      invert.force_attribute(L"y").set_value(int(!flipGreen));
      invert.force_attribute(L"swap_xy").set_value(int(swap));

      auto texNode = hrTextureBind(texRef, displace.child(L"normal_map"));
      ExtractSampler(texNode, texturesSlotToRef[displSlotName]);
    }
    else
    {
      float amount = FindFloat(L"BumpAmount", mtl);
      float radius = FindFloat(L"BumpRadius", mtl);
      //float sigma = FindFloat(L"BumpSigma", mtl);
      //float height = toMeters(FindFloat(L"Height", mtl));

      auto heightNode = displace.force_child(L"height_map");
      displace.force_attribute(L"type").set_value(L"height_bump");
      heightNode.force_attribute(L"amount").set_value(amount);
      heightNode.force_attribute(L"smooth_lvl").set_value(radius);

      auto texNode = hrTextureBind(texRef, displace.child(L"height_map"));
      ExtractSampler(texNode, texturesSlotToRef[displSlotName]);
    }
  }
  else if(mat.child(L"displacement") != nullptr)
  {
    mat.remove_child(L"displacement");
  }
}

void hydraRender_mk3::ExtractHydraMtlCatcher(Mtl * mtl, std::unordered_set<std::wstring> changedParams)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  std::vector<std::wstring> hydraMapNames;

  /* if (mode == HR_OPEN_EXISTING)
  {
  HydraMtlGetChangedTexturesSlotNames(changedParams, hydraMapNames); //only export textures that changed since last export
  }
  else*/
  {
    HydraMtlGetAllTexturesSlotNames(hydraMapNames);
  }

  auto texturesSlotToRef = ExtractHydraTextures(mtl, hydraMapNames);

  hrMaterialOpen(mat, mode);
  {
    auto matNode = hrMaterialParamNode(mat);
    matNode.attribute(L"type").set_value(L"shadow_catcher");

    ExtractHydraMtlCatcherCameraMap(mtl, matNode, texturesSlotToRef);
    ExtractHydraMtlCatcherOpacity(mtl, matNode, texturesSlotToRef);
    ExtractHydraMtlCatcherAO(mtl, matNode, texturesSlotToRef);
    ExtractHydraMtlCatcherReflect(mtl, matNode, texturesSlotToRef);
  }
  hrMaterialClose(mat);
}

void hydraRender_mk3::ExtractHydraMtlCatcherCameraMap(Mtl * mtl, pugi::xml_node & mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{}

void hydraRender_mk3::ExtractHydraMtlCatcherOpacity(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::wstring opacitySlotName = L"OpacityMap";
  bool no_shadows = true; // !FindBool(L"AffectShadows", mtl);
  bool smooth = true;

  if (texturesSlotToRef.find(opacitySlotName) != texturesSlotToRef.end())
  {
    auto opacity = mat.force_child(L"opacity");

    opacity.force_child(L"skip_shadow").force_attribute(L"val").set_value(int(no_shadows));
    opacity.force_attribute(L"smooth").set_value(int(smooth));

    auto texRef = std::get<0>(texturesSlotToRef[opacitySlotName]);
    
    auto texNode = hrTextureBind(texRef, opacity);
    ExtractSampler(texNode, texturesSlotToRef[opacitySlotName]);
  }
  else if (mat.child(L"opacity") != nullptr && mat.child(L"opacity").child(L"texture") != nullptr)
  {
    mat.child(L"opacity").remove_child(L"texture");
  }
}

void hydraRender_mk3::ExtractHydraMtlCatcherAO(Mtl * mtl, pugi::xml_node & mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{}

void hydraRender_mk3::ExtractHydraMtlCatcherReflect(Mtl * mtl, pugi::xml_node & mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  std::string refl_brdf_type = "";

  Color color = FindColor(L"ReflectivityColor", mtl);
  float mult = FindFloat(L"ReflectColorMultiplier", mtl);
  float glossiness = FindFloat(L"ReflectGlossiness", mtl);
  float ior = FindFloat(L"ReflectivityIOR", mtl);
  bool  fresnel = FindBool(L"ReflectFresnelOn", mtl);

  auto refl = mat.force_child(L"reflectivity");
  refl.force_attribute(L"brdf_type").set_value(L"phong");
  refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"luminance");

  Color res_color = evalComponentColorAPI(color, mult, 1, 0);

  refl.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());
  refl.child(L"color").force_attribute(L"tex_apply_mode").set_value(L"multiply");
  refl.force_child(L"glossiness").force_attribute(L"val").set_value(glossiness);
  refl.force_child(L"fresnel").force_attribute(L"val").set_value(int(fresnel));
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(ior);
}

void hydraRender_mk3::ExtractHydraMtlLight(Mtl * mtl, std::unordered_set<std::wstring> changedParams)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  HRLightRef meshLightRef = hrLightCreate(L"my_area_light");

  std::vector<std::wstring> hydraMapNames;

  /* if (mode == HR_OPEN_EXISTING)
  {
  HydraMtlGetChangedTexturesSlotNames(changedParams, hydraMapNames); //only export textures that changed since last export
  }
  else*/
  {
    HydraMtlGetAllTexturesSlotNames(hydraMapNames);
  }

  auto texturesSlotToRef = ExtractHydraTextures(mtl, hydraMapNames);

  hrMaterialOpen(mat, mode);
  {
    //auto emission = mat.force_child(L"emission");
    auto lightNode = hrLightParamNode(meshLightRef);


    Color color = FindColor(L"Color", mtl);
    float mult = FindFloat(L"ColorMultiplier", mtl);
    bool  mult_on = FindBool(L"ColorTint", mtl);
    std::wstring colorMapSlotName = L"ColorMap";
    bool hasColorTexture = false;

    if (texturesSlotToRef.find(colorMapSlotName) != texturesSlotToRef.end())
      hasColorTexture = true;


    Color res_color = evalComponentColorAPI(color, mult, mult_on, hasColorTexture);

    lightNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());


    if (texturesSlotToRef.find(colorMapSlotName) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[colorMapSlotName]);
      auto texNode = hrTextureBind(texRef, lightNode.child(L"color"));
      ExtractSampler(texNode, texturesSlotToRef[colorMapSlotName]);
    }
    else if (lightNode.child(L"color").child(L"texture") != nullptr)
      lightNode.child(L"color").remove_child(L"texture");


    lightNode.force_child(L"multiplier").force_attribute(L"val").set_value(mult);
    lightNode.force_child(L"type").set_value(L"area");
    lightNode.force_child(L"shape").set_value(L"mesh");
    lightNode.force_child(L"distribution").set_value(L"diffuse");
    lightNode.force_child(L"distribution").set_value(L"diffuse");
    //lightNode.force_child(L"mesh").force_attribute(L"id").set_value(mesh.id);

    //lightNode.append_child(L"mesh").append_attribute(L"id") = torusRef.id;
  }
  hrMaterialClose(mat);
}


void hydraRender_mk3::ExtractHydraMtlLayers(Mtl * mtl, std::unordered_set<std::wstring> changedParams)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  std::vector<std::wstring> hydraMapNames;

  HydraMtlGetAllTexturesSlotNames(hydraMapNames);

  auto texturesSlotToRef = ExtractHydraTextures(mtl, hydraMapNames);

  hrMaterialOpen(mat, mode);
  {
    //auto matNode = hrMaterialParamNode(mat);
    //
    //ExtractHydraEmission(mtl, matNode, texturesSlotToRef);
    //ExtractHydraDiffuse(mtl, matNode, texturesSlotToRef);
    //ExtractHydraReflect(mtl, matNode, texturesSlotToRef);
    //ExtractHydraTransparency(mtl, matNode, texturesSlotToRef);
    //ExtractHydraOpacity(mtl, matNode, texturesSlotToRef);
    //ExtractHydraTranslucency(mtl, matNode, texturesSlotToRef);
    //ExtractHydraDisplacement(mtl, matNode, texturesSlotToRef);

  }
  hrMaterialClose(mat);
}






HRMaterialRef hydraRender_mk3::GetHRMatRefFromMtl(Mtl* mtl, HR_OPEN_MODE &mode)
{
  HRMaterialRef mat;
  auto findMatRef = materialsRefLib.find(mtl);

  if (findMatRef == materialsRefLib.end())
  {
    mat = hrMaterialCreate(strConverter::c2ws(mtl->GetName().data()).c_str());
    materialsRefLib[mtl] = mat;
    largestMaterialId = mat.id;
  }
  else
  {
    mat = findMatRef->second;
    mode = HR_OPEN_EXISTING;
  }

  return mat;
}

void hydraRender_mk3::ExtractBlendMaterial(Mtl* mtl, std::vector<HRMaterialRef> &subMatRefs, bool forMatEditor)
{
  /*
  MixAmount		: float   = 1
  Lower		: float   = 0
  Upper		: float   = 1
  UseCurve		: boolean = 1
  Interactive		: integer = 0
  Map 1 : unknown
  Map 2 : unknown
  Mask		: some texmap
  Map 1 Enable		: boolean = 1
  Map 2 Enable		: boolean = 1
  MaskEnable		: boolean = 1
  */

  Texmap* mask = FindTex(L"Mask", mtl);

  bool mask_on = FindBool(L"MaskEnable", mtl);
  bool use_curve = FindBool(L"UseCurve", mtl);
  float mask_mult = FindFloat(L"MixAmount", mtl);
  bool texSupported = false;

  HR_OPEN_MODE mode = HR_WRITE_DISCARD;

  HRMaterialRef matBlend;
  auto findMatRef = materialsRefLib.find(mtl);

 // auto placeholderMat = materialsRefLib.find(mtlList.GetMtl(0))->second;
  
  if (findMatRef == materialsRefLib.end())
  {
    HRMaterialRef mat1, mat2;
    if (FindBool(L"Map 1 Enable", mtl))
    {
      mat1 = subMatRefs.at(0);
    }
    else
    {
      mat1 = placeholderMatRef;
    }

    if (FindBool(L"Map 2 Enable", mtl))
    {
      mat2 = subMatRefs.at(1);
    }
    else
    {
      mat2 = placeholderMatRef;
    }

    matBlend = hrMaterialCreateBlend(strConverter::c2ws(mtl->GetName().data()).c_str(), mat1, mat2);
    materialsRefLib[mtl] = matBlend;
    largestMaterialId = matBlend.id;
  }
  else
  {
    matBlend = findMatRef->second;
    mode = HR_OPEN_EXISTING;
  }

  const std::vector<std::wstring> blendMapNames = { L"Mask" };

  auto texturesSlotToRef = ExtractHydraTextures(mtl, blendMapNames);

  if (!texturesSlotToRef.empty())
  {
    auto texRef = std::get<0>(texturesSlotToRef.at(L"Mask"));
  
    if(!texturesSlotToRef.empty() && texRef.id != -1)
      texSupported = true;
  }

  hrMaterialOpen(matBlend, mode);
  {
    auto matNode = hrMaterialParamNode(matBlend);

    auto blend = matNode.force_child(L"blend");
    if (use_curve)
      blend.force_attribute(L"type").set_value(L"sigmoid_blend");
    else
      blend.force_attribute(L"type").set_value(L"mask_blend");

    auto mask_node = blend.force_child(L"mask");
    if (texSupported && mask_on)
    {
      if (!texturesSlotToRef.empty())
      {
        auto texRef = std::get<0>(texturesSlotToRef.at(L"Mask"));
        auto texNode = hrTextureBind(texRef, mask_node);
        ExtractSampler(texNode, texturesSlotToRef.at(L"Mask"));
      }
      mask_mult = 1.0f;
    }
    else if (mask_node.child(L"texture") != nullptr)
    {
      mask_node.remove_child(L"texture");
    }

    mask_node.force_attribute(L"val").set_value(mask_mult);

    if (use_curve)
    {
      float upper = FindFloat(L"Upper", mtl);
      float lower = FindFloat(L"Lower", mtl);
      blend.force_child(L"exponent").force_attribute(L"val").set_value(0.75f / (upper - lower));
    }
  }
  hrMaterialClose(matBlend);

}

void hydraRender_mk3::ExtractHydraMaterial(Mtl* mtl, std::unordered_set<std::wstring> changedParams)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  std::vector<std::wstring> hydraMapNames;

 /* if (mode == HR_OPEN_EXISTING) 
  {
    HydraMtlGetChangedTexturesSlotNames(changedParams, hydraMapNames); //only export textures that changed since last export
  }
  else*/
  {
    HydraMtlGetAllTexturesSlotNames(hydraMapNames);
  }

  auto texturesSlotToRef = ExtractHydraTextures(mtl, hydraMapNames);

  hrMaterialOpen(mat, mode);
  {
    auto matNode = hrMaterialParamNode(mat);

    ExtractHydraEmission(mtl, matNode, texturesSlotToRef);
    ExtractHydraDiffuse(mtl, matNode, texturesSlotToRef);
    ExtractHydraReflect(mtl, matNode, texturesSlotToRef);
    ExtractHydraTransparency(mtl, matNode, texturesSlotToRef);
    ExtractHydraOpacity(mtl, matNode, texturesSlotToRef);
    ExtractHydraTranslucency(mtl, matNode, texturesSlotToRef);
    ExtractHydraDisplacement(mtl, matNode, texturesSlotToRef);

  }
  hrMaterialClose(mat);
}

void hydraRender_mk3::ExtractStdMaterial(Mtl* mtl)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));

  const std::vector<std::wstring> stdMapNames = { L"Self-Illumination", L"Diffuse Color", L"Specular Color", L"Glossiness",
                                                  L"Opacity", L"Filter Color", L"Bump", L"Reflection", L"Refraction",
                                                  L"Displacement" };

  auto texturesSlotToRef = ExtractHydraTextures(mtl, stdMapNames);

  StdMat2* std = static_cast<StdMat2*>(mtl);
  TimeValue t = GetStaticFrame();

  hrMaterialOpen(mat, mode);
  {
    auto matNode = hrMaterialParamNode(mat);

    ExtractStdMatEmission(std, matNode, texturesSlotToRef, t);
    ExtractStdMatDiffuse(std, matNode, texturesSlotToRef, t);
    ExtractStdMatReflect(std, matNode, texturesSlotToRef, t);
    ExtractStdMatTransparency(std, matNode, texturesSlotToRef, t);
    ExtractStdMatOpacity(std, matNode, texturesSlotToRef, t);
    ExtractStdMatDisplacement(std, matNode, texturesSlotToRef, t);

  }
  hrMaterialClose(mat);
}

void hydraRender_mk3::ExtractStdMatDiffuse(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring diffuseSlotName = L"Diffuse Color";
  bool hasColorTexture = false;

  Color color = mtl->GetDiffuse(t);

  auto diff = mat.force_child(L"diffuse");

  auto mult = mtl->GetTexmapAmt(ID_DI, t);

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, false, hasColorTexture);

  diff.force_attribute(L"brdf_type").set_value(L"lambert");

  auto colorNode = diff.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[diffuseSlotName]);
    
    auto texNode = hrTextureBind(texRef, colorNode);
    ExtractSampler(texNode, texturesSlotToRef[diffuseSlotName]);
  }
  else if (colorNode.child(L"texture") != nullptr)
  {
    colorNode.remove_child(L"texture");
  }

  diff.force_child(L"roughness").force_attribute(L"val").set_value(0.0f);
}

void hydraRender_mk3::ExtractStdMatReflect(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring reflSlotName = L"Specular Color";
  std::wstring reflectGlossName = L"Glossiness";
  bool hasColorTexture = false;

  Color color = mtl->GetSpecular(t) * mtl->GetShinStr(t);

  auto mult = mtl->GetTexmapAmt(ID_SP, t);

  auto refl = mat.force_child(L"reflectivity");
  refl.force_attribute(L"brdf_type").set_value(L"phong");

  refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"maxcolor");


  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, false, hasColorTexture);

  refl.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());


  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflSlotName]);
    
    auto texNode = hrTextureBind(texRef, refl.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[reflSlotName]);
  }
  else if (refl.child(L"color").child(L"texture") != nullptr)
  {
    refl.child(L"color").remove_child(L"texture");
  }

  refl.force_child(L"glossiness").force_attribute(L"val").set_value(mtl->GetShininess(t));

  if (texturesSlotToRef.find(reflectGlossName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflectGlossName]);
    
    auto texNode = hrTextureBind(texRef, refl.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[reflectGlossName]);
  }
  else if (refl.child(L"glossiness").child(L"texture") != nullptr)
  {
    refl.child(L"glossiness").remove_child(L"texture");
  }

  refl.force_child(L"fresnel").force_attribute(L"val").set_value(1);
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(mtl->GetIOR(t));
}

void hydraRender_mk3::ExtractStdMatTransparency(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring transpSlotName = L"Refraction";
  std::wstring transpSlotName2 = L"Filter Color";
  std::wstring transpGlossSlotName = L"TranspGlossinessMap";
  bool hasColorTexture = false;

  auto mult = mtl->GetTexmapAmt(ID_FI, t); //Filter color, ID_RR for refraction... 

  if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end() || texturesSlotToRef.find(transpSlotName2) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  bool isTransparent = (mtl->GetXParency(t) > 0.001f || hasColorTexture);

  if (isTransparent)
  {
    Color color = mtl->GetFilter(t);

    auto transp = mat.force_child(L"transparency");

    Color res_color = evalComponentColorAPI(color, mult, false, hasColorTexture);

    transp.force_attribute(L"brdf_type").set_value(L"phong");

    auto colorNode = transp.force_child(L"color");
    colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());;

    if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpSlotName]);
      
      auto texNode = hrTextureBind(texRef, transp.child(L"color"));
      ExtractSampler(texNode, texturesSlotToRef[transpSlotName]);
    }
    else if (texturesSlotToRef.find(transpSlotName2) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpSlotName2]);
      
      auto texNode = hrTextureBind(texRef, transp.child(L"color"));
      ExtractSampler(texNode, texturesSlotToRef[transpSlotName2]);
    }
    else if (transp.child(L"color").child(L"texture") != nullptr)
    {
      transp.child(L"color").remove_child(L"texture");
    }

    transp.force_child(L"glossiness").force_attribute(L"val").set_value(FindFloat(L"TranspGlossiness", mtl));


    if (texturesSlotToRef.find(transpGlossSlotName) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpGlossSlotName]);

      auto texNode = hrTextureBind(texRef, transp.child(L"glossiness"));
      ExtractSampler(texNode, texturesSlotToRef[transpGlossSlotName]);
    }
    else if (transp.child(L"glossiness").child(L"texture") != nullptr)
    {
      transp.child(L"glossiness").remove_child(L"texture");
    }

    transp.force_child(L"thin_walled").force_attribute(L"val").set_value(0);
    transp.force_child(L"fog_color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());
    transp.force_child(L"fog_multiplier").force_attribute(L"val").set_value(10.0f*mtl->GetOpacFalloff(t));
    transp.force_child(L"ior").force_attribute(L"val").set_value(mtl->GetIOR(t));
  }
}

void hydraRender_mk3::ExtractStdMatOpacity(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring opacitySlotName = L"Opacity";

  if (texturesSlotToRef.find(opacitySlotName) != texturesSlotToRef.end())
  {
    auto opacity = mat.force_child(L"opacity");
    opacity.force_child(L"skip_shadow").force_attribute(L"val").set_value(0);

    auto texRef = std::get<0>(texturesSlotToRef[opacitySlotName]);
    
    auto texNode = hrTextureBind(texRef, opacity);
    ExtractSampler(texNode, texturesSlotToRef[opacitySlotName]);
  }
  else if (mat.child(L"opacity") != nullptr && mat.child(L"opacity").child(L"texture") != nullptr)
  {
    mat.child(L"opacity").remove_child(L"texture");
  }
}

void hydraRender_mk3::ExtractStdMatEmission(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  auto emission = mat.force_child(L"emission");

  Color color = mtl->GetSelfIllumColor(t);

  auto mult = mtl->GetTexmapAmt(ID_SI, t);

  std::wstring emissionSlotName = L"Self-Illumination";
  bool hasColorTexture = false;

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = evalComponentColorAPI(color, mult, false, hasColorTexture);

  emission.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[emissionSlotName]);
    
    auto texNode = hrTextureBind(texRef, emission.child(L"color"));
    ExtractSampler(texNode, texturesSlotToRef[emissionSlotName]);
  }
  else if (emission.child(L"color").child(L"texture") != nullptr)
  {
    emission.child(L"color").remove_child(L"texture");
  }

  emission.force_child(L"cast_gi").force_attribute(L"val").set_value(1);
  emission.force_child(L"multiplier").force_attribute(L"val").set_value(1.0f);
}


void hydraRender_mk3::ExtractStdMatDisplacement(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring displSlotName1 = L"Bump";
  std::wstring displSlotName2 = L"Displacement";
  bool hasBumpTexture = false;
  bool hasDisplTexture = false;

  if (texturesSlotToRef.find(displSlotName1) != texturesSlotToRef.end())
  {
    hasBumpTexture = true;
  }
  if (texturesSlotToRef.find(displSlotName2) != texturesSlotToRef.end())
  {
    hasDisplTexture = true;
  }

  if (hasBumpTexture || hasDisplTexture)
  {
    auto displace = mat.force_child(L"displacement");

    auto heightNode = displace.force_child(L"height_map");
    heightNode.force_attribute(L"smooth_lvl").set_value(0.1f);

    if (hasBumpTexture)
    {
      auto texRef = std::get<0>(texturesSlotToRef[displSlotName1]);
      
      heightNode.force_attribute(L"amount").set_value(mtl->GetTexmapAmt(ID_BU, t));
      auto texNode = hrTextureBind(texRef, displace.child(L"height_map"));
      ExtractSampler(texNode, texturesSlotToRef[displSlotName1]);
    }
    else if(hasDisplTexture)
    {
      auto texRef = std::get<0>(texturesSlotToRef[displSlotName2]);
      
      heightNode.force_attribute(L"amount").set_value(mtl->GetTexmapAmt(ID_DP, t));
      auto texNode = hrTextureBind(texRef, displace.child(L"height_map"));
      ExtractSampler(texNode, texturesSlotToRef[displSlotName2]);
    }
  }
  else if(mat.child(L"displacement") != nullptr && mat.child(L"displacement").child(L"height_map") != nullptr)
  {
    mat.child(L"displacement").remove_child(L"texture");
  }
}


int hydraRender_mk3::ExtractMaterial(Mtl* mtl, std::unordered_set<std::wstring> changedParams)
{
  TimeValue t = GetStaticFrame();

  if (!mtl) return -1;

  TSTR className;
  mtl->GetClassName(className);

  const ULONG HYDRA_MAT_CLASS_ID_PART_A = 0xa3f0e60d;
  const ULONG HYDRA_MAT_CATCHER_CLASS_ID_PART_A = 0x3cd640a9;
  const ULONG HYDRA_MAT_LAYERS_CLASS_ID_PART_A = 0x7a8d0218;

  if (mtl->NumSubMtls() == 0)
  {
    if (mtl->ClassID().PartA() == HYDRA_MAT_CLASS_ID_PART_A)
    {
      ExtractHydraMaterial(mtl, changedParams);
      return 1;
    }
    else if (mtl->ClassID().PartA() == HYDRA_MAT_CATCHER_CLASS_ID_PART_A)
    {
      ExtractHydraMtlCatcher(mtl, changedParams);
      return 1;
    }
    else if (mtl->ClassID().PartA() == HYDRA_MAT_LAYERS_CLASS_ID_PART_A)
    {
      ExtractHydraMtlLayers(mtl, changedParams);
      return 1;
    }
    else if (mtl->ClassID().PartA() == DMTL_CLASS_ID)
    {
      ExtractStdMaterial(mtl);
      return 1;
    }
 /*   else
    {
      UnsupportedMaterial();
      return 1;
    }*/
  }
/*  else
  {
    UnsupportedMaterial();
  }*/

  //TraceNodeCustomProperties(mat->name, mtl, 1);
  return 0;
}






