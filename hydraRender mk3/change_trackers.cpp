#include "change_trackers.h"


namespace Hydra
{

  NodePool::NodePool(INode* initial_node, const NotifierType a_notification_type, TimeValue t) : m_node_notification_type(a_notification_type)
  {
    needExport = true;
    m_ref_obj = initial_node->GetObjOrWSMRef();
    m_notification_client = std::unique_ptr<IImmediateNotificationClient>(INotificationManager::GetManager()->RegisterNewImmediateClient());
    m_pool.clear();

    const ObjectState os = initial_node->EvalWorldState(t);
    isGeoPool = (os.obj->SuperClassID() == SHAPE_CLASS_ID) || (os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID);

    representative_node = initial_node;
    AddNode(initial_node, t);

  }


  NodePool::~NodePool()
  {
    // Unregister all notifications
    if (m_notification_client != nullptr)
    {
      for (INode* node : m_pool)
      {
        m_notification_client->StopMonitoringNode(*node, ~size_t(0), *this, nullptr);
      }
    }
    m_pool.clear();
  }


  void NodePool::NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/)
  {
    const INodeEvent* const node_event = dynamic_cast<const INodeEvent*>(&genericEvent);
    DbgAssert(node_event != nullptr);
    INode* const changed_node = (node_event != nullptr) ? node_event->GetNode() : nullptr;

    auto event_type = genericEvent.GetEventType();
    if (changed_node != nullptr)
    {
      switch (event_type)
      {
      case EventType_Node_Deleted:
        RemoveNode(changed_node);
        if (changed_node == representative_node)
        {
          for (auto& n : m_pool)
          {
            if (n->GetMtl()->ClassID().PartA() == MULTI_CLASS_ID)
            {
              representative_node = n;
              break;
            }
          }
        }
        break;
      case EventType_Node_ParamBlock:
      case EventType_Node_Uncategorized:
      case EventType_Mesh_Vertices:
      case EventType_Mesh_Faces:
      case EventType_Mesh_UVs:
      case EventType_Node_RenderProperty:
   //   case EventType_Node_Reference:
        needExport = true; 
        break;
      case EventType_Node_Material_Replaced:
      case EventType_Node_Material_Updated:
        {
          auto repMat = representative_node->GetMtl();
          auto newNodeMat = changed_node->GetMtl();
          if (newNodeMat != nullptr && repMat != nullptr && newNodeMat->ClassID().PartA() == MULTI_CLASS_ID && repMat->ClassID().PartA() != MULTI_CLASS_ID)
          {
            representative_node = changed_node; //the node with multi material should be representative for correct material remap lists
          }
          needExport = true;
        }
        break;
      /*case EventType_Node_Reference:
        needExport = true;*/
   /*   case EventType_Node_Hide:
        bool rendThisNode = (GetCOREInterface13()->GetRendHidden() || !changed_node->IsNodeHidden());*/
      default:
        break;
      }
    }
  }

  INode* NodePool::GetRepresentativeNode() const
  {
    return representative_node;
  }

  Object* NodePool::GetReferencedObject() const
  {
    return m_ref_obj;
  }

  bool NodePool::IsNodeCompatible(INode* node, TimeValue t) const
  {
    if (node->GetObjOrWSMRef() == m_ref_obj)
    {
      const ObjectState os = node->EvalWorldState(t);
      //SClass_ID SClassID = node->GetObjOrWSMRef()->SuperClassID();
      auto SClassID = os.obj->SuperClassID();
      if (SClassID == SHAPE_CLASS_ID || SClassID == GEOMOBJECT_CLASS_ID)
      {
        INode* const this_node = GetRepresentativeNode();
        Mtl* const this_material = (this_node != nullptr) ? this_node->GetMtl() : nullptr;
        Mtl* const new_material = node->GetMtl();
        if (this_material != new_material)
        {
          return true; 
        }

        auto geom_object = static_cast<GeomObject*>(os.obj);

        if (geom_object != nullptr)
        {
          return !geom_object->IsInstanceDependent();
        }
        else
        {
          return false;
        }
      }
      else if(SClassID == LIGHT_CLASS_ID)
        return true;
      else
        return false;
    }
    else
    {
      return false;
    }
  }

  void NodePool::AddNode(INode* a_node, TimeValue t)
  {
    const auto insert_result = m_pool.insert(a_node);

    // Upon insertion, monitor the node for changes
    if (insert_result.second && (m_notification_client != nullptr))
    {
      m_notification_client->MonitorNode(*a_node, m_node_notification_type, ~size_t(0), *this, nullptr);
    }

    if (isGeoPool)
    {
      auto repMat = representative_node->GetMtl();
      auto newNodeMat = a_node->GetMtl();
      if (newNodeMat != nullptr && repMat != nullptr && newNodeMat->ClassID().PartA() == MULTI_CLASS_ID && repMat->ClassID().PartA() != MULTI_CLASS_ID)
      {
        representative_node = a_node; //the node with multi material should be representative for correct material remap lists
        needExport = true;
      }
    }
  }

  void NodePool::RemoveNode(INode* a_node)
  {
    auto found_it = m_pool.find(a_node);
    if (found_it != m_pool.end())
    {
      if (m_notification_client != nullptr) // Stop monitoring the node
      {
        m_notification_client->StopMonitoringNode(*a_node, ~size_t(0), *this, nullptr);
      }
      m_pool.erase(found_it);
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  MaterialTracker::MaterialTracker()
  {
    m_notification_client = std::unique_ptr<IImmediateNotificationClient>(INotificationManager::GetManager()->RegisterNewImmediateClient());
    materials.clear();
  }

  MaterialTracker::~MaterialTracker()
  {
    MaterialTracker::Clear();
  }

  void	MaterialTracker::AddMtl(Mtl* mtl)
  {
    if (mtl != nullptr)
    {
      auto found_it = materials.find(mtl);
      if (found_it == materials.end())
      {
        materials[mtl] = std::make_pair<bool, std::unordered_set<std::wstring>>(true, std::unordered_set<std::wstring>());
        m_notification_client->MonitorMaterial(*mtl, ~size_t(0), *this, nullptr);
      }
    }
  }

  void	MaterialTracker::RemoveMtl(Mtl* mtl)
  {
    auto found_it = materials.find(mtl);
    if (found_it != materials.end())
    {
      if (m_notification_client != nullptr)
      {
        m_notification_client->StopMonitoringMaterial(*mtl, ~size_t(0), *this, nullptr);
      }
      materials.erase(found_it);
    }
  }

  void MaterialTracker::Clear()
  {
    for (auto mat : materials)
    {
      if (m_notification_client != nullptr)
      {
       m_notification_client->StopMonitoringMaterial(*mat.first, ~size_t(0), *this, nullptr);
      }
    }
    materials.clear();
  }


  void MaterialTracker::NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/)
  {
    const IMaterialEvent* const mat_event = dynamic_cast<const IMaterialEvent*>(&genericEvent);
    DbgAssert(mat_event != nullptr);
    Mtl* const changed_mat = (mat_event != nullptr) ? mat_event->GetMtl() : nullptr;

    const ULONG HYDRA_MAT_CLASS_ID_PART_A = 0xa3f0e60d;
    if (changed_mat != nullptr)
    {
      switch (genericEvent.GetEventType())
      {
      case EventType_Material_Deleted:
        RemoveMtl(changed_mat);
        break;
      case EventType_Material_ParamBlock:
      case EventType_Material_Reference:
        materials[changed_mat].first = true;

        if (changed_mat->ClassID().PartA() == HYDRA_MAT_CLASS_ID_PART_A)
        {
          auto tmp = mat_event->GetParamBlockData();
          if (!tmp.isEmpty())
          {
            auto changed_params = tmp[0].m_ParametersNames;
            for (int i = 0; i < changed_params.length(); ++i)
            {
              std::wstring param = changed_params[i].ToBSTR();
              materials[changed_mat].second.emplace(param);
            }
          }
        }
        break;
      default:
        break;
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  TextureTracker::TextureTracker(std::shared_ptr<MaterialTracker> a_mtlTracker)
  {
    m_notification_client = std::unique_ptr<IImmediateNotificationClient>(INotificationManager::GetManager()->RegisterNewImmediateClient());
    mtlTracker = a_mtlTracker;
    texmaps.clear();
  }

  TextureTracker::~TextureTracker()
  {
    TextureTracker::Clear();
  }

  void TextureTracker::NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* /*userData*/)
  {
    const ITexmapEvent* const texmap_event = dynamic_cast<const ITexmapEvent*>(&genericEvent);
    DbgAssert(texmap_event != nullptr);
    Texmap* const changed_tex = (texmap_event != nullptr) ? texmap_event->GetTexmap() : nullptr;

    const ULONG HYDRA_MAT_CLASS_ID_PART_A = 0xa3f0e60d;
    if (changed_tex != nullptr)
    {
      Mtl* parent;
      MSTR parentName;
      bool found = false;
      switch (genericEvent.GetEventType())
      {
      case EventType_Texmap_Deleted:
        RemoveTexmap(changed_tex);
        break;
      case EventType_Texmap_ParamBlock:
      case EventType_Material_Reference:
        texmaps[changed_tex].first = true;

        //if texmap has changed need to re-export material too
        parent = texmaps[changed_tex].second;
        parentName = parent->GetName();

        
        for (auto mat : mtlTracker->materials)
        {
          if (mat.first->GetName() == parentName)
          {
            mtlTracker->materials[parent].first = true;
            found = true;
            break;
          }
        }

        if(!found)
          RemoveTexmap(changed_tex);
        /*
        if (mtlTracker->materials.find(parent) != mtlTracker->materials.end())
          mtlTracker->materials[parent].first = true;
        else
          RemoveTexmap(changed_tex);*/
        break;
      default:
        break;
      }
    }
  }

  void TextureTracker::AddTexmap(Texmap* tex, Mtl* parentMtl)
  {
    if (tex != nullptr)
    {
      auto found_it = texmaps.find(tex);
      if (found_it == texmaps.end())
      {
        texmaps[tex].first = true;
        texmaps[tex].second = parentMtl;
        m_notification_client->MonitorTexmap(*tex, ~size_t(0), *this, nullptr);
      }
    }
  }

  void TextureTracker::RemoveTexmap(Texmap* tex)
  {
    auto found_it = texmaps.find(tex);
    if (found_it != texmaps.end())
    {
      if (m_notification_client != nullptr)
      {
        m_notification_client->StopMonitoringTexmap(*tex, ~size_t(0), *this, nullptr);
      }
      texmaps.erase(found_it);
    }
  }

  void TextureTracker::Clear()
  {
    for (auto tex : texmaps)
    {
      if (m_notification_client != nullptr)
      {
        m_notification_client->StopMonitoringTexmap(*tex.first, ~size_t(0), *this, nullptr);
      }
    }
    texmaps.clear();
  }



}
