#include "3dsmaxsdk_preinclude.h"
#include "resource.h"

extern HINSTANCE hInstance;

std::string GetMethodPresets(int methodId)
{
  std::string res;

  HRSRC hRes;
  
  if(methodId == 0)
    hRes = FindResourceW(hInstance, MAKEINTRESOURCEW(IDR_STRING1), L"string");
  else
    hRes = FindResourceW(hInstance, MAKEINTRESOURCEW(IDR_STRING2), L"string");

  HGLOBAL hGlobal = NULL;
  DWORD dwTextSize = 0;

  if(hRes != NULL)
  {
    hGlobal    = LoadResource(hInstance, hRes);
    dwTextSize = SizeofResource(hInstance, hRes);
  }

  if(hGlobal != NULL)
  { 
    const char* lpszText = (const char *)LockResource(hGlobal);
    
    char* temp = new char [dwTextSize];
    memcpy(temp, lpszText, dwTextSize);
    temp[dwTextSize-1] = '\0';
    res = temp; 
    delete [] temp;

    UnlockResource(hGlobal);
  }

  if(hRes != NULL)
    FreeResource(hGlobal);

  return res;
}